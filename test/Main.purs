module Test.Main where

import Prelude

import Effect (Effect)
import Effect.Aff (launchAff_)
import Effect.Class.Console (log)
import Test.Spec.Reporter (consoleReporter)
import Test.Spec.Runner (runSpec)

import Test.Flauros.PreludeSpec as Test.Flauros.PreludeSpec
import Test.Flauros.Data.AstronomySpec as Test.Flauros.Data.AstronomySpec

main :: Effect Unit
main = launchAff_ $ runSpec [consoleReporter] do
  Test.Flauros.PreludeSpec.spec
  Test.Flauros.Data.AstronomySpec.spec
