module Test.Flauros.Util where

import Prelude

import Control.Monad.Error.Class (class MonadThrow)
import Effect.Exception (Error)
import Test.Spec.Assertions (fail)

shouldBeWithinE :: forall m. MonadThrow Error m => Number -> Number -> Number -> m Unit
shouldBeWithinE e v1 v2 = 
  when (v1 < v2 - e || v1 > v2 + e) $
    fail $ show v1 <> " ≠ " <> show v2 <> "±" <> show e
    
shouldRoughlyEqual :: forall m. MonadThrow Error m => Number -> Number -> m Unit
shouldRoughlyEqual = shouldBeWithinE 0.00000001