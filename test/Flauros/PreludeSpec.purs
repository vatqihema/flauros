module Test.Flauros.PreludeSpec where

import Prelude
import Prelude
import Test.Spec
import Test.Spec.Assertions

import Data.Number (pi)
import Flauros.Prelude (distance)
import Test.Flauros.Util (shouldRoughlyEqual)

spec :: Spec Unit
spec =
  describe "Flauros.Prelude" do
    distanceSpec

distanceSpec :: Spec Unit
distanceSpec =
  describe "distance" do
    it "should return magnitude when one's magnitude is 0" do
      distance { mag: 3.0, angle: 0.345 } { mag: 0.0, angle: 3.2 } `shouldRoughlyEqual` 3.0
      distance { mag: 0.0, angle: 0.345 } { mag: 7.0, angle: 3.2 } `shouldRoughlyEqual` 7.0
    it "should return twice the magnitude for opposite vecs of same mag" do
      distance { mag: 123.0, angle: 0.0 } { mag: 123.0, angle: pi } `shouldRoughlyEqual` (123.0 * 2.0)
    it "should distance for arbitrary mags and angles" do
      distance { mag: 10.0, angle: 0.3 } { mag: 4.0, angle: pi * 1.5 } `shouldRoughlyEqual` 11.81700539616138
      distance { mag: 12.0, angle: pi * 1.1 } { mag: 13.0, angle: pi * 1.5 } `shouldRoughlyEqual` 14.716884784322268
      distance { mag: -80.0, angle: 0.0 } { mag: -57.0, angle: 0.1 } `shouldRoughlyEqual` 23.970023209509105