module Test.Flauros.Data.AstronomySpec where

import Flauros.Data.Astronomy
import Prelude
import Test.Spec
import Test.Spec.Assertions

import Data.Number (pi)

spec :: Spec Unit
spec =
  describe "Flauros.Data.Astronomy" do
    -- absoluteMagnitudeSpec
    apparentMagnitudeSpec
    distanceFromSunSpec
    reflectiveLuminositySpec

-- absoluteMagnitudeSpec :: Spec Unit
-- absoluteMagnitudeSpec = 
--   describe "absoluteMagnitude" do
--     it "should return expected reflelccc for earth" do
--       reflectiveLuminosity earth 0.0 `shouldEqual` -3.99
--     it "should return expected magnitude for earth" do
--       absoluteMagnitude earth 0.0 `shouldEqual` -3.99

apparentMagnitudeSpec :: Spec Unit
apparentMagnitudeSpec =
  describe "apparentMagnitude" do
    it "should be expected value for Mars from Earth at max" do
      apparentMagnitude mars 0.0 earth 0.0 `shouldEqual` -2.94
    it "should be expected value for Earth from Mars at max" do
      apparentMagnitude earth 0.0 mars 0.0 `shouldEqual` -2.5

distanceFromSunSpec :: Spec Unit
distanceFromSunSpec =
  describe "distanceFromSun" do
    it "should get aphelion at 0" do
      distanceFromSun earth 0.0 `shouldEqual` earth.aphelion
    it "should get semi-major axis at π / 2" do
      distanceFromSun earth (pi / 2.0) `shouldEqual` earth.semiMajorAxis
    it "should get perihelion at π" do
      distanceFromSun earth (pi) `shouldEqual` earth.perihelion
    it "should get semi-major axis at 3π / 2" do
      distanceFromSun earth (3.0 * pi / 2.0) `shouldEqual` earth.semiMajorAxis

reflectiveLuminositySpec :: Spec Unit
reflectiveLuminositySpec =
  describe "reflectiveLuminosity" do
    it "should get expected value for Earth" do
      reflectiveLuminosity earth  0.0     `shouldEqual` 63038451742720040.0
      reflectiveLuminosity earth (pi*0.5) `shouldEqual` 65164678636481840.0
      reflectiveLuminosity earth  pi      `shouldEqual` 67401280130071450.0
      reflectiveLuminosity earth (pi*1.5) `shouldEqual` 65164678636481840.0
    it "should get expected value for Mars" do
      reflectiveLuminosity mars 0.0 `shouldEqual` 12345.0

-------------
-- Planets --
-------------

-- | Values taken from Wikipedia as of revision https://en.wikipedia.org/w/index.php?title=Mars&oldid=1122594309
mars :: {| Planet () }
mars =
  { albedo:        0.17
  , radius:        3389.5--km
  , aphelion:      249261000.0--km
  , perihelion:    206650000.0--km
  , semiMajorAxis: 227939366.0--km
  }