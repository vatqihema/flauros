let seed = 1917;

export const jsRandom = () => Math.random();
export const getCurrentSeed = () => seed;
export const setCurrentSeed = newSeed => () => { seed = newSeed };