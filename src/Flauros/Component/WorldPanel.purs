module Flauros.Component.WorldPanel where

import Prelude

import CSS (rem)
import CSS as CSS
import CSS.Box as CSSBox
import CSS.Overflow as CSSO
import Data.NonEmpty as NE
import Flauros.Component.SeerBook.Settings (settingsPanel, settingsPanel_)
import Flauros.Component.Tab (tabContext, tabContext_)
import Flauros.Component.Tab as Tag
import Flauros.Component.Util (richText_)
import Flauros.Data.Store (mkTranslate, mkTranslateWith, selectNone)
import Flauros.Data.Store as DS
import Flauros.Data.Time (getDate, getDateTime, getMonth, getTime)
import Flauros.Lang (LineParam(..), translateWith)
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry as Registry
import Flauros.Prelude (Identifier, sid, transmute)
import Flauros.Style.Color (voider, voidish)
import Flauros.Style.Color as Color
import Flauros.Style.Typography (seerbookStatus, worldPanelHeader)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.CSS as HCSS
import Halogen.HTML.Properties as HP
import Halogen.Store.Connect (connect)
import Halogen.Store.Monad (class MonadStore)

worldPanel :: forall q i o m. MonadStore DS.Action DS.Store m => H.Component q i o m
worldPanel = connect selectNone $ H.mkComponent
  { initialState: \{ context: ctx@(DS.Store { colors, isDebug }) } ->
      { colors, isDebug
      , translate: mkTranslate ctx
      , translateWith: mkTranslateWith ctx
      }
  , render: \{ colors, translateWith } ->
      HH.div
        [ HP.class_ $ H.ClassName "world-panel"
        , HCSS.style do
            CSS.backgroundColor $ voidish colors
            CSSBox.boxShadow $ NE.singleton $ (Color.voidest colors) `CSSBox.bsColor` CSSBox.shadowWithBlur (rem $ -0.2) (rem 0.2) (rem 0.8)
            CSSO.overflowX CSSO.hidden
            CSSO.overflowY CSSO.hidden
        ]
        [ HH.div
            [ HCSS.style do
                CSS.backgroundColor (voider colors)
            ]
            [ HH.slot_ tabContext_ 0 (tabContext [] "world-panel" [ sid "terminal" ]) $ Tag.withDefInput
                { defaultTab:     sid "terminal"
                , highlightColor: voidish colors
                , renderTab: \selected id ->
                    let comp k = richText_ colors (CSS.fontSize $ rem 1.5) $ translateWith (sid "world-tab")  k [PID id, PBool selected]
                     in case id.base of
                        "terminal" -> comp "terminal"
                        "settings" -> comp "settings"
                        _          -> comp "unknown"
                , renderBody: \tab -> HH.div
                    [ HCSS.style do
                        CSS.backgroundColor (voidish colors)
                    ]
                    [ case tab.base of
                        "terminal" -> HH.p_ [ HH.text "Terminal placeholder" ]
                        "settings" -> HH.slot_ settingsPanel_ 0 settingsPanel unit
                        _          -> HH.p_ [ HH.text "Unknown placeholder" ]
                    ]
                }
            ]
        ]
  , eval: H.mkEval H.defaultEval
  }