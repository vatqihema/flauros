module Flauros.Component.SeerBook where

import Prelude

import CSS (rem)
import CSS as CSS
import CSS.Box as CSSBox
import CSS.Overflow as CSSO
import Data.Argonaut (stringify)
import Data.Argonaut.Decode.Generic (genericDecodeJson)
import Data.Argonaut.Encode.Generic (genericEncodeJson)
import Data.Bounded.Generic (genericBottom, genericTop)
import Data.Enum (class BoundedEnum, class Enum, enumFromTo, fromEnum)
import Data.Enum.Generic (genericCardinality, genericFromEnum, genericPred, genericSucc, genericToEnum)
import Data.Foldable (for_)
import Data.Generic.Rep (class Generic)
import Data.HashMap as HM
import Data.Hashable (class Hashable)
import Data.Int (toNumber)
import Data.Maybe (Maybe(..))
import Data.NonEmpty as NE
import Data.Show.Generic (genericShow)
import Flauros.Component.SeerBook.Apartment (apartmentBrowser, apartmentBrowser_)
import Flauros.Component.SeerBook.Calendar (calendar, calendar_)
import Flauros.Component.SeerBook.Debug (debug, debug_)
import Flauros.Component.SeerBook.News (news, news_)
import Flauros.Component.SeerBook.Profile (profile, profile_)
import Flauros.Component.SeerBook.Settings (settings, settings_)
import Flauros.Component.Tab (TabOutput(..), tabContext, tabContext_)
import Flauros.Component.Tab as Tag
import Flauros.Component.UCSS (easyShadow)
import Flauros.Component.Util (clazz, maybeElem, richText_)
import Flauros.Data.Actor (itemCount, playerID)
import Flauros.Data.Month (monthID, timeOfDayColor)
import Flauros.Data.Store (mkTranslate, mkTranslateWith, selectAllEqOn, selectNone)
import Flauros.Data.Store as DS
import Flauros.Data.System.JSON (class FromJSON, class ToJSON)
import Flauros.Data.Time (getDateTime, getMonth, getTime)
import Flauros.Lang (LineParam(..))
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry as Registry
import Flauros.Prelude (sid)
import Flauros.Style.Color (voider, voidest, voidish)
import Flauros.Style.Color as Color
import Flauros.Style.Typography (seerbookStatus)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.CSS as HCSS
import Halogen.HTML.Properties as HP
import Halogen.Store.Connect (connect)
import Halogen.Store.Monad (class MonadStore, getStore, updateStore)
import Type.Prelude (Proxy(..))

data SeerBookTab = Profile | News | Messages | Calendar | Bag | Saves | Settings | ApartmentBrowser | Debug
derive instance Eq SeerBookTab
derive instance Generic SeerBookTab _
instance fromJSONSeerBookTab :: FromJSON SeerBookTab   where fromJSON = genericDecodeJson
instance toJSONSeerBookTab   :: ToJSON   SeerBookTab   where toJSON   = genericEncodeJson
derive instance Ord SeerBookTab
instance Enum SeerBookTab where
  succ = genericSucc
  pred = genericPred
instance showSeerBookTab :: Show SeerBookTab where
  show = genericShow
instance boundedSeerBookTab :: Bounded SeerBookTab where
  top    = genericTop
  bottom = genericBottom
instance boundedEnumSeerBookTab :: BoundedEnum SeerBookTab where
  cardinality = genericCardinality
  fromEnum    = genericFromEnum
  toEnum      = genericToEnum
instance hashableSeerBookTab :: Hashable SeerBookTab where
  hash = fromEnum

data SeerBookTabAction o = InitSeerBook | SeerBookTab (TabOutput SeerBookTab o)

seerBook :: forall q i o m. MonadStore DS.Action DS.Store m => H.Component q i o m
seerBook = connect selectNone $ H.mkComponent
  { initialState: \{ context: ctx@(DS.Store { colors, facts, isDebug }) } ->
      { colors, facts, isDebug
      , translate: mkTranslate ctx
      , mLastWorldTab: Nothing
      }
  , render: \{ colors, isDebug, translate } ->
      HH.div
        [ HP.class_ $ H.ClassName "seer-book"
        , HCSS.style do
            CSS.backgroundColor $ voidish colors
            easyShadow (voidest colors) 1.0
            CSSO.overflowX CSSO.hidden
            CSSO.overflowY CSSO.hidden
            CSS.height $ CSS.pct 100.0
        ]
        [ HH.slot_ statusBar_ 0 statusBar unit
        , HH.div
            [ clazz "max"
            , HCSS.style do
                CSS.backgroundColor (voider colors)
            ]
            [ flip (HH.slot tabContext_ 0 (tabContext [ clazz "max" ] "seer-book" (enumFromTo Profile (if isDebug then Debug else Settings)))) SeerBookTab $ Tag.withDefInput
                { defaultTab: Profile
                , highlightColor: voidish colors
                , renderTab: \_ ->
                    let css = do
                          CSS.fontSize $ rem 1.5
                          CSS.zIndex 1
                        comp k = richText_ colors css $ translate (sid "seer-tab-icon") k
                     in case _ of
                        ApartmentBrowser -> comp "apartment-browser"
                        Profile  -> comp "profile"
                        News     -> comp "news"
                        Messages -> comp "messages"
                        Calendar -> comp "calendar"
                        Bag      -> comp "bag"
                        Saves    -> comp "saves"
                        Settings -> comp "settings"
                        Debug    -> comp "debug"
                , renderBody: \tab -> HH.div
                    [ clazz "max"
                    , HCSS.style do                                                                              
                        CSS.backgroundColor (voidish colors)
                    ]
                    [ case tab of
                        ApartmentBrowser -> HH.slot_ apartmentBrowser_ 0 apartmentBrowser unit
                        Profile  -> HH.slot_ profile_ 0 profile unit
                        News     -> HH.slot_ news_ 0 news unit
                        Messages -> HH.p_ [ HH.text "Messages placeholder" ]
                        Calendar -> HH.slot_ calendar_ 0 calendar unit
                        Bag      -> HH.p_ [ HH.text "Bag placeholder" ]
                        Saves    -> HH.p_ [ HH.text "Saves placeholder" ]
                        Settings -> HH.slot_ settings_ 0 settings unit
                        Debug    -> HH.slot_ debug_ 0 debug unit
                    ]
                } 
            ]
        ]
  , eval: H.mkEval H.defaultEval
      { handleAction = case _ of
          InitSeerBook -> pure unit
          SeerBookTab out -> case out of
            OnSwitch { new, prev } -> do
              when (new /= prev) do
                when (prev == Settings) do
                  { mLastWorldTab } <- H.get
                  for_ mLastWorldTab \tab ->
                    updateStore $ DS.setUIState "world-panel" tab
                when (new == Settings) do
                  getStore <#> DS.lookupUI "world-panel" >>= \m -> for_ m \json -> do
                    H.modify_ _ { mLastWorldTab = Just json }
                  updateStore $ DS.setUIState "world-panel" (sid "settings")
            _ -> pure unit
      }
  }

statusBar_ = Proxy :: Proxy "statusBar"

statusBar :: forall q i o m. MonadStore DS.Action DS.Store m => H.Component q i o m
statusBar = connect (selectAllEqOn (\(DS.Store { actors, colors, currentTime }) -> { player: HM.lookup playerID actors, colors, currentTime })) $ H.mkComponent
  { initialState: \{ context: ctx@(DS.Store { actors, colors, currentTime, facts }) } ->
      { colors, currentTime, facts
      , mMonth: Nothing
      , mPlayer: HM.lookup playerID actors
      , translateWith: mkTranslateWith ctx
      }
  , render: \{ colors, currentTime, mMonth, mPlayer, translateWith } ->
      maybeElem mPlayer \player ->
        maybeElem mMonth \month ->
          HH.div
            [ HCSS.style do
                CSS.backgroundColor $ voidish colors
                CSSBox.boxShadow $ NE.singleton $ (Color.voidest colors) `CSSBox.bsColor` CSSBox.shadowWithBlur (rem $ -0.25) (rem 0.25) (rem 1.0)
                CSSO.overflowX CSSO.hidden
                CSSO.overflowY CSSO.hidden
            ]
            [ HH.span
                [ HCSS.style do
                    CSS.color $ timeOfDayColor month (getTime currentTime) colors
                    CSS.float CSS.FloatRight
                ]
                [ richText_ colors seerbookStatus $ translateWith (sid "number") "currency" [ PNum $ toNumber (itemCount (sid "money") player) / 100.0 ] ]
            , richText_ colors seerbookStatus $ translateWith (sid "status-bar") "pretty" [ PDateTime $ getDateTime currentTime ]
            ]
  , eval: H.mkEval $ H.defaultEval
      { receive = Just
      , handleAction = \{ context: (DS.Store new) } -> do
          mMonth <- Registry.lookup Regs.month (monthID $ getMonth new.currentTime)
          H.modify_ \s ->
            s { colors = new.colors, currentTime = new.currentTime, mMonth = mMonth, mPlayer = HM.lookup playerID new.actors }
      }
  }