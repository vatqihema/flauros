module Flauros.Component.Animation where

import Prelude

import CSS as CSS
import CSS.Overflow as CSSO
import Data.Maybe (Maybe(..))
import Flauros.Data.Store as DS
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.CSS as HCSS
import Halogen.Store.Monad (class MonadStore)
import Type.Prelude (Proxy(..))

data Animation = SAnim String | AnimThen String Animation

bounce :: Animation
bounce = SAnim "animate__bounce"

type AnimationInput =
  { animation :: Animation
  , color     :: CSS.Color
  , repeat    :: Boolean
  }

data AnimationQuery a = Play a | Stop a

_animatedOverlay = Proxy :: Proxy "animatedOverlay"

animatedOverlay :: forall o m. MonadStore DS.Action DS.Store m => H.Component AnimationQuery AnimationInput o m
animatedOverlay = H.mkComponent
  { initialState: \{ animation, color, repeat } -> { animation, color, repeat, curAnim: Nothing }
  , render: \{ color } ->
      HH.div
        [ HCSS.style do
            CSS.width  $ CSS.pct 100.0
            CSS.height $ CSS.pct 100.0
            CSS.backgroundColor color
            CSSO.overflow CSSO.hidden
            CSS.position CSS.fixed
            CSS.zIndex 2
        ]
        []
  , eval: H.mkEval $ H.defaultEval
  }

  


ripple :: forall i o m. H.Component AnimationQuery i o m
ripple = H.mkComponent
  { initialState: \_ -> false
  , render: case _ of
      false -> HH.text ""
      true ->
        HH.div
          []
          []
  , eval: H.mkEval $ H.defaultEval
  }