module Flauros.Component.Tab where

import Prelude

import CSS (Color, CSS)
import CSS as CSS
import DOM.HTML.Indexed (HTMLdiv)
import Data.Array (mapWithIndex)
import Data.Array as Array
import Data.Either (hush)
import Data.Foldable (for_)
import Data.Hashable (class Hashable, hash)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Newtype (unwrap)
import Effect.Class.Console as Console
import Flauros.Component.Util (AnchorDirection(..), clazz, whenElem)
import Flauros.Data.Store (selectKey, setUIState)
import Flauros.Data.Store as DS
import Flauros.Data.System.JSON (class FromJSON, class ToJSON, fromJSON)
import Flauros.Log (LogType(..))
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.CSS as HCSS
import Halogen.HTML.Events as HE
import Halogen.Store.Connect (connect)
import Halogen.Store.Monad (class MonadStore, getStore, updateStore)
import Prim.Row (class Nub, class Union)
import Record as Record
import Type.Prelude (Proxy(..))

data TabOutput k o
  = OnOutput o
  | OnSwitch { prev :: k, new :: k }

type TabContextInput k q o m s =
  { anchor         :: AnchorDirection
  , defaultTab     :: k
  , rememberTab    :: k -> Boolean
  , renderBody     :: k -> HH.HTML (H.ComponentSlot s m (TabAction k o)) (TabAction k o)
  , renderTab      :: Boolean -> k -> HH.HTML (H.ComponentSlot s m (TabAction k o)) (TabAction k o)
  , tabCSS         :: Boolean -> k -> CSS
  , highlightColor :: Color
  }

withDefInput :: forall k r1 r2 r3
             . Union r1
                ( anchor         :: AnchorDirection
                , rememberTab    :: k -> Boolean
                , tabCSS         :: Boolean -> k -> CSS
                ) r2
             => Nub r2 r3
             => {| r1 } -> {| r3 }
withDefInput r = Record.merge r
  { anchor: AnchorTop
  , rememberTab: \(_ :: k) -> true
  , tabCSS: \(_ :: Boolean) (_ :: k) -> pure unit :: CSS
  }

data TabAction k o = TabInit | TabSwitch k | StateReload k | TabReceive o

tabContext_ = Proxy :: Proxy "tabContext"

tabContext :: forall q m k o s. Eq k => Hashable k => FromJSON k => ToJSON k => MonadStore DS.Action DS.Store m
           => Array (HH.IProp HTMLdiv (TabAction k o))
           -> String
           -> Array k
           -> H.Component q (TabContextInput k q o m s) (TabOutput k o) m
tabContext props name tabs = connect (selectKey (unwrap >>> _.uiState) name) $ H.mkComponent
  { initialState: \{ context: ctx, input } -> Record.merge input
      { currentTab: fromMaybe input.defaultTab (ctx >>= (hush <<< fromJSON))
      , isInit: false
      }
  , render: \{ anchor, currentTab, isInit, highlightColor, renderBody, renderTab, tabCSS } ->
      let forTabs = flip mapWithIndex (if Array.elem currentTab tabs then tabs else Array.snoc tabs currentTab)
          panelCSS = do
            CSS.height $ CSS.pct 100.0
            CSS.width  $ CSS.pct 100.0
       in HH.div
            props
            [ HH.div
                [ HCSS.style do
                    when (anchor == AnchorTop || anchor == AnchorBottom) do
                      CSS.width $ CSS.pct 100.0
                      CSS.display CSS.flex
                      CSS.justifyContent CSS.spaceBetween
                    when (anchor == AnchorRight) do
                      CSS.float CSS.floatRight
                      CSS.height $ CSS.pct 100.0
                ] $ forTabs \i k ->
                  HH.div
                    [ HCSS.style do
                        when (anchor == AnchorTop || anchor == AnchorBottom) do
                          CSS.display CSS.inlineFlex
                          CSS.padding (CSS.rem 0.5) (CSS.rem 0.5) (CSS.rem 0.0) (CSS.rem 0.5)
                        when (currentTab == k) $
                          CSS.backgroundColor highlightColor
                        tabCSS (currentTab == k) k
                    , HE.onClick \_ -> TabSwitch k
                    ]
                    [ renderTab (currentTab == k) k
                    ]
            , HH.div [ HCSS.style panelCSS ]
                [ HH.div
                    [ clazz "max" ]
                    -- if we render this immediately, for some reason when defaultTab /= StateReload tab and the panel is a component, it will be duplicated on the bottom of the screen
                    [ whenElem isInit $ \_ -> renderBody currentTab
                    ]
                ]
            ]
  , eval: H.mkEval $ H.defaultEval
      { initialize = Just TabInit
      , receive = \{ context } -> case context of
          Nothing -> Nothing
          Just k  -> StateReload <$> hush (fromJSON k)
      , handleAction = case _ of
          TabInit -> do
            -- DS.log Info "TabInit"
            -- DS.logStringify Info =<< (map _.currentTab H.get)
            H.modify_ _ { isInit = true }
          StateReload k -> do
            { currentTab } <- H.get
            when (currentTab /= k) do
              H.raise $ OnSwitch { prev: currentTab, new: k }
              H.modify_ _ { currentTab = k }
          TabSwitch k -> do
            { currentTab } <- H.get
            when (currentTab /= k) do
              H.raise $ OnSwitch { prev: currentTab, new: k }
              H.modify_ _ { currentTab = k }
              updateStore $ setUIState name k
          TabReceive o -> do
            H.raise $ OnOutput o
      }
  }