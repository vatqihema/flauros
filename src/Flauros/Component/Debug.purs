module Flauros.Component.Debug where

import Prelude

import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Flauros.Data.Store (log)
import Flauros.Data.Store as DS
import Flauros.Data.System.Browser (readLocalStorage, writeLocalStorage)
import Flauros.Data.System.Save as Save
import Flauros.Log (LogType(..))
import Halogen as H
import Halogen.HTML as HH
import Halogen.Store.Connect (Connected, connect)
import Halogen.Store.Monad (class MonadStore, getStore, updateStore)
import Halogen.Store.Select (selectAll)
import Type.Prelude (Proxy(..))

data AutoSaveAction i = Load | Save (Connected DS.Store i)

_devAutoSave = Proxy :: Proxy "devAutoSave"

devAutoSave :: forall q i o m. MonadStore DS.Action DS.Store m => H.Component q i o m
devAutoSave = connect selectAll $ H.mkComponent
  { initialState: \_ -> unit
  , render: \_ ->
      HH.text ""
  , eval: H.mkEval $ H.defaultEval
      { initialize   = Just Load
      , receive      = Just <<< Save
      , handleAction = case _ of
          Load -> do
            readLocalStorage "global" >>= case _ of
              Left err     -> log Error (show err)
              Right global -> readLocalStorage "dev-autosave" >>= case _ of
                Left err     -> log Error (show err)
                Right unique -> Save.deserializeAndApply { global, unique }
            -- updateStore $ DS.action _ { isDebug = false }
            -- DS.Store st <- getStore
            -- DS.log Info $ "loaded a debug autosave, uiState:"
            -- DS.logStringify Info st.uiState
          Save { context } -> do
            { global, unique } <- Save.serializeStore context
            writeLocalStorage "global" global
            writeLocalStorage "dev-autosave" unique
            -- DS.log Info "saved a debug autosave"
      }
  }