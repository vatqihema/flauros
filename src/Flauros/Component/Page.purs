module Flauros.Component.Page where

import Prelude

import CSS as CSS
import CSS.Common (auto)
import Data.Maybe (Maybe(..))
import Flauros.Component.Debug (_devAutoSave, devAutoSave)
import Flauros.Component.SeerBook (seerBook)
import Flauros.Component.TopBar (_topBar, topBar)
import Flauros.Component.WorldPanel (worldPanel)
import Flauros.Data.Store as DS
import Flauros.Startup (startup)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.CSS as HCSS
import Halogen.HTML.Properties as HP
import Halogen.Store.Monad (class MonadStore)
import Type.Prelude (Proxy(..))

className :: HH.ClassName
className = HH.ClassName "flauros-page"

page :: forall q i o m. MonadStore DS.Action DS.Store m => H.Component q i o m
page = H.mkComponent
  { initialState: \_ -> false
  , render: \isLoaded ->
      if   isLoaded
      then
        HH.div
          [ HP.class_ className
          , HCSS.style do
              CSS.width $ CSS.vw 100.0
              CSS.height $ CSS.vh 100.0
              CSS.maxWidth $ CSS.vw 70.0
              CSS.margin (CSS.px 0.0) auto (CSS.px 0.0) auto
          ]
          [ HH.slot_ _devAutoSave 0 devAutoSave unit
          , HH.slot_ _topBar 1 topBar unit
          , HH.div
              [ HCSS.style do
                  CSS.zIndex 1
                  CSS.float CSS.floatRight
                  CSS.width $ CSS.pct 70.0
                  CSS.height $ CSS.pct 100.0
              ]
              [ HH.slot_ (Proxy :: Proxy "worldPanel") 3 worldPanel unit
              ]
          , HH.div
              [ HCSS.style do
                  CSS.zIndex 2
                  CSS.position CSS.relative
                  CSS.width $ CSS.pct 30.0
                  CSS.height $ CSS.pct 75.0
              ]
              [ HH.slot_ (Proxy :: Proxy "seerBook") 2 seerBook unit
              ]
          ]
        else
          HH.div_
            [ HH.text "Loading..." ]
  , eval: H.mkEval H.defaultEval
      { initialize = Just unit
      , handleAction = \_ -> do
          startup
          H.put true
      }
  }