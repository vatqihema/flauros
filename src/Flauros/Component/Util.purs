module Flauros.Component.Util where

import Prelude

import CSS (CSS, Color)
import CSS as CSS
import DOM.HTML.Indexed (HTMLspan)
import Data.Array as A
import Data.Foldable (for_)
import Data.List (List(..), (:))
import Data.Map as Map
import Data.Maybe (Maybe(..), fromMaybe)
import Flauros.Component.UCSS as UCSS
import Flauros.Component.Util.Icon (getSVGIcon)
import Flauros.Lang.Text (IconStyle(..), Mirror(..), Text(..), TextPart(..))
import Flauros.Style.Color (Colors, defaultColors)
import Flauros.Style.Typography (mirrorH, mirrorV)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.CSS as HCSS
import Halogen.HTML.Properties as HP
import Halogen.Store.Connect (Connected)

data AnchorDirection = AnchorTop | AnchorBottom | AnchorLeft | AnchorRight
derive instance Eq AnchorDirection

clazz :: forall r i. String -> HP.IProp ( class :: String | r ) i
clazz = HP.class_ <<< HH.ClassName

clazzes :: forall r i. Array String -> HP.IProp ( class :: String | r ) i
clazzes = HP.classes <<< map HH.ClassName

maybeElem :: forall w i a. Maybe a -> (a -> HH.HTML w i) -> HH.HTML w i
maybeElem m f = case m of
  Just j  -> f j
  Nothing -> HH.text ""

whenElem :: forall w i. Boolean -> (Unit -> HH.HTML w i) -> HH.HTML w i
whenElem b f = if b then f unit else HH.text ""

data StoreUpdateAction ctx i = Init | Receive (Connected ctx i)

iconClasses :: Array HH.ClassName
iconClasses = [ HH.ClassName "material-symbols-outlined", HH.ClassName "icon" ]

wrapHTML :: forall q i o m s. (Unit -> HH.HTML (H.ComponentSlot s m i) i) -> H.Component q i o m
wrapHTML render = H.mkComponent
  { initialState: \_ -> unit
  , render
  , eval: H.mkEval H.defaultEval
      { receive = Just
      , handleAction = case _ of
          _ -> H.modify_ identity
      }
  }

data RichTextProc = Normal | InLabel | InTooltip

richText_ :: forall w i. Colors -> CSS -> Text -> HH.HTML w i
richText_ = richText []

richText :: forall w i. Array (HH.IProp HTMLspan i) -> Colors -> CSS -> Text -> HH.HTML w i
richText props colors typography (Text txt) =
  HH.span
    [ HCSS.style typography ]
    [ HH.span props (go [] Nothing [] [] txt) ]
  where go :: Array (HH.HTML w i) -> Maybe Color -> Array CSS -> Array String -> List TextPart -> Array (HH.HTML w i)
        go acc _ _ _ Nil = acc
        go acc mColor csss classes (part:parts) = case part of
            TBr              -> A.cons  HH.br_ $ go acc mColor csss classes parts
            TBackground col  -> goWithCSS $ CSS.backgroundColor $ col colors
            TClasses clss    -> go acc mColor csss (classes <> clss) parts
            TClear           -> go acc Nothing [] [] parts
            TColor col       -> go acc (Just $ col colors) csss classes parts
            TIcon style name -> A.cons (mkSymbol style name) $ go acc mColor csss classes parts
            TFont ff         -> goWithCSS $ CSS.fontFaceFamily ff
            TFontStyle fs    -> goWithCSS $ CSS.fontStyle fs
            TFontWeight w    -> goWithCSS $ CSS.fontWeight (CSS.weight w)
            TMeta _          -> go acc mColor csss classes parts
            TMirror m        -> goWithCSS $ case m of
              MirrorH -> mirrorH
              MirrorV -> mirrorV
            TSortPriority _  -> go acc mColor csss classes parts
            TStr s           -> A.cons (mkTxt s)             $ go acc mColor csss classes parts
            TSVG name s      -> A.cons (getSVGIcon name s (fromMaybe (defaultColors.white) mColor)) $ go acc mColor csss classes parts
            TSVGPath p w h   -> A.cons (mkSVG p w h)         $ go acc mColor csss classes parts
            TTooltip label t -> A.cons (mkTooltip label t)   $ go acc mColor csss classes parts
          where mkTxt s = HH.span
                  [ HCSS.style do
                      for_ csss identity
                      for_ mColor CSS.color
                  ]
                  [ HH.text s ]
                icon = case _ of
                  Outlined -> UCSS.iconOutlined
                  Rounded  -> UCSS.iconRounded
                mkSymbol style name = HH.span
                  [ HCSS.style do
                      for_ csss identity
                      for_ mColor CSS.color
                  ]
                  [ (icon style)
                      [ HCSS.style do
                          UCSS.fontVariationSettings (Map.singleton UCSS.opsz 4.0)
                          CSS.key (CSS.fromString "font-size") "unset"
                      ]
                      name
                  ]
                goWithCSS css = go acc mColor (A.cons css csss) classes parts
                mkSVG path w h =
                  HH.img
                    [ HCSS.style do
                        CSS.width  (CSS.px w)
                        CSS.height (CSS.px h)
                    ]
                mkTooltip (Text label) ttip =
                  HH.span
                  [ HP.title ttip ]
                  $ go [] mColor csss classes label