module Flauros.Component.UCSS where

import Prelude

import CSS (CSS, Color, rem)
import CSS as CSS
import CSS.Box as CSSBox
import DOM.HTML.Indexed (HTMLspan)
import Data.FoldableWithIndex (foldlWithIndex)
import Data.Map (Map)
import Data.Newtype (class Newtype)
import Data.NonEmpty as NE
import Flauros.Style.Color (Colors, voidest)
import Halogen.HTML as HH
import Halogen.HTML.Properties as HP

data BorderCollapse = Collapse | Separate

borderCollapse :: BorderCollapse -> CSS
borderCollapse = CSS.key (CSS.fromString "border-collapse") <<< case _ of
  Collapse -> "collapse"
  Separate -> "separate"

borderNone :: CSS
borderNone = CSS.key (CSS.fromString "border") "none"

userSelectNone :: CSS
userSelectNone = CSS.key (CSS.fromString "user-select") "none"

rightToLeft :: CSS
rightToLeft = CSS.key (CSS.fromString "direction") "rtl"

-- important :: forall v. CSS -> CSS
-- important = case _ of

-- opsz :: Int -> CSS
-- opsz = CSS.key (CSS.fromString "font-variation-settings") <<< ("'FILL' 0, 'wght' 100, 'GRAD' 0, 'opsz' " <> _) <<< show

newtype FontAxis = FontAxis String

derive instance eqFontAxis      :: Eq FontAxis
derive instance newtypeFontAxis :: Newtype FontAxis _
derive instance ordFontAxis     :: Ord FontAxis

wght :: FontAxis
wght = FontAxis "wght"
wdth :: FontAxis
wdth = FontAxis "wdth"
slnt :: FontAxis
slnt = FontAxis "slnt"
ital :: FontAxis
ital = FontAxis "ital"
opsz :: FontAxis
opsz = FontAxis "opsz"

fontVariationSettings :: Map FontAxis Number -> CSS
fontVariationSettings = foldlWithIndex (\(FontAxis axis) acc num -> acc <> " '" <> axis <> "' " <> show num) ""
                    >>> CSS.key (CSS.fromString "font-variation-settings")

iconOutlined :: forall w i. Array (HH.IProp HTMLspan i) -> String -> HH.HTML w i
iconOutlined props name =
  HH.span
    (props <> [ HP.classes [ HH.ClassName "material-symbols-outlined", HH.ClassName "icon"] ])
    [ HH.text name ]

iconOutlined_ :: forall w i. String -> HH.HTML w i
iconOutlined_ = iconOutlined []

iconRounded :: forall w i. Array (HH.IProp HTMLspan i) -> String -> HH.HTML w i
iconRounded props name =
  HH.span
    (props <> [ HP.classes [ HH.ClassName "material-symbols-rounded", HH.ClassName "icon"] ])
    [ HH.text name ]

iconRounded_ :: forall w i. String -> HH.HTML w i
iconRounded_ = iconRounded []

buttonCSS :: Colors -> CSS
buttonCSS colors = do
  CSSBox.boxShadow $ NE.singleton $ (voidest colors) `CSSBox.bsColor` CSSBox.shadowWithBlur (rem $ -0.25) (rem 0.25) (rem 1.0)

easyShadow :: Color -> Number -> CSS
easyShadow color mag = CSSBox.boxShadow
                     $ NE.singleton
                     $ color `CSSBox.bsColor` CSSBox.shadowWithBlur (rem $ mag * -0.25) (rem $ mag * 0.25) (rem mag)