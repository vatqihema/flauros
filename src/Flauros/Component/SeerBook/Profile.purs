module Flauros.Component.SeerBook.Profile where

import Prelude

import CSS as CSS
import Data.FunctorWithIndex (mapWithIndex)
import Data.HashMap as HM
import Data.Hashable (hash)
import Data.Int (toNumber)
import Data.Map as Map
import Data.Maybe (Maybe(..))
import Data.Newtype (unwrap)
import Data.Traversable (for)
import Data.TraversableWithIndex (forWithIndex)
import Flauros.Component.Util (clazz, clazzes, maybeElem, richText_, whenElem)
import Flauros.Component.Util.Icon as Icon
import Flauros.Data.Actor (playerID)
import Flauros.Data.Store (mkTranslate, mkTranslateAll, mkTranslateWith, selectAllEqOn, selectNone)
import Flauros.Data.Store as DS
import Flauros.Lang (LineParam(..))
import Flauros.Lang.Text (plainify, (&))
import Flauros.Prefab.Registries as Registries
import Flauros.Prefab.Registry as Registry
import Flauros.Prelude (sid, transmute)
import Flauros.Style.Color as Color
import Flauros.Style.Typography as Typography
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.CSS as HCSS
import Halogen.HTML.Properties as HP
import Halogen.Store.Connect (Connected, connect)
import Halogen.Store.Monad (class MonadStore)
import Type.Prelude (Proxy(..))

data ProfileAction = ProfInit | ProfReceive DS.Store

profile_ = Proxy :: Proxy "profile"

profile :: forall q i o m. MonadStore DS.Action DS.Store m => H.Component q i o m
profile = connect (selectAllEqOn (HM.lookup playerID <<< _.actors <<< unwrap)) $ H.mkComponent
  { initialState: \{ context: ctx@(DS.Store { actors, colors }) } ->
      { colors
      , mPlayer:       HM.lookup playerID actors
      , translate:     mkTranslate ctx
      , translateWith: mkTranslateWith ctx
      }
  , render: \{ colors, mPlayer, translate, translateWith } ->
      maybeElem mPlayer \player -> do
        HH.div
          [ clazzes [ "profile", "max" ]
          , HCSS.style do
              CSS.background $ CSS.url "resources/images/seerbook-backgrounds/profile-waterfall.webp"
              CSS.height $ CSS.pct 100.0
          ]
          [ HH.div
              [ HCSS.style do
                  CSS.borderBottom CSS.dashed (CSS.rem 0.1) colors.sand
                  CSS.backgroundColor $ Color.withAlpha 0.05 colors.cornflower
              ]
              [ HH.div
                  [ HCSS.style do
                      CSS.float CSS.floatRight
                      CSS.marginRight $ CSS.rem 0.5
                      CSS.marginTop   $ CSS.rem 0.5
                  , HP.title $ plainify $ translate (sid "profile") "artichoke-desc"
                  ]
                  [ Icon.artichokeIcon 64.0 colors.gold ]
              , HH.div
                  [ HCSS.style $ CSS.marginLeft $ CSS.rem 0.3 ]
                  [ richText_ colors Typography.seerbookHeader $ "★" & translateWith (sid "name") "full" [PName player.name]
                  , HH.br_
                  , richText_ colors Typography.retro $ translate (sid "profile") "nationality" & " • "
                  , richText_ colors Typography.retro $ translate (sid "profile") "license"
                  , HH.br_
                  , richText_ colors Typography.retro $ translate (sid "profile") "birthday"
                  , HH.br_
                  , richText_ colors Typography.retro $ translateWith (sid "profile") "ancestry" [PID player.ancestry] & " • "
                  , richText_ colors Typography.retro $ translateWith (sid "profile") "address" []
                  ]
              ]
          , HH.div_ $ HM.values $ flip mapWithIndex player.stats \id stat ->
              HH.div
                [ HCSS.style $ CSS.paddingLeft $ CSS.px $ toNumber $ hash id `mod` 7 ]
                [ richText_ colors Typography.handwriting $ translateWith id "desc" [PStat stat]
                ]
          ]
  , eval: H.mkEval $ H.defaultEval
      { initialize   = Just ProfInit
      , receive      = Just <<< ProfReceive <<< _.context
      , handleAction = case _ of
          ProfInit      -> pure unit
          ProfReceive (DS.Store { actors, colors }) ->
            H.modify_ _
              { mPlayer = HM.lookup playerID actors
              , colors  = colors
              }
      }
  }