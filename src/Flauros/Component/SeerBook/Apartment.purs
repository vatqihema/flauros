module Flauros.Component.SeerBook.Apartment where

import Prelude

import CSS as CSS
import Data.Array ((..))
import Data.Array as Array
import Data.Foldable (for_, maximum, minimum)
import Data.Hashable (hash)
import Data.Int (toNumber)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Traversable (for)
import Flauros.Component.UCSS (easyShadow)
import Flauros.Component.Util (clazzes, maybeElem, richText, richText_, whenElem)
import Flauros.Data.Gen as Gen
import Flauros.Data.Knowledge as Knowledge
import Flauros.Data.Store (mkTranslate, mkTranslateWith, selectNone)
import Flauros.Data.Store as DS
import Flauros.Lang (LineParam(..))
import Flauros.Lang.Text (color, plainify, (&))
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry as Registry
import Flauros.Prelude (Identifier, nthByMod, sid)
import Flauros.Style.Color as Color
import Flauros.Style.Typography as Typography
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.CSS as HCSS
import Halogen.Store.Connect (connect)
import Halogen.Store.Monad (class MonadStore, getStore)
import Type.Prelude (Proxy(..))

data ProfileAction = ProfInit

apartmentBrowserID :: Identifier
apartmentBrowserID = sid "apartment-browser"

apartmentBrowser_ = Proxy :: Proxy "apartmentBrowser"

apartmentBrowser :: forall q i o m. MonadStore DS.Action DS.Store m => H.Component q i o m
apartmentBrowser = connect selectNone $ H.mkComponent
  { initialState: \{ context: store@(DS.Store { colors, isDebug }) } ->
      { colors, isDebug
      , translate:     mkTranslate store
      , translateWith: mkTranslateWith store
      , apartments:    []
      , store
      }
  , render: \{ apartments, colors, isDebug, store, translate, translateWith } ->
      HH.div
        [ clazzes [ "apartment-browser", "max" ]
        , HCSS.style do
            -- CSS.backgroundColor colors.voider
            CSS.background $ CSS.url "resources/images/seerbook-backgrounds/apartment.jpg"
            CSS.height $ CSS.pct 100.0
        ]
        [ HH.div
            [ HCSS.style do
                CSS.borderBottom CSS.solid (CSS.rem 0.05) colors.gold
                CSS.borderTop    CSS.solid (CSS.rem 0.05) colors.gold
            ]
            [ HH.div
                [ clazzes ["animate__animated", "animate__slideInRight"] ]
                [ richText_ colors Typography.theNews $ translate (sid "tab-title") "apartment-browser" ]
            ]
        , HH.div
            [ HCSS.style do
                -- CSS.backgroundColor colors.voidest
                pure unit
            ]
            ( flip map apartments \apartment ->
                let buildingName = Knowledge.eval translateWith apartment.buildingName
                    hsh          = hash $ plainify $ buildingName
                    nameColor    = fromMaybe _.yellow $ nthByMod hsh Color.allHighlightColors
                    unitPrices   = apartment.availableUnits <#> apartment.monthlyRent store
                 in HH.div
                    [ HCSS.style do
                        CSS.backgroundColor $ Color.withAlpha 0.06 $ nameColor colors
                        easyShadow colors.voidest 0.25
                        CSS.borderBottom CSS.solid (CSS.rem 0.05) colors.voidest
                    ]
                    [ HH.div
                      [ HCSS.style do
                          CSS.marginLeft $ CSS.rem 0.2
                      ]
                      [ richText_ colors (CSS.marginLeft (CSS.rem $ (toNumber (hsh `mod` 10) * 0.015 + 0.1)) *> Typography.apartmentHeader) buildingName
                      , whenElem (apartment.quality >= 4.0) \_ ->
                          richText_ colors Typography.apartmentHeader $ color nameColor & "   " & translateWith apartmentBrowserID "high-quality" [PNum apartment.quality]
                      , richText [ HCSS.style $ CSS.float CSS.FloatRight ] colors Typography.apartmentHeader $ color nameColor & translateWith (sid "apartment-browser") "stars" [PNum apartment.quality]
                      , HH.br_
                      , richText_ colors Typography.apartmentBody $ color _.lavender & Knowledge.eval translateWith apartment.address
                      , richText [ HCSS.style $ CSS.float CSS.FloatRight ] colors Typography.apartmentBody $ color _.lavender & translateWith (sid "apartment-browser") "units-available" [PInt $ Array.length apartment.availableUnits]
                      , whenElem (false && isDebug) \_ ->
                          HH.div
                            [ HCSS.style $ CSS.float CSS.FloatRight ]
                            [ richText_ colors Typography.apartmentBody $ "B " & translateWith (sid "date") "yyyy-mm-dd" [PDate apartment.built] & ", "
                            , richText_ colors Typography.apartmentBody $ "R " & translateWith (sid "date") "yyyy-mm-dd" [PDate apartment.lastRefurbished]
                            ]
                      , HH.br_
                      , maybeElem ({ mn: _, mx: _ } <$> minimum unitPrices <*> maximum unitPrices) \{ mn, mx } ->
                          richText_ colors Typography.monoBody $ color nameColor & translateWith (sid "apartment-browser") "monthly-rent" [PNum mn, PNum mx]
                      , richText [ HCSS.style $ CSS.float CSS.FloatRight ] colors Typography.apartmentBody $ Knowledge.eval translateWith apartment.describeUnits
                      ]
                    ]
            )
        ]
  , eval: H.mkEval $ H.defaultEval
      { initialize   = Just ProfInit
      , handleAction = case _ of
          ProfInit -> do
            mApartmentGen <- Registry.lookup Regs.housing (sid "starter-apartment")
            for_ mApartmentGen \gen -> do
              -- todo not quick, save these setup results!
              apartments <- for (0..6) \idx -> do
                store <- getStore
                Gen.quick store unit gen
              H.modify_ _ { apartments = apartments }


      }
  }