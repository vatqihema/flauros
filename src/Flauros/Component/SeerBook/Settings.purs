module Flauros.Component.SeerBook.Settings where

import Prelude

import CSS (pct, toHexString)
import CSS as CSS
import CSS.Color as Color
import CSS.Overflow (hidden, overflow)
import Data.Array ((..))
import Data.Array as Array
import Data.Function (on)
import Data.FunctorWithIndex (mapWithIndex)
import Data.HashMap (HashMap)
import Data.HashMap as HM
import Data.Int (even)
import Data.Map as Map
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Newtype (unwrap)
import Data.Set as Set
import Data.Tuple (Tuple(..), fst, snd)
import Data.Tuple.Nested ((/\))
import Flauros.Component.Tab (TabAction(..), TabContextInput, TabOutput(..), tabContext, tabContext_)
import Flauros.Component.Tab as Tag
import Flauros.Component.UCSS (easyShadow)
import Flauros.Component.UCSS as UCSS
import Flauros.Component.Util (AnchorDirection(..), clazz, clazzes, richText_, whenElem)
import Flauros.Component.Util as Util
import Flauros.Data.Setting (Setting, SettingBehavior(..), Storage(..), ValueRange(..))
import Flauros.Data.Setting as Setting
import Flauros.Data.Store (mkTranslate, mkTranslateAll, mkTranslateWith, selectAllWhen, selectNone, selectStyle)
import Flauros.Data.Store as DS
import Flauros.Lang.Text (compareText, plainify)
import Flauros.Log (LogType(..))
import Flauros.Prefab.Registries (Setting')
import Flauros.Prefab.Registries as Registries
import Flauros.Prefab.Registry as Registry
import Flauros.Prelude (IDMap, Identified, Identifier, groupOn, sid, (|>))
import Flauros.Style.Color (Colors)
import Flauros.Style.Typography as Typography
import Foreign (Foreign, unsafeFromForeign, unsafeToForeign)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.CSS as HCSS
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties (InputType(..))
import Halogen.HTML.Properties as HP
import Halogen.Store.Connect (connect)
import Halogen.Store.Monad (class MonadStore, getStore, updateStore)
import Halogen.Store.Select (Selector)
import Type.Prelude (Proxy(..))
import Type.Row (type (+))
import Unsafe.Reference (unsafeRefEq)
import Web.Event.Event (Event)

foreign import eventValue :: Event -> String

data SettingOutput = SettingOutput (forall m. MonadStore DS.Action DS.Store m => m Unit)
  { storage :: Storage
  , update  :: IDMap Foreign -> IDMap Foreign
  } 

data SettingsAction = SetInit | SetTabOutput (TabOutput String SettingOutput) | SetReceive DS.Store

settings_ = Proxy :: Proxy "settings"

settings :: forall q i o m. MonadStore DS.Action DS.Store m => H.Component q i o m
settings = connect selectNone $ H.mkComponent
  { initialState: \{ context: ctx@(DS.Store { colors }) } ->
      let translateAll = mkTranslateAll ctx
          catSymbols   = translateAll (sid "setting/category-symbol")
       in { colors
          , translate:     mkTranslate ctx
          , translateWith: mkTranslateWith ctx
          , translateAll
          , catNames:      translateAll (sid "setting/category-name")
          , catSymbols
          , categories:    Map.keys catSymbols
          , allSettings:   HM.empty
          }
  , render: \{ allSettings, colors, translate } ->
      whenElem (not (HM.isEmpty allSettings)) $ \_ ->
        HH.div
          [ clazzes [ "settings", "max" ]
          , HCSS.style do
              -- CSS.background $ CSS.url "resources/images/seerbook-backgrounds/settings.webp"
              CSS.height $ CSS.pct 100.0
          ]
          [ HH.div
              [ HCSS.style do
                  CSS.backgroundColor colors.voidish
                  easyShadow colors.voidest 0.25
              ]
              [ richText_ colors Typography.monoHeader $ translate (sid "tab-title") "settings" ]
          ]
  , eval: H.mkEval $ H.defaultEval
      { initialize   = Just SetInit
      , handleAction = case _ of
          SetInit -> do
            allSettings <- Registry.lookupAll Registries.setting
            H.modify_ _ { allSettings = allSettings }
          _ -> pure unit
      }
  }

settingsPanel_ = Proxy :: Proxy "settingsPanel"

selectSettings :: Selector DS.Store DS.Store
selectSettings = selectAllWhen \(DS.Store a) (DS.Store b) ->
  a.colors          `unsafeRefEq` b.colors &&
  a.settingsBrowser `unsafeRefEq` b.settingsBrowser &&
  a.settingsSave    `unsafeRefEq` b.settingsSave

settingsPanel :: forall q i o m. MonadStore DS.Action DS.Store m => H.Component q i o m
settingsPanel = connect selectSettings $ H.mkComponent
  { initialState: \{ context: ctx@(DS.Store { colors, settingsBrowser, settingsSave }) } ->
      let translateAll = mkTranslateAll ctx
       in { colors
          , translate:     mkTranslate ctx
          , categories:    map fst $ Array.sortBy (compareText `on` snd)
                                   $ Map.toUnfoldable
                                   $ translateAll (sid "setting/category-name")
          , allSettings:   HM.empty :: IDMap {| Identified + Setting' + () }
          , settingsByCat: HM.empty :: HashMap String (Array {| Identified + Setting' + () })
          , settingsBrowser, settingsSave
          }
  , render: \st@{ allSettings, categories, colors, settingsByCat, translate } ->
      whenElem (not (HM.isEmpty allSettings)) $ \_ ->
        HH.div
          [ clazzes [ "settings", "max" ]
          , HCSS.style do
              CSS.backgroundColor colors.voider
              CSS.height $ CSS.pct 100.0
          ]
          [ flip (HH.slot tabContext_ 0 (tabContext [ clazz "max" ] "settings-category" categories)) SetTabOutput $ Tag.withDefInput
              { anchor:     AnchorRight
              , defaultTab: "accessibility"
              , highlightColor: colors.purple
              , renderTab: \b k ->
                  let css = do
                        Typography.seerbookHeader
                        CSS.color (if b then colors.white else colors.lavender)
                        -- CSS.width $ CSS.pct 25.0
                        CSS.marginLeft $ CSS.rem 2.0
                   in richText_ colors css $ translate (sid "setting/category-name") k
              , tabCSS: \isSelected _ ->
                  when isSelected do
                    easyShadow colors.voidest 0.2
              , renderBody: \k ->
                  let title = translate (sid "setting/category-title") k
                   in HH.div
                      [ HCSS.style do
                          CSS.backgroundColor colors.voidish
                          overflow hidden
                      ]
                      [ HH.div
                          [ HCSS.style do
                              easyShadow colors.voidest 0.5
                              CSS.backgroundColor colors.black
                              CSS.textWhitespace CSS.whitespaceNoWrap
                          ]
                          [ richText_ colors Typography.monoHeader $ Array.foldl (\acc _ -> acc <> title) title (0..2) ]
                      , HH.table
                          [ clazz "wrap"
                          , HCSS.style $ CSS.width $ pct 100.0
                          ]
                          [ HH.tbody_ $ fromMaybe [] $ HM.lookup k settingsByCat <#> \byCat -> byCat
                              |> map (\s -> translate s.id "name" /\ s)
                              |> Array.sortBy (compareText `on` fst)
                              |> map \(Tuple settingName setting) ->
                                HH.tr_
                                  [ HH.td
                                      [ HCSS.style do
                                          CSS.width $ CSS.pct 47.0
                                      ]
                                      [ HH.div_
                                          [ HH.span [ HCSS.style $ CSS.marginLeft $ CSS.rem 1.5 ]
                                              [ richText_ colors Typography.seerbookHeader settingName ]
                                          , HH.br_
                                          , HH.span [ HCSS.style $ CSS.marginLeft $ CSS.rem 0.5 ]
                                              [ richText_ colors Typography.seerbookBody $ translate setting.id "desc" ]
                                          ]
                                      ]
                                  , HH.td
                                      [ HCSS.style do
                                          CSS.width $ CSS.pct 48.0
                                      ]
                                      [ setting.behavior |> \(SettingBehavior { onChange, storage, values }) -> case values of
                                          ColorPicker fromColor toColor ->
                                            let curColor = toColor $ Setting.currentValue setting st
                                                hex      = toHexString $ curColor
                                              in HH.div_
                                                  [ HH.input
                                                      [ HP.type_ HP.InputColor
                                                      , HP.value hex
                                                      , HE.onChange \evt -> 
                                                          let val = eventValue evt
                                                              col = fromColor $ fromMaybe curColor $ Color.fromHexString val
                                                              prev = Setting.currentValue setting st
                                                           in TabReceive $ SettingOutput
                                                              (onChange prev col :: forall m1. MonadStore DS.Action DS.Store m1 => m1 Unit)
                                                              { storage
                                                              , update: HM.insert setting.id (unsafeToForeign col)
                                                              }
                                                      ]
                                                  ]
                                          _ -> HH.span_ [ HH.text "Unimplemented" ]
                                      ]
                                  , HH.td
                                      [ HCSS.style do
                                          CSS.width $ CSS.pct 5.0
                                      ]
                                      [ setting.behavior |> \(SettingBehavior { default, onChange, storage }) ->
                                          HH.button
                                            [ clazz "simple"
                                            , HP.type_ HP.ButtonButton
                                            , HE.onClick \_ -> 
                                                let prev = Setting.currentValue setting st
                                                 in TabReceive $ SettingOutput
                                                    (onChange prev default)
                                                    { storage
                                                    , update: HM.insert setting.id default
                                                    }
                                            , HCSS.style do
                                                CSS.backgroundColor colors.voider
                                                CSS.color           colors.sand
                                                easyShadow colors.voidest 0.25
                                            ]
                                            [ HH.text $ plainify $ translate (sid "setting/action") "reset" ]
                                      ]
                                  ]
                          ]
                      ]
              }
          ]
  , eval: H.mkEval $ H.defaultEval
      { initialize   = Just SetInit
      , receive      = \{ context } -> Just $ SetReceive $ context
      , handleAction = case _ of
          SetInit -> do
            allSettings <- Registry.lookupAll Registries.setting
            H.modify_ _
              { allSettings = allSettings
              , settingsByCat = groupOn _.category $ HM.values allSettings
              }
          SetReceive st@(DS.Store { colors, settingsBrowser, settingsSave }) -> do
            H.modify_ _
              { colors          = colors
              , settingsBrowser = settingsBrowser
              , settingsSave    = settingsSave
              , translate       = mkTranslate st
              }
          SetTabOutput output -> case output of
            OnOutput (SettingOutput action { storage, update }) -> do
              updateStore $ DS.action \st -> case storage of
                BrowserStorage -> st { settingsBrowser = update st.settingsBrowser }
                SaveStorage    -> st { settingsSave    = update st.settingsSave }
              action
            _ -> pure unit
      }
  }