module Flauros.Component.SeerBook.Debug where

import Prelude

import CSS as CSS
import Data.HashMap as HM
import Data.Map as Map
import Data.Maybe (Maybe(..))
import Flauros.Component.Util (clazzes, richText_, whenElem)
import Flauros.Data.Store (mkTranslate, mkTranslateAll, mkTranslateWith, selectNone)
import Flauros.Data.Store as DS
import Flauros.Prefab.Registries as Registries
import Flauros.Prefab.Registry as Registry
import Flauros.Prelude (sid)
import Flauros.Style.Typography as Typography
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.CSS as HCSS
import Halogen.Store.Connect (connect)
import Halogen.Store.Monad (class MonadStore)
import Type.Prelude (Proxy(..))

data DebugAction = DebugInit

debug_ = Proxy :: Proxy "debug"

debug :: forall q i o m. MonadStore DS.Action DS.Store m => H.Component q i o m
debug = connect selectNone $ H.mkComponent
  { initialState: \{ context: ctx@(DS.Store { colors }) } ->
      { colors
      , translate:     mkTranslate ctx
      }
  , render: \{ colors, translate } ->
      HH.div
        [ clazzes [ "profile", "max" ]
        , HCSS.style do
            CSS.background $ CSS.url "resources/images/seerbook-backgrounds/debug.webp"
            CSS.height $ CSS.pct 100.0
        ]
        [ richText_ colors Typography.theNews $ translate (sid "tab-title") "debug"
        ]
  , eval: H.mkEval $ H.defaultEval
      { initialize   = Just DebugInit
      , handleAction = case _ of
          DebugInit -> pure unit
      }
  }
          