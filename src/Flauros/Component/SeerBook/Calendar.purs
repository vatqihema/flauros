module Flauros.Component.SeerBook.Calendar where

import Prelude

import CSS (rem)
import CSS as CSS
import CSS.Common (center, middle)
import CSS.Size (unitless)
import CSS.TextAlign as CSST
import CSS.VerticalAlign (verticalAlign)
import Data.Array ((..))
import Data.Array as Array
import Data.Date (Date)
import Data.Date as Date
import Data.DateTime (month, weekday)
import Data.Enum (fromEnum, pred, succ)
import Data.Foldable (foldl, for_)
import Data.Int (odd, toNumber)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Time.Duration (Days(..))
import Flauros.Component.Badge (badge_)
import Flauros.Component.UCSS (easyShadow)
import Flauros.Component.UCSS as UCSS
import Flauros.Component.Util (clazz, clazzes, iconClasses, maybeElem, richText_)
import Flauros.Data.Holiday (HolidaySchedule, holidaysOn, isVisible, mkSchedule)
import Flauros.Data.Month (Month, monthID)
import Flauros.Data.Astronomy (MoonPhase(..), moonPhase, phaseID)
import Flauros.Data.Store (mkTranslate, mkTranslateWith, selectNone)
import Flauros.Data.Store as DS
import Flauros.Data.Time (class HasDate, adjustDate, atMidnight, firstDayOfMonth, getDate, getDay, getMonth, getYear)
import Flauros.Lang (LineParam(..), Translate, TranslateWith)
import Flauros.Lang.Text (plainify, tooltip, (&))
import Flauros.Log (LogType(..))
import Flauros.Prefab.Registries as Registries
import Flauros.Prefab.Registry as Registry
import Flauros.Prelude (Identified, default, sid)
import Flauros.Style.Color (Colors, transparent, voider, voidest, withAlpha)
import Flauros.Style.Typography as Typography
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.CSS as HCSS
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.Store.Connect (Connected, connect)
import Halogen.Store.Monad (class MonadStore)
import Type.Prelude (Proxy(..))
import Type.Row (type (+))

data CalendarAction = CalInit | CalFilter | CalChangeDate Date | CalMonthPred | CalToday | CalMonthSucc

calendar_ = Proxy :: Proxy "calendar"

calendar :: forall q i o m. MonadStore DS.Action DS.Store m => H.Component q i o m
calendar = connect selectNone $ H.mkComponent
  { initialState: \{ context: context@(DS.Store { colors, currentTime }) } ->
      { calStartDate: calendarStart currentTime
      , firstDay:     firstDayOfMonth currentTime
      , curDate:      getDate currentTime
      , currentTime
      , mHolidays:    Nothing
      , mMonth:       Nothing
      , colors
      , translate:     mkTranslate context
      , translateWith: mkTranslateWith context
      }
  , render: \st@{ calStartDate, colors, curDate, firstDay, mHolidays, mMonth, translate } ->
      maybeElem mMonth $ \month ->
        maybeElem mHolidays $ \holidays ->
          let _ = unit
            in HH.div
                [ clazzes ["max", "calendar"]
                , HCSS.style do
                    CSS.backgroundColor $ withAlpha 0.025 $ month.theme.highlight colors
                ]
                [ HH.div
                    [ HCSS.style do
                        CSS.backgroundColor colors.voidish
                    ]
                    [ HH.p 
                        [ HCSS.style do
                            Typography.theNews
                            CSS.color $ month.theme.text colors
                        ]
                        [ HH.div [ HCSS.style $ CSS.float CSS.FloatLeft ]
                            [ richText_ colors Typography.theNews $ translate month.id "symbol"
                            , richText_ colors Typography.theNews $ translate month.id "name"
                            , HH.text $ " " <> show (fromEnum $ getYear firstDay)
                            ]
                        , HH.div_ []
                        , HH.div [ HCSS.style $ CSS.float CSS.FloatRight ]
                            [ HH.span
                                [ HP.classes iconClasses
                                , HE.onClick \_ -> CalFilter
                                , HP.title   $ plainify $ translate (sid "calendar") "filter"
                                , HCSS.style $ easyShadow (voidest colors) 1.0
                                ]
                                [ HH.text "filter_alt" ]
                            , HH.span
                                [ HP.classes iconClasses
                                , HE.onClick \_ -> CalMonthPred
                                , HP.title   $ plainify $ translate (sid "calendar") "month-last"
                                , HCSS.style $ easyShadow (voidest colors) 1.0
                                ]
                                [ HH.text "keyboard_arrow_up" ]
                            , HH.span
                                [ HP.classes iconClasses
                                , HE.onClick \_ -> CalToday
                                , HP.title   $ plainify $ translate (sid "calendar") "today"
                                , HCSS.style $ easyShadow (voidest colors) 1.0
                                ]
                                [ HH.text "today" ]
                            , HH.span
                                [ HP.classes iconClasses
                                , HE.onClick \_ -> CalMonthSucc
                                , HP.title   $ plainify $ translate (sid "calendar") "month-next"
                                ]
                                [ HH.text "keyboard_arrow_down" ]
                            ]
                        ]
                    , HH.slot _calendarTable 0 calendarTable { calStartDate, holidays, month, curDate } CalChangeDate
                    , dateSummary st month holidays curDate
                    ]
                ]
  , eval: H.mkEval $ H.defaultEval
      { initialize   = Just CalInit
      , handleAction = case _ of
          CalInit -> do
            { curDate } <- H.get
            month <- Registry.lookup Registries.month $ monthID (month (getDate curDate))
            holidays <- mkSchedule <$> Registry.lookupAll Registries.holiday
            H.modify_ _ { mMonth = month, mHolidays = Just holidays }
          CalChangeDate newDate -> do
            H.modify_ _ { curDate = newDate }
          CalToday -> do
            { currentTime } <- H.get
            month <- Registry.lookup Registries.month $ monthID (month (getDate currentTime))
            H.modify_ _
              { mMonth       = month
              , calStartDate = calendarStart currentTime
              , curDate      = getDate currentTime
              , firstDay     = firstDayOfMonth currentTime
              }
          CalMonthPred -> do
            { firstDay, mMonth } <- H.get
            for_ mMonth \month -> do
              newMonth <- Registry.lookup Registries.month $ monthID (fromMaybe Date.December $ pred month.enum)
              let newFirstDay = firstDayOfMonth $ adjustDate (Days $ -1.0) firstDay
              H.modify_ _
                { calStartDate = calendarStart newFirstDay
                , firstDay     = newFirstDay
                , mMonth       = newMonth
                }
          CalMonthSucc -> do
            { firstDay, mMonth } <- H.get
            for_ mMonth \month -> do
              newMonth <- Registry.lookup Registries.month $ monthID (fromMaybe Date.January $ succ month.enum)
              let newFirstDay = firstDayOfMonth $ adjustDate (Days $ 32.0) firstDay
              H.modify_ _
                { calStartDate = calendarStart newFirstDay
                , firstDay     = newFirstDay
                , mMonth       = newMonth
                }
          CalFilter -> do
            DS.log Warning "Filter functionality is not yet implemented"
      }
  }

-- Sub Components --

type CalendarTableInput r = { calStartDate :: Date, holidays :: HolidaySchedule, month :: {| Identified + Month + r }, curDate :: Date }

data CalendarTableAction r = CTChange Date | CTReceive (Connected DS.Store (CalendarTableInput r))

_calendarTable = Proxy :: Proxy "calendarTable"

calendarTable :: forall q m r. MonadStore DS.Action DS.Store m => H.Component q (CalendarTableInput r) Date m
calendarTable = connect selectNone $ H.mkComponent
  { initialState: \{ context: ctx@(DS.Store { colors, currentTime }), input: { calStartDate, holidays, month, curDate } } ->
      { calStartDate, colors, curDate, holidays, month
      , todaysDate:    getDate currentTime
      , highlightDate: curDate
      , translate:     mkTranslate ctx
      }
  , render: \{ calStartDate, colors, holidays, highlightDate, month, todaysDate, translate } ->
      let theadCSS = do
            Typography.lori
            CSS.color $ month.theme.text colors
          mkBadgeText date =
            let iconTooltip id = tooltip (translate id "symbol") (plainify $ translate id "name")
                phaseIcon = case moonPhase date of
                  p | p == NewMoon
                   || p == FullMoon -> iconTooltip (phaseID p) 
                  _                 -> default
                addHoliday acc holiday@{ id }
                  | isVisible holiday = acc & iconTooltip id
                  | otherwise         = acc
             in phaseIcon & (foldl addHoliday default (holidaysOn date holidays))
          mkCell shift =
            let date = adjustDate (Days $ toNumber shift) calStartDate
             in HH.td
                  [ HE.onClick \_ -> CTChange date
                  , HCSS.style do
                      UCSS.borderNone
                      UCSS.userSelectNone
                      CSS.alignItems center 
                      CSS.borderSpacing $ unitless 0.0
                      when (getDate date == todaysDate) do
                        CSS.border CSS.dashed (CSS.px 2.0) (month.theme.highlight colors)
                      CSS.backgroundColor $ case unit of
                        _ | date == highlightDate       -> month.theme.highlight colors
                        _ | getMonth date /= month.enum -> voider colors
                        _ | odd shift                   -> withAlpha 0.055 $ month.theme.highlight colors
                        _                               -> transparent colors
                      CSS.padding (rem 0.8) (rem 0.8) (rem 0.8) (rem 0.8)
                      verticalAlign middle
                  ]
                  [ HH.p
                      [ HCSS.style do
                          CSST.textAlign CSST.center
                          CSS.width $ CSS.pct 100.0
                      ]
                      [ badge_
                          ( richText_ colors Typography.calendarBadge (mkBadgeText date) )
                          [ HH.text (show $ fromEnum $ getDay date) ]
                      ]
                  ]
          mkRow idx = HH.tr [ HCSS.style UCSS.borderNone] 
            (0..6 <#> \s -> mkCell $ idx * 7 + s)
          mkHeader day = HH.th [ HCSS.style theadCSS ] [ richText_ colors Typography.calendarHeader $ translate (sid "day-of-week-abbr") day ]
       in HH.table
            [ HCSS.style do
                UCSS.borderCollapse UCSS.Collapse
                CSS.width $ CSS.pct 100.0
            ]
            [ HH.thead_ (mkHeader <$> ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"])
            , HH.tbody_ (mkRow <$> 0..5)
            ]
  , eval: H.mkEval $ H.defaultEval
      { handleAction = case _ of
          CTChange dt ->
            H.raise dt
          CTReceive { input: { calStartDate, curDate, month } } ->
            H.modify_ _ { calStartDate = calStartDate, highlightDate = curDate, month = month }
      , receive = Just <<< CTReceive
      }
  }

type DateSummaryInput r =
  ( colors        :: Colors
  , curDate       :: Date
  , translate     :: Translate
  , translateWith :: TranslateWith
  | r
  )

dateSummary :: forall w i r1 r2. {| DateSummaryInput + r1 } -> {| Identified + Month + r2 } -> HolidaySchedule -> Date -> HH.HTML w i
dateSummary { colors, translate, translateWith } month holidaySchedule sumDate =
  let phase    = phaseID $ moonPhase sumDate
      holidays = Array.filter isVisible $ holidaysOn sumDate holidaySchedule
      holidayLines = holidays <#> \holiday -> HH.div_
        [ richText_ colors Typography.calendarBody $ translate holiday.id "symbol"
        , richText_ colors Typography.calendarBody $ translateWith holiday.id "calendar" [PDate sumDate]
        ]
      sumDT = atMidnight sumDate
   in HH.div
        [ HCSS.style $ easyShadow (voidest colors) 1.0
        ]
        [ HH.div_
            ( [ richText_ colors (Typography.theNews *> CSS.fontSize (CSS.rem 1.3) *> CSS.color (month.theme.text colors)) $
                  translateWith (sid "calendar") "date-title" (pure $ PDateTime sumDT)
              , HH.div
                  [ HCSS.style do
                      CSS.float CSS.FloatRight
                  ]
                  [ richText_ colors (Typography.calendarHeader *> CSS.fontSize (CSS.rem 0.9)) $ translateWith phase "name"   (pure $ PDateTime sumDT)
                  , richText_ colors Typography.calendarHeader                                 $ translateWith phase "symbol" (pure $ PDateTime sumDT)
                  ]
              , HH.div_
                  [ richText_ colors Typography.calendarBody $ translateWith (sid "calendar") "weather-forecast" [PDate sumDate] ]
              ] <> holidayLines
            )
        ]

-- Utils --

calendarStart :: forall d. HasDate d => d -> Date
calendarStart d = adjustDate (Days $ -dayOffset) fdom
  where dayOffset = toNumber (fromEnum (weekday $ getDate fdom) - 1)
        fdom      = firstDayOfMonth d