module Flauros.Component.Util.Floating where

import Prelude

import CSS (Color, px, vh, vw)
import CSS as CSS
import DOM.HTML.Indexed (Interactive, HTMLdiv)
import Data.Array (mapWithIndex)
import Data.Either (hush)
import Data.Maybe (fromMaybe)
import Data.Newtype (unwrap)
import Data.Traversable (for, for_)
import Effect.Class (class MonadEffect, liftEffect)
import Flauros.Component.Util (whenElem)
import Flauros.Data.Store (selectKey, setUIState)
import Flauros.Data.Store as DS
import Flauros.Data.System.JSON (class FromJSON, class ToJSON, fromJSON, toJSON)
import Flauros.Style.Color (defaultColors, transparent)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.CSS as HCSS
import Halogen.HTML.Events (onScroll)
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties (IProp(..))
import Halogen.HTML.Properties as HP
import Halogen.Query.HalogenM as HQ
import Halogen.Store.Connect (connect)
import Halogen.Store.Monad (class MonadStore, updateStore)
import Record as Record
import Type.Prelude (Proxy(..))
import Web.DOM.Element (getBoundingClientRect)
import Web.DOM.Element as Elem
import Web.DOM.Node as Node
import Web.Event.Event (Event)

type FloatingProps = Interactive (clickOutsideClose :: Boolean, onScroll :: Event)

data FloatingAction = Open | Close

dialog_ = Proxy :: Proxy "dialog"

type FloatingInput =
  { dimColor :: Color
  }

defInput :: FloatingInput
defInput =
  { dimColor: transparent defaultColors
  }

floating :: forall q o m s. MonadEffect m => Array (HH.IProp HTMLdiv FloatingAction) -> Array (HH.HTML (H.ComponentSlot s m FloatingAction) FloatingAction) -> H.Component q FloatingInput o m
floating props elems = H.mkComponent
  { initialState: \{ dimColor } ->
      { dimColor
      , isOpen: false
      , x: 0.0, y: 0.0
      }
  , render: \{ dimColor, isOpen, x, y } ->
      whenElem isOpen \_ -> do
        HH.div
          [ HP.ref $ H.RefLabel "dialogRoot" ]
          [ HH.div
              [ HE.onClick \_ -> Close
              , HCSS.style do
                  CSS.zIndex 10000
                  CSS.position CSS.absolute
                  CSS.width (vw 100.0)
                  CSS.height (vh 100.0)
                  CSS.backgroundColor dimColor
              ]
              []
          , HH.div
              [ HCSS.style do
                  CSS.zIndex 10001
                  CSS.position CSS.absolute
                  CSS.left (px x)
                  CSS.top  (px y)
              ]
              [ HH.div props elems ]
          ]
  , eval: H.mkEval $ H.defaultEval
      { handleAction = case _ of
          Open  -> do
            mNode <- HQ.getRef (H.RefLabel "dialogRoot") <#> map Elem.toNode
            for_ mNode \node -> do
              mParent <- liftEffect $ Node.parentElement node
              for_ mParent \parent -> do
                rect <- liftEffect $ getBoundingClientRect parent
                H.modify_ _
                  { isOpen = true
                  , x      = rect.left
                  , y      = rect.bottom
                  }
          Close -> do
            H.modify_ _ { isOpen = false }
      }
  }