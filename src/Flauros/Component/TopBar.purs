module Flauros.Component.TopBar where

import Prelude

import CSS as CSS
import Data.Maybe (Maybe(..))
import Flauros.Component.Util (StoreUpdateAction(..), maybeElem, richText_)
import Flauros.Data.Month (monthID, timeOfDayColor)
import Flauros.Data.Store (selectAllWhen)
import Flauros.Data.Store as DS
import Flauros.Data.Time (getMonth, getTime)
import Flauros.Lang (LineParam(..))
import Flauros.Prefab.Core.Line.Environment (cityInfo)
import Flauros.Prefab.Registries as Registries
import Flauros.Prefab.Registry as Registry
import Flauros.Prelude (sid)
import Flauros.Style.Typography as Typography
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.CSS as HCSS
import Halogen.Store.Connect (connect)
import Halogen.Store.Monad (class MonadStore)
import Halogen.Store.Select (Selector)
import Type.Prelude (Proxy(..))

_topBar = Proxy :: Proxy "topBar"

selector :: Selector DS.Store DS.Store
selector = selectAllWhen \(DS.Store a) (DS.Store b) ->
  getMonth a.currentTime == getMonth b.currentTime && a.colors == b.colors && a.weather == b.weather

topBar :: forall q i o m. MonadStore DS.Action DS.Store m => H.Component q i o m
topBar = connect selector $ H.mkComponent
  { initialState: deriveState
  , render: \{ currentTime, colors, mMonth, mWeather, temperature, translate, translateWith } ->
      maybeElem mMonth $ \month ->
        maybeElem mWeather $ \weather ->
          HH.div_
            [ let css = do
                    Typography.cityName
                    CSS.color $ timeOfDayColor month (getTime currentTime) colors
               in richText_ colors css $ translate cityInfo.id "name"
            , HH.p
                [ HCSS.style do
                    Typography.weatherReport
                    CSS.color $ weather.colors.text colors
                ]
                [ richText_ colors Typography.weatherReport $ translate weather.id "icon"
                , richText_ colors Typography.weatherReport $ translate weather.id "long"
                , HH.text $ ", "
                , richText_ colors Typography.weatherReport $ translateWith (sid "temperature") "tagline" [ PInt temperature ]
                , HH.text $ ". "
                , richText_ colors Typography.weatherReport $ translate weather.id "tagline"
                ]
            ]
  , eval: H.mkEval $ H.defaultEval
      { initialize   = Just Init
      , receive      = Just <<< Receive
      , handleAction = \action -> do
          st@{ currentTime, weatherID } <- case action of
            Init        -> H.get
            Receive ctx -> pure $ deriveState ctx
          H.put =<< st { mMonth = _, mWeather = _}
                <$> Registry.lookup Registries.month (monthID (getMonth currentTime))
                <*> Registry.lookup Registries.weather weatherID
      }
  }
  where deriveState { context: store@(DS.Store { colors, currentTime, temperature, weather }) } =
          { weatherID: weather
          , currentTime
          , colors
          , mMonth: Nothing
          , mWeather: Nothing
          , temperature
          , translate: DS.mkTranslate store
          , translateWith: DS.mkTranslateWith store
          }
