module Flauros.Component.Badge where

import Prelude

import CSS (rem)
import CSS as CSS
import CSS.Common (middle)
import CSS.Size (unitless)
import CSS.VerticalAlign (verticalAlign)
import Flauros.Component.UCSS as UCSS
import Halogen.HTML as HH
import Halogen.HTML.CSS as HCSS

-- data BadgeY = Top | Middle | Bottom

-- data BadgeX = Left | Center | Right

-- type BadgeInput =
--   { pos :: { x :: BadgeX, y :: BadgeY }
--   }

-- defBadgeInput :: BadgeInput
-- defBadgeInput =
--   { pos: { x: Right, y: Top }
--   }

-- badge_ :: forall w i. BadgeInput -> HH.HTML w i -> Array (HH.HTML w i) -> HH.HTML w i
-- badge_ { pos: { x, y } } label inner =
badge_ :: forall w i. HH.HTML w i -> Array (HH.HTML w i) -> HH.HTML w i
badge_ label inner =
  HH.div_
    [ HH.div
        [ HCSS.style do
            CSS.display CSS.inlineFlex
            CSS.lineHeight (unitless 0.0)
            CSS.padding (rem 0.0) (rem 0.0) (rem 0.0) (rem 0.5)
            CSS.position CSS.absolute
            CSS.zIndex 1
        ]
        [ label ]
    , HH.div_ inner
    ]