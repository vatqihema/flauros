module Flauros.Startup where

import Prelude

import Data.HashMap as HM
import Data.Int (toNumber)
import Data.Map as Map
import Data.Maybe (Maybe(..))
import Data.Traversable (for_)
import Effect.Class (class MonadEffect, liftEffect)
import Effect.Class.Console as Console
import Flauros.Data.Actor (playerID)
import Flauros.Data.Gen as Gen
import Flauros.Data.Knowledge (KnowVal(..), worldID)
import Flauros.Data.Month (monthOf)
import Flauros.Data.Store as DS
import Flauros.Data.System.Browser (localStorageSize)
import Flauros.Data.Time (getDate, getDateTime)
import Flauros.Log (LogType(..))
import Flauros.Mechanics.Fact as Fact
import Flauros.Mechanics.Player (randomLicense)
import Flauros.Mechanics.Weather (genDefHistory)
import Flauros.Prefab.Core as Core
import Flauros.Prefab.Core.Country (althesia)
import Flauros.Prefab.Temp as Temp
import Flauros.Prefab.Essential as Essential
import Flauros.Prefab.Registries as Registries
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry as Registry
import Flauros.Prelude (default, sid)
import Flauros.Random (jsRandom, setCurrentSeed)
import Halogen.Store.Monad (class MonadStore, getStore, updateStore)
import Random.LCG (lcgM)

initNewGame :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
initNewGame = do
  store <- getStore

  liftEffect do
    seed <- jsRandom <#> (_ * toNumber lcgM)
    Console.log $ show seed
    setCurrentSeed seed

  let DS.Store { currentTime } = store
  months   <- Registry.lookupAll Registries.month
  weathers <- Registry.lookupAll Registries.weather
  let todayWeather = { weather: sid "clear", chance: 100, temperature: 2 }
  history  <- genDefHistory store (monthOf months) weathers
          <#> Map.insert (getDate currentTime) todayWeather
  updateStore \(DS.Store s) -> DS.Store $ s
    { weatherHistory = history
    , weather     = todayWeather.weather
    , temperature = todayWeather.temperature
    }
  -- testing
  localStorageSize >>= \size ->
    when (size == 0) do
      initFacts
      Registry.lookup Regs.actor (sid "farris-c'amri") >>= case _ of
        Just player -> updateStore $ DS.action \st ->
          st { actors = HM.insert playerID player { id = playerID } st.actors }
        Nothing     -> DS.log Warning "The default test player was not found. Using fallback."
      pure unit
  

initFacts :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
initFacts = do
  store <- getStore
  let DS.Store { currentTime } = store
      dt = getDateTime currentTime
      playerFact n what = Fact.newM playerID n $ { who: playerID, what, whenFirst: dt, whenLast: dt }
      worldFact  n what = Fact.newM worldID  n $ { who: worldID, what, whenFirst: dt, whenLast: dt }
  -- world facts
  worldFact (sid "city-name") $ KnowLine { id: sid "city-name", key: "default-name", params: [] }
  -- player facts
  licenseNum <- randomLicense
  mAddressGen <- Registry.lookup Regs.label (sid "street-name")
  for_ mAddressGen \gen -> do
    address <- Gen.quick store unit gen
    playerFact (sid "address") $ KnowLine { id: gen.id, key: "label", params: address.labelParams }
  playerFact (sid "city")        $ KnowStr "Palecaide"
  playerFact (sid "nationality") $ KnowID althesia.id
  playerFact (sid "license")     $ KnowStr licenseNum

startup :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
startup = do
  Essential.init
  Core.init
  Temp.init
  initNewGame -- TODO stop doing this all the time