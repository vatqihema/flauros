module Flauros.Mechanics.Event where

import Prelude

import Effect.Class (class MonadEffect)
import Flauros.Data.Store as DS
import Halogen.Store.Monad (class MonadStore)

tickStartOfDayEventMods :: forall m. MonadEffect m => MonadStore DS.Store DS.Action m => m Unit
tickStartOfDayEventMods = do
  pure unit