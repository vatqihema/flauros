module Flauros.Mechanics.Fact where
-- ^ Meant to be imported qualified

import Prelude

import Data.HashMap as HM
import Data.Maybe (Maybe(..), fromMaybe)
import Flauros.Data.Knowledge (Fact, KnowVal(..))
import Flauros.Data.Store (Action)
import Flauros.Data.Store as DS
import Flauros.Data.Time (getDateTime)
import Flauros.Lang.Text (Text, plain)
import Flauros.Prelude (Identifier, insertIn, prettyShowID, transmute, updateIn)
import Halogen.Store.Monad (class MonadStore, getStore, updateStore)

clearLocal :: Identifier -> Action
clearLocal sc = DS.action \st -> st { facts = HM.delete sc st.facts }

clearLocalM :: forall m. MonadStore DS.Action DS.Store m => Identifier -> m Unit
clearLocalM sc = updateStore $ clearLocal sc

asText :: DS.Store -> KnowVal -> Text
asText store = case _ of
  KnowBool b -> if b then plain "true" else plain "false"
  KnowID id  -> plain $ prettyShowID id
  KnowInt i  -> plain $ show i
  KnowLine l -> DS.mkTranslateWith store l.id l.key (map transmute l.params)
  KnowNull   -> plain ""
  KnowNum n  -> plain $ show n
  KnowStr s  -> plain s

text :: Identifier -> Identifier -> DS.Store -> Text
text sc k st = asText st $ val sc k st

textM :: forall m. MonadStore DS.Action DS.Store m => Identifier -> Identifier -> m Text
textM sc k = do
  store <- getStore
  asText store <$> valM sc k

lookup :: Identifier -> Identifier -> DS.Store -> Maybe Fact
lookup sc key (DS.Store { facts }) = HM.lookup sc facts >>= HM.lookup key

lookupM :: forall m. MonadStore DS.Action DS.Store m => Identifier -> Identifier -> m (Maybe Fact)
lookupM sc key = getStore <#> lookup sc key  

new :: forall r. Identifier -> Identifier -> { who :: Identifier, what :: KnowVal | r } -> Action
new sc key { who, what } = DS.action \st -> st
  { facts = flip (insertIn sc key) st.facts
      { who, what
      , whenFirst: getDateTime st.currentTime
      , whenLast:  getDateTime st.currentTime
      }
  }

newM :: forall m r. MonadStore DS.Action DS.Store m => Identifier -> Identifier -> { who :: Identifier, what :: KnowVal | r } -> m Unit
newM sc key fact = updateStore $ new sc key fact

update :: Identifier -> Identifier -> (KnowVal -> KnowVal) -> Action
update sc key fn = DS.action \st -> st
  { facts = (\f -> updateIn f sc key st.facts) \fact -> Just fact
      { what     = fn fact.what
      , whenLast = getDateTime st.currentTime
      }
  }

updateM :: forall m. MonadStore DS.Action DS.Store m => Identifier -> Identifier -> (KnowVal -> KnowVal) -> m Unit
updateM sc key f = updateStore $ update sc key f

val :: Identifier -> Identifier -> DS.Store -> KnowVal
val sc key (DS.Store { facts }) = fromMaybe KnowNull (HM.lookup sc facts >>= HM.lookup key <#> _.what)

valM :: forall m. MonadStore DS.Action DS.Store m => Identifier -> Identifier -> m KnowVal
valM sc key = getStore <#> val sc key