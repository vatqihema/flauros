module Flauros.Mechanics.Player where

import Prelude

import Data.Array ((..))
import Data.Array as Array
import Data.Enum (fromEnum)
import Data.FoldableWithIndex (foldlWithIndex)
import Data.Int64 as Int64
import Data.Maybe (fromMaybe)
import Data.String as String
import Data.String.CodeUnits (fromCharArray)
import Data.Traversable (traverse)
import Effect.Class (class MonadEffect, liftEffect)
import Effect.Now (nowDateTime)
import Flauros.Data.Time (dateHash, getHour, getMinute)
import Flauros.Prelude ((|>))
import Flauros.Random (randomNth)

validIDChars :: Array Char
validIDChars = ['2', '3', '4', '5', '6', '7', '8', '9', 'Ð', 'F', 'G', 'H', 'J', 'M', 'Þ', 'Q', 'R', 'V', 'W', 'X']

randomLicense :: forall m. MonadEffect m => m String
randomLicense = liftEffect do
  time <- nowDateTime
  let seconds      = fromEnum (getHour time) * 60 + fromEnum (getMinute time)
      secondsInDay = 24 * 60 * 60
      dtHash       = dateHash time
      timeLong     = Int64.fromInt dtHash * Int64.fromInt secondsInDay + Int64.fromInt seconds
      timeStr      = Int64.toString timeLong
      dashOffset   = (String.length timeStr) `mod` 5
      timeDigits   = String.toCodePointArray timeStr
                  |> foldlWithIndex (\idx acc c -> if   idx /= 0 && idx `mod` 5 == dashOffset
                                                   then acc <> [String.codePointFromChar '-'] <> [c]
                                                   else Array.snoc acc c) []
                  |> String.fromCodePointArray
  let randomChar = fromMaybe 'X' <$> randomNth validIDChars
  chars <- map fromCharArray $ traverse (\_ -> randomChar) (0..4)
  pure $ chars <> timeDigits