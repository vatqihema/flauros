module Flauros.Mechanics.Weather where

import Prelude

import Data.Array (catMaybes, foldM, (..))
import Data.Array as Array
import Data.Date (Date, diff, lastDayOfMonth)
import Data.Enum (fromEnum)
import Data.Foldable (foldr)
import Data.HashMap as HM
import Data.Int (floor, toNumber)
import Data.List (find)
import Data.Map as Map
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Time.Duration (Days(..))
import Data.TraversableWithIndex (traverseWithIndex)
import Effect.Class (class MonadEffect)
import Flauros.Data.Month (Month)
import Flauros.Data.Time (class HasDate, CurrentTime, adjustDate, getDate, getDay, getMonth, getYear, isReoccurance, toReoccurance)
import Flauros.Data.Weather (Weather, WeatherHistory, WeatherHistoryRow, startOfFall, startOfSpring, startOfSummer, startOfWinter, weatherOn)
import Flauros.Prelude (IDMap, default, sid)
import Flauros.Random (random, randomNth, randomRange, randomWeighted)
import Type.Row (type (+))

genPrediction :: forall rm rw d store m. HasDate d => MonadEffect m
  => store
  -> (Date -> {| Month + rm })
  -> IDMap {| Weather store + rw }
  -> WeatherHistory
  -> d
  -> m WeatherHistoryRow
genPrediction store monthOf weathers weatherHistory hasDate =
  let date          = getDate hasDate
      getAt days = do
        history <- Map.lookup (adjustDate (Days days) date) weatherHistory
        weather <- HM.lookup history.weather weathers
        pure { days, history, weather }
      last7Recs = catMaybes $ map (getAt <<< toNumber) (-7 .. -1)
      alterChance { days, history, weather } = flip HM.alter history.weather $ Just
                                           <<< (_ * (-1.0 / days))
                                           <<< (_ * weather.contagion)
                                           <<< fromMaybe 1.0
      chances :: IDMap Number
      chances   = foldr alterChance HM.empty last7Recs
      month     = monthOf date
      yesterday = weatherOn (adjustDate (Days $ -1.0) date) weatherHistory

      tempFor key = case toReoccurance date of
        r | r == startOfSpring
         || r == startOfSummer -> pure month.temp.max -- ensure that on season transition, it will feel the most like that season for the given weather/month
        r | r == startOfFall
         || r == startOfWinter -> pure month.temp.min
        _ -> case HM.lookup key weathers of
          Nothing      -> randomRange month.temp.min month.temp.max
          Just weather -> map (\t -> t + weather.tempMod store date t) $ case yesterday of
            Nothing              -> randomRange month.temp.min month.temp.max
            Just { temperature } ->
              let cap    = (_ + temperature) >>> max month.temp.min >>> min month.temp.max
                  change flip = if flip then -1 else 1
              in randomRange 0.0 1.0 >>= case _ of
                  r | r <= month.tempChangeRate ->
                    let dayOfMonthRatio = toNumber (fromEnum (getDay date)) / toNumber (fromEnum (lastDayOfMonth (getYear date) (getMonth date)))
                        tempSignRatio   = month.tempCurve dayOfMonthRatio
                    in (cap <<< change <<< (_ <= tempSignRatio)) <$> (randomRange 0.0 1.0)
                  _ -> pure temperature
   in do
        rolls <- HM.values <$> traverseWithIndex (\key chance -> random <#> \roll -> { roll, chance, key }) chances
        case find (\{ roll, chance } -> roll <= chance) rolls of
          Just { roll, chance, key } -> do
            temperature <- tempFor key
            pure { weather: key, temperature, chance: floor (max 45.0 (min 96.0 (chance * (1.0 - roll) * 150.0))) } -- should add faker if another had higher chance?
          Nothing -> case getAt (-1.0) <#> _.weather <#> _.followedBy of
            Just weathersArr | not (Array.null weathersArr) -> do
              chance      <- randomRange 50 90
              weather     <- fromMaybe (sid "clear") <$> randomNth weathersArr 
              temperature <- tempFor weather
              pure { weather, temperature, chance }
            _ -> 
              let Days daysSinceStart = getDate (default :: CurrentTime) `diff` date
                  isDelayOver k = HM.lookup k weathers >>= _.delay <#> (_ <= floor daysSinceStart)
                  filtered = HM.filterKeys (isDelayOver >>> fromMaybe true) month.weather
              in randomWeighted filtered >>= case _ of
                  Just key -> do
                    chance      <- randomRange 30 75
                    temperature <- tempFor key
                    pure { weather: key, temperature, chance }
                  Nothing -> pure
                    { weather: sid "clear", temperature: 0, chance: 999 } -- fall-back, should never happen

genDefHistory :: forall rm rw store m. MonadEffect m
  => store
  -> (Date -> {| Month + rm })
  -> IDMap {| Weather store + rw }
  -> m WeatherHistory
genDefHistory store monthOf weathers =
  let start = getDate (default :: CurrentTime)
      foldF history shift = do
        let date = adjustDate (Days $ toNumber shift) start
        prediction <- genPrediction store monthOf weathers history date
        pure $ Map.insert (date) prediction history
   in foldM foldF Map.empty (-10 .. 10)