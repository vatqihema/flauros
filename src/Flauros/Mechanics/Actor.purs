module Flauros.Mechanics.Actor where

import Prelude

import Data.HashMap as HM
import Data.Maybe (Maybe(..))
import Effect.Class (class MonadEffect)
import Flauros.Data.Actor (Actor)
import Flauros.Data.Stat (StatState, defStatState, modifyBase, orDefStat)
import Flauros.Data.Store as DS
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry as Registry
import Flauros.Prelude (Identifier)
import Halogen.Store.Monad (class MonadStore)

preprocess :: forall m r. MonadEffect m => MonadStore DS.Action DS.Store m => {| Actor r } -> m {| Actor r }
preprocess actor = do
  
  pure actor

----------
-- Stat --
----------

addStatM :: forall m r. MonadEffect m => Identifier -> Number -> {| Actor r } -> m {| Actor r }
addStatM id n = modStatM id (modifyBase (_ + n))

modStatM :: forall m r. MonadEffect m => Identifier -> (StatState -> StatState) -> {| Actor r } -> m {| Actor r }
modStatM id f actor = do
  stat <- case HM.lookup id actor.stats of
    Just s  -> pure s
    Nothing -> Registry.lookup Regs.stat id <#> orDefStat >>> defStatState
  pure actor { stats = HM.insert id (f stat) actor.stats }