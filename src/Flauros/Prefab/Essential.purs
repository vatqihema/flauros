module Flauros.Prefab.Essential where

import Prelude

import Data.Date (Month(..))
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Holiday (Holiday, Priority(..), allDay, withDefHoliday)
import Flauros.Data.Item (Item, little, withDefItem)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Data.Weather (withDefWeather)
import Flauros.Lang (asLines, english, esperanto, line)
import Flauros.Lang.Text (color, (&))
import Flauros.Prefab.Registries (Weather')
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register, (++))
import Flauros.Prelude (Identified, MyID(..), asHashMap, asMap, defaultID, prettyShowID, withDefTags)
import Flauros.Style.Color (red)
import Halogen.Store.Monad (class MonadStore)
import Prim.Row (class Nub)
import Record as Record
import Type.Row (type (+))

-- default and load-bearing records
-- game should (ideally) be able to run even if core is disabled as long as it can rely on these
-- in practice, they more serve as a placeholder for when things go missing
-- i.e. item id 

-- things outside of this still required
-- Core.Month.*
-- Core.Moon.*
-- Core.Weather.clear

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.ancestry defaultAncestry
  register Regs.holiday  defaultHoliday
  register Regs.item     defaultItem
  register Regs.weather  defaultWeather
--   register (Regs.ancestry ++ Regs.holiday ++ Regs.item ++ Regs.weather) defaultRecord

-- -- defaultRecord :: forall r. Nub (Identified + LangInfo' + Item + Holiday + Weather' + ()) r => {| r }
-- defaultRecord = Record.nub $ withDefItem $ withDefHoliday $ withDefWeather
--   { id:       defaultID "all"
--   , colors:   { text: red }
--   , priority: Hidden
--   , span:     asMap [ { month: April, day: 15 } /\ allDay ]
--   , price:    1.0
--   , size:     little
--   , lines: asLines
--       [ english /\
--           [ "long"    /\ line \(MyID id) -> color red & "missing " & prettyShowID id & " long"
--           , "short"   /\ line \(MyID id) -> color red & "missing " & prettyShowID id & " short"
--           , "icon"    /\ line "⁉️"
--           , "tagline" /\ line \(MyID id) -> color red & "missing " & prettyShowID id & " tagline"
--           ]
--       , esperanto /\
--           [ "long"    /\ line \(MyID id) -> color red & "ne havas " & prettyShowID id & " longan"
--           , "short"   /\ line \(MyID id) -> color red & "ne havas " & prettyShowID id & " mallongan"
--           , "tagline" /\ line \(MyID id) -> color red & "ne havas " & prettyShowID id & " titolon"
--           ]
--       ]
--   }

defaultAncestry :: {| Identified + LangInfo' + () }
defaultAncestry = withDefTags
  { id:        defaultID "ancestry"
  , lines:     asLines
      [ english /\
          [ "name"   /\ line \(MyID id) -> color red & "missing " & prettyShowID id & " name"
          , "adj"    /\ line \(MyID id) -> color red & "missing " & prettyShowID id & " adj"
          ]
      , esperanto /\
          [ "name"   /\ line \(MyID id) -> color red & "ne havas " & prettyShowID id & " nomon"
          , "adj"    /\ line \(MyID id) -> color red & "ne havas " & prettyShowID id & " adj"
          ]
      ]
  }

defaultItem :: {| Identified + LangInfo' + Item + () }
defaultItem = withDefItem
  { id:        defaultID "item"
  , price:     1.0
  , size:      little
  , lines:     asLines
      [ english /\
          [ "long"   /\ line \(MyID id) -> color red & "missing " & prettyShowID id & " name"
          , "short"  /\ line \(MyID id) -> color red & "missing " & prettyShowID id & " name"
          , "desc"   /\ line \(MyID id) -> color red & "missing " & prettyShowID id & " desc"
          , "symbol" /\ line "⁉️"
          ]
      , esperanto /\
          [ "long"   /\ line \(MyID id) -> color red & "ne havas " & prettyShowID id & " nomon"
          , "short"  /\ line \(MyID id) -> color red & "ne havas " & prettyShowID id & " nomon"
          , "desc"   /\ line \(MyID id) -> color red & "ne havas " & prettyShowID id & " priskribon"
          ]
      ]
  }

defaultHoliday :: {| Identified + Holiday + LangInfo' + () }
defaultHoliday = withDefHoliday
  { id:        defaultID "holiday"
  , priority:  Hidden
  , span:      asMap [ { month: April, day: 15 } /\ allDay ]
  , eventMods: asHashMap []
  , lines:     asLines
      [ english /\
          [ "name"     /\ line \(MyID id) -> color red & "missing " & prettyShowID id & " name"
          , "symbol"   /\ line  "⁉️"
          , "calendar" /\ line \(MyID id) -> color red & "missing " & prettyShowID id & " calendar desc"
          ]
      ,  esperanto /\
          [ "name"     /\ line \(MyID id) -> color red & "ne havas " & prettyShowID id & " nomon"
          , "calendar" /\ line \(MyID id) -> color red & "ne havas " & prettyShowID id & " kalendarskribon"
          ]
      ]
  }

defaultWeather :: {| Identified + LangInfo' + Weather' + () }
defaultWeather = withDefWeather
  { id:        defaultID "weather"
  , colors:    { text: red }
  , lines: asLines
      [ english /\
          [ "long"    /\ line \(MyID id) -> color red & "missing " & prettyShowID id & " long"
          , "short"   /\ line \(MyID id) -> color red & "missing " & prettyShowID id & " short"
          , "icon"    /\ line "⁉️"
          , "tagline" /\ line \(MyID id) -> color red & "missing " & prettyShowID id & " tagline"
          ]
      , esperanto /\
          [ "long"    /\ line \(MyID id) -> color red & "ne havas " & prettyShowID id & " longan"
          , "short"   /\ line \(MyID id) -> color red & "ne havas " & prettyShowID id & " mallongan"
          , "tagline" /\ line \(MyID id) -> color red & "ne havas " & prettyShowID id & " titolon"
          ]
      ]
  }