module Flauros.Prefab.Registry where

import Prelude

import Data.Array as Array
import Data.Foldable (for_)
import Data.Generic.Rep (class Generic)
import Data.HashMap (HashMap)
import Data.HashMap as HM
import Data.HashSet (HashSet)
import Data.HashSet as HS
import Data.Hashable (class Hashable, hash)
import Data.List (List(..), (:))
import Data.List as List
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Newtype (class Newtype, unwrap)
import Data.String as String
import Data.Symbol (class IsSymbol, reflectSymbol)
import Data.TraversableWithIndex (forWithIndex)
import Data.Tuple (Tuple)
import Data.Tuple.Nested ((/\))
import Effect (Effect)
import Effect.Class (class MonadEffect, liftEffect)
import Effect.Exception (throw)
import Flauros.Data.FormList (FormList(..), FormRow, IDFormList)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Data.System.JSON (class FromJSON, class ToJSON, fromJSON, toJSON)
import Flauros.Lang (setTranslation)
import Flauros.Prelude (class Transmute, IDMap, Identified, Identifier, asHashSet, isSubsetOf, prettyShowID, sid, transmute, (|>))
import Foreign (Foreign, isUndefined, unsafeFromForeign, unsafeToForeign)
import Halogen.Store.Monad (class MonadStore)
import Prim.Row (class Nub, class Union)
import Type.Prelude (Proxy(..))
import Type.Row (type (+))
import Type.RowList (class RowToList, Cons, Nil, RowList)

type ForeignRegKey = Foreign
foreign import mkEmptyKey :: Unit -> ForeignRegKey
foreign import pushKeyNode :: ForeignRegKey -> Foreign -> Effect Unit
foreign import compileRegistryKey :: RegistryKey -> Foreign
foreign import addToRegistry :: String -> String -> Foreign -> Effect Unit
foreign import lookupInRegistries :: Array String -> String -> Effect Foreign
foreign import lookupAllInRegistries :: Array String -> Effect (Array Foreign)
foreign import lookupInWhichRegistries :: String -> Effect (Array String)

data Registry (r :: Row (Row Type)) = Registry Foreign

mkRegistry :: forall xrow xs proxy. RowToList xrow xs => RowListKeys xs => proxy xrow -> Registry xrow
mkRegistry _ = Registry $ compileRegistryKey $ rowListKeys (Proxy :: Proxy xs)

data RegistryKey = RKLeaf String | RKBranch (List RegistryKey)
instance Semigroup RegistryKey where
  append (RKLeaf a) (RKLeaf b)     = RKBranch ((RKLeaf a) : (RKLeaf b) : Nil)
  append (RKLeaf a) (RKBranch b)   = RKBranch ((RKLeaf a) : b)
  append (RKBranch a) (RKLeaf b)   = RKBranch (List.snoc a (RKLeaf b))
  append (RKBranch a) (RKBranch b) = RKBranch (a <> b)

class RowListKeys :: forall k. RowList k -> Constraint
class RowListKeys xs where 
  rowListKeys :: forall proxy. proxy xs -> RegistryKey

instance nilRowListKeys :: RowListKeys Nil where
  rowListKeys _ = RKBranch mempty

instance consRowListKeysFormList
    :: (IsSymbol name, RowListKeys tail, RowListKeys ml, RowListKeys rl, RowToList meta ml, RowToList registry rl)
    => RowListKeys (Cons name (FormList meta registry) tail) where
  rowListKeys _ = (RKBranch (RKLeaf name : meta : regi : Nil)) <> keys
    where name = reflectSymbol (Proxy :: Proxy name)
          meta = rowListKeys (Proxy :: Proxy ml)
          regi = rowListKeys (Proxy :: Proxy rl)
          keys = rowListKeys (Proxy :: Proxy tail)

instance consRowListKeysP1
    :: (IsSymbol name, RowListKeys tail, RowToList a al, RowListKeys al)
    => RowListKeys (Cons name (typ a) tail) where
  rowListKeys _ = RKBranch (RKLeaf (reflectSymbol (Proxy :: Proxy name)) : rowListKeys (Proxy :: Proxy al) : Nil)
               <> rowListKeys (Proxy :: Proxy tail)

instance consRowListKeysP2
    :: (IsSymbol name, RowListKeys tail, RowToList a al, RowListKeys al, RowToList b bl, RowListKeys bl)
    => RowListKeys (Cons name (typ a b) tail) where
  rowListKeys _ = RKBranch (RKLeaf (reflectSymbol (Proxy :: Proxy name)) : rowListKeys (Proxy :: Proxy al) : rowListKeys (Proxy :: Proxy bl) : Nil)
               <> rowListKeys (Proxy :: Proxy tail)

instance consRowListKeysP0 :: (IsSymbol name, RowListKeys tail) => RowListKeys (Cons name typ tail) where
  rowListKeys _ = RKLeaf (reflectSymbol (Proxy :: Proxy name))
               <> rowListKeys (Proxy :: Proxy tail)

class RegistrySumType (xs :: RowList (Row Type)) (rec :: Row Type) | xs -> rec

instance nilRegistrySumType
  :: RegistrySumType Nil ()

instance consRegistrySumType
  :: ( RegistrySumType tail r2
     , Union r1 r2 r3
     , Nub r3 r4
     )
  => RegistrySumType (Cons name r1 tail) r4

newtype RegistryIDSet = RegistryIDSet (HashSet String)
derive instance eqRegistryIDSet      :: Eq RegistryIDSet
derive instance genericRegistryIDSet :: Generic RegistryIDSet _
derive instance newtypeRegistryIDSet :: Newtype RegistryIDSet _
derive newtype instance hashableRegistryIDSet :: Hashable RegistryIDSet
instance fromJSONRegistryIDSet   :: FromJSON RegistryIDSet   where fromJSON = fromJSON >>> map RegistryIDSet
instance toJSONRegistryIDSet     :: ToJSON RegistryIDSet     where toJSON   = unwrap >>> toJSON
instance showRegistryIDSet :: Show RegistryIDSet where
  show (RegistryIDSet keys) = "[" <> String.joinWith ", " (Array.fromFoldable keys) <> "]"

mkList :: forall r meta. Registry r -> (Array (FormRow meta)) -> FormList meta r
mkList _ arr = FormList arr

flattenKeyStrings :: RegistryKey -> Array String
flattenKeyStrings = go
  where go (RKLeaf s)    = [s]
        go (RKBranch ls) = Array.fromFoldable $ map sh ls
        sh = case _ of
          RKLeaf s    -> s
          RKBranch ls -> map sh ls |> Array.fromFoldable |> String.joinWith "🌈" |> ("🗿" <> _)

registryNames :: forall registry xrow xs. RowToList xrow xs => RowListKeys xs => registry xrow -> Array String
registryNames _ = Array.fromFoldable $ flattenKeyStrings $ rowListKeys (Proxy :: Proxy xs)

registryIDSet :: forall registry xrow xs. RowToList xrow xs => RowListKeys xs => registry xrow -> RegistryIDSet
registryIDSet = registryNames >>> HS.fromArray >>> RegistryIDSet

-- | Registers the given element for the given registry. Will override any existing with the same ID
register :: forall registry xrow xs r m. RowToList xrow xs => RegistrySumType xs r => RowListKeys xs => MonadEffect m => MonadStore DS.Action DS.Store m
         => registry xrow
         -> {| Identified + LangInfo' + r }
         -> m Unit
register reg obj = do
  let regNames = registryNames reg
  -- for each lang
  _ <- forWithIndex obj.lines \lang lines ->
    -- for each line
    forWithIndex lines \key v ->
      setTranslation lang obj.id key v
  liftEffect do
    for_ regNames \regName ->
      addToRegistry regName obj.id.base $ unsafeToForeign obj

-- | Registers the given element for the given registry. Will override any existing with the same ID
registerList :: forall registry xrow xs r m meta ml
              . RowToList xrow xs
             => RegistrySumType xs r
             => RowListKeys xs
             => MonadEffect m
             => MonadStore DS.Action DS.Store m
             => RowToList meta ml
             => RowListKeys ml
             => registry xrow
             -> IDFormList meta xrow
             -> m Unit
registerList reg obj = do
  let regNames = String.joinWith "🤔" $ registryNames reg
      meta     = String.joinWith "🤔" $ flattenKeyStrings $ rowListKeys (Proxy :: Proxy ml)
      regName  = "📃" <> regNames <> "🏁" <> meta
  liftEffect do
    addToRegistry regName obj.id.base $ unsafeToForeign obj

-- | Given a registry and an ID, returns an element that contains *at least* the fields expected by the registry if it exists
lookup :: forall registry xrow xs r m. MonadEffect m => RowToList xrow xs => RegistrySumType xs r => RowListKeys xs
       => registry xrow
       -> Identifier
       -> m (Maybe {| Identified r}) -- no LangInfo back, use `translate`` instead!
lookup reg id@{ base } = liftEffect do
  foreignObj <- lookupInRegistries (registryNames reg) base
  if   isUndefined foreignObj
  then pure Nothing
  else
    let baseObj  = unsafeFromForeign foreignObj
        fixIDObj = if baseObj.id /= id then baseObj { id = id } else baseObj
     in if HS.isEmpty id.mods
        then pure $ Just fixIDObj
        else do
          mods <- lookupModifiers id.mods
          HS.toArray id.mods
            |> map (\m -> HM.lookup (sid m) mods)
            |> Array.catMaybes
            |> Array.foldM (flip applyModifier) fixIDObj
            |> map Just

lookupAll :: forall registry xrow xs r m. MonadEffect m => RowToList xrow xs => RegistrySumType xs r => RowListKeys xs
          => registry xrow
          -> m (IDMap {| Identified r }) -- no LangInfo back, use `translate`` instead!
lookupAll reg = liftEffect do
  objs <- lookupAllInRegistries (registryNames reg)
  if   Array.null objs
  then pure HM.empty
  else pure
     $ HM.fromFoldable
     $ map (\obj -> let e = unsafeFromForeign obj in e.id /\ e) objs

-- | Registers the given element for the given registry. Will override any existing with the same ID
lookupList :: forall registry xrow xs r m meta ml
              . RowToList xrow xs
             => RegistrySumType xs r
             => RowListKeys xs
             => MonadEffect m
             => RowToList meta ml
             => RowListKeys ml
             => registry xrow
             -> Identifier
             -> m (Maybe (IDFormList meta xrow))
lookupList reg { base } = liftEffect do
  let regNames = String.joinWith "🤔" $ registryNames reg
      meta     = String.joinWith "🤔" $ flattenKeyStrings $ rowListKeys (Proxy :: Proxy ml)
      regName  = "📃" <> regNames <> "🏁" <> meta
  foreignObj <- lookupInRegistries [regName] base
  if   isUndefined foreignObj
  then pure Nothing
  else pure $ Just $ unsafeFromForeign foreignObj

-- | As `lookup`, but throws an exception if the given object is not registered
expect :: forall registry xrow xs r. RowToList xrow xs => RegistrySumType xs r => RowListKeys xs
       => registry xrow
       -> Identifier
       -> Effect {| Identified r} -- no LangInfo back, use `translate`` instead!
expect reg id = lookup reg id >>= case _ of
  Just obj -> pure obj
  Nothing  -> throw $ "expect: no instance in registry `" <> (show (registryNames reg)) <> "` for element " <> prettyShowID id

-- | Given two registries, returns another registry for accessing objects that match both's fields
registrySum :: forall xrow1 xrow2 xrow3 xrow4 xs. Union xrow1 xrow2 xrow3 => Nub xrow3 xrow4 => RowToList xrow4 xs => RowListKeys xs
            => Registry xrow1
            -> Registry xrow2
            -> Registry xrow4
registrySum _ _ = mkRegistry (Proxy :: Proxy xrow4)

infixr 5 registrySum as ++

-----------
-- Monad --
-----------

forM :: forall registry xrow xs r m a b. MonadEffect m => RowToList xrow xs => RegistrySumType xs r => RowListKeys xs => Transmute b (Maybe a)
     => registry xrow -> Identifier -> ({| Identified r} -> m b) -> m (Maybe a)
forM reg id f = do
  lookup reg id >>= case _ of
    Just obj -> transmute <$> f obj
    Nothing  -> pure Nothing

forM_ :: forall registry xrow xs r m b. MonadEffect m => RowToList xrow xs => RegistrySumType xs r => RowListKeys xs
      => registry xrow -> Identifier -> ({| Identified r} -> m b) -> m Unit
forM_ reg id f = do
  result <- lookup reg id
  for_ result (void <<< f)

elseM :: forall registry xrow xs r m a. MonadEffect m => RowToList xrow xs => RegistrySumType xs r => RowListKeys xs
       => a -> registry xrow -> Identifier -> ({| Identified r} -> m a) -> m a
elseM defVal reg id f = fromMaybe defVal <$> forM reg id f

forListM :: forall registry xrow xs r m meta ml a b
         . RowToList xrow xs
         => RegistrySumType xs r
         => RowListKeys xs
         => MonadEffect m
         => RowToList meta ml
         => RowListKeys ml
         => Transmute b (Maybe a)
         => registry xrow
         -> Identifier
         -> (IDFormList meta xrow -> m b)
         -> m (Maybe a)
forListM reg id f = do
  lookupList reg id >>= case _ of
    Just obj -> transmute <$> f obj
    Nothing  -> pure Nothing

elseListM :: forall registry xrow xs r m meta ml a
         . RowToList xrow xs
         => RegistrySumType xs r
         => RowListKeys xs
         => MonadEffect m
         => RowToList meta ml
         => RowListKeys ml
         => a
         -> registry xrow
         -> Identifier
         -> (IDFormList meta xrow -> m a)
         -> m a
elseListM defVal reg id f = fromMaybe defVal <$> forListM reg id f

randomList :: forall registry xrow xs r m meta ml
         . RowToList xrow xs
         => RegistrySumType xs r
         => RowListKeys xs
         => MonadEffect m
         => RowToList meta ml
         => RowListKeys ml
         => registry xrow
         -> Identifier
         -> m (Maybe (FormRow meta))
randomList reg id = do
  lookupList reg id >>= case _ of
    Just obj ->
      let FormList forms = obj.list
       in obj.random forms
    Nothing  -> pure Nothing

--------------
-- Modifier --
--------------

modifierRegistry :: Registry (modifier :: Modifier ())
modifierRegistry = mkRegistry Proxy

-- | Has a map of RegistryIDSet to mapper functions; first RegistryIDSet to be a subset of the element's registries is applied
type Modifier r =
  ( modify :: HashMap RegistryIDSet (Foreign -> Foreign)
  -- ^ Map of registry sets to mapper functions for elements in those registries
  -- key with the most matching registries is chosen; in case of tie, hash (registry set) is used
  | r
  )

-- | Returns a tuple for Modifier.modify map init that maps a set of Registries to a record mapper function
modifyWith :: forall registry xrow xs r. RegistrySumType xs r
           => RowToList xrow xs
           => RowListKeys xs
           => registry xrow
           -> ({| Identified + r } -> {| Identified + r })
           -> Tuple RegistryIDSet (Foreign -> Foreign)
modifyWith reg f = registryIDSet reg /\ (unsafeFromForeign >>> f >>> unsafeToForeign)

lookupModifiers :: forall m. MonadEffect m => HashSet String -> m (IDMap {| Identified + Modifier + () })
lookupModifiers modIDs =
  let keysToKeep = HS.map sid modIDs
   in lookupAll modifierRegistry
  <#> HM.filterKeys (flip HS.member keysToKeep)

applyModifier :: forall rmod rrec m. MonadEffect m => {| Modifier + rmod } -> {| Identified + rrec } -> m {| Identified + rrec }
applyModifier mod rec = liftEffect do
  allRegNames <- asHashSet <$> lookupInWhichRegistries rec.id.base
  let mostSpecificKey = HM.keys mod.modify
                     |> map unwrap
                     |> Array.filter (_ `isSubsetOf` allRegNames)
                     |> Array.sortWith (\e -> HS.size e /\ hash e)
                     |> Array.head
  case mostSpecificKey >>= \k -> HM.lookup (RegistryIDSet k) mod.modify of
    Nothing -> pure rec
    Just f  -> pure (unsafeFromForeign $ f $ unsafeToForeign rec)

    