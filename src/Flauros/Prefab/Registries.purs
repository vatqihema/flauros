module Flauros.Prefab.Registries where

import Flauros.Data.Actor (Actor)
import Flauros.Data.Astronomy (Moon, Planet)
import Flauros.Data.Country (Country)
import Flauros.Data.Dialogue (Dialogue)
import Flauros.Data.Event (Consumable)
import Flauros.Data.Gen (Gen_)
import Flauros.Data.Holiday (Holiday)
import Flauros.Data.Item (Item)
import Flauros.Data.Knowledge (KnowVal, KnowTranslatable)
import Flauros.Data.Location (FullHousing)
import Flauros.Data.Module (Module)
import Flauros.Data.Month (Month)
import Flauros.Data.News (NewsStory, NewsGen)
import Flauros.Data.Setting (Setting)
import Flauros.Data.Stat (StatDef)
import Flauros.Data.Store as DS
import Flauros.Data.TagInfo (TagInfo)
import Flauros.Data.Weather (Weather)
import Flauros.Lang (Name)
import Flauros.Prefab.Registry (Modifier, Registry, mkRegistry)
import Flauros.Prelude (Identified)
import Type.Prelude (Proxy(..))
import Type.Row (type (+))

actor :: Registry (actor :: Actor ())
actor = mkRegistry Proxy

type Label r = ( labelParams :: Array KnowVal | r)

ancestry :: Registry (ancestry :: ())
ancestry = mkRegistry Proxy

consumable :: Registry (consumable :: Consumable ())
consumable = mkRegistry Proxy

country :: Registry (country :: Country ())
country = mkRegistry Proxy

dialogue :: Registry (dialogue :: Dialogue ())
dialogue = mkRegistry Proxy

holiday :: Registry (holiday :: Holiday ())
holiday = mkRegistry Proxy

type FullHousing' r = FullHousing DS.Store r
housing :: Registry (housing :: Gen_ {| FullHousing' () } ())
housing = mkRegistry Proxy

-- | Registry for use in FormList that can contain any element and no specialization
id :: Registry (id :: ())
id = mkRegistry Proxy

item :: Registry (item :: Item ())
item = mkRegistry Proxy

itemGen :: Registry (itemGen :: Gen_ {| Identified + Item + () } ())
itemGen = mkRegistry Proxy

label :: Registry (label :: Gen_ {| Identified + Label + () } + ())
label = mkRegistry Proxy

line :: Registry (line :: ())
line = mkRegistry Proxy

month :: Registry (month :: Month ())
month = mkRegistry Proxy

modifier :: Registry (modifier :: Modifier ())
modifier = mkRegistry Proxy

mod :: Registry (module :: Module ())
mod = mkRegistry Proxy

moon :: Registry (moon :: Moon ())
moon = mkRegistry Proxy

type NameGen r = ( name :: Name | r)

nameGen :: Registry (name :: Gen_ {| Identified + NameGen + () } + ())
nameGen = mkRegistry Proxy

newsGen :: Registry (news :: Gen_ {| Identified + NewsStory + () } + NewsGen DS.Store + ())
newsGen = mkRegistry Proxy

planet :: Registry (planet :: Planet ())
planet = mkRegistry Proxy

type Setting' r = Setting DS.Action DS.Store r

sentenceGen :: Registry (news :: Gen_ KnowTranslatable + ())
sentenceGen = mkRegistry Proxy

setting :: Registry (setting :: Setting' ())
setting = mkRegistry Proxy

stat :: Registry (stat :: StatDef ())
stat = mkRegistry Proxy

tagInfo :: Registry (tagInfo :: TagInfo ())
tagInfo = mkRegistry Proxy

type Weather' r = Weather DS.Store r

weather :: Registry (weather :: Weather' ())
weather = mkRegistry Proxy