module Flauros.Prefab.Core where

import Prelude

import Data.HashSet (HashSet)
import Data.HashSet as HS
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Module (Module, Version(..), withDefModule)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (asLines, english, esperanto, line)
import Flauros.Log (LogType(..))
import Flauros.Prefab.Core.Ancestry as Ancestry
import Flauros.Prefab.Core.Astronomy as Astronomy
import Flauros.Prefab.Core.Country as Country
import Flauros.Prefab.Core.Holiday.Fall as Holiday.Fall
import Flauros.Prefab.Core.Holiday.Spring as Holiday.Spring
import Flauros.Prefab.Core.Holiday.Summer as Holiday.Summer
import Flauros.Prefab.Core.Holiday.Winter as Holiday.Winter
import Flauros.Prefab.Core.Housing.Apartment as Housing.Apartment
import Flauros.Prefab.Core.Item.Base as Item.Base
import Flauros.Prefab.Core.Item.Furniture as Item.Furniture
import Flauros.Prefab.Core.Item.Material as Item.Material
import Flauros.Prefab.Core.Item.Plant as Item.Plant
import Flauros.Prefab.Core.Item.Weapon as Item.Weapon
import Flauros.Prefab.Core.Line.Environment as Line.Environment
import Flauros.Prefab.Core.Line.Formatting as Line.Formatting
import Flauros.Prefab.Core.Line.SeerBook as Line.SeerBook
import Flauros.Prefab.Core.Line.WorldPanel as Line.WorldPanel
import Flauros.Prefab.Core.Month as Month
import Flauros.Prefab.Core.Name.Address as Name.Address
import Flauros.Prefab.Core.News.Clickbait as News.Clickbait
import Flauros.Prefab.Core.News.PSA as News.PSA
import Flauros.Prefab.Core.News.Sketchy as News.Sketchy
import Flauros.Prefab.Core.Setting.Base as Setting.Base
import Flauros.Prefab.Core.Setting.Color as Setting.Color
import Flauros.Prefab.Core.StatDef as StatDef
import Flauros.Prefab.Core.TagInfo as TagInfo
import Flauros.Prefab.Core.Test as Test
import Flauros.Prefab.Core.Weather as Weather
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, asHashSet, modID)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

core :: {| Identified + LangInfo' + Module + () }
core = withDefModule
  { id:           modID "core"
  , authors:      asHashSet [ "vatqihema" ]
  , version:      Version 0 1 0 0
  , dependencies: HS.empty :: HashSet String
  , priority:     -100000.0 -- You can load before this, but hopefully only intentionally
  , lines:     asLines
      [ english /\
          [ "name" /\ line "Core"
          , "desc" /\ line "The base game module. You probably shouldn't disable this!"
          ]
      , esperanto /\
          [ "name" /\ line "Kerno"
          ]
      ]
  }

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.mod core
  Ancestry.init
  Astronomy.init
  Country.init
  Holiday.Fall.init
  Holiday.Spring.init
  Holiday.Summer.init
  Holiday.Winter.init
  Housing.Apartment.init
  Item.Base.init
  Item.Furniture.init
  Item.Material.init
  Item.Plant.init
  Item.Weapon.init
  Line.Environment.init
  Line.Formatting.init
  Line.SeerBook.init
  Line.WorldPanel.init
  Month.init
  Name.Address.init
  News.Clickbait.init
  News.PSA.init
  News.Sketchy.init
  Setting.Base.init
  Setting.Color.init
  StatDef.init
  TagInfo.init
  Test.init
  Weather.init
  DS.log Info "Core.init has finished"