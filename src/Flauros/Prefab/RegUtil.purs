module Flauros.Prefab.RegUtil where

import Prelude

import Data.Maybe (Maybe(..))
import Effect.Class (class MonadEffect)
import Flauros.Data.Actor (giveList)
import Flauros.Data.FormList (IDFormList)
import Flauros.Data.Item (Item)
import Flauros.Data.Store (Action)
import Flauros.Data.Store as DS
import Flauros.Log (LogType(..))
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry as Registry
import Flauros.Prelude (Identifier, prettyShowID)
import Halogen.Store.Monad (class MonadStore)

lookupAndGive :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => Identifier -> Identifier -> m Action
lookupAndGive actorID listID = do
  Registry.lookupList Regs.item listID >>= case _ of
    Just (ls :: IDFormList (count :: Int) (item :: Item ())) ->
      pure $ DS.onActor actorID $ giveList ls
    Nothing -> do
      DS.log Error $ "RegUtil.lookupAndGive `" <> prettyShowID actorID <> "` failed to find list `" <> prettyShowID listID <> "` in the registry"
      pure identity