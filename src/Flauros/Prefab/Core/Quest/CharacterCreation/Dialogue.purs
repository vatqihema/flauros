module Flauros.Prefab.Core.Quest.CharacterCreation.Dialogue where

import Prelude

import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Actor (playerID)
import Flauros.Data.Dialogue (Dialogue, withDefDialogue)
import Flauros.Data.Dialogue as D
import Flauros.Data.FormList (IDFormList, formlist)
import Flauros.Data.Item (Item)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (asLines)
import Flauros.Lang.Text ((&))
import Flauros.Prefab.Core.Item.Base (dollars, money)
import Flauros.Prefab.Core.Item.Furniture (bed, foldingChair, icebox, movingBox, sleepingBag, sofa)
import Flauros.Prefab.RegUtil as RegUtil
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (Registry(..), register, registerList)
import Flauros.Prefab.Registry as Registry
import Flauros.Prelude (Identified, Identifier, asHashMap, scoped, sid, withMod)
import Halogen.Store.Monad (class MonadStore, updateStore)
import Type.Row (type (+))

ccID :: Identifier
ccID = sid "character-creation"

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.dialogue housingSituation
  registerList Regs.item defaultFurnitureBase
  registerList Regs.item defaultFurnitureAlmostEmpty
  registerList Regs.item defaultFurnitureCrowded
  registerList Regs.item defaultFurnitureSparse

housingSituation :: {| Identified + LangInfo' + Dialogue + () }
housingSituation = withDefDialogue
  { id: scoped ccID "housing-situation"
  , nodes: asHashMap
      [ "furniture" /\
          [ D.narrateLn "And how is the furniture at your new place?"
          , D.topic "It's sparse, but has the essentials."
              [ D.run do updateStore =<< RegUtil.lookupAndGive playerID defaultFurnitureSparse.id
              ]
          , D.topic "It'll be crowded until I get the moving boxes sorted."
              [ D.narrateLn "You better get on it then, unless you're already planning for the move out."
              , D.run do updateStore =<< RegUtil.lookupAndGive playerID defaultFurnitureCrowded.id
              ]
          , D.topic "It's almost empty."
              [ D.narrate   "Well, that's a shame. "
              , D.narrateLn "Though an empty apartment means plenty of room to grow."
              , D.run do updateStore =<< RegUtil.lookupAndGive playerID defaultFurnitureAlmostEmpty.id
              ]
          , D.ask
          , D.run do updateStore =<< RegUtil.lookupAndGive playerID defaultFurnitureBase.id  
          ]
      ]
  , lines: asLines []
  }

defaultFurnitureBase :: IDFormList (count :: Int) (item :: Item ())
defaultFurnitureBase = formlist (scoped ccID "default-furniture-base")
  [ { meta: { count: 1 }, id: foldingChair.id }
  , { meta: { count: 1 }, id: icebox.id }
  , { meta: { count: 1 }, id: movingBox.id }
  ]

defaultFurnitureAlmostEmpty :: IDFormList (count :: Int) (item :: Item ())
defaultFurnitureAlmostEmpty = formlist (scoped ccID "default-furniture-almost-empty")
  [ { meta: { count: dollars 113 }, id: money.id }
  , { meta: { count:           1 }, id: movingBox.id }
  , { meta: { count:           1 }, id: sleepingBag.id }
  ]

defaultFurnitureCrowded :: IDFormList (count :: Int) (item :: Item ())
defaultFurnitureCrowded = formlist (scoped ccID "default-furniture-crowded")
  [ { meta: { count: 1 }, id: bed.id }
  , { meta: { count: 1 }, id: foldingChair.id }
  , { meta: { count: 1 }, id: sleepingBag.id }
  , { meta: { count: 6 }, id: movingBox.id }
  , { meta: { count: 1 }, id: sofa.id `withMod` "second-hand" }
  ]

defaultFurnitureSparse :: IDFormList (count :: Int) (item :: Item ())
defaultFurnitureSparse = formlist (scoped ccID "default-furniture-sparse")
  [ { meta: { count: (dollars 49) }, id: money.id }
  , { meta: { count: 1 }, id: bed.id }
  , { meta: { count: 2 }, id: movingBox.id }
  , { meta: { count: 1 }, id: sofa.id `withMod` "second-hand" }
  ]