module Flauros.Prefab.Core.StatDef where

import Prelude

import Data.Int (round)
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Stat (StatDef, StatState, totalStat, withDefStatDef)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (Line, LineParam(..), asLines, english, esperanto, line, paramFail)
import Flauros.Lang.Text (Text, clear, color, italics, (&))
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, sid, transmute)
import Flauros.Style.Color (clay, cornflower, grass, lavender, orange, pink, purple, sand, yellow)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.stat energy
  register Regs.stat hunger
  -- Skills
  register Regs.stat alchemy
  register Regs.stat artificy
  register Regs.stat awareness
  register Regs.stat bardistry
  register Regs.stat brawling
  register Regs.stat druidcraft
  register Regs.stat expression
  register Regs.stat finesse
  register Regs.stat knowledge
  register Regs.stat necromancy
  register Regs.stat organization
  register Regs.stat transmutation
  register Regs.stat useTech

---------------
-- Resources --
---------------

energy :: {| Identified + LangInfo' + StatDef + () }
energy = withDefStatDef
  { id:   sid "energy"
  , min: (-2.0), max: 4.0
  , lines:     asLines
      [ english /\
          [ "name" /\ line "energy"
          , "abbr" /\ line "ene"
          , "desc" /\ line case _ of
              [PStat stat] -> transmute $ case round $ totalStat stat of
                i | i <= (-2) -> "enervated"
                (-1)          -> "exhausted"
                0             -> "fatigued"
                1             -> "weary"
                2             -> "aware"
                3             -> "jumpy"
                _             -> "agitated"
              p -> paramFail "energy/desc" p
          ]
      , esperanto /\
          [
          ]
      ]
  }

hunger :: {| Identified + LangInfo' + StatDef + () }
hunger = withDefStatDef
  { id:   sid "hunger"
  , min: (-2.0), max: 4.0
  , lines:     asLines
      [ english /\
          [ "name" /\ line "hunger"
          , "abbr" /\ line "hng"
          , "desc" /\ line case _ of
              [PStat stat] -> transmute $ case round $ totalStat stat of
                i | i <= (-2) -> "starving"
                (-1)          -> "famished"
                0             -> "peckish"
                1             -> "content"
                2             -> "content"
                3             -> "full"
                _             -> "stuffed"
              p -> paramFail "hunger/desc" p
          ]
      , esperanto /\
          [
          ]
      ]
  }

------------
-- Skills --
------------

ratingStyle :: StatState -> Text
ratingStyle stat = case round $ totalStat stat of
  s | s <= (-2) -> transmute $ color lavender
  (-1)          -> transmute $ color clay
  0             -> transmute $ color sand
  1             -> transmute $ color grass
  2             -> transmute $ color purple
  3             -> color yellow & italics
  _             -> color pink & italics

sentenceStyle :: StatState -> Text
sentenceStyle stat = transmute $ case stat.mod of
  m | between (-1e-5) 1e-5 m -> color sand
  m | m < 0.0                -> color orange
  _                          -> color cornflower

englishDescribe :: String -> Line DS.Store
englishDescribe name = line case _ of
  [PStat st] ->
    let i   = round $ totalStat st
        adj = case i of
          s | s <= (-2) -> "terrible"
          (-1)          -> "mediocre"
          0             -> "okay"
          1             -> "good"
          2             -> "great"
          3             -> "amazing"
          _             -> "extraordinary"
        sStyle = sentenceStyle st
        rStyle = ratingStyle st
      in sStyle & "You are " & rStyle & adj & clear & sStyle & " at " & name & (if i < 0 then "..." else "")
  p -> paramFail "stat/*/desc" p

esperantoDescribe :: String -> Line DS.Store
esperantoDescribe name = line case _ of
  [PStat st] ->
    let i   = round $ totalStat st
        adj = case i of
          s | s <= (-2) -> "teruras"
          (-1)          -> "submezas"
          0             -> "mezas"
          1             -> "lertas"
          2             -> "lertegas"
          3             -> "mirindas"
          _             -> "fantaziegas"
        sStyle = sentenceStyle st
        rStyle = ratingStyle st
      in sStyle & "Vi " & rStyle & adj & clear & sStyle & " pri " & name & (if i < 0 then "..." else "")
  p -> paramFail "stat/*/desc" p

alchemy :: {| Identified + LangInfo' + StatDef + () }
alchemy = withDefStatDef
  { id:   sid "alchemy"
  , min: (-2.0), max: 4.0
  , lines:     asLines
      [ english /\
          [ "name" /\ line "alchemy"
          , "abbr" /\ line "alc"
          , "desc" /\ englishDescribe "alchemy"
          ]
      , esperanto /\
          [ "name" /\ line "alĥemio"
          , "abbr" /\ line "alĥ"
          , "desc" /\ esperantoDescribe "alĥemio"
          ]
      ]
  }

artificy :: {| Identified + LangInfo' + StatDef + () }
artificy = withDefStatDef
  { id:   sid "artificy"
  , min: (-2.0), max: 4.0
  , lines:     asLines
      [ english /\
          [ "name" /\ line "artificy"
          , "abbr" /\ line "art"
          , "desc" /\ englishDescribe "artificy"
          ]
      , esperanto /\
          [ "name" /\ line "artifika" -- implicita (lerto), ĉi tio devas esti sufikso "artifikerto"?
          , "abbr" /\ line "art"
          , "desc" /\ esperantoDescribe "artifika"
          ]
      ]
  }

awareness :: {| Identified + LangInfo' + StatDef + () }
awareness = withDefStatDef
  { id:   sid "awareness"
  , min: (-2.0), max: 4.0
  , lines:     asLines
      [ english /\
          [ "name" /\ line "awareness"
          , "abbr" /\ line "awr"
          , "desc" /\ englishDescribe "awareness"
          ]
      , esperanto /\
          [ "name" /\ line "konscio"
          , "abbr" /\ line "kon"
          , "desc" /\ esperantoDescribe "konscio"
          ]
      ]
  }

bardistry :: {| Identified + LangInfo' + StatDef + () }
bardistry = withDefStatDef
  { id:   sid "bardistry"
  , min: (-2.0), max: 4.0
  , lines:     asLines
      [ english /\
          [ "name" /\ line "bardistry"
          , "abbr" /\ line "bar"
          , "desc" /\ englishDescribe "bardistry"
          ]
      , esperanto /\
          [ "name" /\ line "bardeca"
          , "abbr" /\ line "bar"
          , "desc" /\ esperantoDescribe "bardeca"
          ]
      ]
  }

brawling :: {| Identified + LangInfo' + StatDef + () }
brawling = withDefStatDef
  { id:   sid "brawling"
  , min: (-2.0), max: 4.0
  , lines:     asLines
      [ english /\
          [ "name" /\ line "brawling"
          , "abbr" /\ line "bra"
          , "desc" /\ englishDescribe "brawling"
          ]
      , esperanto /\
          [ "name" /\ line "interbatiĝa"
          , "abbr" /\ line "bat"
          , "desc" /\ esperantoDescribe "interbatiĝa"
          ]
      ]
  }

druidcraft :: {| Identified + LangInfo' + StatDef + () }
druidcraft = withDefStatDef
  { id:   sid "druidcraft"
  , min: (-2.0), max: 4.0
  , lines:     asLines
      [ english /\
          [ "name" /\ line "druidcraft"
          , "abbr" /\ line "drd"
          , "desc" /\ englishDescribe "druidcraft"
          ]
      , esperanto /\
          [ "name" /\ line "druidado"
          , "abbr" /\ line "drd"
          , "desc" /\ esperantoDescribe "druidado"
          ]
      ]
  }

expression :: {| Identified + LangInfo' + StatDef + () }
expression = withDefStatDef
  { id:   sid "expression"
  , min: (-2.0), max: 4.0
  , lines:     asLines
      [ english /\
          [ "name" /\ line "expression"
          , "abbr" /\ line "exp"
          , "desc" /\ englishDescribe "expression"
          ]
      , esperanto /\
          [ "name" /\ line "esprimo"
          , "abbr" /\ line "esp"
          , "desc" /\ esperantoDescribe "esprimo"
          ]
      ]
  }

finesse :: {| Identified + LangInfo' + StatDef + () }
finesse = withDefStatDef
  { id:   sid "finesse"
  , min: (-2.0), max: 4.0
  , lines:     asLines
      [ english /\
          [ "name" /\ line "finesse"
          , "abbr" /\ line "fns"
          , "desc" /\ englishDescribe "finesse"
          ]
      , esperanto /\
          [ "name" /\ line "teĥniko"
          , "abbr" /\ line "teĥ"
          , "desc" /\ esperantoDescribe "teĥniko"
          ]
      ]
  }

knowledge :: {| Identified + LangInfo' + StatDef + () }
knowledge = withDefStatDef
  { id:   sid "knowledge"
  , min: (-2.0), max: 4.0
  , lines:     asLines
      [ english /\
          [ "name" /\ line "knowledge"
          , "abbr" /\ line "knw"
          , "desc" /\ englishDescribe "knowledge"
          ]
      , esperanto /\
          [ "name" /\ line "scio"
          , "abbr" /\ line "sci"
          , "desc" /\ esperantoDescribe "scio"
          ]
      ]
  }

necromancy :: {| Identified + LangInfo' + StatDef + () }
necromancy = withDefStatDef
  { id:   sid "necromancy"
  , min: (-2.0), max: 4.0
  , lines:     asLines
      [ english /\
          [ "name" /\ line "necromancy"
          , "abbr" /\ line "ncr"
          , "desc" /\ englishDescribe "necromancy"
          ]
      , esperanto /\
          [ "name" /\ line "mortmagia"
          , "abbr" /\ line "mtm"
          , "desc" /\ esperantoDescribe "mortmagia"
          ]
      ]
  }

organization :: {| Identified + LangInfo' + StatDef + () }
organization = withDefStatDef
  { id:   sid "organization"
  , min: (-2.0), max: 4.0
  , lines:     asLines
      [ english /\
          [ "name" /\ line "organization"
          , "abbr" /\ line "org"
          , "desc" /\ englishDescribe "organization"
          ]
      , esperanto /\
          [ "name" /\ line "organizo"
          , "abbr" /\ line "org"
          , "desc" /\ esperantoDescribe "organizo"
          ]
      ]
  }

transmutation :: {| Identified + LangInfo' + StatDef + () }
transmutation = withDefStatDef
  { id:   sid "transmutation"
  , min: (-2.0), max: 4.0
  , lines:     asLines
      [ english /\
          [ "name" /\ line "transmutation"
          , "abbr" /\ line "trn"
          , "desc" /\ englishDescribe "transmutation"
          ]
      , esperanto /\
          [ "name" /\ line "aliformeca"
          , "abbr" /\ line "ali"
          , "desc" /\ esperantoDescribe "aliformeca"
          ]
      ]
  }

useTech :: {| Identified + LangInfo' + StatDef + () }
useTech = withDefStatDef
  { id:   sid "use-tech"
  , min: (-2.0), max: 4.0
  , lines:     asLines
      [ english /\
          [ "name" /\ line "use tech"
          , "abbr" /\ line "tec"
          , "desc" /\ englishDescribe "tech use"
          ]
      , esperanto /\
          [ "name" /\ line "teknologio"
          , "abbr" /\ line "tek"
          , "desc" /\ esperantoDescribe "teknologio"
          ]
      ]
  }