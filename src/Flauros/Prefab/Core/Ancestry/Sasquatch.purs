module Flauros.Prefab.Core.Ancestry.Sasquatch where

import Prelude

import Data.HashSet as HS
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Actor (playerID)
import Flauros.Data.Dialogue (Dialogue, narrator, onAction, withDefDialogue)
import Flauros.Data.Dialogue as D
import Flauros.Data.FormList (FormList(..), IDFormList, formlist)
import Flauros.Data.Knowledge (Scope(..), worldID)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (asLines, english, esperanto, line)
import Flauros.Lang.Text (italics, (&))
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (Registry(..), register, registerList)
import Flauros.Prelude (Identified, Identifier, asHashMap, asHashSet, scoped, sid, withDefTags)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.ancestry sasquatch
  register Regs.dialogue sasquatchCreate
  registerList Regs.dialogue prompts
  
sasquatch :: {| Identified + LangInfo' + () }
sasquatch = withDefTags
  { id:    sid "sasquatch"
  , tags:  asHashSet [ "humanoid" ]
  , lines: asLines
      [ english /\
          [ "name" /\ line "sasquatch"
          , "adj"  /\ line "sasquatchewan"
          ]
      , esperanto /\
          [ "name" /\ line "saskvaĉo"
          , "adj"  /\ line "saskvaĉa"
          ]
      ]
  }

prompts :: IDFormList (node :: String) (dialogue :: Dialogue ())
prompts = formlist (scoped sasquatch.id "prompts")
  [ { id: sasquatchCreate.id, meta: { node: "blurry" } }
  ]

sasquatchCreate :: {| Identified + LangInfo' + Dialogue + () }
sasquatchCreate = withDefDialogue
  { id: scoped sasquatch.id "creation"
  , nodes: asHashMap
      [ "blurry" /\
          [ rememberBlurryReal true -- as this point it's considered real
          , D.narrateLn "Do pictures of you ever turn out blurry?"
          , D.topic "They always do!"
              [ D.narrateLn "That must be awful, I'm sorry."
              , rememberBlurry true
              ]
          , D.topic "No, but it runs in my family."
              [ D.narrate "That's fortunate for you. Probably don't have many family photos hanging."
              , rememberBlurry false
              , D.remember { who: playerID, name: local "blurry-family", what: true }
              ]
          , D.topic "Not at all."
              [ D.narrate "Then you're free to take selfies to your hearts content!"
              , rememberBlurry false
              ]
          , D.topic "Wait, is that a real thing?"
              [ D.narrateLn (italics & "Is it?")
              , D.topic_ "Yes, and I have it." $
                  rememberBlurry true
              , D.topic_ "No, but it's real." $
                  rememberBlurry false
              , D.topic "No, it isn't."
                  [ D.narrateLn "Then it isn't."
                  , rememberBlurry false
                  , rememberBlurryReal false
                  ]
              ]
          ]
      ]
  , lines: asLines []
  }
  where local = scoped sasquatch.id
        rememberBlurry b     = D.remember { who: playerID, name: local "blurry",      what: b }
        rememberBlurryReal b = D.remember { who: worldID,  name: local "blurry-real", what: b }