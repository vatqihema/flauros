module Flauros.Prefab.Core.Ancestry.Gnome where

import Prelude

import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (asLines, english, esperanto, line)
-- import Flauros.Lang.Text ((&))
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, asHashSet, sid, withDefTags)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.ancestry gnome
  
gnome :: {| Identified + LangInfo' + () }
gnome = withDefTags
  { id:    sid "gnome"
  , tags:  asHashSet [ "humanoid", "subterranean"  ]
  , lines: asLines
      [ english /\
          [ "name" /\ line "gnome"
          , "adj"  /\ line "gnomish"
          ]
      , esperanto /\
          [ "name" /\ line "gnomo"
          , "adj"  /\ line "gnoma"
          ]
      ]
  }

  -- Phrygian cap?