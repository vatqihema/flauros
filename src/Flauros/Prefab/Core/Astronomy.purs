module Flauros.Prefab.Core.Astronomy where

import Prelude

import Data.Date (Month(..))
import Data.Enum (fromEnum)
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Astronomy (Moon, MoonPhase(..), Planet, firstQuarterID, fullMoonID, lastQuarterID, newMoonID, waningCrescentID, waningGibbousID, waxingCrescentID, waxingGibbousID, withDefMoon, withDefPlanet)
import Flauros.Data.Astronomy as Astronomy
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Data.Time (class HasDate, getDate, getYear, mkDate)
import Flauros.Lang (LineParam(..), asLines, english, esperanto, line)
import Flauros.Lang.Text (color, (&))
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, asHashMap, sid, withDefTags)
import Flauros.Style.Color (blue, cyan, grass, orange, red, yellow)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.moon newMoon
  register Regs.moon waxingCrescent
  register Regs.moon firstQuarter
  register Regs.moon waxingGibbous
  register Regs.moon fullMoon
  register Regs.moon waningCrescent
  register Regs.moon lastQuarter
  register Regs.moon waningGibbous

-------------
-- Planets --
-------------

-- Octugna

octugna :: {| Identified + LangInfo' + Planet + () }
octugna = withDefPlanet
  { id:            sid "octugna"
  , albedo:        Astronomy.earth.albedo
  , aphelion:      Astronomy.earth.aphelion
  , perihelion:    Astronomy.earth.perihelion
  , radius:        Astronomy.earth.radius
  , semiMajorAxis: Astronomy.earth.semiMajorAxis
  , lines:     asLines
      [ english /\
          [ "name"   /\ line (color grass & "Octugna")
          , "symbol" /\ line "🌱"
          ]
      , esperanto /\
          [ "name"   /\ line (color grass & "Oktuĥnja")
          ]
      ]
  }

-----------------
-- Moon Phases --
-----------------

newMoon :: {| Identified + LangInfo' + Moon + () }
newMoon = withDefMoon
  { id:        newMoonID
  , phase:     NewMoon
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 3.0 ]
  , lines:     asLines
      [ english /\
          [ "name"   /\ line (color blue & "New moon")
          , "symbol" /\ line "🌑"
          ]
      , esperanto /\
          [ "name"   /\ line (color blue & "Nova luno")
          ]
      ]
  }

waxingCrescent :: {| Identified + LangInfo' + Moon + () }
waxingCrescent = withDefMoon
  { id:        waxingCrescentID
  , phase:     WaxingCrescent
  , lines:     asLines
      [ english /\
          [ "name"   /\ line (color orange & "Waxing crescent")
          , "symbol" /\ line "🌒"
          ]
      , esperanto /\
          [ "name"   /\ line (color orange & "Unua arko")
          ]
      ]
  }

firstQuarter :: {| Identified + LangInfo' + Moon + () }
firstQuarter = withDefMoon
  { id:        firstQuarterID
  , phase:     FirstQuarter
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ -1.0 ]
  , lines:     asLines
      [ english /\
          [ "name"   /\ line (color yellow & "First quarter")
          , "symbol" /\ line "🌓"
          ]
      , esperanto /\
          [ "name"   /\ line (color yellow & "Unua duono")
          ]
      ]
  }

waxingGibbous :: {| Identified + LangInfo' + Moon + () }
waxingGibbous = withDefMoon
  { id:        waxingGibbousID
  , phase:     WaxingGibbous
  , lines:     asLines
      [ english /\
          [ "name"   /\ line (color yellow & "Waxing gibbous")
          , "symbol" /\ line "🌔"
          ]
      , esperanto /\
          [ "name"   /\ line (color yellow & "Unua ĝibo")
          ]
      ]
  }

isHarvestMoon :: forall d. HasDate d => d -> Boolean
isHarvestMoon d = between begin end $ getDate d
  where year  = fromEnum $ getYear d
        begin = mkDate year September 8
        end   = mkDate year October   7

fullMoon :: {| Identified + LangInfo' + Moon + () }
fullMoon = withDefMoon
  { id:        fullMoonID
  , phase:     FullMoon
  , eventMods: asHashMap
    [ "ghostly-phenomenon" /\ 4.0
    , "mythos"             /\ 1.0
    ]
  , lines:     asLines
      [ english /\
          [ "name"   /\ line case _ of
              [PDateTime dt] | isHarvestMoon dt -> color red  & "Harvest moon"
              _                                 -> color cyan & "Full moon"
          , "symbol" /\ line "🌕"
          ]
      , esperanto /\
          [ "name"   /\ line case _ of
              [PDateTime dt] | isHarvestMoon dt -> color red  & "Rikoltluno" -- doesn't seem to be a common idea in Esperanto works, maybe this should always be Pleno luno?
              _                                 -> color cyan & "Pleno luno"
          ]
      ]
  }

waningGibbous :: {| Identified + LangInfo' + Moon + () }
waningGibbous = withDefMoon
  { id:        waningGibbousID
  , phase:     WaningGibbous
  , lines:     asLines
      [ english /\
          [ "name"   /\ line (color yellow & "Waning gibbous")
          , "symbol" /\ line "🌖"
          ]
      , esperanto /\
          [ "name"   /\ line (color yellow & "Lasta ĝibo")
          ]
      ]
  }

lastQuarter :: {| Identified + LangInfo' + Moon + () }
lastQuarter = withDefMoon
  { id:        lastQuarterID
  , phase:     LastQuarter
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ -1.0 ]
  , lines:     asLines
      [ english /\
          [ "name"   /\ line (color yellow & "Last quarter")
          , "symbol" /\ line "🌗"
          ]
      , esperanto /\
          [ "name"   /\ line (color yellow & "Lasta duono")
          ]
      ]
  }

waningCrescent :: {| Identified + LangInfo' + Moon + () }
waningCrescent = withDefMoon
  { id:        waningCrescentID
  , phase:     WaningCrescent
  , lines:     asLines
      [ english /\
          [ "name"   /\ line (color orange & "Waning crescent")
          , "symbol" /\ line "🌘"
          ]
      , esperanto /\
          [ "name"   /\ line (color orange & "Lasta arko")
          ]
      ]
  }
