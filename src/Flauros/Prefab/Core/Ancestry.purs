module Flauros.Prefab.Core.Ancestry where

import Prelude

import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (asLines, english, esperanto, line)
import Flauros.Lang.Text ((&))
import Flauros.Prefab.Core.Ancestry.Gnome as Ancestry.Gnome
import Flauros.Prefab.Core.Ancestry.Sasquatch as Ancestry.Sasquatch
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, asHashSet, sid, withDefTags)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.ancestry chimera
  register Regs.ancestry chowfolk
  register Regs.ancestry demonkin
  register Regs.ancestry dragonkin
  register Regs.ancestry dwarf
  register Regs.ancestry elf
  register Regs.ancestry goblin
  register Regs.ancestry golem
  register Regs.ancestry greyling
  register Regs.ancestry harpy
  register Regs.ancestry human
  register Regs.ancestry kobold
  register Regs.ancestry moosotaur
  register Regs.ancestry orc
  Ancestry.Gnome.init
  Ancestry.Sasquatch.init
  
chimera :: {| Identified + LangInfo' + () }
chimera = withDefTags
  { id:    sid "chimera"
  , tags:  asHashSet [ "humanoid" ]
  , lines: asLines
      [ english /\
          [ "name" /\ line "chimera"
          , "adj"  /\ line "chimeran"
          ]
      , esperanto /\
          [ "name" /\ line "ĥimero"
          , "adj"  /\ line "ĥimera"
          ]
      ]
  }
  
chowfolk :: {| Identified + LangInfo' + () }
chowfolk = withDefTags
  { id:    sid "chowfolk"
  , tags:  asHashSet [ "humanoid" ]
  , lines: asLines
      [ english /\
          [ "name" /\ line "chowfolk"
          , "adj"  /\ line "chowfolk"
          ]
      , esperanto /\
          [ "name" /\ line "ĉaŭulo"
          , "adj"  /\ line "ĉaŭula"
          ]
      ]
  }
  
demonkin :: {| Identified + LangInfo' + () }
demonkin = withDefTags
  { id:    sid "demonkin"
  , tags:  asHashSet [ "demon", "humanoid" ]
  , lines: asLines
      [ english /\
          [ "name" /\ line "demonkin"
          , "adj"  /\ line "demonish"
          ]
      , esperanto /\
          [ "name" /\ line "demonulo"
          , "adj"  /\ line "demonula"
          ]
      ]
  }
  
dragonkin :: {| Identified + LangInfo' + () }
dragonkin = withDefTags
  { id:    sid "dragonkin"
  , tags:  asHashSet [ "dragon", "humanoid" ]
  , lines: asLines
      [ english /\
          [ "name" /\ line "dragonkin"
          , "adj"  /\ line "dragonish"
          ]
      , esperanto /\
          [ "name" /\ line "drakulo"
          , "adj"  /\ line "drakula"
          ]
      ]
  }
  
dwarf :: {| Identified + LangInfo' + () }
dwarf = withDefTags
  { id:    sid "dwarf"
  , tags:  asHashSet [ "humanoid", "subterranean" ]
  , lines: asLines
      [ english /\
          [ "name" /\ line "dwarf"
          , "adj"  /\ line "dwarven"
          ]
      , esperanto /\
          [ "name" /\ line "dvarfo"
          , "adj"  /\ line "dvarfa"
          ]
      ]
  }
  
elf :: {| Identified + LangInfo' + () }
elf = withDefTags
  { id:    sid "elf"
  , tags:  asHashSet [ "humanoid" ]
  , lines: asLines
      [ english /\
          [ "name" /\ line "elf"
          , "adj"  /\ line "elven"
          ]
      , esperanto /\
          [ "name" /\ line "elfo"
          , "adj"  /\ line "elfa"
          ]
      ]
  }
  
golem :: {| Identified + LangInfo' + () }
golem = withDefTags
  { id:    sid "golem"
  , tags:  asHashSet [ "construct", "humanoid" ]
  , lines: asLines
      [ english /\
          [ "name" /\ line "golem"
          , "adj"  /\ line "golem"
          ]
      , esperanto /\
          [ "name" /\ line "golemo"
          , "adj"  /\ line "golema"
          ]
      ]
  }
  
greyling :: {| Identified + LangInfo' + () }
greyling = withDefTags
  { id:    sid "greyling"
  , tags:  asHashSet [ "humanoid", "alien" ]
  , lines: asLines
      [ english /\
          [ "name" /\ line "greyling"
          , "adj"  /\ line "greyling"
          ]
      , esperanto /\
          [ "name" /\ line "grizulo"
          , "adj"  /\ line "grizula"
          ]
      ]
  }
  
goblin :: {| Identified + LangInfo' + () }
goblin = withDefTags
  { id:    sid "goblin"
  , tags:  asHashSet [ "humanoid" ]
  , lines: asLines
      [ english /\
          [ "name" /\ line "goblin"
          , "adj"  /\ line "goblin"
          ]
      , esperanto /\
          [ "name" /\ line "goblino"
          , "adj"  /\ line "goblina"
          ]
      ]
  }
  
harpy :: {| Identified + LangInfo' + () }
harpy = withDefTags
  { id:    sid "harpy"
  , tags:  asHashSet [ "humanoid" ]
  , lines: asLines
      [ english /\
          [ "name" /\ line "harpy"
          , "adj"  /\ line "harpy"
          , "desc" /\ line
            ( "Harpies bear both avian and humanoid traits, generally including a pair of wings and taloned feet."
            & "\nIn Althesia harpies are associated with traditional fishing communities and birdsong signalling language."
            & "\nCommon feather and features are similar to that of owls, seagulls, crows, pigeons, swans, and ducks."
            )
          ]
      , esperanto /\
          [ "name" /\ line "harpio"
          , "adj"  /\ line "harpia"
          ]
      ]
  }
  
human :: {| Identified + LangInfo' + () }
human = withDefTags
  { id:    sid "human"
  , tags:  asHashSet [ "humanoid" ]
  , lines: asLines
      [ english /\
          [ "name" /\ line "human"
          , "adj"  /\ line "human"
          ]
      , esperanto /\
          [ "name" /\ line "homo"
          , "adj"  /\ line "homa"
          ]
      ]
  }
  
kobold :: {| Identified + LangInfo' + () }
kobold = withDefTags
  { id:    sid "kobold"
  , tags:  asHashSet [ "humanoid", "subterranean" ]
  , lines: asLines
      [ english /\
          [ "name" /\ line "kobold"
          , "adj"  /\ line "kobold"
          ]
      , esperanto /\
          [ "name" /\ line "koboldo"
          , "adj"  /\ line "kobolda"
          ]
      ]
  }
  
moosotaur :: {| Identified + LangInfo' + () }
moosotaur = withDefTags
  { id:    sid "moosotaur"
  , tags:  asHashSet [ "humanoid" ]
  , lines: asLines
      [ english /\
          [ "name" /\ line "moosotaur"
          , "adj"  /\ line "moosotaurish"
          ]
      , esperanto /\
          [ "name" /\ line "alkotaŭro"
          , "adj"  /\ line "alkotaŭra"
          ]
      ]
  }
  
orc :: {| Identified + LangInfo' + () }
orc = withDefTags
  { id:    sid "orc"
  , tags:  asHashSet [ "humanoid" ]
  , lines: asLines
      [ english /\
          [ "name" /\ line "orc"
          , "adj"  /\ line "orcish"
          ]
      , esperanto /\
          [ "name" /\ line "orko"
          , "adj"  /\ line "orka"
          ]
      ]
  }