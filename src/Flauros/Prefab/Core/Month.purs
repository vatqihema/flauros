module Flauros.Prefab.Core.Month where

import Prelude

import CSS.Color (rgb)
import Data.Date as Date
import Data.Enum (toEnum)
import Data.Maybe (fromJust)
import Data.Time (Time(..))
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Month (Month, withDefMonth, withDefTheme)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (asLines, english, esperanto, line)
import Flauros.Prefab.Core.Item.Plant (balsamroot, dandelion, grapeSprig, grassWidow, honeysuckle, lupine, milfoil, sagebrushViolet, snowberryBouquet, squashBlossom, thistle, yarrow)
import Flauros.Prefab.Core.Weather as Weather
import Flauros.Prefab.Registries as Registries
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, asHashMap, sid)
import Flauros.Style.Color (clay, cornflower, cyan, green, lavender, lilac, lime, navy, orange, pink, purple, red)
import Halogen.Store.Monad (class MonadStore)
import Partial.Unsafe (unsafePartial)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Registries.month january
  register Registries.month february
  register Registries.month march
  register Registries.month april
  register Registries.month may
  register Registries.month june
  register Registries.month july
  register Registries.month august
  register Registries.month september
  register Registries.month october
  register Registries.month november
  register Registries.month december

hhmm :: Int -> Int -> Time
hhmm h m = unsafePartial fromJust (Time <$> (toEnum h) <*> (toEnum m) <*> (toEnum 0) <*> (toEnum 0))

january :: {| Identified + LangInfo' + Month + () }
january = withDefMonth
  { id:        sid "january"
  , enum:      Date.January
  , flower:    milfoil.id
  , theme:     withDefTheme
      { text:      \_ -> rgb 0xac 0xcc 0xe4 -- #accce4
      , highlight: \_ -> rgb 0x95 0x91 0xe5 -- #9591e5
      , day:       cornflower
      , dusk:      lilac
      , night:     navy
      }
  , temp:      { min: -2, max: 0 }
  , timeOfDay: { dawn:  hhmm  7  0
               , day:   hhmm  8 30
               , dusk:  hhmm 16 30
               , night: hhmm 17 30
               }
  , weather:   asHashMap 
      [ Weather.blizzard.id       /\ 0.2
      , Weather.bloodRain.id      /\ 0.05
      , Weather.clear.id          /\ 1.3
      , Weather.fog.id            /\ 0.1
      , Weather.lightRain.id      /\ 0.4
      , Weather.snow.id           /\ 1.0
      , Weather.warpstorm.id      /\ 0.005
      , Weather.windstorm.id      /\ 0.05
      , Weather.windy.id          /\ 0.25
      ]
  , lines:     asLines
      [ english /\
          [ "name"   /\ line "January"
          , "short"  /\ line "Jan"
          , "symbol" /\ line "❄️"
          ]
      , esperanto /\
          [ "name"   /\ line "januaro"
          , "short"  /\ line "jan"
          ]
      ]
  }

february :: {| Identified + LangInfo' + Month + () }
february = withDefMonth
  { id:        sid "february"
  , enum:      Date.February
  , flower:    grassWidow.id
  , theme:     withDefTheme
      { text:      \_ -> rgb 0xb3 0xe3 0xda -- #b3e3da
      , highlight: \_ -> rgb 0xa6 0x70 0x95 -- #a67095
      , dawn:      pink
      , day:       cornflower
      , dusk:      lilac
      }
  , temp:      { min: -1, max: 1 }
  , timeOfDay: { dawn:  hhmm  6 30
               , day:   hhmm  8  0
               , dusk:  hhmm 17 15
               , night: hhmm 18 30
               }
  , tempCurve: \ratio -> 0.25 + ratio * 0.5
  , weather:   asHashMap 
      [ Weather.blizzard.id       /\ 0.1
      , Weather.bloodRain.id      /\ 0.05
      , Weather.clear.id          /\ 1.5
      , Weather.fog.id            /\ 0.2
      , Weather.lightRain.id      /\ 0.3
      , Weather.heavyRain.id      /\ 0.1
      , Weather.snow.id           /\ 0.5
      , Weather.lightningStorm.id /\ 0.2
      , Weather.warpstorm.id      /\ 0.005
      , Weather.windstorm.id      /\ 0.05
      , Weather.windy.id          /\ 0.3
      ]
  , lines:     asLines
      [ english /\
          [ "name"   /\ line "February"
          , "short"  /\ line "Feb"
          , "symbol" /\ line "🏔️"
          ]
      , esperanto /\
          [ "name"   /\ line "februaro"
          , "short"  /\ line "feb"
          ]
      ]
  }

march :: {| Identified + LangInfo' + Month + () }
march = withDefMonth
  { id:        sid "march"
  , enum:      Date.March
  , flower:    sagebrushViolet.id
  , theme:     withDefTheme
      { text:      \_ -> rgb 0xb0 0xeb 0x93 -- #b0eb93
      , highlight: \_ -> rgb 0x52 0x90 0x56 -- #529056
      , day:       green
      , dusk:      lilac
      , night:     navy
      }
  , temp:      { min: -1, max: 2 }
  , timeOfDay: { dawn:  hhmm  6  0
               , day:   hhmm  7  0
               , dusk:  hhmm 17 45
               , night: hhmm 19 15
               }
  , tempChangeRate: 0.66
  , tempCurve:      \ratio -> 0.1 + ratio * 0.7
  , weather:   asHashMap
      [ Weather.bloodRain.id      /\ 0.1
      , Weather.clear.id          /\ 1.2
      , Weather.fog.id            /\ 0.2
      , Weather.lightRain.id      /\ 0.5
      , Weather.heavyRain.id      /\ 0.4
      , Weather.snow.id           /\ 0.1
      , Weather.lightningStorm.id /\ 0.4
      , Weather.warpstorm.id      /\ 0.05
      , Weather.windstorm.id      /\ 0.1
      , Weather.windy.id          /\ 0.3
      ]
  , lines:     asLines
      [ english /\
          [ "name"   /\ line "March"
          , "short"  /\ line "Mar"
          , "symbol" /\ line "🍀"
          ]
      , esperanto /\
          [ "name"   /\ line "marto"
          , "short"  /\ line "mar"
          ]
      ]
  }

april :: {| Identified + LangInfo' + Month + () }
april = withDefMonth
  { id:        sid "april"
  , enum:      Date.April
  , flower:    dandelion.id
  , theme:     withDefTheme
      { text:      \_ -> rgb 0xb3 0xe3 0xda -- #b3e3da
      , highlight: \_ -> rgb 0x60 0x9a 0xc6 -- #609ac6
      , dawn:      pink
      , day:       lime
      , night:     navy
      }
  , temp:      { min: 0, max: 2 }
  , timeOfDay: { dawn:  hhmm  5  0
               , day:   hhmm  6 30
               , dusk:  hhmm 18 30
               , night: hhmm 20  0
               }
  , tempChangeRate: 0.5
  , tempCurve:      \ratio -> 0.25 + ratio * 0.5
  , weather:   asHashMap
      [ Weather.bloodRain.id      /\ 0.2
      , Weather.clear.id          /\ 1.0
      , Weather.lightRain.id      /\ 0.7
      , Weather.heavyRain.id      /\ 0.5
      , Weather.lightningStorm.id /\ 0.4
      , Weather.warpstorm.id      /\ 0.05
      , Weather.windstorm.id      /\ 0.2
      , Weather.windy.id          /\ 0.5
      ]
  , lines:     asLines
      [ english /\
          [ "name"   /\ line "April"
          , "short"  /\ line "Apr"
          , "symbol" /\ line "☔"
          ]
      , esperanto /\
          [ "name"   /\ line "aprilo"
          , "short"  /\ line "apr"
          ]
      ]
  }

may :: {| Identified + LangInfo' + Month + () }
may = withDefMonth
  { id:        sid "may"
  , enum:      Date.May
  , flower:    balsamroot.id
  , theme:     withDefTheme
      { text:      \_ -> rgb 0xff 0xf7 0xa0 -- #fff7a0
      , highlight: \_ -> rgb 0xcd 0x5f 0x61 -- #cd5f61
      , dawn:      cornflower
      , dusk:      red
      , night:     navy
      }
  , temp:      { min: 1, max: 3 }
  , timeOfDay: { dawn:  hhmm  4 30
               , day:   hhmm  6  0
               , dusk:  hhmm 20  0
               , night: hhmm 21 30
               }
  , tempCurve: \ratio -> 0.4 + ratio * 0.2
  , weather:   asHashMap
      [ Weather.bloodRain.id      /\ 0.075
      , Weather.clear.id          /\ 1.0
      , Weather.insectSwarm.id    /\ 0.05
      , Weather.lightRain.id      /\ 0.7
      , Weather.heavyRain.id      /\ 0.4
      , Weather.lightningStorm.id /\ 0.2
      , Weather.warpstorm.id      /\ 0.01
      , Weather.windstorm.id      /\ 0.2
      , Weather.windy.id          /\ 0.6
      ]
  , tempChangeRate: 0.5
  , lines:     asLines
      [ english /\
          [ "name"   /\ line "May"
          , "short"  /\ line "May"
          , "symbol" /\ line "🌼"
          ]
      , esperanto /\
          [ "name"   /\ line "majo"
          , "short"  /\ line "maj"
          ]
      ]
  }

june :: {| Identified + LangInfo' + Month + () }
june = withDefMonth
  { id:        sid "june"
  , enum:      Date.June
  , flower:    lupine.id
  , theme:     withDefTheme
      { text:      \_ -> rgb 0xb0 0xeb 0x93 -- #b0eb93
      , highlight: \_ -> rgb 0x25 0xa1 0x94 -- #25a194
      , dawn:      cornflower
      , night:     lilac
      }
  , temp:      { min: 1, max: 4 }
  , timeOfDay: { dawn:  hhmm  4  0
               , day:   hhmm  5 45
               , dusk:  hhmm 21  0
               , night: hhmm 22 30
               }
  , tempCurve: \ratio -> 0.4 + ratio * 0.2
  , weather:   asHashMap
      [ Weather.bloodRain.id      /\ 0.05
      , Weather.clear.id          /\ 2.0
      , Weather.insectSwarm.id    /\ 0.05
      , Weather.lightRain.id      /\ 0.1
      , Weather.heavyRain.id      /\ 0.2
      , Weather.lightningStorm.id /\ 0.1
      , Weather.warpstorm.id      /\ 0.01
      , Weather.windstorm.id      /\ 0.2
      , Weather.windy.id          /\ 0.3
      ]
  , lines:     asLines
      [ english /\
          [ "name"   /\ line "June"
          , "short"  /\ line "Jun"
          , "symbol" /\ line "🌲"
          ]
      , esperanto /\
          [ "name"   /\ line "june"
          , "short"  /\ line "jun"
          ]
      ]
  }

july :: {| Identified + LangInfo' + Month + () }
july = withDefMonth
  { id:        sid "july"
  , enum:      Date.July
  , flower:    yarrow.id
  , theme:     withDefTheme
      { text:      \_ -> rgb 0xff 0xf7 0xa0 -- #fff7a0
      , highlight: \_ -> rgb 0xcd 0x5f 0x61 -- #cd5f61
      , dawn:      cornflower
      , night:     lilac
      }
  , temp:      { min: 2, max: 4 }
  , timeOfDay: { dawn:  hhmm  4 15
               , day:   hhmm  6  5
               , dusk:  hhmm 20 45
               , night: hhmm 22 15
               }
  , weather:   asHashMap
      [ Weather.bloodRain.id      /\ 0.05
      , Weather.clear.id          /\ 2.0
      , Weather.insectSwarm.id    /\ 0.05
      , Weather.lightRain.id      /\ 0.1
      , Weather.heavyRain.id      /\ 0.05
      , Weather.lightningStorm.id /\ 0.1
      , Weather.mirage.id         /\ 0.05
      , Weather.wildfires.id      /\ 0.4
      , Weather.smoke.id          /\ 0.1
      , Weather.warpstorm.id      /\ 0.01
      , Weather.windstorm.id      /\ 0.1
      , Weather.windy.id          /\ 0.1
      ]
  , tempChangeRate: 0.2
  , lines:     asLines
      [ english /\
          [ "name"   /\ line "July"
          , "short"  /\ line "Jul"
          , "symbol" /\ line "🏖️"
          ]
      , esperanto /\
          [ "name"   /\ line "julio"
          , "short"  /\ line "jul"
          ]
      ]
  }

august :: {| Identified + LangInfo' + Month + () }
august = withDefMonth
  { id:        sid "august"
  , enum:      Date.August
  , flower:    honeysuckle.id
  , theme:     withDefTheme
      { text:      \_ -> rgb 0xd9 0xc8 0xbf -- #d9c8bf
      , highlight: \_ -> rgb 0xc1 0x84 0x76 -- #c18476
      }
  , temp:      { min: 2, max: 4 }
  , timeOfDay: { dawn:  hhmm  5  0
               , day:   hhmm  6 30
               , dusk:  hhmm 20 15
               , night: hhmm 21 15
               }
  , weather:   asHashMap
      [ Weather.bloodRain.id      /\ 0.025
      , Weather.clear.id          /\ 2.0
      , Weather.insectSwarm.id    /\ 0.1
      , Weather.lightRain.id      /\ 0.1
      , Weather.heavyRain.id      /\ 0.05
      , Weather.lightningStorm.id /\ 0.15
      , Weather.mirage.id         /\ 0.05
      , Weather.smoke.id          /\ 0.3
      , Weather.warpstorm.id      /\ 0.01
      , Weather.wildfires.id      /\ 0.6
      , Weather.windstorm.id      /\ 0.2
      , Weather.windy.id          /\ 0.2
      ]
  , lines:     asLines
      [ english /\
          [ "name"   /\ line "August"
          , "short"  /\ line "Aug"
          , "symbol" /\ line "⛺"
          ]
      , esperanto /\
          [ "name"   /\ line "aŭgusto"
          , "short"  /\ line "aŭg"
          ]
      ]
  }

september :: {| Identified + LangInfo' + Month + () }
september = withDefMonth
  { id:        sid "september"
  , enum:      Date.September
  , flower:    squashBlossom.id
  , theme:     withDefTheme
      { text:      \_ -> rgb 0xff 0xaa 0x84 -- #ffaa84
      , highlight: \_ -> rgb 0xe5 0x7e 0x7c -- #e57e6c
      , dawn:      pink
      , dusk:      clay
      }
  , temp:      { min: 0, max: 2 }
  , timeOfDay: { dawn:  hhmm  5 45
               , day:   hhmm  7  0
               , dusk:  hhmm 19 30
               , night: hhmm 20 30
               }
  , tempChangeRate: 0.5
  , tempCurve:      \ratio -> 0.9 - ratio * 0.7
  , weather:   asHashMap
      [ Weather.bloodRain.id      /\ 0.1
      , Weather.clear.id          /\ 1.3
      , Weather.insectSwarm.id    /\ 0.05
      , Weather.lightRain.id      /\ 0.3
      , Weather.heavyRain.id      /\ 0.15
      , Weather.lightningStorm.id /\ 0.2
      , Weather.mirage.id         /\ 0.1
      , Weather.smoke.id          /\ 0.2
      , Weather.warpstorm.id      /\ 0.05
      , Weather.wildfires.id      /\ 0.1
      , Weather.windstorm.id      /\ 0.3
      , Weather.windy.id          /\ 1.3
      ]
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 0.5 ] -- almost spooky month
  , lines:     asLines
      [ english /\
          [ "name"   /\ line "September"
          , "short"  /\ line "Sep"
          , "symbol" /\ line "🍂"
          ]
      , esperanto /\
          [ "name"   /\ line "septembro"
          , "short"  /\ line "sep"
          ]
      ]
  }

october :: {| Identified + LangInfo' + Month + () }
october = withDefMonth
  { id:        sid "october"
  , enum:      Date.October
  , flower:    grapeSprig.id
  , theme:     withDefTheme
      { text:      \_ -> rgb 0x6c 0x56 0x71 -- #6c5671
      , highlight: \_ -> rgb 0xdf 0x9b 0x54 -- #df9b54
      , dawn:      pink
      , day:       orange
      , dusk:      purple
      , night:     lavender
      }
  , temp:      { min: -1, max: 1 }
  , timeOfDay: { dawn:  hhmm  6 30
               , day:   hhmm  8  0
               , dusk:  hhmm 17 45
               , night: hhmm 19 30
               }
  , tempCurve: \ratio -> 0.75 - ratio * 0.5
  , weather:   asHashMap
      [ Weather.bloodRain.id      /\ 0.2
      , Weather.clear.id          /\ 1.0
      , Weather.insectSwarm.id    /\ 0.075
      , Weather.lightRain.id      /\ 0.2
      , Weather.heavyRain.id      /\ 0.1
      , Weather.mirage.id         /\ 0.2
      , Weather.snow.id           /\ 0.1
      , Weather.lightningStorm.id /\ 0.2
      , Weather.warpstorm.id      /\ 0.08
      , Weather.windstorm.id      /\ 0.1
      , Weather.windy.id          /\ 1.0
      ]
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 1.0 ] -- spooky month
  , lines:     asLines
      [ english /\
          [ "name"   /\ line "October"
          , "short"  /\ line "Oct"
          , "symbol" /\ line "🎃"
          ]
      , esperanto /\
          [ "name"   /\ line "oktobro"
          , "short"  /\ line "okt"
          ]
      ]
  }

november :: {| Identified + LangInfo' + Month + () }
november = withDefMonth
  { id:        sid "november"
  , enum:      Date.November
  , flower:    thistle.id
  , theme:     withDefTheme
      { text:      \_ -> rgb 0xb3 0xe3 0xda -- #b3e3da
      , highlight: \_ -> rgb 0xad 0x72 0x5a -- #ad725a
      , dawn:      cornflower
      , day:       cyan
      , dusk:      clay
      , night:     lavender
      }
  , temp:      { min: -2, max: 1 }
  , timeOfDay: { dawn:  hhmm  7  0
               , day:   hhmm  8 30
               , dusk:  hhmm 16 45
               , night: hhmm 18 10
               }
  , tempCurve: \ratio -> 0.75 - ratio * 0.5
  , weather:   asHashMap
      [ Weather.bloodRain.id      /\ 0.1
      , Weather.clear.id          /\ 1.2
      , Weather.fog.id            /\ 0.25
      , Weather.lightRain.id      /\ 0.3
      , Weather.heavyRain.id      /\ 0.2
      , Weather.mirage.id         /\ 0.3
      , Weather.snow.id           /\ 0.5
      , Weather.lightningStorm.id /\ 0.2
      , Weather.warpstorm.id      /\ 0.2
      , Weather.windstorm.id      /\ 0.3
      , Weather.windy.id          /\ 0.4
      ]
  , tempChangeRate: 0.45
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 0.5 ] -- winding down from spooky month
  , lines:     asLines
      [ english /\
          [ "name"   /\ line "November"
          , "short"  /\ line "Nov"
          , "symbol" /\ line "🕯️"
          ]
      , esperanto /\
          [ "name"   /\ line "novembro"
          , "short"  /\ line "nov"
          ]
      ]
  }

december :: {| Identified + LangInfo' + Month + () }
december = withDefMonth
  { id:        sid "december"
  , enum:      Date.December
  , flower:    snowberryBouquet.id
  , theme:     withDefTheme
      { text:      \_ -> rgb 0xff 0xf7 0xe4 -- #fff7e4
      , highlight: \_ -> rgb 0x69 0x8d 0xdc -- #698ddc
      , dawn:      cornflower
      , day:       cyan
      , dusk:      lilac
      , night:     navy
      }
  , temp:      { min: -2, max: 0 }
  , timeOfDay: { dawn:  hhmm  7 30
               , day:   hhmm  9  0
               , dusk:  hhmm 16  0
               , night: hhmm 17 15
               }
  , weather:   asHashMap
      [ Weather.blizzard.id  /\ 0.3
      , Weather.clear.id     /\ 1.3
      , Weather.snow.id      /\ 1.0
      , Weather.windstorm.id /\ 0.1
      , Weather.windy.id     /\ 0.1
      ]
  , lines:     asLines
      [ english /\
          [ "name"   /\ line "December"
          , "short"  /\ line "Dec"
          , "symbol" /\ line "☃️"
          ]
      , esperanto /\
          [ "name"   /\ line "decembro"
          , "short"  /\ line "dec"
          ]
      ]
  }