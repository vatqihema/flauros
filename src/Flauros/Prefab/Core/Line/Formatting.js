export const toLocaleCurrency = locale => ccy => num => num.toLocaleString(locale, { style: 'currency', currency: ccy })
export const toLocaleCurrencyNoFraction = locale => ccy => num => num.toLocaleString(locale, { style: 'currency', currency: ccy, minimumFractionDigits: 0 })
export const toLocaleString = locale => num => num.toLocaleString(locale)