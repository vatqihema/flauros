module Flauros.Prefab.Core.Line.Formatting where

import Prelude

import Data.Array as Array
import Data.Enum (class BoundedEnum, fromEnum)
import Data.Int (floor, round, toNumber)
import Data.Maybe (Maybe(..), fromMaybe, fromMaybe')
import Data.String as String
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Data.Time (getDay, getMonth, getYear)
import Flauros.Lang (LineParam(..), Translator(..), TranslatorWith(..), asLines, english, esperanto, line, paramFail)
import Flauros.Lang.Text ((&))
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, padBefore, sid, transmute, withDefTags)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

-- | I break purity :(
foreign import toLocaleCurrency :: String -> String -> Number -> String
foreign import toLocaleCurrencyNoFraction :: String -> String -> Number -> String
-- | me too :(
foreign import toLocaleString :: String -> Number -> String

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.line date
  register Regs.line name
  register Regs.line number

pad :: forall e. BoundedEnum e => Int -> e -> String
pad n  = padBefore n '0' <<< show <<< fromEnum 

date :: {| Identified + LangInfo' + () }
date = withDefTags
  { id:       sid "date"
  , lines:    asLines
      [ english /\
          [ "yyyy-mm-dd" /\ line case _ of
              [PDate d]     -> pad 4 (getYear d) & "-" & pad 2 (getMonth d) & "-" & pad 2 (getDay d)
              [PDateTime d] -> pad 4 (getYear d) & "-" & pad 2 (getMonth d) & "-" & pad 2 (getDay d)
              p        -> paramFail "date/yyyy-mm-dd" p
          ]
      ]
  }

name :: {| Identified + LangInfo' + () }
name = withDefTags
  { id:       sid "name"
  , lines:    asLines
      [ english /\
          [ "family" /\ line case _ of
              [ PName n ] -> transmute case n.format of
                  "family-given" -> fromMaybe "" (Array.head n.names)
                  "first-last"   -> fromMaybe "" (Array.last n.names)
                  _              -> String.joinWith " " n.names
              ps -> paramFail "name/family" ps
          , "full" /\ line \(Translator translate) (TranslatorWith translateWith) ps -> case ps of
              [ PName n ] -> case n.format of
                  "family-given" -> transmute $ String.joinWith " " $ Array.catMaybes [ Array.last n.names, Array.head n.names ]
                  "first-last"   -> transmute $ String.joinWith " " $ Array.catMaybes [ Array.head n.names, Array.last n.names ]
                  "title"        -> fromMaybe' (\_ -> paramFail "name/family" ps) do
                    title  <- n.names Array.!! 0
                    format <- n.names Array.!! 0
                    let names = Array.take 2 n.names
                    Just $ translateWith (sid "name")  "given" [PName { format, names }]
                         & " the "
                         & translate     (sid "title") title
                  _              -> transmute $ String.joinWith " " n.names
              _ -> paramFail "name/family" ps
          , "given" /\ line \(Translator translate) (TranslatorWith translateWith) ps -> case ps of
              [ PName n ] -> case n.format of
                  "family-given" -> transmute $ fromMaybe "" (Array.last n.names)
                  "first-last"   -> transmute $ fromMaybe "" (Array.head n.names)
                  "title"        -> fromMaybe' (\_ -> paramFail "name/family" ps) do
                    title  <- n.names Array.!! 0
                    format <- n.names Array.!! 0
                    let names = Array.take 2 n.names
                    Just $ translate     (sid "title") title
                         & " "
                         & translateWith (sid "name")  "given" [PName { format, names }]
                  _              -> transmute $ String.joinWith " " n.names
              _ -> paramFail "name/family" ps
          ]
      ]
  }

englishOrdinal :: Int -> String
englishOrdinal i
  | 1 == (i `mod` 10) && i /= 11 = show i <> "st"
  | 2 == (i `mod` 10) && i /= 12 = show i <> "nd"
  | 3 == (i `mod` 10) && i /= 13 = show i <> "rd"
  | otherwise                    = show i <> "th"

esperantoOrdinal :: Int -> String
esperantoOrdinal i = show i <> "-a"

number :: {| Identified + LangInfo' + () }
number = withDefTags
  { id:       sid "number"
  , lines:    asLines
      [ english /\
          [ "currency" /\ line case _ of
              [PInt n] -> transmute $ toLocaleCurrency "en-US" "USD" (toNumber n)
              [PNum n] -> transmute $ toLocaleCurrency "en-US" "USD" n
              p        -> paramFail "number/currency" p
          , "currency-no-fraction" /\ line case _ of
              [PInt n] -> transmute $ toLocaleCurrencyNoFraction "en-US" "USD" (toNumber n)
              [PNum n] -> transmute $ toLocaleCurrencyNoFraction "en-US" "USD" $ toNumber $ round n
              p        -> paramFail "number/currency-no-fraction" p
          , "ordinal" /\ line case _ of
                [PInt i] -> englishOrdinal i
                [PNum n] -> englishOrdinal $ floor n
                p        -> "Unknown number " <> show p
          ]
      , esperanto /\
          [ "currency" /\ line case _ of
              [PInt n] -> toLocaleString "pol" (toNumber n / 100.0) & "₷"   -- la spesmilo!
              p        -> paramFail "number/currency" p
          , "ordinal" /\ line case _ of
                [PInt i] -> esperantoOrdinal i
                [PNum n] -> esperantoOrdinal $ floor n
                p        -> "Nekonaka nombro " <> show p
          ]
      ]
  }
