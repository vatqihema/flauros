module Flauros.Prefab.Core.Line.Environment where

import Prelude

import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Actor (playerID)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (LineParam(..), asLines, english, esperanto, line, paramFail)
import Flauros.Mechanics.Fact as Fact
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, sid, transmute, withDefTags)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.line cityInfo
  register Regs.line temperature

cityInfo :: {| Identified + LangInfo' + () }
cityInfo = withDefTags
  { id:       sid "city-info"
  , lines:    asLines
      [ english /\
          [ "default-name" /\ line "Palecaide"
          , "name"         /\ line (Fact.text playerID (sid "city"))
          ]
      ]
  }

temperature :: {| Identified + LangInfo' + () }
temperature = withDefTags
  { id:       sid "temperature"
  , lines:    asLines
      [ english /\
          [ "tagline" /\ line case _ of
              [ PInt temp ] -> transmute $ case temp of
                t | t <= -4 -> "arctic"
                t | t <= -3 -> "frigid"
                t | t <= -2 -> "freezing"
                t | t <= -1 -> "chilly"
                t | t <=  0 -> "cool"
                t | t <=  1 -> "warm"
                t | t <=  2 -> "hot"
                t | t <=  4 -> "sweltering"
                _           -> "oppresive heat"
              p -> paramFail "temperature/tagline" p
          ]
      , esperanto /\
          [ "tagline" /\ line case _ of
              [ PInt temp ] -> transmute $ case temp of
                t | t <= -4 -> "arkta"
                t | t <= -3 -> "frostega"
                t | t <= -2 -> "frosta"
                t | t <= -1 -> "friska"
                t | t <=  0 -> "malwarmeta"
                t | t <=  1 -> "varma"
                t | t <=  2 -> "vermega"
                t | t <=  4 -> "bruliganta"
                _           -> "sufoka varmeco"
              p -> paramFail "temperature/tagline" p
          ]
      ]
  }