module Flauros.Prefab.Core.Line.WorldPanel where

import Prelude

import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (Line, LineParam(..), asLines, english, esperanto, line, paramFail)
import Flauros.Lang.Text (Text(..), color, outlinedIcon, roundedIcon, tfont, (&))
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, prettyShowID, sid, transmute, withDefTags)
import Flauros.Style.Color (green, lilac, purple, red)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.line tabs

mkTabTitle :: forall s. Text -> Text -> Line s
mkTabTitle whenSelected whenNot = line case _ of
  [_, PBool selected] -> if selected then whenSelected else whenNot
  p                   -> paramFail "world-tab" p

tabs :: {| Identified + LangInfo' + () }
tabs = withDefTags
  { id:       sid "world-tab"
  , lines:    asLines
      [ english /\
          [ "terminal" /\ mkTabTitle
              (color purple & roundedIcon "auto_stories" & tfont "Caveat" & "terminal")
              (color purple & roundedIcon "auto_stories")
          , "settings" /\ line (color green & roundedIcon "settings" & tfont "Ubuntu Mono" & "Settings")
          , "unknown" /\ line case _ of
              [PID id, PBool selected] -> case selected of
                false -> color red & outlinedIcon "not_listed_location"
                true  -> color red & "missing " & prettyShowID id
              p -> paramFail "world-tab" p
          ]
      ]
  }