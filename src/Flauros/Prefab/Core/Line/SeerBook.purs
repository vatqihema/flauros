module Flauros.Prefab.Core.Line.SeerBook where

import Prelude

import Data.Array (replicate)
import Data.Date as Date
import Data.Enum (fromEnum)
import Data.Foldable (foldl)
import Data.Int (round, toNumber)
import Data.Maybe (Maybe(..))
import Data.Number as Number
import Data.String as String
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Actor (playerID)
import Flauros.Data.Knowledge (KnowVal(..), asNumber)
import Flauros.Data.Month (monthID)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Data.Time (getAgeInYears, getDate, getDay, getDayOfWeek, getHour, getMinute, getMonth, getYear, weekdayStr)
import Flauros.Data.Weather (weatherOn)
import Flauros.Lang (LineParam(..), Translator(..), TranslatorWith(..), asLines, english, esperanto, line, paramFail)
import Flauros.Lang.Text (Text, color, mapText, outlinedIcon, plain, roundedIcon, tooltip, tsvg, whenText, (&))
import Flauros.Mechanics.Fact as Fact
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, padBefore, sid, transmute, withDefTags)
import Flauros.Style.Color (blue, cornflower, gold, green, orange, pink, purple, red, redder, white, yellow)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.line apartmentBrowser
  register Regs.line calendar
  register Regs.line dayOfWeek
  register Regs.line dayOfWeekAbbr
  register Regs.line inventoryFilterIcons
  register Regs.line profile
  register Regs.line statusBar
  register Regs.line tabTitle
  register Regs.line tabIcon

apartmentBrowser :: {| Identified + LangInfo' + () }
apartmentBrowser = withDefTags
  { id:       sid "apartment-browser"
  , lines:    asLines
      [ english /\
          [ "high-quality" /\ line case _ of
              [ PNum quality ] -> transmute $ tooltip
                (tsvg "Laurel crown" 24.0 & "")
                ("These apartments have been certified noble based on an average score of " <> show ((Number.round $ quality * 100.0) / 100.0) <> " / 5")
              ps -> paramFail "apartment-browser/high-quality" ps
          , "monthly-rent" /\ line \(TranslatorWith translateWith) -> case _ of
              [PNum mn, PNum mx] ->
                if mn == mx
                then translateWith (sid "number") "currency-no-fraction" [PNum mn]
                else translateWith (sid "number") "currency-no-fraction" [PNum mn] & " to " & translateWith (sid "number") "currency-no-fraction" [PNum mx]
              ps -> paramFail "apartment-browser/monthly-rent" ps
          , "stars" /\ line case _ of
              [PNum quality] ->
                let i = round $ quality * 2.0
                    whole = i / 2
                    half  = i `mod` 2
                 in transmute $
                    tooltip ( foldl (&) mempty (replicate whole (outlinedIcon "hotel_class"))
                            & (if half /= 0 then transmute $ outlinedIcon "star_half" else mempty :: Text)
                            ) ( "Rating: " <> show (toNumber (round (quality * 100.0)) / 100.0) <> " / 5" )
              ps -> paramFail "apartment-browser/stars" ps
          , "units-available" /\ line case _ of
              [PInt n] -> show n & " Available Unit" & (if n /= 1 then "s" else "")
              ps       -> paramFail "apartment-browser/stars" ps
          ]
      ]
  }

calendar :: {| Identified + LangInfo' + () }
calendar = withDefTags
  { id:       sid "calendar"
  , lines:    asLines
      [ english /\
          [ "date-title" /\ line \(DS.Store { currentTime }) (Translator translate) (TranslatorWith translateWith) -> case _ of
                [PDateTime dt] -> plain (if getDate dt == getDate currentTime then "Today, " else "")
                               <> translate (sid "day-of-week") (weekdayStr $ getDayOfWeek dt)
                               <> plain " the "
                               <> translateWith (sid "number") "ordinal" (pure (PInt $ fromEnum $ getDay dt))
                p            -> plain $ "Unknown " <> show p
          , "filter"       /\ line "Filter events"
          , "notes"        /\ line "Notes"
          , "month-last"   /\ line "Last month"
          , "month-next"   /\ line "Next month"
          , "today"        /\ line "Today"
          , "weather-forecast" /\ line \(DS.Store { currentTime, weatherHistory }) (Translator translate) (TranslatorWith translateWith) -> case _ of
                [PDate day] -> case weatherOn day weatherHistory of
                  Nothing  -> mempty
                  Just row -> translate row.weather "icon"
                           <> (whenText (day > getDate currentTime) \_ -> plain (show row.chance) <> plain "%" )
                           <> plain " "
                           <> translate row.weather "long"
                           <> plain ", "
                           <> translateWith (sid "temperature") "tagline" [ PInt row.temperature ]
                ps -> plain $ "Invalid params " <> show ps
          ]
      , esperanto /\
          [ "date-title"   /\ line \(DS.Store { currentTime }) param (Translator translate) (TranslatorWith translateWith) -> case param of
                [PDateTime dt] -> plain (if getDate dt == getDate currentTime then "Hodiaŭ, " else "")
                               <> translate (sid "day-of-week") (weekdayStr $ getDayOfWeek dt)
                               <> plain " la "
                               <> translateWith (sid "number") "ordinal" (pure (PInt $ fromEnum $ getDay dt))
                p -> plain $ "Nekonaka dato " <> show p
          , "filter"       /\ line "Filtri eventojn"
          , "notes"        /\ line "Komentoj"
          , "month-last"   /\ line "Venontmonate"
          , "month-next"   /\ line "Pasintmonate"
          , "today"        /\ line "Hodiaŭ"
          ]
      ]
  }

dayOfWeek :: {| Identified + LangInfo' + () }
dayOfWeek = withDefTags
  { id:       sid "day-of-week"
  , lines:    asLines
      [ english /\
          [ "monday"    /\ line "Monday"
          , "tuesday"   /\ line "Tuesday"
          , "wednesday" /\ line "Wednesday"
          , "thursday"  /\ line "Thursday"
          , "friday"    /\ line "Friday"
          , "saturday"  /\ line "Saturday"
          , "sunday"    /\ line "Sunday"
          ]
      , esperanto /\
          [ "monday"    /\ line "lundo"
          , "tuesday"   /\ line "mardo"
          , "wednesday" /\ line "merkredo"
          , "thursday"  /\ line "ĵaŭdo"
          , "friday"    /\ line "vendredo"
          , "saturday"  /\ line "sabato"
          , "sunday"    /\ line "dimanĉo"
          ]
      ]
  }

dayOfWeekAbbr :: {| Identified + LangInfo' + () }
dayOfWeekAbbr = withDefTags
  { id:       sid "day-of-week-abbr"
  , lines:    asLines
      [ english /\
          [ "monday"    /\ line "Mon"
          , "tuesday"   /\ line "Tue"
          , "wednesday" /\ line "Wed"
          , "thursday"  /\ line "Thu"
          , "friday"    /\ line "Fri"
          , "saturday"  /\ line "Sat"
          , "sunday"    /\ line "Sun"
          ]
      , esperanto /\
          [ "monday"    /\ line "lun"
          , "tuesday"   /\ line "mar"
          , "wednesday" /\ line "mer"
          , "thursday"  /\ line "ĵaŭ"
          , "friday"    /\ line "ven"
          , "saturday"  /\ line "sab"
          , "sunday"    /\ line "dim"
          ]
      ]
  }

profile :: {| Identified + LangInfo' + () }
profile = withDefTags
  { id:       sid "profile"
  , lines:    asLines
      [ english /\
          [ "address"        /\ line \store -> color _.yellow
                                             & tooltip ( Fact.text playerID (sid "address") store
                                                       & ", "
                                                       & Fact.text playerID (sid "city") store
                                                       ) "921945 Khemitzas, Althesia"
          , "ancestry"       /\ line \(Translator translate) -> case _ of
              [PID id] -> mapText String.toUpper (color _.red & "ANC "& translate id "name")
              ps       -> paramFail "profile/ancestry" ps
          , "artichoke"      /\ line "The Golden Artichoke Emblem"
          , "artichoke-desc" /\ line "A symbol of Althesian excellence. You're not quite sure why."
          , "birthday" /\ line \(Translator translate) (TranslatorWith translateWith) store@(DS.Store { currentTime }) -> case DS.withActor playerID _.birthday store of
              Just dt -> ( tooltip ( "BD "
                                   & translate (monthID $ getMonth dt) "name"
                                   & " "
                                   & translateWith (sid "number") "ordinal" [PInt $ fromEnum $ getDay dt]
                                   & ", "
                                   & show (fromEnum (getYear dt))
                                   & " ("
                                   & translateWith (sid "date") "yyyy-mm-dd" [PDate dt]
                                   & ")"
                                   )
                                   (show (getAgeInYears currentTime dt) <> " years old")
                         ) & ""
              Nothing -> plain "Unknown birthday"
          , "license" /\ line \st -> (color blue & "ID# " & Fact.text playerID (sid "license") st)
          , "nationality" /\ line \store (Translator translate) ->
              color blue & case Fact.val playerID (sid "nationality") store of 
                 KnowID id -> translate id "name"
                 f         -> "player/nationality expected ID, found " & show (asNumber f)
          ]
      ]
  }

statusBar :: {| Identified + LangInfo' + () }
statusBar = withDefTags
  { id:       sid "status-bar"
  , lines:    asLines
      [ english /\
          [ "pretty" /\ line \(Translator translate) (TranslatorWith translateWith) -> case _ of
                [PDateTime dt] -> padBefore 2 '0' (show (fromEnum (getHour dt)))
                                & ":"
                                & padBefore 2 '0' (show (fromEnum (getMinute dt)))
                                & " "
                                & translate (sid "day-of-week-abbr") (weekdayStr (Date.weekday $ getDate dt))
                                & ", "
                                & translate (monthID $ getMonth dt) "short"
                                & " "
                                & translateWith (sid "number") "ordinal" [PInt (fromEnum (getDay dt))]
                p              -> paramFail "status-bar/pretty" p
          ]
      ]
  }

-- | Group of all available icons to be chosen for a new user created filter
-- Colors should be randomized on component picker load
inventoryFilterIcons :: {| Identified + LangInfo' + () }
inventoryFilterIcons = withDefTags
  { id:       sid "filter-icon"
  , lines:    asLines $
      let mkIcon name = name /\ line (outlinedIcon name)
       in [ english /\ -- key name doesn't matter, always read as a group
              [ mkIcon "anchor"
              , mkIcon "balance"
              , mkIcon "bakery_dining"
              , mkIcon "bed"
              , mkIcon "bolt"
              , mkIcon "brush"
              , mkIcon "bubble_chart"
              , mkIcon "build"
              , mkIcon "cases"
              , mkIcon "chair"
              , mkIcon "coffee"
              , mkIcon "cookie"
              , mkIcon "cooking"
              , mkIcon "coronavirus"
              , mkIcon "cruelty_free"
              , mkIcon "cut"
              , mkIcon "delete"
              , mkIcon "diamond"
              , mkIcon "eco"
              , mkIcon "edit"
              , mkIcon "emoji_food_beverage"
              , mkIcon "fastfood"
              , mkIcon "favorite"
              , mkIcon "flag"
              , mkIcon "forest"
              , mkIcon "gavel"
              , mkIcon "hand_bones"
              , mkIcon "help"
              , mkIcon "house"
              , mkIcon "hive"
              , mkIcon "liquor"
              , mkIcon "local_bar"
              , mkIcon "local_florist"
              , mkIcon "luggage"
              , mkIcon "markunread_mailbox"
              , mkIcon "medical_services"
              , mkIcon "music_note"
              , mkIcon "open_with"
              , mkIcon "palette"
              , mkIcon "pedal_bike"
              , mkIcon "pets"
              , mkIcon "psychology"
              , mkIcon "potted_plant"
              , mkIcon "photocamera"
              , mkIcon "public"
              , mkIcon "rainy"
              , mkIcon "restaurant_menu"
              , mkIcon "rib_cage"
              , mkIcon "shopping_bag"
              , mkIcon "savings"
              , mkIcon "school"
              , mkIcon "send_time_extension"
              , mkIcon "severe_cold"
              , mkIcon "shield_moon"
              , mkIcon "smoking_rooms"
              , mkIcon "skull"
              , mkIcon "star_half"
              , mkIcon "support"
              , mkIcon "swords"
              , mkIcon "token"
              , mkIcon "toys_fan"
              , mkIcon "transgender"
              , mkIcon "vpn_key"
              , mkIcon "water_drop"
              , mkIcon "work"
              ]
          ]
  }

tabIcon :: {| Identified + LangInfo' + () }
tabIcon = withDefTags
  { id:       sid "seer-tab-icon"
  , lines:    asLines
      [ english /\
          [ "apartment-browser" /\ line (tooltip (color gold & outlinedIcon "house") "apartment shopping")
          , "bag"      /\ line (tooltip (color orange     & outlinedIcon "shopping_bag")   "handy haversack")
          , "calendar" /\ line (tooltip (color red        & outlinedIcon "calendar_month") "calendar")
          , "debug"    /\ line (tooltip (color redder     & outlinedIcon "bug_report")     "debug")
          , "messages" /\ line (tooltip (color purple     & outlinedIcon "chat")           "messages")
          , "news"     /\ line (tooltip (color yellow     & outlinedIcon "web")            "the news")
          , "profile"  /\ line (tooltip (color cornflower & outlinedIcon "account_box")    "profile")
          , "saves"    /\ line (tooltip (color pink       & outlinedIcon "bookmarks")      "saves")
          , "settings" /\ line (tooltip (color green      & outlinedIcon "settings")       "settings")
          ]
      , esperanto /\
          [ "apartment-browser" /\ line (tooltip (color gold & outlinedIcon "house") "apartmentbutikumo")
          , "bag"      /\ line (tooltip (color orange     & outlinedIcon "shopping_bag")   "utila ujo")
          , "calendar" /\ line (tooltip (color red        & outlinedIcon "calendar_month") "kalendaro")
          , "debug"    /\ line (tooltip (color redder     & outlinedIcon "bug_report")     "sencimigi")
          , "messages" /\ line (tooltip (color purple     & outlinedIcon "chat")           "sendito")
          , "news"     /\ line (tooltip (color yellow     & outlinedIcon "web")            "la novaĵoj")
          , "profile"  /\ line (tooltip (color cornflower & outlinedIcon "account_box")    "karakterizo")
          , "saves"    /\ line (tooltip (color pink       & outlinedIcon "bookmarks")      "konservoj")
          , "settings" /\ line (tooltip (color green      & outlinedIcon "settings")       "agordoj")
          ]
      ]
  }

tabTitle :: {| Identified + LangInfo' + () }
tabTitle = withDefTags
  { id:       sid "tab-title"
  , lines:    asLines
      [ english /\
          [ "apartment-browser" /\ line (color gold & "✤ « Apartments »")
          , "bag"      /\ line "handy haversack"
          , "calendar" /\ line "calendar"
          , "debug"    /\ line "🐞debug"
          , "messages" /\ line "messages"
          , "news"     /\ line "📰Þe News"
          , "profile"  /\ line "unused"
          , "saves"    /\ line "saves"
          , "settings" /\ line (roundedIcon "tune" & "Settings")
          ]
      , esperanto /\
          [ "apartment-browser" /\ line (color gold & "✤ « Apartamentoj »")
          , "bag"      /\ line "utila ujo"
          , "calendar" /\ line "kalendaro"
          , "debug"    /\ line "sencimigi"
          , "messages" /\ line "🐞sendito"
          , "news"     /\ line "📰L' Novaĵ'"
          , "profile"  /\ line "maluzata"
          , "saves"    /\ line "konservoj"
          , "settings" /\ line (color white & "agordoj")
          ]
      ]
  }