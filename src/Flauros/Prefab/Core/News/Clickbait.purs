module Flauros.Prefab.Core.News.Clickbait where

import Prelude

import Data.Int (odd)
import Data.Maybe (fromMaybe)
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Gen (Gen_, mkGenFns, withDefGen)
import Flauros.Data.News (NewsGen, NewsStory, Priority(..), withDefNews)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (LineParam(..), asLines, english, esperanto, line)
import Flauros.Lang.Text (color, (&))
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, sid)
import Flauros.Random (random, randomNth, randomRange)
import Flauros.Style.Color (purple)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.newsGen meteorShower

meteorShower :: {| Identified + LangInfo' + Gen_ {| Identified + NewsStory + () } + NewsGen DS.Store + () }
meteorShower = withDefGen
  { id:       sid "meteor-shower"
  , priority: \_ -> Normal
  , setup: mkGenFns
      { setup:  \_ _ -> do
          num   <- fromMaybe 5 <$> randomNth [4, 5, 6, 8, 10]
          comet <- random
          phr1  <- randomRange 0 2
          phr2  <- randomRange 0 2
          pure { num, comet, phr1, phr2 }
      , create: \id { num, comet, phr1, phr2 } -> withDefNews
          { id
          , headline: [PInt num, PBool comet, PInt phr1, PInt phr2]
          }
      }
  , lines:     asLines
      [ english /\
          [ "headline" /\ line case _ of
              [PInt i, PBool c, PInt phr1, PInt phr2] ->
                let loc1 = odd (phr1 + phr2)
                 in color purple
                  & (if c then "☄️" else "🌠")
                  & show i
                  & (if phr1 == 0 then "best"  else if phr1 == 1 then "awesome" else "unbelievable")
                  & " "
                  & (if loc1 then "spots" else "places")
                  & " to "
                  & (if phr2 == 0 then "watch" else if phr2 == 1 then "see"     else "view")
                  & " tomorrow's "
                  & (if c then "comets" else "meteor shower")
              p -> "Unknown "  & show p
          ]
      , esperanto /\ []
      ]
  }