module Flauros.Prefab.Core.News.PSA where

import Prelude

import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Gen (Gen_, mkGenFns, withDefGen)
import Flauros.Data.News (NewsGen, NewsStory, Priority(..), withDefNews)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Data.Weather (Season(..), season)
import Flauros.Lang (LineParam(..), asLines, english, esperanto, line, paramFail)
import Flauros.Lang.Text (clear, color, italics, outlinedIcon, (&))
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, sid)
import Flauros.Random (randomRange)
import Flauros.Style.Color (red)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.newsGen druidWatch

druidWatch :: {| Identified + LangInfo' + Gen_ {| Identified + NewsStory + () } + NewsGen DS.Store + () }
druidWatch = withDefGen
  { id:       sid "druid-watch"
  , priority: \_ -> Normal
  , setup: mkGenFns
      { setup:  \_ _ -> randomRange 0 9
      , create: \id i -> withDefNews
          { id
          , headline: [PInt i]
          }
      }
  , lines:     asLines
      [ english /\
          [ "headline" /\ line \(DS.Store { currentTime }) -> case _ of
              [PInt i] -> color red & outlinedIcon "report" & case i of
                0 -> "Druidcraft: if you see something, say something." & ""
                1 -> "Friendly neighborhood gardener, or sinister druid saboteur? Don't get fooled." & ""
                2 -> "Suspect a neighbor of druidry? Don't wait until it's too late." & ""
                3 -> "Talk to your kids about responsible naturalism." & ""
                4 -> "Druids, in my neighborhood? It's more likely than you think. Know the signs." & ""
                5 -> "Remember, association with druids, treants, fey, and their ilk is strictly prohibited." & ""
                6 -> "Druidstry is a gateway to demonology and hard drugs. Talk to your children before " & italics & "they" & clear & " do."
                _ -> case season currentTime of
                  Spring -> "Druids also welcome the warmth of spring. Report any suspected activity." & ""
                  Summer -> "Hikers and campers are advised to watch for druidic activity." & ""
                  Fall   -> "Be alert! Harvest coincides with larger and more frequent druidic sabbaths." & ""
                  Winter -> "Even in the cold a druid's work does not stop. Don't lower your guard!" & ""
              p -> paramFail "druid-watch/headline" p
          ]
      , esperanto /\ []
      ]
  }