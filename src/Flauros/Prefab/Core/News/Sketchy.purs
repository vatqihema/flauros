module Flauros.Prefab.Core.News.Sketchy where

import Prelude

import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Gen (Gen_, mkGenFns, withDefGen)
import Flauros.Data.News (NewsGen, NewsStory, Priority(..), withDefNews)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (LineParam(..), asLines, english, esperanto, line, paramFail)
import Flauros.Lang.Text (color, (&))
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, sid)
import Flauros.Random (randomRange)
import Flauros.Style.Color (orange)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.newsGen sus

sus :: {| Identified + LangInfo' + Gen_ {| Identified + NewsStory + () } + NewsGen DS.Store + () }
sus = withDefGen
  { id:       sid "sus"
  , priority: \_ -> Never -- these are triggered, never random
  , setup: mkGenFns
      { setup:  \_ _ -> randomRange 0 43
      , create: \id idx -> withDefNews
          { id
          , headline: [PInt idx]
          }
      }
  , lines: asLines
      [ english /\
          [ "headline" /\ line case _ of
              [PInt idx] -> color orange & case idx of
                0  -> "Is it really just imposter syndrome?"
                1  -> "Why was that carriage parked outside for so long?"
                2  -> "They know."
                3  -> "They slumber in the depths. The awakening is soon."
                4  -> "When was the last time you were happy?"
                5  -> "Flesh is transient."
                6  -> "When are you going to figure it out?"
                7  -> "They hate you."
                8  -> "This is just a simulation."
                9  -> "How did you slip up? How did they find out?"
                10 -> "Do you think they approve of what you've been doing?"
                11 -> "Have your prayers been answered?"
                12 -> "You need to eat better."
                13 -> "Those sirens are for you."
                14 -> "You are not immune to propaganda."
                15 -> "Stop staying up so late."
                16 -> "Nobody's buying it."
                17 -> "You're absolutely transparent."
                18 -> "They've read all your messages."
                19 -> "Is this the life you wanted?"
                20 -> "Your clothes looks awful."
                21 -> "..."
                22 -> "What more don't you know?"
                23 -> "You need to work on your morning routine."
                24 -> "Don't look in the closet."
                25 -> "What was that thud?"
                26 -> "Are your doors locked?"
                27 -> "What lists are you on?"
                28 -> "You don't know how bad things really are."
                29 -> "That same carriage keeps passing by. What are they looking for?"
                30 -> "Your demise isn't far away."
                31 -> "Your vote doesn't matter."
                32 -> "Get some more sleep. You'll need it in the coming days."
                33 -> "The clock is ticking."
                34 -> "They don't trust you. Do you trust them?"
                35 -> "Drink more water."
                36 -> "If only you made better choices..."
                37 -> "I'd rethink that if I were you."
                38 -> "Are you paranoid or do you just need more sleep?"
                39 -> "Don't turn around."
                40 -> "Don't assume these can't be helpful."
                41 -> "They'll hear you."
                42 -> "And with strange aeons even death may die."
                _  -> "Tick tock. Tick tock."
              p -> paramFail "sus/headline" p
          ]
      , esperanto /\ []
      ]
  }