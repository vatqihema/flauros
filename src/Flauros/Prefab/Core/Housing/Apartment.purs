module Flauros.Prefab.Core.Housing.Apartment where

import Prelude

import Data.Array ((..))
import Data.Array as Array
import Data.Enum (fromEnum)
import Data.Foldable (foldl, sum)
import Data.FoldableWithIndex (foldlWithIndex)
import Data.FunctorWithIndex (mapWithIndex)
import Data.HashSet (HashSet)
import Data.HashSet as HS
import Data.Hashable (hash)
import Data.Int (round, toNumber)
import Data.List (List(..), (:))
import Data.Map (Map)
import Data.Map as Map
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Ord (abs)
import Data.String as String
import Data.Time.Duration (Days(..))
import Data.Traversable (for, traverse)
import Data.Tuple (Tuple(..))
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.FormList (FormRow, IDFormList, formlist, noMeta)
import Flauros.Data.Gen (Gen_, mkGenFns, withDefGen)
import Flauros.Data.Gen as Gen
import Flauros.Data.Item (inflationMult)
import Flauros.Data.Knowledge (KnowVal(..), defTranslatable, worldID)
import Flauros.Data.Location (Building, Condition(..), Housing, Location, RoomSize(..), RoomType(..), Surroundings(..), conditionRentMult, roomSizeRentMult, roomTypeBaseRent)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Data.Time (adjustDate, daysBetween, getDate)
import Flauros.Lang (Line, LineParam(..), TranslatorWith(..), asLines, english, line, paramFail)
import Flauros.Lang.Text (Text, color, plain, plainify, roundedIcon, (&))
import Flauros.Prefab.Core.Name.Address as Address
import Flauros.Prefab.Registries (FullHousing')
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (randomList, register, registerList)
import Flauros.Prefab.Registry as Registry
import Flauros.Prelude (Identified, isPrefixOf, justWhen, mapElemOrDef, nthByMod, occurrences, sid, transmute)
import Flauros.Random (curveRandom, random, randomEnum, randomNthElse, randomRange)
import Flauros.Style.Color as Color
import Halogen.Store.Monad (class MonadStore)
import Record as Record
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  registerList Regs.id apartmentOwners
  register Regs.housing starterApartment

apartmentOwners :: IDFormList () (id :: ())
apartmentOwners = formlist (sid "apartment-owners")
  [ noMeta $ sid "ænthem"
  , noMeta $ sid "berke-and-foehammer"
  , noMeta $ sid "catfiske"
  , noMeta $ sid "church-of-grace"
  , noMeta $ sid "redcappe"
  , noMeta $ sid "university"
  ]

-- Calculations

baseMonthlyRent :: forall r. {| Location + Building + Housing + r } -> DS.Store -> { unit :: String, floor :: Int } -> Number
baseMonthlyRent house (DS.Store { currentTime }) { floor } =
  let timeMult = 1.2 + inflationMult currentTime 3.5
      qualityMult = 0.95 + 0.05 * house.quality
      base = case house.surroundings of
        Gentrified -> 175.0
        Slum       -> 100.0
        Rural      -> 115.0
        _          -> 130.0
      roomMods = house.rooms <#> \r -> roomTypeBaseRent r.type * roomSizeRentMult r.size * conditionRentMult r.condition
      variation = 1.1 - 0.2 * (toNumber (hash house.address `mod` 100) / 100.0)
      lastRefurbishMult = min 1.0 $ max 0.4 $ 1.25 - (abs $ daysBetween house.lastRefurbished currentTime - 5.0 * 365.0) / (150.0 * 365.0)
      builtMult         = min 1.0 $ max 0.5 $ 1.25 - (abs $ daysBetween house.lastRefurbished currentTime - 20.0 * 365.0) / (300.0 * 365.0)
      floorMod =
        if   floor >= 0
        then (if house.floors.aboveground >= 10 then 1.0 else 1.0 - 0.015 * toNumber floor)
        else (if house.floors.basement    >= 10 then 1.0 else 1.0 + 0.03  * toNumber floor)
   in (base + sum roomMods) * timeMult * qualityMult * lastRefurbishMult * builtMult * floorMod * variation

-- Generation

starterApartment :: {| Identified + LangInfo' + Gen_ {| FullHousing' () } + () }
starterApartment =
  let id = sid "starter-apartment"
      streetID = Address.streetName.id
      nameColor hsh = fromMaybe _.yellow $ nthByMod hsh Color.allHighlightColors
      nameHash tw ps = hash $ plainify $ tw Address.streetName.id "name" ps
      gemName hsh = fromMaybe (plain "") (nthByMod hsh apartmentGemNames)
      numName hsh = fromMaybe "Un" $ nthByMod hsh [ "One", "Ein", "Uno", "Two", "Zwei", "Deux", "Three", "Tri", "Four", "Quad", "Five", "Six", "Seven", "Eight", "Okto", "Ocho", "Nine", "Eleven", "Twelve", "Þirteen", "Gio" ]
      mkName v = line \(TranslatorWith translateWith) ps -> 
        let txt = v translateWith ps
         in color (nameColor $ hash $ plainify txt) & txt
      letter hsh =
        let letters = ["Æ", "A", "Œ", "Ð", "E", "I", "O", "U", "V", "Y", "W", "Z", "Þ", "F", "Ȝ", "Ƿ", "C", "Kay", "Bee", "M", "H", "T", "S"]
         in fromMaybe "Æ" (nthByMod hsh letters)
         <> (if hsh `mod` 3 == 0 then fromMaybe "Æ" (nthByMod (hsh * 7 + 13) letters) else "")
      atName hsh = fromMaybe "Arcæne" $ nthByMod hsh apartmentBrandNames
   in withDefGen
      { id
      , setup: mkGenFns
          { setup:  \store _ -> do
              let DS.Store { currentTime } = store
                  countLines = DS.mkCountLines store
                  defTrans   = defTranslatable streetID
              surroundings <- randomNthElse Downtown [ Downtown, Gentrified, Slum, Suburb, University ]
              builtDays           <- curveRandom { goal: 365 * 23, rolls: 3, min: 300, max: 365 * 216 }
              lastRefurbishedDays <- curveRandom { goal: min (365 * 20) (builtDays / 2), rolls: 2, min: 20, max: builtDays }
              let built           = adjustDate (Days $ negate $ toNumber builtDays) $ getDate currentTime
                  lastRefurbished = adjustDate (Days $ negate $ toNumber lastRefurbishedDays) $ getDate currentTime
              isUnder <- (_ <= 0.1) <$> random
              let underFix f
                    | isUnder   = { basement: max 1 f.aboveground, aboveground: max 1 f.basement }
                    | otherwise = f
              floors <- underFix <$> case surroundings of
                Downtown -> { basement: _, aboveground: _} <$> curveRandom { goal: 5, rolls: 2, min: 2, max: if builtDays < (365 * 40) then 10 else 2 } <*> curveRandom { goal: 5, rolls: 2, min: 2, max: if builtDays < (365 * 40) then 30 else 8 } 
                Rural    -> pure { basement: 1, aboveground: 2 }
                _        -> { basement: _, aboveground: _} <$> curveRandom { goal: 2, rolls: 2, min: 1, max: if builtDays < (365 * 40) then 4 else 2 } <*> curveRandom { goal: 3, rolls: 2, min: 2, max: if builtDays < (365 * 40) then 16 else 6 } 
              let floor = 1 -- default until a unit is purchased
              
              hasBasementUnits <- random <#> \d -> floors.basement >= floors.aboveground || d <= 0.1
              availableUnits <- do
                slots <- curveRandom { goal: 3, rolls: 2, min: 1, max: 7 }
                let mkAvailability _ = do
                      floor <- randomRange (negate floors.basement) floors.aboveground <#> case _ of
                        0 -> if floors.aboveground > 1 then 2 else 1
                        f -> f
                      tens  <- curveRandom { goal: 10, rolls: 3, min: 1, max: 99 }
                      let tenStr   = show tens
                          floorStr = String.replace (String.Pattern "-") (String.Replacement "B") $ show floor
                      pure { floor, unit: floorStr <> (if String.length tenStr < 2 then "0" <> tenStr else tenStr) }
                traverse mkAvailability (1 .. slots)
                
              mOwner :: Maybe (FormRow ()) <- randomList Regs.id apartmentOwners.id
              let owner = fromMaybe worldID $ map _.id mOwner

              let qualityGoal = case surroundings of
                    Gentrified -> 4.0
                    Slum       -> 2.0
                    _          -> 3.0
              quality  <- curveRandom { goal: qualityGoal, rolls: 3, min: 1.0, max: 5.0 }

              avgRoomSize :: RoomSize <- randomEnum
              let conditions = case quality of
                    q | q <= 2.0 -> [ Dingy ]
                    q | q <= 3.5 -> [ Dingy, Maintained ]
                    q | q <= 4.5 -> [ Pristine, Maintained ]
                    _            -> [ Pristine ]
                  mkRoom typ = do
                    ranSize :: RoomSize <- randomEnum
                    let size = if abs (fromEnum ranSize - fromEnum avgRoomSize) > 1 then avgRoomSize else ranSize
                    condition <- randomNthElse Dingy conditions
                    pure { type: typ, size, condition }
              hasBalcony <- if hasBasementUnits then pure false else case surroundings of
                Downtown -> (_ <= 0.2) <$> random
                Slum     -> (_ <= 0.1) <$> random
                _        -> (_ <= 0.5) <$> random
              { baseRooms, isStudio } <- randomRange 0 2 >>= case _ of
                0 -> do
                  bedCount <- curveRandom { goal: 0, rolls: 2, min: 0, max: 2 }
                  beds    <- if bedCount > 0 then for (1..bedCount) \_ -> mkRoom Bedroom else pure []
                  bath    <- mkRoom Bath
                  closet  <- mkRoom Closet
                  studio  <- mkRoom Studio
                  pure { baseRooms: beds <> [ bath, studio, closet ], isStudio: true }
                1 -> do
                  bedCount <- curveRandom { goal: 1, rolls: 2, min: 1, max: 3 }
                  beds    <- for (1..bedCount) \_ -> mkRoom Bedroom
                  bathCount <- if bedCount >= 2 then curveRandom { goal: 1, rolls: 2, min: 1, max: 2 } else pure 1
                  baths   <- for (1..bathCount) \_ -> mkRoom Bath
                  closet  <- mkRoom Closet
                  kitchen <- mkRoom Kitchen
                  pure { baseRooms: beds <> baths <> [ kitchen, closet], isStudio: false }
                _ -> do
                  bath    <- mkRoom Bath
                  bedroom <- mkRoom Bedroom
                  kitchen <- mkRoom Kitchen
                  livingRoom <- do
                    condition <- randomNthElse Dingy conditions
                    pure { type: LivingRoom, size: Small, condition }
                  pure { baseRooms: [ bath, bedroom, kitchen, livingRoom ], isStudio: false }
              rooms <- if hasBalcony then (Array.snoc baseRooms <$> mkRoom Balcony) else pure baseRooms

              { address, namingAddress } <- Registry.elseM { address: defTrans, namingAddress: defTrans } Regs.label streetID \gen -> do
                params <- Gen.quick store unit gen <#> _.labelParams
                addressNumber <- KnowStr <<< show <$> if floors.aboveground + floors.basement >= 6 then randomRange 100 13000 else curveRandom { goal: 100, rolls: 2, min: 100, max: 5000 }
                let address = { id, key: "address", params: Array.cons addressNumber params }
                namingAddress <- random >>= case _ of
                  d | d <= (0.75 - quality * 0.1) -> do
                    namingParams <- Gen.quick store unit gen <#> _.labelParams
                    pure { id, key: "address", params: namingParams }
                  _ -> pure { id, key: "address", params }
                pure { address, namingAddress }
              buildingName <- do
                let prefix = "building-name " <> show (max 1 $ min 5 $ round quality) <> " "
                idx <- randomRange 0 (countLines (prefix `isPrefixOf` _) id - 1)
                pure { id, key: prefix <> show idx, params: namingAddress.params }

              let unitAddress   = { id, key: "office", params: [] } -- defaults to the main office, since nothing is rented
                  describeUnits = { id, key: "describe-unit", params: [ KnowBool isStudio, KnowBool hasBalcony, KnowInt (occurrences (\r -> r.type == Bedroom) rooms), KnowInt (occurrences (\r -> r.type == Bath) rooms) ] }

              pure { address, surroundings, buildingName, built, lastRefurbished, floors, owner, floor, rooms, unitAddress, availableUnits, describeUnits, quality }
          , create: \id r ->
              (Record.merge r
                { id
                , tags: HS.empty :: HashSet String
                , monthlyRent: baseMonthlyRent r
                }) :: {| FullHousing' () }
          }
      , lines:     asLines
          [ english /\
              ( [ "address"  /\ line \(TranslatorWith translateWith) ps -> case Array.uncons ps of
                    Just { head: PStr num, tail } -> num & " " & translateWith streetID "label" tail
                    _ -> paramFail "starter-apartment/address" ps
                , "office"  /\ line "Office"
                , "describe-unit" /\ line case _ of
                    [PBool isStudio, PBool hasBalcony, PInt beds, PInt baths] -> transmute $ String.joinWith " - " $ Array.catMaybes
                      [ justWhen isStudio "Studio"
                      , justWhen (not isStudio || beds /= 0) $
                          show beds <> (if beds /= 1 then " Beds" else " Bed")
                      , justWhen (baths /= 0) $
                          show baths <> (if baths /= 1 then " Baths" else " Bath")
                      , justWhen hasBalcony "Balcony"
                      ]
                    ps -> paramFail "starter-apartment/describe-unit" ps
                , "rent" /\ line \(TranslatorWith translateWith) -> case _ of
                    [PNum n] -> translateWith (sid "number") "currency" [PNum n]
                    ps       -> paramFail "starter-apartment/rent" ps
                , "unit-address" /\ line \(TranslatorWith translateWith) ps -> case Array.uncons ps of
                    Just { head: PStr unt, tail } -> translateWith streetID "label" tail & " Apt " & unt
                    _ -> paramFail "starter-apartment/unit-address" ps
                ] <>
                qualityNames "building-name" mkName
                  [ [1, 2, 3, 4, 5] /\ \tw ps -> "The " & tw Address.streetName.id "name" ps
                  , [      3, 4, 5] /\ \tw ps -> "þe " & tw Address.streetName.id "name" ps
                  , [1, 2, 3, 4, 5] /\ \tw ps -> "Ðer " & tw Address.streetName.id "name" ps
                  , [   2, 3, 4, 5] /\ \tw ps -> "La " & tw Address.streetName.id "name" ps
                  , [   2, 3, 4   ] /\ \tw ps -> tw Address.streetName.id "name" ps & " Suites"
                  , [   2, 3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Æcres"
                  , [   2, 3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Residency"
                  , [1, 2, 3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Apartments"
                  , [1, 2, 3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Apartments"
                  , [1, 2, 3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Apts"
                  , [   2, 3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Tower"
                  , [1, 2, 3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Flats"
                  , [      3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Flæts"
                  , [1, 2, 3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Grove"
                  , [1, 2, 3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Lodge"
                  , [1, 2, 3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Longhaus"
                  , [1, 2, 3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Renting"
                  , [1, 2, 3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Hausing"
                  , [1, 2, 3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Circle"
                  , [1, 2, 3      ] /\ \tw ps -> tw Address.streetName.id "name" ps & " Hamlet"
                  , [1, 2, 3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Village"
                  , [1, 2, 3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Ford"
                  , [   2, 3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Lofts"
                  , [1, 2, 3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Union"
                  , [1, 2         ] /\ \tw ps -> tw Address.streetName.id "name" ps & " Hovel"
                  , [1, 2, 3      ] /\ \tw ps -> tw Address.streetName.id "name" ps & " Living"
                  , [      3, 4, 5] /\ \tw ps -> "Castle " & tw Address.streetName.id "name" ps
                  , [      3, 4, 5] /\ \tw ps -> "Château " & tw Address.streetName.id "name" ps
                  , [   2, 3, 4, 5] /\ \tw ps -> "Casa " & tw Address.streetName.id "name" ps
                  , [1, 2, 3, 4, 5] /\ \tw ps -> "Fort " & tw Address.streetName.id "name" ps
                  , [      3, 4, 5] /\ \tw ps -> "The " & tw Address.streetName.id "name" ps & " Plaza"
                  , [      3, 4, 5] /\ \tw ps -> tw Address.streetName.id "name" ps & " Plaza suites"
                  , [1, 2, 3, 4   ] /\ \tw ps -> "Haus of " & tw Address.streetName.id "name" ps
                  , [   2, 3, 4   ] /\ \tw ps -> "Connection on " & tw Address.streetName.id "name" ps
                  , [      3, 4, 5] /\ \tw ps -> "The Grand " & tw Address.streetName.id "name" ps
                  , [1, 2, 3, 4, 5] /\ \tw ps -> "Gran " & tw Address.streetName.id "name" ps
                  , [   2, 3, 4, 5] /\ \tw ps -> "Courte of " & tw Address.streetName.id "name" ps
                  , [1, 2, 3, 4, 5] /\ \tw ps -> "Crown of " & tw Address.streetName.id "name" ps
                  , [1, 2, 3, 4, 5] /\ \tw ps -> "Heart of " & tw Address.streetName.id "name" ps
                  , [      3, 4, 5] /\ \tw ps -> "Artichoke of " & tw Address.streetName.id "name" ps
                  , [1, 2, 3, 4, 5] /\ \tw ps -> "The Ivy on " & tw Address.streetName.id "name" ps
                  , [1, 2, 3      ] /\ \tw ps -> "Inn of " & tw Address.streetName.id "name" ps
                  , [1, 2, 3, 4   ] /\ \tw ps -> atName (nameHash tw ps) & " @ " & tw Address.streetName.id "name" ps
                  , [1, 2, 3, 4, 5] /\ \tw ps -> atName (nameHash tw ps) & " at " & tw Address.streetName.id "name" ps
                  , [   2,    4, 5] /\ \tw ps -> atName (nameHash tw ps) & " æt " & tw Address.streetName.id "name" ps
                  , [   2, 3,    5] /\ \tw ps -> "Saint " & tw Address.streetName.id "name" ps
                  , [   2,    4, 5] /\ \tw ps -> "Old " & tw Address.streetName.id "name" ps
                  , [1, 2, 3, 4, 5] /\ \tw ps -> "Tower " & show (5 + (nameHash tw ps) `mod` 24)
                  , [   2, 3, 4   ] /\ \tw ps -> let street = tw Address.streetName.id "name" ps
                               in "The " & letter (hash $ plainify street) & " at " & street
                  , [      3, 4, 5] /\ \tw ps -> let hsh = nameHash tw ps
                                                  in letter hsh & "#" & show (7 + hsh `mod` 36) & " Studios"
                  , [1, 2, 3, 4, 5] /\ \tw ps -> let hsh = nameHash tw ps
                                                  in letter hsh & "&" & letter (hsh + 3) & " Tower"
                  , [1, 2, 3      ] /\ \tw ps -> tw Address.streetName.id "name" ps & " LLC"
                  , [   2, 3, 4   ] /\ \tw ps -> "The " & numName (hash (plainify (tw Address.streetName.id "name" ps)))
                  , [1, 2, 3      ] /\ \tw ps -> numName (hash (plainify (tw Address.streetName.id "name" ps))) & " Star Apartments"
                  , [         4, 5] /\ \tw ps -> gemName $ nameHash tw ps
                  , [         4, 5] /\ \tw ps -> "þe " & (gemName $ nameHash tw ps)
                  ]
              )
          ]
      }

-- sentence gen for most helpful review, top review?

qualityNames :: forall a. String -> (a -> Line DS.Store) -> Array (Tuple (Array Int) a) -> Array (Tuple String (Line DS.Store))
qualityNames prefix f arr =
  let forRanks e = foldl (flip (mapElemOrDef (e : _)))
      rankMap = foldl (\acc (Tuple r v) -> forRanks (f v) acc r) (Map.empty :: Map Int (List (Line DS.Store)) ) arr
   in Array.fromFoldable $ foldlWithIndex (\q acc ns -> (mapWithIndex (\i v -> (prefix <> " " <> show q <> " " <> show i) /\ v) ns) <> acc) Nil rankMap

apartmentGemNames :: Array Text
apartmentGemNames =
  [ color _.sand     & i & "Agate"
  , color _.cyan     & i & "Aquamarine"
  , color _.orange   & i & "Æmber"
  , color _.purple   & i & "Æmeþyst"
  , color _.pink     & i & "Beryl"
  , color _.gold     & i & "Citrine"
  , color _.cyan     & i & "Diamond"
  , color _.redder   & i & "Garnet"
  , color _.green    & i & "Emerald"
  , color _.red      & i & "Jasper"
  , color _.blue         & "Lapis" & i & "Lazuli"
  , color _.purple       & "Lapis" & i & "Vacuus"
  , color _.grass    & i & "Malachite"
  , color _.lavender & i & "Onyx"
  , color _.yellow   & i & "Peridot"
  , color _.white    & i & "Quartz"
  , color _.redder   & i & "Ruby"
  , color _.blue     & i & "Sapphire"
  , color _.orange   & i & "Topaz"
  ]
  where i = roundedIcon "diamond"

apartmentBrandNames :: Array String
apartmentBrandNames =
  [ "Arcane"
  , "Æstrol"
  , "Ethereal"
  , "Granite"
  , "Hermitage"
  , "Ioun"
  , "Obsidian"
  , "Planes"
  , "Regal"
  ]