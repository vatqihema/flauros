module Flauros.Prefab.Core.Holiday.Fall where

import Prelude

import Data.Date (Month(..))
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Holiday (Holiday, Priority(..), allDay, withDefHoliday)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Data.Weather as W
import Flauros.Lang (asLines, english, esperanto, line)
import Flauros.Lang.Text (clear, color, fontWeight, outlinedIcon, (&))
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, asHashMap, asMap, asHashSet, sid)
import Flauros.Style.Color (clay, orange)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

-- | Initializes holidays in September through November
init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.holiday harvestFestival
  register Regs.holiday startOfFall

harvestFestival :: {| Identified + Holiday + LangInfo' + () }
harvestFestival = withDefHoliday
  { id:        sid "harvest-festival"
  , tags:      asHashSet [ "federal-holiday" ]
  , priority:  Major
  , span:      asMap [ { month: October, day: 3 } /\ allDay ]
  , eventMods: asHashMap
      [ "ghostly-phenomenon" /\ 5.0
      , "mythos"             /\ 3.0
      ]
  , lines:     asLines
      [ english /\
          [ "name"     /\ line "The Harvest Festival"
          , "symbol"   /\ line "🌾"
          , "calendar" /\ line ("It's the " & color orange & fontWeight 700.0 & "Harvest Festival" & clear & "!")
          , "desc"     /\ line ""
          , "welcome"  /\ line (color orange & "🎃Today is the Harvest Festival! " & color clay & "Lantern lighting will begin at 20:00.")
          ]
      ,  esperanto /\
          [ "name"     /\ line "La Rikolta Festivalo"
          , "calendar" /\ line ("Estas la " & color orange & fontWeight 700.0 & "Rikolta Festivalo" & clear & "!")
          , "desc"     /\ line ""
          ]
      ]
  }

startOfFall :: {| Identified + Holiday + LangInfo' + () }
startOfFall = withDefHoliday
  { id:        sid "start-of-fall"
  , priority:  Minor
  , span:      asMap [ W.startOfFall /\ allDay ]
  , lines:     asLines
      [ english /\
          [ "name"     /\ line "Start Of Fall"
          , "symbol"   /\ line (color orange & outlinedIcon "spa")
          , "calendar" /\ line ("It's " & color orange & "the start of fall" & clear & "!")
          , "desc"     /\ line ""
          ]
      ,  esperanto /\
          [ "name"     /\ line "Komenco de Aŭtuno"
          , "calendar" /\ line ("Estas " & color orange & "la komenco de aŭtuno" & clear & "!")
          , "desc"     /\ line ""
          ]
      ]
  }