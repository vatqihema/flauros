module Flauros.Prefab.Core.Holiday.Winter where

import Prelude

import Data.Date (Month(..))
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Holiday (Holiday, Priority(..), allDay, withDefHoliday)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Data.Weather as W
import Flauros.Lang (asLines, english, esperanto, line)
import Flauros.Lang.Text (clear, color, roundedIcon, (&))
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, asHashMap, asMap, asHashSet, sid)
import Flauros.Style.Color (pink, red, white)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

-- | Initializes holidays in December through January
init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.holiday newYearsDay
  register Regs.holiday newYearsEve
  register Regs.holiday startOfWinter
  register Regs.holiday stSkrunkFeast

newYearsDay :: {| Identified + Holiday + LangInfo' + () }
newYearsDay = withDefHoliday
  { id:        sid "new-years-day"
  , tags:      asHashSet [ "federal-holiday" ]
  , priority:  Major
  , span:      asMap [ { month: January, day: 1 } /\ allDay ]
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 2.0 ]
  , lines:     asLines
      [ english /\
          [ "name"     /\ line "New Year's Day"
          , "symbol"   /\ line "🎉"
          , "calendar" /\ line ("It's " & color pink & "New Year's Day" & clear & "!")
          , "desc"     /\ line "Fireworks, drinking, and no holiday obligations! Welcome the new year!"
          ]
      ,  esperanto /\
          [ "name"     /\ line "Jarfina tago"
          , "calendar" /\ line ("Estas la " & color pink & "jarfina tago" & clear & "!")
          , "desc"     /\ line "Fajraĵoj kaj drinkado, sen festotaga devo! Bonvenigu la novan jaron!"
          ]
      ]
  }

newYearsEve :: {| Identified + Holiday + LangInfo' + () }
newYearsEve = withDefHoliday
  { id:        sid "new-years-eve"
  , priority:  Major
  , span:      asMap [ { month: December, day: 31 } /\ allDay ]
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 4.0 ]
  , lines:     asLines
      [ english /\
          [ "name"     /\ line "New Year's Eve"
          , "symbol"   /\ line "🎆"
          , "calendar" /\ line ("It's " & color pink & "New Year's Eve" & clear & "!")
          ]
      ,  esperanto /\
          [ "name"     /\ line "Novjara tago"
          , "calendar" /\ line ("Estas la " & color pink & "novjara tago" & clear & "!")
          ]
      ]
  }

stSkrunkFeast :: {| Identified + Holiday + LangInfo' + () }
stSkrunkFeast = withDefHoliday
  { id:        sid "st-skrunk-feast"
  , priority:  Minor
  , span:      asMap [ { month: December, day: 12 } /\ allDay ]
  , lines:     asLines
      [ english /\
          [ "name"     /\ line "St. Skrunk's Feast"
          , "symbol"   /\ line "🥨"
          , "calendar" /\ line ("It's " & color red & "St. Skrunk's Feast" & clear & "!")
          , "desc"     /\ line ""
          ]
      ,  esperanto /\
          [ "name"     /\ line "Festeno de Sankta Skrunko"
          , "calendar" /\ line ("Estas la " & color red & "festeno de Sankta Skrunko" & clear & "!")
          , "desc"     /\ line ""
          ]
      ]
  }

startOfWinter :: {| Identified + Holiday + LangInfo' + () }
startOfWinter = withDefHoliday
  { id:        sid "start-of-winter"
  , priority:  Minor
  , span:      asMap [ W.startOfWinter /\ allDay ]
  , lines:     asLines
      [ english /\
          [ "name"     /\ line "Start Of Winter"
          , "symbol"   /\ line (color white & roundedIcon "ac_unit")
          , "calendar" /\ line ("It's " & color red & "the start of winter" & clear & "!")
          , "desc"     /\ line ""
          ]
      ,  esperanto /\
          [ "name"     /\ line "Komenco de Vintro"
          , "calendar" /\ line ("Estas " & color red & "la komenco de vintro" & clear & "!")
          , "desc"     /\ line ""
          ]
      ]
  }