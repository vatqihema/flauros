module Flauros.Prefab.Core.Holiday.Summer where

import Prelude

import Data.Date (Month(..))
import Data.Enum (fromEnum)
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Holiday (Holiday, Priority(..), allDay, withDefHoliday)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Data.Time (getYear, mkTimeHM)
import Flauros.Data.Weather as W
import Flauros.Lang (LineParam(..), TranslatorWith(..), asLines, english, esperanto, line, paramFail)
import Flauros.Lang.Text (clear, color, plain, roundedIcon, (&))
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, asHashMap, asMap, asHashSet, sid)
import Flauros.Style.Color (blue, red, yellow)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

-- | Initializes holidays in June through August
init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.holiday independenceDay
  register Regs.holiday longestDay
  register Regs.holiday startOfSummer
  
independenceDayYear :: Int
independenceDayYear = 1089

independenceDay :: {| Identified + Holiday + LangInfo' + () }
independenceDay = withDefHoliday
  { id:        sid "independence-day"
  , tags:      asHashSet [ "federal-holiday" ]
  , priority:  Major
  , span:      asMap [ { month: June, day: 6 } /\ allDay ]
  , lines:     asLines
      [ english /\
          [ "name"     /\ line "Independence Day"
          , "symbol"   /\ line "🥁"
          , "calendar" /\ line \(TranslatorWith translateWith) -> case _ of
              [PDate dt] -> case (fromEnum $ getYear dt) - independenceDayYear of
                y | y < 0 -> mempty
                y         -> "It's the "
                           & color red  & translateWith (sid "number") "ordinal" [PInt y]
                           & color blue & " Althesian Independence Day" & clear & "!"
              p -> paramFail "independence-day/calendar" p
          , "desc"     /\ line ""
          ]
      ,  esperanto /\
          [ "name"     /\ line "Sendependeca tago"
          , "calendar" /\ line \(TranslatorWith translateWith) -> case _ of
              [PDate dt] -> case (fromEnum $ getYear dt) - independenceDayYear of
                y | y < 0 -> mempty
                y         -> "Estas la "
                           & color red  & translateWith (sid "number") "ordinal" [PInt y]
                           & color blue & plain " Altezia sendependeca tago" & clear & "!"
              p -> paramFail "independence-day/calendar" p
          , "desc"     /\ line ""
          ]
      ]
  }

longestDay :: {| Identified + Holiday + LangInfo' + () }
longestDay = withDefHoliday
  { id:        sid "longest-day"
  , tags:      asHashSet [ "federal-holiday" ]
  , priority:  Major
  , span:      asMap
      [ { month: June, day: 20 } /\ { begin: mkTimeHM 4 0, end: top }
      , { month: June, day: 21 } /\ { begin: bottom,       end: mkTimeHM 21 0 }
      ]
  , eventMods: asHashMap
      [ "ghostly-phenomenon" /\ 12.0
      , "mythos"             /\ 3.0
      ]
  , lines:     asLines
      [ english /\
          [ "name"     /\ line "The longest day"
          , "symbol"   /\ line "🌞"
          , "calendar" /\ line ("It's " & color yellow & "the longest day" & clear & "!")
          , "desc"     /\ line "Summer activities and festivies celebrating 40 hours of continual sunlight on the summer's solstice."
          ]
      ,  esperanto /\
          [ "name"     /\ line "La plej longa tago"
          , "calendar" /\ line ("Estas " & color yellow & "la plej longa tago" & clear & "!")
          , "desc"     /\ line "Someraj agadoj kaj festadoj celebras 40 horoj da sumlumado ĉirkaŭ la somera solstico."
          ]
      ]
  }

startOfSummer :: {| Identified + Holiday + LangInfo' + () }
startOfSummer = withDefHoliday
  { id:        sid "start-of-summer"
  , priority:  Minor
  , span:      asMap [ W.startOfSummer /\ allDay ]
  , lines:     asLines
      [ english /\
          [ "name"     /\ line "Start Of Summer"
          , "symbol"   /\ line (color yellow & roundedIcon "brightness_high")
          , "calendar" /\ line ("It's " & color yellow & "the start of summer" & clear & "!")
          , "desc"     /\ line ""
          ]
      ,  esperanto /\
          [ "name"     /\ line "Komenco de Somero"
          , "calendar" /\ line ("Estas " & color yellow & "la komenco de somero" & clear & "!")
          , "desc"     /\ line ""
          ]
      ]
  }