module Flauros.Prefab.Core.Holiday.Spring where

import Prelude

import Data.Date (Month(..))
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Holiday (Holiday, Priority(..), allDay, withDefHoliday)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Data.Weather as W
import Flauros.Lang (asLines, english, esperanto, line)
import Flauros.Lang.Text (clear, color, outlinedIcon, (&))
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, asMap, sid)
import Flauros.Style.Color (blue, clay, grass, green, lime)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

-- | Initializes holidays in March through May
init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.holiday flagDay
  register Regs.holiday oldClothesDay
  register Regs.holiday startOfSpring

flagDay :: {| Identified + Holiday + LangInfo' + () }
flagDay = withDefHoliday
  { id:        sid "flag-day"
  , priority:  Minor
  , span:      asMap [ { month: May, day: 1 } /\ allDay ]
  , lines:     asLines
      [ english /\
          [ "name"     /\ line "Flag day"
          , "symbol"   /\ line "🚩"
          , "calendar" /\ line ("It's " & color blue & "flag " & color grass & "day" & clear & "!")
          , "desc"     /\ line ""
          ]
      ,  esperanto /\
          [ "name"     /\ line "La flagtago"
          , "calendar" /\ line ("Estas la " & color blue & "flag" & color grass & "tago" & clear & "!")
          , "desc"     /\ line ""
          ]
      ]
  }

oldClothesDay :: {| Identified + Holiday + LangInfo' + () }
oldClothesDay = withDefHoliday
  { id:        sid "old-clothes-day"
  , priority:  Minor
  , span:      asMap [ { month: May, day: 22 } /\ allDay ]
  , lines:     asLines
      [ english /\
          [ "name"     /\ line "Old Clothes Day"
          , "symbol"   /\ line "🧥"
          , "calendar" /\ line ("It's " & color clay & "Old Clothes Day" & clear & "!")
          , "desc"     /\ line ""
          ]
      ,  esperanto /\
          [ "name"     /\ line "Tago de maljunaj vestoj"
          , "calendar" /\ line ("Estas la tago de " & color clay & "maljunaj vestoj" & clear & "!")
          , "desc"     /\ line ""
          ]
      ]
  }

startOfSpring :: {| Identified + Holiday + LangInfo' + () }
startOfSpring = withDefHoliday
  { id:        sid "start-of-spring"
  , priority:  Minor
  , span:      asMap [ W.startOfSpring /\ allDay ]
  , lines:     asLines
      [ english /\
          [ "name"     /\ line "Start Of Spring"
          , "symbol"   /\ line ("" & color green & outlinedIcon "local_florist")
          , "calendar" /\ line ("It's " & color lime & "the start of spring" & clear & "!")
          , "desc"     /\ line ""
          ]
      ,  esperanto /\
          [ "name"     /\ line "Komenco de Printempo"
          , "calendar" /\ line ("Estas " & color lime & "la komenco de printempo" & clear & "!")
          , "desc"     /\ line ""
          ]
      ]
  }