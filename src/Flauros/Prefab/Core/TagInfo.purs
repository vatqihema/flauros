module Flauros.Prefab.Core.TagInfo where

import Prelude

import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Data.TagInfo (TagInfo, withDefTagInfo)
import Flauros.Lang (asLines, english, esperanto, line)
import Flauros.Lang.Text (color, (&))
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, asHashSet, sid)
import Flauros.Style.Color (clay, cornflower, orange)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.tagInfo chilled
  register Regs.tagInfo crawlspace
  register Regs.tagInfo illicit

chilled :: {| Identified + LangInfo' + TagInfo + () }
chilled = withDefTagInfo
  { id:          sid "chilled"
  , lines: asLines
      [ english /\
          [ "analysis" /\ line (color cornflower & "It's chilled")
          , "‽1"       /\ line "cold"
          , "‽2"       /\ line "chilled"
          , "‽3"       /\ line "cool"
          ]
      , esperanto /\
          [ "analysis" /\ line (color cornflower & "Ĝi friskigitas")
          , "‽1"       /\ line "friska"
          , "‽2"       /\ line "malvarma"
          ]
      ]
  }

crawlspace :: {| Identified + LangInfo' + TagInfo + () }
crawlspace = withDefTagInfo
  { id:          sid "crawlspace"
  , lines: asLines
      [ english /\
          [ "analysis" /\ line (color clay & "Found in the crawlspace world") -- todo better name
          , "‽1"       /\ line "crawlspace"
          ]
      , esperanto /\
          [ "analysis" /\ line (color clay & "Ĝi fondatas en la rampspacmundo")
          , "?1"       /\ line "rampspac"
          , "!1"       /\ line "rampspaca"
          ]
      ]
  }

illicit :: {| Identified + LangInfo' + TagInfo + () }
illicit = withDefTagInfo
  { id:          sid "illicit"
  , tags:        asHashSet [ "warning" ]
  , lines: asLines
      [ english /\
          [ "analysis" /\ line (color orange & "Ownership of this item might be restricted! See local laws for more information.")
          , "‽1"       /\ line "illicit"
          , "‽2"       /\ line "illegal"
          , "?3"       /\ line "banned"
          ]
      , esperanto /\
          [ "analysis" /\ line (color orange & "La proprieta de ĉi tiu ero eble limigitas! Vidu lokajn leĝojn por plia informo.")
          , "‽1"       /\ line "kontraŭ"
          , "‽2"       /\ line "neleĝa"
          ]
      ]
  }
