module Flauros.Prefab.Core.Test where

import Prelude

import Data.Date (Month(..))
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Actor (Actor, mkFirstLast, withDefActor)
import Flauros.Data.Stat (baseState, modifyMod)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Data.Time (mkDate)
import Flauros.Lang (asLines)
import Flauros.Prefab.Core.Ancestry (chimera)
import Flauros.Prefab.Core.Item.Base (money)
import Flauros.Prefab.Core.Item.Plant (balsamroot)
import Flauros.Prefab.Core.StatDef (awareness, expression, finesse, necromancy, organization, transmutation, useTech)
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, asHashMap, sid)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.actor farrisC'Amri

farrisC'Amri :: {| Identified + LangInfo' + Actor + () }
farrisC'Amri = withDefActor
  { id:       sid "farris-c'amri"
  , name:     mkFirstLast "Farris" "c'Amrî"
  , ancestry: chimera.id
  , birthday: mkDate 1198 September 23
  , bag:      asHashMap
      [ balsamroot.id /\ 5
      , money.id      /\ 12345
      ]
  , stats: asHashMap
      [ awareness.id     /\ baseState 1.0
      , expression.id    /\ modifyMod (_ + 2.0) (baseState 1.0)
      , finesse.id       /\ baseState 0.0
      , necromancy.id    /\ modifyMod (_ + 1.0) (baseState 1.0)
      , organization.id  /\ baseState 1.0
      , transmutation.id /\ modifyMod (_ - 1.0) (baseState 0.0)
      , useTech.id       /\ baseState (-1.0)
      ]
  , lines: asLines []
  }