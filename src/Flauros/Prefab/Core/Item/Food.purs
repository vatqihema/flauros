module Flauros.Prefab.Core.Item.Food where

import Prelude

import Data.Either (Either(..))
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Gen (Gen_, mkGenFns, withDefGen)
import Flauros.Data.Knowledge (KnowVal(..), KnowTranslatable)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (LineParam(..), Translator(..), asLines, english, esperanto, line, paramFail)
import Flauros.Lang.Text (Text(..), (&))
import Flauros.Prefab.Registries (Label)
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, isPrefixOf, mapWithPrefixedIndex, sid, withDefTags)
import Flauros.Random (random, randomRange)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.sentenceGen eatFoodSentence

eatFoodSentence :: {| Identified + LangInfo' + Gen_ KnowTranslatable + () }
eatFoodSentence =
  withDefGen
    { id:    sid "sentence/eat-food"
    , setup: mkGenFns
        { setup: \store _ -> do
            pure unit
        , create: \id _ -> { id, key: "sentence", params: [] }
        }
    , lines:     asLines
        [ english /\
            [ "sentence" /\ line ""
            ]
        ]
    }