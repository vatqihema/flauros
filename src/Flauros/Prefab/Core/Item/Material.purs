module Flauros.Prefab.Core.Item.Material where

import Prelude

import Data.HashSet as HS
import Data.Number (round)
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Item (Item, nextSizeUp, tiny, withDefItem)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Data.TagInfo (TagInfo, withDefTagInfo)
import Flauros.Lang (PreviousResult(..), asLines, colorPrevious, english, esperanto, line, sufikso)
import Flauros.Lang.Text (color, isEmptyText, (&))
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (Modifier, modifyWith, register, (++))
import Flauros.Prelude (Identified, asHashMap, asHashSet, sid)
import Flauros.Style.Color (blue, clay, cornflower, cyan, green, lavender, lime, orange, pink, purple, red, redder, white)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register (Regs.modifier ++ Regs.tagInfo) adamantine
  register (Regs.modifier ++ Regs.tagInfo ++ Regs.item) bone
  register (Regs.modifier ++ Regs.tagInfo) celestial
  register (Regs.modifier ++ Regs.tagInfo) coldIron
  register  Regs.modifier                  deluxe
  register (Regs.modifier ++ Regs.tagInfo) demonic
  register (Regs.modifier ++ Regs.tagInfo) ectoplasmic
  register (Regs.modifier ++ Regs.tagInfo) ladybug
  register (Regs.modifier ++ Regs.tagInfo) mithril
  register (Regs.modifier ++ Regs.tagInfo) nuclear
  register (Regs.modifier ++ Regs.tagInfo) orichalcum
  register (Regs.modifier ++ Regs.tagInfo) rivertine
  register (Regs.modifier ++ Regs.tagInfo) secondHand
  register (Regs.modifier ++ Regs.tagInfo) silver
  register (Regs.modifier ++ Regs.tagInfo) spider
  register (Regs.modifier ++ Regs.tagInfo) umbral
  -- Gem
  register (Regs.modifier ++ Regs.tagInfo ++ Regs.item) diamond
  register (Regs.modifier ++ Regs.tagInfo ++ Regs.item) jade

------------
-- Metals --
------------

adamantine :: {| Identified + LangInfo' + Modifier + TagInfo + () }
adamantine = withDefTagInfo
  { id:     sid "adamantine"
  , modify: asHashMap
      [ Regs.item `modifyWith` \item -> item
          { tags      = HS.insert "adamantine" item.tags
          , textColor = lavender
          , price     = round $ item.price * 12.0 + item.size * 200.0
          }
      ]
  , lines: asLines
      [ english /\
          [ "analysis" /\ line (color lavender & "It's made of adamantine") -- dark grey sparkly backdrop?
          , "=1"       /\ line "adamant"
          , "=2"       /\ line "adamantine"
          , "=3"       /\ line "adamantium"
          , "long"     /\ line \(PreviousResult prev) -> color lavender & "mithril " & prev
          , "short"    /\ colorPrevious lavender
          ]
      , esperanto /\
          [ "analysis" /\ line (color lavender & "Ĝi faritas el adamantino")
          , "=1"       /\ line "adamantin"
          , "=2"       /\ line "adamant"
          , "long"     /\ line \(PreviousResult prev) ps -> color lavender & "adamantina" & sufikso ps & " " & prev
          , "short"    /\ colorPrevious lavender
          ]
      ]
  }

coldIron :: {| Identified + LangInfo' + Modifier + TagInfo + () }
coldIron = withDefTagInfo
  { id:     sid "cold-iron"
  , modify: asHashMap
      [ Regs.item `modifyWith` \item -> item
          { tags      = HS.insert "cold-iron" item.tags
          , price     = round $ item.price * 1.5 + item.size * 10.0
          }
      ]
  , lines: asLines
      [ english /\
          [ "analysis" /\ line "It's made of cold iron"
          , "=1"       /\ line "coldiron"
          , "long"     /\ line \(PreviousResult prev) -> "coldiron " & prev
          ]
      , esperanto /\
          [ "analysis" /\ line "Ĝi faritas el malvarmfero"
          , "=1"       /\ line "malvarmfer"
          , "=2"       /\ line "fero"
          , "long"     /\ line \(PreviousResult prev) ps -> "malvarmfera" & sufikso ps & " " & prev
          ]
      ]
  }

mithril :: {| Identified + LangInfo' + Modifier + TagInfo + () }
mithril = withDefTagInfo
  { id:     sid "mithril"
  , modify: asHashMap
      [ Regs.item `modifyWith` \item -> item
          { tags      = HS.insert "mithril" item.tags
          , textColor = cyan
          , price     = round $ item.price * 5.0 + item.size * 90.0
          }
      ]
  , lines: asLines
      [ english /\
          [ "analysis" /\ line (color cyan & "It's made of mithril")
          , "=1"       /\ line "mithril"
          , "long"     /\ line \(PreviousResult prev) -> color cyan & "mithril " & prev
          , "short"    /\ colorPrevious cyan
          ]
      , esperanto /\
          [ "analysis" /\ line (color cyan & "Ĝi faritas el mitrilo")
          , "=1"       /\ line "mitril"
          , "long"     /\ line \(PreviousResult prev) ps -> color cyan & "mitrila" & sufikso ps & " " & prev
          , "short"    /\ colorPrevious cyan
          ]
      ]
  }

orichalcum :: {| Identified + LangInfo' + Modifier + TagInfo + () }
orichalcum = withDefTagInfo
  { id:     sid "orichalcum"
  , modify: asHashMap
      [ Regs.item `modifyWith` \item -> item
          { tags      = HS.insert "orichalcum" item.tags
          , textColor = orange
          , price     = round $ item.price * 8.0 + item.size * 150.0
          }
      ]
  , lines: asLines
      [ english /\
          [ "analysis" /\ line (color orange & "It's made of orichalcum") -- replace text with mask of sparkly backdrop?
          , "=1"       /\ line "orichalcum"
          , "long"     /\ line \(PreviousResult prev) -> color orange & "orichalcum " & prev
          , "short"    /\ colorPrevious orange
          ]
      , esperanto /\
          [ "analysis" /\ line (color orange & "Ĝi faritas el oriĥalko")
          , "=1"       /\ line "oriĥalk"
          , "long"     /\ line \(PreviousResult prev) ps -> color orange & "oriĥalka" & sufikso ps & " " & prev
          , "short"    /\ colorPrevious orange
          ]
      ]
  }

rivertine :: {| Identified + LangInfo' + Modifier + TagInfo + () }
rivertine = withDefTagInfo
  { id:     sid "rivertine"
  , modify: asHashMap
      [ Regs.item `modifyWith` \item -> item
          { tags      = HS.insert "rivertine" item.tags
          , textColor = blue
          , price     = round $ item.price * 4.0 + item.size * 60.0
          }
      ]
  , lines: asLines
      [ english /\
          [ "analysis" /\ line (color blue & "It's formed of rivertine")
          , "=1"       /\ line "rivertine"
          , "=2"       /\ line "river"
          , "long"     /\ line \(PreviousResult prev) -> color blue & "rivertine " & prev
          , "short"    /\ colorPrevious blue
          ]
      , esperanto /\
          [ "analysis" /\ line (color blue & "Ĝi formatas el riverecaĵo")
          , "=1"       /\ line "riverecaĵ"
          , "=2"       /\ line "river"
          , "long"     /\ line \(PreviousResult prev) ps -> color blue & "riverecaĵa" & sufikso ps & " " & prev
          , "short"    /\ colorPrevious blue
          ]
      ]
  }

silver :: {| Identified + LangInfo' + Modifier + TagInfo + () }
silver = withDefTagInfo
  { id:     sid "silver"
  , modify: asHashMap
      [ Regs.item `modifyWith` \item -> item
          { tags      = HS.insert "silver" item.tags
          , price     = round $ item.price * 3.0 + item.size * 60.0
          }
      ]
  , lines: asLines
      [ english /\
          [ "analysis" /\ line "It's silvered"
          , "=1"       /\ line "silver"
          , "=1"       /\ line "silvered"
          , "long"     /\ line \(PreviousResult prev) -> "silver " & prev
          ]
      , esperanto /\
          [ "analysis" /\ line "Ĝi arĝentiĝatas"
          , "=1"       /\ line "arĝent"
          , "long"     /\ line \(PreviousResult prev) ps -> "arĝenta" & sufikso ps & " " & prev
          ]
      ]
  }

---------------
-- Furniture --
---------------

ladybug :: {| Identified + LangInfo' + Modifier + TagInfo + () }
ladybug = withDefTagInfo
  { id:     sid "ladybug"
  , modify: asHashMap
      [ Regs.item `modifyWith` \item -> item
          { tags      = HS.insert "bug" item.tags
          , textColor = redder
          }
      ]
  , lines: asLines
      [ english /\
          [ "analysis" /\ line (color redder & "It's shaped like a ladybug.")
          , "=1"       /\ line "ladybug"
          , "=2"       /\ line "ladybird"
          , "long"     /\ line \(PreviousResult prev) -> color redder & "ladybug " & prev
          , "short"    /\ line \(PreviousResult prev) -> color redder & "ladybug " & prev
          ]
      , esperanto /\
          [ "analysis" /\ line (color redder & "Ĝi formatas kiel kokcinelo.")
          , "=1"       /\ line "kokcinel"
          , "long"     /\ line \(PreviousResult prev) -> color redder & "kokcinel" & prev
          , "short"    /\ line \(PreviousResult prev) -> color redder & "kokcinel" & prev
          ]
      ]
  }

spider :: {| Identified + LangInfo' + Modifier + TagInfo + () }
spider = withDefTagInfo
  { id:     sid "spider"
  , modify: asHashMap
      [ Regs.item `modifyWith` \item -> item
          { tags      = HS.insert "bug" item.tags
          , textColor = lavender
          }
      ]
  , lines: asLines
      [ english /\
          [ "analysis" /\ line (color lavender & "It's shaped like a spider.")
          , "=1"       /\ line "spider"
          , "long"     /\ line \(PreviousResult prev) -> color lavender & "spider " & prev
          , "short"    /\ line \(PreviousResult prev) -> color lavender & "spider " & prev
          ]
      , esperanto /\
          [ "analysis" /\ line (color lavender & "Ĝi formatas kiel araneo.")
          , "=1"       /\ line "arane"
          , "long"     /\ line \(PreviousResult prev) -> color lavender & "aranea " & prev
          , "short"    /\ line \(PreviousResult prev) -> color lavender & "aranea " & prev
          ]
      ]
  }

-------------------
-- Gem & Mineral --
-------------------

diamond :: {| Identified + LangInfo' + Item + Modifier + TagInfo + () }
diamond = withDefItem
  { id:     sid "diamond"
  , tags:   asHashSet [ "diamond" ]
  , price:     500.0
  , size:      tiny
  , textColor: cornflower
  , modify: asHashMap
      [ Regs.item `modifyWith` \item -> item
          { tags      = HS.insert "diamond" item.tags
          , textColor = cornflower
          , price     = round $ item.price * 30.0 + item.size * 20000.0
          }
      ]
  , lines: asLines
      [ english /\
          [ "analysis" /\ line (color cornflower & "It's made of diamond.")
          , "=1"       /\ line "diamond"
          , "long"     /\ line \(PreviousResult prev) -> color cornflower & "diamond" & (if isEmptyText prev then "" else " ") & prev
          , "short"    /\ colorPrevious cornflower
          ]
      , esperanto /\
          [ "analysis" /\ line (color cornflower & "Ĝi faritas en diamanto")
          , "=1"       /\ line "diamant"
          , "long"     /\ line \(PreviousResult prev) ps -> color cornflower & "diamanta" & sufikso ps & (if isEmptyText prev then "" else " ") & prev
          , "short"    /\ colorPrevious cornflower
          ]
      ]
  }

jade :: {| Identified + LangInfo' + Item + Modifier + TagInfo + () }
jade = withDefItem
  { id:     sid "jade"
  , tags:   asHashSet [ "jade" ]
  , price:     50.0
  , size:      tiny
  , textColor: green
  , modify: asHashMap
      [ Regs.item `modifyWith` \item -> item
          { tags      = HS.insert "jade" item.tags
          , textColor = green
          , price     = round $ item.price * 6.0 + item.size * 108.0
          }
      ]
  , lines: asLines
      [ english /\
          [ "analysis" /\ line (color green & "It's made of jade.")
          , "=1"       /\ line "jade"
          , "long"     /\ line \(PreviousResult prev) -> color green & "jade" & (if isEmptyText prev then "" else " ") & prev
          , "short"    /\ colorPrevious green
          ]
      , esperanto /\
          [ "analysis" /\ line (color green & "Ĝi faritas en jado")
          , "=1"       /\ line "jado"
          , "=2"       /\ line "jada"
          , "long"     /\ line \(PreviousResult prev) ps -> color green & "jada" & sufikso ps & (if isEmptyText prev then "" else " ") & prev
          , "short"    /\ colorPrevious green
          ]
      ]
  }
  
----------
-- Misc --
----------

bone :: {| Identified + LangInfo' + Item + Modifier + TagInfo + () }
bone = withDefItem
  { id:     sid "bone"
  , tags:   asHashSet [ "bone" ]
  , price:     1.0
  , size:      tiny
  , textColor: white
  , modify: asHashMap
      [ Regs.item `modifyWith` \item -> item
          { tags      = HS.insert "bone" item.tags
          , textColor = white
          }
      ]
  , lines: asLines
      [ english /\
          [ "analysis" /\ line (color white & "It's made of bone.")
          , "=1"       /\ line "bone"
          , "long"     /\ line \(PreviousResult prev) -> color white & "bone" & (if isEmptyText prev then "" else " ") & prev
          , "short"    /\ colorPrevious white
          ]
      , esperanto /\
          [ "analysis" /\ line (color white & "Ĝi faritas en osto")
          , "=1"       /\ line "osto"
          , "=2"       /\ line "osta"
          , "long"     /\ line \(PreviousResult prev) ps -> color white & "osta" & sufikso ps & (if isEmptyText prev then "" else " ") & prev
          , "short"    /\ colorPrevious white
          ]
      ]
  }

celestial :: {| Identified + LangInfo' + Modifier + TagInfo + () }
celestial = withDefTagInfo
  { id:     sid "celestial"
  , modify: asHashMap
      [ Regs.item `modifyWith` \item -> item
          { tags      = HS.insert "celestial" item.tags
          , textColor = pink
          , price     = round $ item.price * 9.0 + item.size * 300.0
          }
      ]
  , lines: asLines
      [ english /\
          [ "analysis" /\ line (color pink & "It's from heaven.")
          , "=1"       /\ line "angel"
          , "=2"       /\ line "angelic"
          , "=3"       /\ line "celestial"
          , "=4"       /\ line "heaven"
          , "=5"       /\ line "paradise"
          , "long"     /\ line \(PreviousResult prev) -> color pink & "angelic " & prev
          , "short"    /\ colorPrevious pink
          ]
      , esperanto /\
          [ "analysis" /\ line (color pink & "Ĝi estas el Paradizo.")
          , "=1"       /\ line "anĝel"
          , "=2"       /\ line "ĉiel"
          , "=3"       /\ line "paradiz"
          , "long"     /\ line \(PreviousResult prev) ps -> color pink & "anĝela" & sufikso ps & " " & prev
          , "short"    /\ colorPrevious pink
          ]
      ]
  }

deluxe :: {| Identified + LangInfo' + Modifier + () }
deluxe = withDefTagInfo
  { id:     sid "deluxe"
  , modify: asHashMap
      [ Regs.item `modifyWith` \item -> item
          { size  = nextSizeUp item.size
          , price = item.price * 1.5
          }
      ]
  , lines: asLines
      [ english /\
          [ "long"     /\ line \(PreviousResult prev) -> "deluxe " & prev
          ]
      , esperanto /\
          [ "long"     /\ line \(PreviousResult prev) ps -> "luksa" & sufikso ps & " " & prev
          ]
      ]
  }

demonic :: {| Identified + LangInfo' + Modifier + TagInfo + () }
demonic = withDefTagInfo
  { id:     sid "demonic"
  , modify: asHashMap
      [ Regs.item `modifyWith` \item -> item
          { tags      = HS.insert "demonic" item.tags
          , textColor = red
          , price     = round $ item.price * 9.0 + item.size * 260.0
          }
      ]
  , lines: asLines
      [ english /\
          [ "analysis" /\ line (color red & "It's made in hell.")
          , "=1"       /\ line "demon"
          , "=2"       /\ line "demonic"
          , "=3"       /\ line "hell"
          , "=4"       /\ line "hellish"
          , "=5"       /\ line "infernal"
          , "long"     /\ line \(PreviousResult prev) -> color red & "demonic " & prev
          , "short"    /\ colorPrevious red
          ]
      , esperanto /\
          [ "analysis" /\ line (color red & "Ĝi faritas en infero")
          , "=1"       /\ line "infer"
          , "=2"       /\ line "gehen"
          , "=3"       /\ line "demon"
          , "long"     /\ line \(PreviousResult prev) ps -> color red & "demona" & sufikso ps & " " & prev
          , "short"    /\ colorPrevious red
          ]
      ]
  }

ectoplasmic :: {| Identified + LangInfo' + Modifier + TagInfo + () }
ectoplasmic = withDefTagInfo
  { id:     sid "ectoplasmic"
  , modify: asHashMap
      [ Regs.item `modifyWith` \item -> item
          { tags      = HS.insert "ectoplasmic" item.tags
          , textColor = lime
          , price     = round $ item.price * 2.0 + item.size * 30.0
          }
      ]
  , lines: asLines
      [ english /\
          [ "analysis" /\ line (color lime & "It's made of ectoplasm.")
          , "=1"       /\ line "ectoplasm"
          , "=2"       /\ line "ectoplasmic"
          , "=3"       /\ line "ghost"
          , "=4"       /\ line "ghostly"
          , "long"     /\ line \(PreviousResult prev) -> color lime & "ectoplasmic " & prev
          , "short"    /\ colorPrevious lime
          ]
      , esperanto /\
          [ "analysis" /\ line (color lime & "Ĝi faritas el ektoplasmo") -- aŭ eksterplasmo?
          , "=1"       /\ line "ektoplasm"
          , "=2"       /\ line "fantom"
          , "=3"       /\ line "eksterplasm"
          , "long"     /\ line \(PreviousResult prev) ps -> color lime & "ektoplasma" & sufikso ps & " " & prev
          , "short"    /\ colorPrevious lime
          ]
      ]
  }

nuclear :: {| Identified + LangInfo' + Modifier + TagInfo + () }
nuclear = withDefTagInfo
  { id:     sid "nuclear"
  , modify: asHashMap
      [ Regs.item `modifyWith` \item -> item
          { tags      = HS.insert "nuclear" item.tags
          , textColor = green
          , price     = round $ item.price * 15.0 + item.size * 110.0
          }
      ]
  , lines: asLines
      [ english /\
          [ "analysis" /\ line (color green & "It's powered by nuclear energy.")
          , "=1"       /\ line "nuclear"
          , "=2"       /\ line "atomic"
          , "long"     /\ line \(PreviousResult prev) -> color green & "nuclear " & prev
          , "short"    /\ colorPrevious green
          ]
      , esperanto /\
          [ "analysis" /\ line (color green & "Ĝi funkciigitas per nukleo")
          , "=1"       /\ line "nukle"
          , "long"     /\ line \(PreviousResult prev) ps -> color green & "nuklea" & sufikso ps & " " & prev
          , "short"    /\ colorPrevious green
          ]
      ]
  }

secondHand :: {| Identified + LangInfo' + Modifier + TagInfo + () }
secondHand = withDefTagInfo
  { id:     sid "second-hand"
  , modify: asHashMap
      [ Regs.item `modifyWith` \item -> item
          { tags      = HS.insert "second-hand" item.tags
          , textColor = clay
          , price     = item.price * 0.4
          }
      ]
  , lines: asLines
      [ english /\
          [ "analysis" /\ line "It's second hand, but in good shape"
          , "=1"       /\ line "second-hand"
          , "=2"       /\ line "worn"
          , "=3"       /\ line "used"
          , "long"     /\ line \(PreviousResult prev) -> "used " & prev
          ]
      , esperanto /\
          [ "analysis" /\ line "Ĝi eluzitas, sed ankoraŭ bona"
          , "=1"       /\ line "eluzit"
          , "=2"       /\ line "uzit"
          , "long"     /\ line \(PreviousResult prev) ps -> "uzita" & sufikso ps & " " & prev
          ]
      ]
  }

umbral :: {| Identified + LangInfo' + Modifier + TagInfo + () }
umbral = withDefTagInfo
  { id:     sid "umbral"
  , tags:   asHashSet [ "warning" ]
  , modify: asHashMap
      [ Regs.item `modifyWith` \item -> item
          { tags      = HS.insert "mithril" item.tags
          , textColor = purple
          , price     = round $ item.price * 5.0 + item.size * 10.0
          }
      ]
  , lines: asLines
      [ english /\
          [ "analysis" /\ line (color purple & "It's not from this reality")
          , "=1"       /\ line "umbra"
          , "=2"       /\ line "shadow"
          , "long"     /\ line \(PreviousResult prev) -> color purple & "umbral " & prev
          , "short"    /\ colorPrevious purple
          ]
      , esperanto /\
          [ "analysis" /\ line (color purple & "Ĝi ne estas el ĉi tiu realo")
          , "=1"       /\ line "ombr"
          , "=2"       /\ line "ombrej"
          , "long"     /\ line \(PreviousResult prev) ps -> color purple & "ombra" & sufikso ps & " " & prev
          , "short"    /\ colorPrevious purple
          ]
      ]
  }
