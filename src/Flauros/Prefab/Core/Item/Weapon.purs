module Flauros.Prefab.Core.Item.Weapon where

import Prelude

import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Item (Item, little, medium, miniscule, tiny, withDefItem)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (asLines, english, esperanto, line)
import Flauros.Prefab.Registries as Registries
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, asHashSet, sid)
import Flauros.Style.Color (clay, lilac, orange, pink, purple, sand, white, yellow)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Registries.item bardiche
  register Registries.item crossbow
  register Registries.item holyRevenger
  register Registries.item knife
  register Registries.item morningStar

bardiche :: {| Identified + LangInfo' + Item + () }
bardiche = withDefItem
  { id:        sid "bardiche"
  , tags:      asHashSet [ "weapon", "melee", "polearm", "metal" ]
  , price:     50.0
  , size:      little
  , textColor: sand
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "bardiche"
          , "short" /\ line "bardiche"
          ]
      , esperanto /\
          [ "long"  /\ line "berdiŝo"
          , "short" /\ line "berdiŝo"
          ]
      ]
  }

crossbow :: {| Identified + LangInfo' + Item + () }
crossbow = withDefItem
  { id:        sid "crossbow"
  , tags:      asHashSet [ "weapon", "ranged", "crossbow", "wood" ]
  , price:     80.0
  , size:      little
  , textColor: sand
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "crossbow"
          , "short" /\ line "crossbow"
          ]
      , esperanto /\
          [ "long"  /\ line "arbalesto"
          , "short" /\ line "arbalesto"
          ]
      ]
  }

holyRevenger :: {| Identified + LangInfo' + Item + () }
holyRevenger = withDefItem
  { id:        sid "holy-revenger"
  , tags:      asHashSet [ "weapon", "melee", "sword", "enchanted", "divine", "smite" ]
  , price:     4000.0
  , size:      medium
  , textColor: yellow
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "holy revenger"
          , "short" /\ line "holy revenger"
          ]
      , esperanto /\
          [ "long"  /\ line "sankta venĝilo"
          , "short" /\ line "sankta venĝilo"
          ]
      ]
  }

knife :: {| Identified + LangInfo' + Item + () }
knife = withDefItem
  { id:        sid "knife"
  , tags:      asHashSet [ "weapon", "melee", "knife", "metal" ]
  , price:     6.0
  , size:      tiny
  , textColor: sand
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "knife"
          , "short" /\ line "knife"
          ]
      , esperanto /\
          [ "long"  /\ line "tranĉilo"
          , "short" /\ line "tranĉilo"
          ]
      ]
  }

morningStar :: {| Identified + LangInfo' + Item + () }
morningStar = withDefItem
  { id:        sid "morning-star"
  , tags:      asHashSet [ "weapon", "melee", "mace", "metal" ]
  , price:     20.0
  , size:      little
  , textColor: sand
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "morning star"
          , "short" /\ line "morning star"
          ]
      , esperanto /\
          [ "long"  /\ line "matenastelo"
          , "short" /\ line "matenastelo"
          ]
      ]
  }