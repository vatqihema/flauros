module Flauros.Prefab.Core.Item.Base where

import Prelude

import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Item (Item, miniscule, withDefItem)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (asLines, english, esperanto, line)
import Flauros.Prefab.Registries as Registries
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, sid)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Registries.item money

dollars :: Int -> Int
dollars n = n * 100

cents :: Int -> Int
cents n = n

money :: {| Identified + LangInfo' + Item + () }
money = withDefItem
  { id:        sid "money"
  , price:     0.01 -- 100 units of "money" equals 1 price
  , size:      miniscule
  , lines:     asLines
      [ english /\
          [ "long"   /\ line "$silver piece"
          , "short"  /\ line "$sp"
          , "symbol" /\ line "$"
          ]
      , esperanto /\
          [ "long"   /\ line "₷arĝent-ero"
          , "short"  /\ line "₷AE"
          , "symbol" /\ line "₷" -- la spesmilo!
          ]
      ]
  }