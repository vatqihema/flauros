module Flauros.Prefab.Core.Item.Furniture where

import Prelude

import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Item (Item, big, little, medium, withDefItem)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (asLines, english, esperanto, line)
import Flauros.Prefab.Registries as Registries
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, asHashSet, sid)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Registries.item bed
  register Registries.item foldingChair
  register Registries.item icebox
  register Registries.item movingBox
  register Registries.item pictureBox
  register Registries.item sleepingBag
  register Registries.item sofa

bed :: {| Identified + LangInfo' + Item + () }
bed = withDefItem
  { id:        sid "bed"
  , tags:      asHashSet [ "furniture", "sleeping", "fabric" ]
  , price:     200.0
  , size:      big
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "bed"
          , "short" /\ line "bed"
          ]
      , esperanto /\
          [ "long"  /\ line "lito"
          , "short" /\ line "lito"
          ]
      ]
  }

foldingChair :: {| Identified + LangInfo' + Item + () }
foldingChair = withDefItem
  { id:        sid "folding-chair"
  , tags:      asHashSet [ "furniture", "metal", "seating", "foldable", "weapon", "melee" ]
  , price:     20.0
  , size:      medium
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "folding chair"
          , "short" /\ line "folding chair"
          ]
      , esperanto /\
          [ "long"  /\ line "faldseĝo"
          , "short" /\ line "faldseĝo"
          ]
      ]
  }

icebox :: {| Identified + LangInfo' + Item + () }
icebox = withDefItem
  { id:        sid "icebox"
  , tags:      asHashSet [ "furniture", "container", "metal", "chilled", "enchanted" ]
  , price:     160.0
  , size:      big
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "icebox"
          , "short" /\ line "icebox"
          ]
      , esperanto /\
          [ "long"  /\ line "glaciujo"
          , "short" /\ line "glaciujo"
          ]
      ]
  }

-- sorting through gets random/semi random items?
movingBox :: {| Identified + LangInfo' + Item + () }
movingBox = withDefItem
  { id:        sid "moving-box"
  , tags:      asHashSet [ "furniture", "sleeping", "fabric" ]
  , price:     2.0
  , size:      medium
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "moving box"
          , "short" /\ line "box"
          , "desc"  /\ line "You need to get these sorted out."
          ]
      , esperanto /\
          [ "long"  /\ line "mova skatolo"
          , "short" /\ line "skatolo"
          ]
      ]
  }

pictureBox :: {| Identified + LangInfo' + Item + () }
pictureBox = withDefItem
  { id:        sid "picture-box"
  , tags:      asHashSet [ "furniture", "activity", "enchanted", "illusion", "wood" ]
  , price:     350.0
  , size:      medium
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "picture box"
          , "short" /\ line "picture box"
          ]
      , esperanto /\
          [ "long"  /\ line "bildokesto"
          , "short" /\ line "bildokesto"
          ]
      ]
  }

sleepingBag :: {| Identified + LangInfo' + Item + () }
sleepingBag = withDefItem
  { id:        sid "sleeping-bag"
  , tags:      asHashSet [ "furniture", "sleeping", "fabric" ]
  , price:     12.0
  , size:      little
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "sleeping bag"
          , "short" /\ line "sleeping bag"
          , "desc"  /\ line "Wrap-styled sleeping."
          ]
      , esperanto /\
          [ "long"  /\ line "dormosako"
          , "short" /\ line "dormosako"
          ]
      ]
  }

sofa :: {| Identified + LangInfo' + Item + () }
sofa = withDefItem
  { id:        sid "sofa-bag"
  , tags:      asHashSet [ "furniture", "sleeping", "fabric" ]
  , price:     120.0
  , size:      big
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "sofa"
          , "short" /\ line "sofa"
          ]
      ]
  }