module Flauros.Prefab.Core.Item.Plant where

import Prelude

import Data.Maybe (Maybe(..))
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Event (ActionResult(..), Consumable, onEvent)
import Flauros.Data.Item (Item, miniscule, tiny, withDefItem)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (asLines, english, esperanto, line)
import Flauros.Mechanics.Actor (addStatM)
import Flauros.Prefab.Core.StatDef (hunger)
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register, (++))
import Flauros.Prelude (Identified, asHashSet, sid)
import Flauros.Style.Color (clay, green, lilac, orange, pink, purple, sand, white, yellow)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  let edibleItemReg = Regs.item ++ Regs.consumable
  register Regs.item appleblossom
  register Regs.item balsamroot
  register Regs.item blackberry
  register Regs.item cattail
  register Regs.item cherryblossom
  register edibleItemReg dandelion
  register edibleItemReg grapeSprig
  register Regs.item grassWidow
  register Regs.item honeysuckle
  register Regs.item lupine
  register Regs.item milfoil
  register Regs.item mistletoe
  register Regs.item purpleShamrock
  register Regs.item sagebrushViolet
  register Regs.item showyPhlox
  register Regs.item snowberryBouquet
  register Regs.item squashBlossom
  register Regs.item squirrelTail
  register Regs.item sunflower
  register Regs.item thistle
  register Regs.item yarrow

appleblossom :: {| Identified + LangInfo' + Item + () }
appleblossom = withDefItem
  { id:        sid "appleblossom"
  , tags:      asHashSet [ "edible", "flower", "fragrant", "plant" ]
  , price:     1.25
  , size:      tiny
  , textColor: pink
  , lines:     asLines
      [ english /\
          [ "long" /\ line "appleblossoms"
          , "short" /\ line "appleblossoms"
          ]
      , esperanto /\
          [ "long"  /\ line "pomfloroj"
          , "short" /\ line "pomfloroj"
          ]
      ]
  }

balsamroot :: {| Identified + LangInfo' + Item + () }
balsamroot = withDefItem
  { id:        sid "balsamroot"
  , tags:      asHashSet [ "bitter", "edible", "flower", "fuzzy", "plant" ]
  , price:     1.0
  , size:      tiny
  , textColor: yellow
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "arrowleaf basalmroot"
          , "short" /\ line "arrowleaf basalmroot"
          ]
      , esperanto /\
          [ "long"  /\ line "sagofolia balzamradiko"
          , "short" /\ line "sagofolia balzamradiko"
          ]
      ]
  }

blackberry :: {| Identified + LangInfo' + Item + () }
blackberry = withDefItem
  { id:        sid "blackberry"
  , tags:      asHashSet [ "berry", "edible", "fruit", "plant", "sweet", "tart" ]
  , price:     1.0
  , size:      tiny
  , textColor: purple
  , units:     Just "kg"
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "blackberries"
          , "short" /\ line "blackberries"
          ]
      , esperanto /\
          [ "long"  /\ line "rubusberoj"
          , "short" /\ line "rubusberoj"
          ]
      ]
  }

blackberryFlower :: {| Identified + LangInfo' + Item + () }
blackberryFlower = withDefItem
  { id:        sid "blackberry-flower"
  , tags:      asHashSet ["edible", "flower", "plant" ]
  , price:     1.0
  , size:      tiny
  , textColor: white
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "blackberry flower"
          , "short" /\ line "blackberry flower"
          ]
      , esperanto /\
          [ "long"  /\ line "rubusa floro"
          , "short" /\ line "rubusa floro"
          ]
      ]
  }

camas :: {| Identified + LangInfo' + Item + () }
camas = withDefItem
  { id:        sid "camas"
  , tags:      asHashSet [ "edible", "flower", "plant", "sweet" ]
  , price:     2.5
  , size:      tiny
  , textColor: purple
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "camas bouquet"
          , "short" /\ line "camas bouquet"
          ]
      , esperanto /\
          [ "long"  /\ line "kamasfloraro"
          , "short" /\ line "kamasfloraro"
          ]
      ]
  }

cattail :: {| Identified + LangInfo' + Item + () }
cattail = withDefItem
  { id:        sid "cattail"
  , tags:      asHashSet [ "plant" ]
  , price:     2.0
  , size:      tiny
  , textColor: clay
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "broadleaf cattail"
          , "short" /\ line "broadleaf cattail"
          ]
      , esperanto /\
          [ "long"  /\ line "larĝafolia tifeo"
          , "short" /\ line "larĝafolia tifeo"
          ]
      ]
  }

cherryblossom :: {| Identified + LangInfo' + Item + () }
cherryblossom = withDefItem
  { id:        sid "cherryblossom"
  , tags:      asHashSet [ "edible", "flower", "fragrant", "plant" ]
  , price:     2.5
  , size:      tiny
  , textColor: pink
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "cherryblossoms"
          , "short" /\ line "cherryblossoms"
          ]
      , esperanto /\
          [ "long"  /\ line "ĉerizaj floroj"
          , "short" /\ line "ĉerizaj floroj"
          ]
      ]
  }

dandelion :: {| Identified + LangInfo' + Item + Consumable + () }
dandelion = withDefItem
  { id:        sid "dandelion"
  , tags:      asHashSet [ "bitter", "edible", "flower", "plant" ]
  , price:     1.0
  , size:      miniscule
  , textColor: yellow
  , onConsume: \actor -> onEvent do
      Yield <$> addStatM hunger.id 0.2 actor
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "dandelion"
          , "short" /\ line "dandelion"
          ]
      , esperanto /\
          [ "long"  /\ line "leontodo"
          , "short" /\ line "leontodo"
          ]
      ]
  }

grapeSprig :: {| Identified + LangInfo' + Item + Consumable () }
grapeSprig = withDefItem
  { id:        sid "grape-sprig"
  , tags:      asHashSet [ "sweet", "sour", "crunchy", "edible", "plant" ]
  , price:     2.50
  , size:      tiny
  , textColor: purple
  , onConsume: \actor -> onEvent do
      Yield <$> addStatM hunger.id 0.5 actor
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "grape sprig"
          , "short" /\ line "grapes"
          ]
      , esperanto /\
          [ "long"  /\ line "vinberbranĉeto"
          , "short" /\ line "vinberoj"
          ]
      ]
  }

grassWidow :: {| Identified + LangInfo' + Item + () }
grassWidow = withDefItem
  { id:        sid "grass-widow"
  , tags:      asHashSet [ "bitter", "edible", "flower", "plant" ]
  , price:     2.0
  , size:      tiny
  , textColor: purple
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "grass widow"
          , "short" /\ line "grass widow"
          ]
      , esperanto /\
          [ "long"  /\ line "herbvidvo"
          , "short" /\ line "herbvidvo"
          ]
      ]
  }

honeysuckle :: {| Identified + LangInfo' + Item + () }
honeysuckle = withDefItem
  { id:        sid "honeysuckle"
  , tags:      asHashSet [ "edible", "flower", "plant", "sweet" ]
  , price:     2.25
  , size:      miniscule
  , textColor: orange
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "honeysuckle"
          , "short" /\ line "honeysuckle"
          ]
      , esperanto /\
          [ "long"  /\ line "lonicero"
          , "short" /\ line "lonicero"
          ]
      ]
  }

lupine :: {| Identified + LangInfo' + Item + () }
lupine = withDefItem
  { id:        sid "lupine"
  , tags:      asHashSet [ "bitter", "edible", "flower", "plant" ]
  , price:     3.5
  , size:      tiny
  , textColor: purple
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "broadleaf lupine"
          , "short" /\ line "broadleaf lupine"
          ]
      , esperanto /\
          [ "long"  /\ line "larĝafolia lupeno"
          , "short" /\ line "larĝafolia lupeno"
          ]
      ]
  }

milfoil :: {| Identified + LangInfo' + Item + () }
milfoil = withDefItem
  { id:        sid "milfoil"
  , tags:      asHashSet [ "bitter", "flower", "plant" ]
  , price:     0.25
  , size:      tiny
  , textColor: green
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "milfoil sprig"
          , "short" /\ line "milfoil"
          ]
      , esperanto /\
          [ "long"  /\ line "milfolibranĉeto"
          , "short" /\ line "milfolio"
          ]
      ]
  }

mistletoe :: {| Identified + LangInfo' + Item + () }
mistletoe = withDefItem
  { id:        sid "mistletoe"
  , tags:      asHashSet [ "bitter", "flower", "plant" ]
  , price:     1.25
  , size:      tiny
  , textColor: green
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "mistletoe sprig"
          , "short" /\ line "mistletoe"
          ]
      , esperanto /\
          [ "long"  /\ line "viskobranĉeto"
          , "short" /\ line "visko"
          ]
      ]
  }

purpleShamrock :: {| Identified + LangInfo' + Item + () }
purpleShamrock = withDefItem
  { id:        sid "purple-shamrock"
  , tags:      asHashSet [ "bitter", "flower", "plant" ]
  , price:     1.25
  , size:      tiny
  , textColor: purple
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "purple shamrock"
          , "short" /\ line "purple shamrock"
          ]
      , esperanto /\
          [ "long"  /\ line "purpura trifolio"
          , "short" /\ line "purpura trifolio"
          ]
      ]
  }

sagebrushViolet :: {| Identified + LangInfo' + Item + () }
sagebrushViolet = withDefItem
  { id:        sid "sagebrush-violet"
  , tags:      asHashSet [ "bland", "edible", "flower", "plant" ]
  , price:     2.5
  , size:      miniscule
  , textColor: purple
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "sagebrush violets"
          , "short" /\ line "sagebrush violets"
          ]
      , esperanto /\
          [ "long"  /\ line "salvibrosaj violoj"
          , "short" /\ line "salvibrosaj violoj"
          ]
      ]
  }

showyPhlox :: {| Identified + LangInfo' + Item + () }
showyPhlox = withDefItem
  { id:        sid "showy-phlox"
  , tags:      asHashSet [ "bland", "edible", "flower", "plant" ]
  , price:     1.5
  , size:      miniscule
  , textColor: white
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "showy phlox"
          , "short" /\ line "showy phlox"
          ]
      , esperanto /\
          [ "long"  /\ line "bombasta flokso"
          , "short" /\ line "bombasta flokso"
          ]
      ]
  }

snowberryBouquet :: {| Identified + LangInfo' + Item + () }
snowberryBouquet = withDefItem
  { id:        sid "snowberry-bouquet"
  , tags:      asHashSet [ "edible", "flower", "plant", "poison", "sour" ]
  , price:     3.0
  , size:      miniscule
  , textColor: white
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "snowberry bouquet"
          , "short" /\ line "snowberry bouquet"
          ]
      , esperanto /\
          [ "long"  /\ line "neĝbera floraro"
          , "short" /\ line "neĝbera floraro"
          ]
      ]
  }

squashBlossom :: {| Identified + LangInfo' + Item + () }
squashBlossom = withDefItem
  { id:        sid "squash-blossom"
  , tags:      asHashSet [ "edible", "flower", "plant" ]
  , price:     1.75
  , size:      tiny
  , textColor: yellow
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "squash blossom sprig"
          , "short" /\ line "squash blossoms"
          ]
      , esperanto /\
          [ "long"  /\ line "skvaŝflorbranĉeto"
          , "short" /\ line "skvaŝfloroj"
          ]
      ]
  }

squirrelTail :: {| Identified + LangInfo' + Item + () }
squirrelTail = withDefItem
  { id:        sid "squirrel-tail"
  , tags:      asHashSet [ "flower", "plant" ]
  , price:     1.75
  , size:      tiny
  , textColor: sand
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "squirreltail sheaf"
          , "short" /\ line "squirreltail sheaf"
          ]
      , esperanto /\
          [ "long"  /\ line "basena sekalo"
          , "short" /\ line "basena sekalo"
          ]
      ]
  }

sunflower :: {| Identified + LangInfo' + Item + () }
sunflower = withDefItem
  { id:        sid "sunflower"
  , tags:      asHashSet [ "edible", "flower", "nutty", "plant" ]
  , price:     3.0
  , size:      miniscule
  , textColor: yellow
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "sunflower"
          , "short" /\ line "sunflower"
          ]
      , esperanto /\
          [ "long"  /\ line "sunflower"
          , "short" /\ line "sunflower"
          ]
      ]
  }

thistle :: {| Identified + LangInfo' + Item + () }
thistle = withDefItem
  { id:        sid "thistle"
  , tags:      asHashSet [ "flower", "plant" ]
  , price:     4.0
  , size:      miniscule
  , textColor: lilac
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "wavyleaf thistle"
          , "short" /\ line "wavyleaf thistle"
          ]
      , esperanto /\
          [ "long"  /\ line "ondafolia kardo"
          , "short" /\ line "ondafolia kardo"
          ]
      ]
  }

yarrow :: {| Identified + LangInfo' + Item + () }
yarrow = withDefItem
  { id:        sid "yarrow"
  , tags:      asHashSet [ "flower", "plant" ]
  , price:     4.0
  , size:      miniscule
  , textColor: yellow
  , lines:     asLines
      [ english /\
          [ "long"  /\ line "yarrow"
          , "short" /\ line "yarrow"
          ]
      , esperanto /\
          [ "long"  /\ line "akileo"
          , "short" /\ line "akileo"
          ]
      ]
  }