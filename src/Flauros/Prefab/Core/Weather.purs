module Flauros.Prefab.Core.Weather where

import Prelude

import Data.Date (Date, Month(..))
import Data.Maybe (Maybe(..))
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Data.Time (TimeOfDay(..), dateHash, getMonth)
import Flauros.Data.Weather (Season(..), season, withDefWeather)
import Flauros.Lang (asLines, english, esperanto, line)
import Flauros.Lang.Text (background, color, italics, plain, (&))
import Flauros.Lang.Text as Text
import Flauros.Prefab.Registries (Weather')
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, asHashMap, asHashSet, sid, (|>))
import Flauros.Style.Color (blue, clay, cornflower, cyan, grass, green, lavender, orange, pink, purple, red, redder, sand, white, yellow)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

hashMod :: Int -> Date -> Boolean
hashMod n date = 0 == (dateHash date) `mod` n

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.weather blizzard
  register Regs.weather bloodRain
  register Regs.weather clear
  register Regs.weather fog
  register Regs.weather frogRain
  register Regs.weather heavyRain
  register Regs.weather insectSwarm
  register Regs.weather lightningStorm
  register Regs.weather lightRain
  register Regs.weather mirage
  register Regs.weather smoke
  register Regs.weather snow
  register Regs.weather squidFall
  register Regs.weather utterdark
  register Regs.weather wildfires
  register Regs.weather windstorm
  register Regs.weather windy

blizzard :: {| Identified + LangInfo' + Weather' + () }
blizzard = withDefWeather
  { id:         sid "blizzard"
  , tags:       asHashSet [ "obscuring", "show-chance" ]
  , colors:     { text: white }
  , contagion:  0.6
  , eventMods:  asHashMap [ "ghostly-phenomenon" /\ 0.4 ]
  , followedBy: [ sid "snow" ]
  , tempMod:   \_ date _ -> negate (1 + (dateHash date `mod` 2))
  , lines: asLines
      [ english /\
          [ "long"    /\ line (color white & "blizzard")
          , "short"   /\ line (color white & "blizzard")
          , "icon"    /\ line "🌨️"
          , "tagline" /\ line "A winter advisory warning is in effect."
          ]
      , esperanto /\
          [ "long"    /\ line (color white & "neĝventego")
          , "short"   /\ line (color white & "neĝventego")
          , "tagline" /\ line "Vintra konsulta averto okazas."
          ]
      ]
  }

bloodRain :: {| Identified + LangInfo' + Weather' + () }
bloodRain = withDefWeather
  { id:        sid "blood-rain"
  , tags:      asHashSet [ "obscuring", "show-chance" ]
  , colors:    { text: redder }
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 1.0 ]
  , delay:     Just 20
  , lines: asLines
      [ english /\
          [ "long"    /\ line (color redder & "blood rain")
          , "short"   /\ line (color redder & "blood rain")
          , "icon"    /\ line "🩸"
          , "tagline" /\ line \(DS.Store store) -> case store.temperature of
              t | t < 0 -> "Expect sparse bloody hail."
              t | t < 1 -> "A day to hide inside and not wear white."
              t | t < 3 -> "Comfortably warm, if a little macabre."
              _         -> "Hot, humid, and raining blood."
          ]
      , esperanto /\
          [ "long"    /\ line (color redder & "sangopluvo")
          , "short"   /\ line (color redder & "sangopluvo")
          ]
      ]
  }

clear :: {| Identified + LangInfo' + Weather' + () }
clear = withDefWeather
  { id:        sid "clear"
  , colors:    { text: sand }
  , contagion: 0.2
  , lines: asLines
      [ english /\
          [ "long"    /\ line (color sand & "clear")
          , "short"   /\ line (color sand & "clear")
          , "icon"    /\ line \(DS.Store { currentTime, temperature, timeOfDay }) -> case timeOfDay of
              Dawn  -> "🌅"
              Day   -> case season currentTime of
                Winter | temperature <= -1 -> "☁️"
                Spring | temperature <=  0 -> "☁️"
                Spring | temperature <=  1 -> "🌤️"
                _                          -> "☀️"
              Dusk  -> "🌇"
              Night -> "🌃"
          , "tagline" /\ line \(DS.Store { currentTime, temperature, timeOfDay }) -> case timeOfDay of
              Dawn  -> case temperature of
                t | t <= -2 -> "A lovely day to stay inside and sleep in."
                t | t <= -1 -> "Be sure to button up!"
                t | t <=  2 -> "A real pleasant morning."
                _           -> "Looks like it's going to be a very hot day."
              Day   ->
                let ssn = season currentTime
                 in case temperature of
                    t | t <= -2 -> "Stay inside if you can."
                    t | t <= -1 -> "Be sure to button up!"
                    t | t <=  2 -> case ssn of
                      Spring | t == 2 -> "It's sunny and dandelions are everywhere."
                      Spring          -> "It's overcast."
                      Summer          -> "A nice summer day."
                      Winter          -> "A comfortable winter day."
                      _               -> "A real pleasant day."
                    _           -> "Limit your time in the heat!"
              Dusk  -> case temperature of
                t | t <= -2 -> "It's well below freezing and getting colder."
                t | t <= -1 -> "It's only going to get colder as the sun goes down."
                t | t <=  2 -> "Comfortable weather to watch the sunset"
                _           -> "It's not going to cool down much when night comes."
              Night ->
                let ssn = season currentTime
                 in case temperature of
                    t | t <= -2 -> "Horribly chill and horrible dark."
                    t | t <= -1 -> "A good night for a hot bath."
                    t | t <=  2 -> case ssn of
                      Spring | t == 2 -> "It's a warm spring night."
                      Summer          -> "A cool summer night."
                      Winter          -> "A cozy winter night."
                      _               -> "A comfortable night fit for stargazing."
                    _           -> "It's not going to cool down much overnight."
          ]
      , esperanto /\
          [ "long"    /\ line (color sand & "klara ĉielo")
          , "short"   /\ line (color sand & "klara")
          ]
      ]
  }

fog :: {| Identified + LangInfo' + Weather' + () }
fog = withDefWeather
  { id:        sid "fog"
  , tags:      asHashSet [ "obscuring" ]
  , colors:    { text: yellow }
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 0.4 ]
  , contagion: 0.4
  , tempMod:   \_ date _ -> if hashMod 4 date then -2 else -1
  , lines: asLines
      [ english /\
          [ "long"    /\ line (color yellow & "dense fog")
          , "short"   /\ line (color yellow & "foggy")
          , "icon"    /\ line "🌁"
          , "tagline" /\ line \(DS.Store store) -> case store.temperature of
              t | t < 0 -> "Expect reduced visibility and icy roads."
              t | t < 3 -> "Slow down in reduced visibility."
              _         -> "Intense heat and humidity levels."
          ]
      , esperanto /\
          [ "long"    /\ line (color yellow & "densa nebulo")
          , "short"   /\ line (color yellow & "nebulo")
          ]
      ]
  }

frogRain :: {| Identified + LangInfo' + Weather' + () }
frogRain = withDefWeather
  { id:         sid "frog-rain"
  , tags:      asHashSet [ "obscuring" ]
  , colors:     { text: green }
  , eventMods:  asHashMap [ "ghostly-phenomenon" /\ 0.3 ]
  , followedBy: [ sid "light-rain" ]
  , lines: asLines
      [ english /\
          [ "long"    /\ line (color green & "frog rain")
          , "short"   /\ line (color green & "frogs")
          , "icon"    /\ line "🐸"
          , "tagline" /\ line "The ribbits are deafening."
          ]
      , esperanto /\
          [ "long"    /\ line (color green & "ranoplumo")
          , "short"   /\ line (color green & "ranoj")
          ]
      ]
  }

heavyRain :: {| Identified + LangInfo' + Weather' + () }
heavyRain = withDefWeather
  { id:        sid "heavy-rain"
  , tags:      asHashSet [ "show-chance" ]
  , colors:    { text: blue }
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 0.25 ]
  , contagion: 0.2
  , tempMod:   \_ date -> case _ of
      t | t <= 0          -> 0
      _ | hashMod 2 date -> -1
      t                  -> negate (t / 2)
  , lines: asLines
      [ english /\
          [ "long"    /\ line (color blue & "heavy rain")
          , "short"   /\ line (color blue & "very rainy")
          , "icon"    /\ line "🌧️"
          , "tagline" /\ line \(DS.Store { temperature, timeOfDay }) -> case temperature of
              t | t <= -1 -> "Expect frequent hail showers."
              t | t <=  1 -> if   timeOfDay <= Day
                             then "A day to appreciate staying inside."
                             else "A night to appreciate staying inside."
              t | t <= 3  -> "A refreshing downpour to cool the heat."
              _           -> "Expect hot shower-temperature rain."
          ]
      , esperanto /\
          [ "long"    /\ line (color blue & "pluvego")
          , "short"   /\ line (color blue & "pluvego")
          ]
      ]
  }

data Insect = Gelugfly | Locust | Tick | Bee | Mosquito | Fly

derive instance eqInsect :: Eq Insect

insectOf :: Month -> Insect
insectOf = case _ of
  m | m <= February
   || m == December  -> Gelugfly
  m | m <= March     -> Locust
  m | m <= May       -> Tick
  m | m == June      -> Bee
  m | m <= September -> Mosquito
  _                  -> Fly

insectSwarm :: {| Identified + LangInfo' + Weather' + () }
insectSwarm = withDefWeather
  { id:        sid "insect-swarm"
  , tags:      asHashSet [ "obscuring" ]
  , colors:    { text: clay }
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 0.7 ]
  , contagion: 0.75
  , delay:     Just 30
  , lines: asLines
      [ english /\ -- no gameplay functional, why have I added this
          [ "long"    /\ line \(DS.Store { currentTime }) -> case insectOf (getMonth currentTime) of
              Bee      -> color yellow   & "bee swarms"
              Fly      -> color lavender & "fly swarms"
              Gelugfly -> color cyan     & "gelugfly swarms"
              Locust   -> color yellow   & "locust swarms"
              Mosquito -> color red      & "mosquito swarms"
              Tick     -> color clay     & "tick swarms"
          , "short"   /\ line \(DS.Store { currentTime }) -> case insectOf (getMonth currentTime) of
              Bee      -> color yellow   & "swarms"
              Fly      -> color lavender & "swarms"
              Gelugfly -> color cyan     & "swarms"
              Locust   -> color yellow   & "swarms"
              Mosquito -> color red      & "swarms"
              _        -> color clay     & "swarms"
          , "icon"    /\ line \(DS.Store { currentTime }) -> case insectOf (getMonth currentTime) of
              Bee      -> "🐝"
              Locust   -> "🦗"
              Mosquito -> "🦟"
              Tick     -> "🕷️"
              _        -> "🪰"
          , "tagline" /\ line \(DS.Store { currentTime, temperature }) -> case insectOf (getMonth currentTime) of
              Bee      -> "Suprisingly pleasant despite the buzzing."
              Gelugfly -> "Careful not to catch frostbite!"
              _        -> case temperature of
                t | t < 0 ->"Expect to navigate around lethargic insects."
                t | t < 1 -> "Wear heavy clothing and cover your gardens."
                t | t < 3 -> "Try to wear breathable protective gear."
                _         -> "Burning and buzzing."
          ]
      , esperanto /\
          [ "long"    /\ line \(DS.Store { currentTime }) -> case insectOf (getMonth currentTime) of
              Bee      -> color yellow   & "svarmoj da abeloj"
              Fly      -> color lavender & "svarmoj da muŝoj"
              Gelugfly -> color cyan     & "svarmoj da geldipteroj"
              Locust   -> color yellow   & "svarmoj da saltulo"
              Mosquito -> color red      & "svarmoj da moskitoj"
              Tick     -> color clay     & "svarmoj da iksodoj"
          , "short"   /\ line \(DS.Store { currentTime }) -> case insectOf (getMonth currentTime) of
              Bee      -> color yellow   & "svarmoj"
              Fly      -> color lavender & "svarmoj"
              Gelugfly -> color cyan     & "svarmoj"
              Locust   -> color yellow   & "svarmoj"
              Mosquito -> color red      & "svarmoj"
              _        -> color clay     & "svarmoj"
          ]
      ]
  }
  
lightningStorm :: {| Identified + LangInfo' + Weather' + () }
lightningStorm = withDefWeather
  { id:        sid "lightning-storm"
  , tags:      asHashSet [ "obscuring", "show-chance" ]
  , colors:    { text: lavender }
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 0.4 ]
  , delay:     Just 7
  , tempMod:   \_ date -> case _ of
      t | t <= 0                     -> 0
      _ | 0 == dateHash date `mod` 2 -> -1
      t                              -> negate (t / 2)
  , lines: asLines
      [ english /\
          [ "long"    /\ line (color purple & "lightning storm")
          , "short"   /\ line (color purple & "stormy")
          , "icon"    /\ line "🌩️"
          , "tagline" /\ line \(DS.Store { temperature, timeOfDay }) -> case temperature of
              t | t < -2 -> "Find immediate shelter."
              t | t <  0 -> "A miserable time to be outside."
              t | t <  3 -> case timeOfDay of
                Night    -> "A dark and stormy night."
                _        -> "It just keeps pouring down."
              _          -> "Hoping the lightning doesn't cause fires."
          ]
      , esperanto /\
          [ "long"    /\ line (color purple & "fulmŝtormo")
          , "short"   /\ line (color purple & "ŝtorma")
          ]
      ]
  }

lightRain :: {| Identified + LangInfo' + Weather' + () }
lightRain = withDefWeather
  { id:        sid "light-rain"
  , tags:      asHashSet [ "obscuring", "show-chance" ]
  , colors:    { text: cornflower }
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 0.1 ]
  , contagion: 0.6
  , tempMod:   \_ date -> case _ of
      t | t <= 0 -> 0
      t | t <= 3 -> if hashMod 2 date then -1 else 0
      t          -> if hashMod 2 date then -1 else negate (t / 2)
  , lines: asLines
      [ english /\
          [ "long"    /\ line (color cornflower & "light rain")
          , "short"   /\ line (color cornflower & "rainy")
          , "icon"    /\ line "☔"
          , "tagline" /\ line \(DS.Store { temperature }) -> case temperature of
              t | t < 0 -> "Expect sparse hail showers."
              t | t < 1 -> "Make sure to carry an umbrella!"
              t | t < 3 -> "Rain in the heat is always refreshing."
              _         -> "The rain is not helping with the heat!"
          ]
      , esperanto /\
          [ "long"    /\ line (color cornflower & "pluveto")
          , "short"   /\ line (color cornflower & "pluveto")
          ]
      ]
  }

mirage :: {| Identified + LangInfo' + Weather' + () }
mirage = withDefWeather
  { id:         sid "mirage"
  , tags:       asHashSet [ "obscuring" ]
  , colors:     { text: pink }
  , contagion:  0.666
  , delay:      Just 26
  , followedBy: [ sid "clear" ]
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 0.15 ]
  , lines: asLines
      [ english /\
          [ "long"    /\ line (color pink & "mirage")
          , "short"   /\ line (color pink & "mirage")
          , "icon"    /\ line "🪞"
          , "tagline" /\ line ("Follow all traffic instructions in obscured areas, and have a form of " & italics & "true seeing" & Text.clear & " prepared if possible.")
          ]
      , esperanto /\
          [ "long"    /\ line (color pink & "miraĝo")
          , "short"   /\ line (color pink & "miraĝo")
          ]
      ]
  }

smoke :: {| Identified + LangInfo' + Weather' + () }
smoke = withDefWeather
  { id:        sid "smoke"
  , tags:      asHashSet [ "obscuring" ]
  , colors:    { text: lavender }
  , contagion: 0.666
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 0.15 ]
  , lines: asLines
      [ english /\
          [ "long"    /\ line (color lavender & "smoke")
          , "short"   /\ line (color lavender & "smoky")
          , "icon"    /\ line "🌫️"
          , "tagline" /\ line \(DS.Store store) -> case store.timeOfDay of
              Dawn  -> "The smoke is causing a " & color red & "red sunrise."
              Day   -> plain "Poor air quality. Consider wearing a mask!"
              Dusk  -> "The sunset will appear " & color red & "exceptionally red this evening."
              Night -> "The moon is visible through the haze, and it's a " & color orange & "bright orange."
          ]
      , esperanto /\
          [ "long"    /\ line (color lavender & "fumo")
          , "short"   /\ line (color lavender & "fuma")
          ]
      ]
  }
  
snow :: {| Identified + LangInfo' + Weather' + () }
snow = withDefWeather
  { id:        sid "snow"
  , tags:      asHashSet [ "obscuring", "show-chance" ]
  , colors:    { text: white }
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 0.1 ]
  , tempMod:   \_ date _ ->
      if   0 == (dateHash date) `mod` 2
      then -1
      else 0
  , lines: asLines
      [ english /\
          [ "long"    /\ line (color white & "snow")
          , "short"   /\ line (color white & "snow")
          , "icon"    /\ line "🌨️"
          , "tagline" /\ line \(DS.Store store) -> case store.temperature of
              t | t <= -2 -> "A winter advisory warning is in effect."
              t | t <=  0 -> "The fresh snow looks like glitter✨"
              _           -> "The flakes are melting as they land."
          ]
      , esperanto /\
          [ "long"    /\ line (color white & "neĝo")
          , "short"   /\ line (color white & "neĝo")
          ]
      ]
  }
  
squidFall :: {| Identified + LangInfo' + Weather' + () }
squidFall = withDefWeather
  { id:        sid "squid-fall"
  , tags:      asHashSet [ "obscuring", "show-chance" ]
  , colors:    { text: pink }
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 2.0 ]
  , tempMod:   \_ date _ ->
      if   0 == (dateHash date) `mod` 2
      then -1
      else 0
  , lines: asLines
      [ english /\
          [ "long"    /\ line (color pink & "squidfall")
          , "short"   /\ line (color pink & "squid")
          , "icon"    /\ line "🦑"
          , "tagline" /\ line "Expect medium to heavy levels of falling squids."
          ]
      , esperanto /\
          [ "long"    /\ line (color pink & "kalmarofalo")
          , "short"   /\ line (color pink & "kalmaro")
          ]
      ]
  }
  
utterdark :: {| Identified + LangInfo' + Weather' + () }
utterdark = withDefWeather
  { id:        sid "utterdark"
  , tags:      asHashSet [ "obscuring" ]
  , colors:    { text: lavender }
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 1.0 ]
  , tempMod:   \_ _ t -> t / 2 |> min 0 |> (_ - 1)
  , lines: asLines
      [ english /\
          [ "long"    /\ line (color lavender & "utterdark")
          , "short"   /\ line (color lavender & "utterdark")
          , "icon"    /\ line "🌌"
          , "tagline" /\ line \(DS.Store store) -> case store.temperature of
              t | t <  0 -> "The sky is an icy void."
              _          -> "It's pitch black outside."
          ]
      , esperanto /\
          [ "long"    /\ line (color lavender & "totalmallumo")
          , "short"   /\ line (color lavender & "mallumo")
          ]
      ]
  }
  
warpstorm :: {| Identified + LangInfo' + Weather' + () }
warpstorm = withDefWeather
  { id:        sid "warpstorm"
  , tags:      asHashSet [ "obscuring" ]
  , colors:    { text: purple }
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 1.5, "wild-magic" /\ 2.0 ]
  , delay:     Just 60
  , followedBy: [ sid "blood-rain", sid "clear", sid "frog-rain", sid "insect-swarm", sid "lightning-storm", sid "mirage", sid "snow", sid "squid-fall", sid "utterdark", sid "windstorm" ]
  , tempMod:   \_ _ _ -> -1
  , lines: asLines
      [ english /\
          [ "long"    /\ line \(DS.Store { currentTime }) -> case season currentTime of
              Summer -> color yellow & "positive warpstorm"
              Winter -> color white & background _.navy & "negative warpstorm"
              _      -> color purple & "warpstorm"
          , "short"    /\ line \(DS.Store { currentTime }) -> case season currentTime of
              Summer -> color yellow & "positive warpstorm"
              Winter -> color white & background _.navy & "negative warpstorm"
              _      -> color purple & "warpstorm"
          , "icon"    /\ line \(DS.Store { currentTime }) -> case season currentTime of
              Summer -> "🌟"
              Winter -> "👾"
              _      -> "🌌"
          , "tagline" /\ line "Stay inside for the duration of the storm."
          ]
      ]
  }
  
wildfires :: {| Identified + LangInfo' + Weather' + () }
wildfires = withDefWeather
  { id:         sid "wildfires"
  , tags:       asHashSet [ "obscuring" ]
  , colors:     { text: red }
  , eventMods:  asHashMap [ "ghostly-phenomenon" /\ 0.3 ]
  , contagion:  0.9
  , followedBy: [ sid "smoke" ]
  , tempMod:    \_ date _ ->
      if   hashMod 2 date
      then 1
      else if   hashMod 3 date
           then -1
           else  0
  , lines: asLines
      [ english /\
          [ "long"    /\ line (color red & "wildfires")
          , "short"   /\ line (color red & "fires")
          , "icon"    /\ line "🔥"
          , "tagline" /\ line "Hazardous air quality. Try to wear a mask when you're out!"
          ]
      , esperanto /\
          [ "long"    /\ line (color red & "sovaĝofajroj")
          , "short"   /\ line (color red & "fajroj")
          , "tagline" /\ line "Riska aera stato. Provu porti maskon kiam eksteren!"
          ]
      ]
  }
  
windstorm :: {| Identified + LangInfo' + Weather' + () }
windstorm = withDefWeather
  { id:        sid "windstorm"
  , tags:      asHashSet [ "obscuring" ]
  , colors:    { text: lavender }
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 0.4 ]
  , contagion: 0.3
  , delay:     Just 7
  , tempMod:   \_ _ _ -> -1
  , lines: asLines
      [ english /\
          [ "long"    /\ line \(DS.Store { temperature }) -> case temperature of
              t | t >= 3 -> color clay     & "sirocco"
              _          -> color lavender & "windstorm"
          , "short"    /\ line \(DS.Store { temperature }) -> case temperature of
              t | t >= 3 -> color clay     & "sirocco"
              _          -> color lavender & "windstorm"
          , "icon"    /\ line "🌪️"
          , "tagline" /\ line \(DS.Store store) -> case store.temperature of
              t | t >= 3 -> color clay & "Beware irritation from dust and hot conditions."
              _          -> plain "Stay indoors and away from trees."
          ]
      , esperanto /\
          [ "long"    /\ line \(DS.Store { temperature }) -> case temperature of
              t | t >= 3 -> color clay     & "siroko"
              _          -> color lavender & "ventoŝtormo"
          , "short"    /\ line \(DS.Store { temperature }) -> case temperature of
              t | t >= 3 -> color clay     & "siroko"
              _          -> color lavender & "ventoŝtormo"
          ]
      ]
  }
  
windy :: {| Identified + LangInfo' + Weather' + () }
windy = withDefWeather
  { id:        sid "windy"
  , colors:    { text: grass }
  , eventMods: asHashMap [ "ghostly-phenomenon" /\ 0.1 ]
  , contagion: 0.4
  , tempMod:   \_ date _ -> dateHash date `mod` 2
  , lines: asLines
      [ english /\
          [ "long"    /\ line (color grass & "windy")
          , "short"   /\ line (color grass & "windy")
          , "icon"    /\ line "🍃"
          , "tagline" /\ line \(DS.Store store) -> case store.temperature of
              t | t <= -2 -> "Protect against windchill-related frostbite."
              t | t <= -1 -> "It's best to stay inside and out of the icy wind."
              t | t <=  0 -> "Prepare for windchill."
              t | t <=  2 -> "The breeze is very pleasent."
              _           -> "A hot, dry, and dusty wind."
          ]
      , esperanto /\
          [ "long"    /\ line (color grass & "venta")
          , "short"   /\ line (color grass & "venta")
          ]
      ]
  }