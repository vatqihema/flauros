module Flauros.Prefab.Core.Setting.Base where

import Prelude

import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Setting (Storage(..), ValueRange(..), mkBehavior, withDefSetting)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (asLines, english, esperanto, line)
import Flauros.Lang.Text (color, outlinedIcon, (&))
import Flauros.Prefab.Registries (Setting')
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, sid, withDefTags)
import Flauros.Style.Color (blue, defaultColors, red, redder, yellow)
import Halogen.Store.Monad (class MonadStore, updateStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.line    action
  register Regs.line    categoryName
  register Regs.line    categorySymbol
  register Regs.line    categoryTitle

action :: {| Identified + LangInfo' + () }
action = withDefTags
  { id:       sid "setting/action"
  , lines:    asLines
      [ english /\
          [ "reset" /\ line "Reset"
          ]
      ]
  }

categoryName :: {| Identified + LangInfo' + () }
categoryName = withDefTags
  { id:       sid "setting/category-name"
  , lines:    asLines
      [ english /\
          [ "accessibility" /\ line "accessibility"
          , "color"         /\ line "color"
          , "debug"         /\ line "debug"
          ]
      , esperanto /\
          [ "accessibility" /\ line "alireblo"
          , "color"         /\ line "koloro"
          , "debug"         /\ line "sencimigi"
          ]
      ]
  }

categorySymbol :: {| Identified + LangInfo' + () }
categorySymbol = withDefTags
  { id:       sid "setting/category-symbol"
  , lines:    asLines
      [ english /\
          [ "accessibility" /\ line (color blue   & outlinedIcon "settings_accessibility")
          , "color"         /\ line (color yellow & outlinedIcon "palette")
          , "debug"         /\ line (color red    & outlinedIcon "emoji_nature")
          ]
      ]
  }

categoryTitle :: {| Identified + LangInfo' + () }
categoryTitle = withDefTags
  { id:       sid "setting/category-title"
  , lines:    asLines
      [ english /\
          [ "accessibility" /\ line (color blue   & "💙Accessibility")
          , "color"         /\ line (color yellow & "🎨Color")
          , "debug"         /\ line (color redder & "🐞Debug")
          ]
      , esperanto /\
          [ "accessibility" /\ line (color blue   & "💙Alireblo")
          , "color"         /\ line (color yellow & "🎨Koloro")
          , "debug"         /\ line (color redder & "🐞Sencimigi")
          ]
      ]
  }
