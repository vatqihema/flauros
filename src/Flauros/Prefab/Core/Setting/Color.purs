module Flauros.Prefab.Core.Setting.Color where


import Prelude

import Data.Map (Map)
import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Setting (Storage(..), ValueRange(..), mkBehavior, withDefConfig, withDefSetting)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (Lang(..), Line, asLines, english, esperanto, line)
import Flauros.Lang.Text (color, outlinedIcon, roundedIcon, (&))
import Flauros.Log (LogType(..))
import Flauros.Prefab.Registries (Setting')
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, sid, withDefTags)
import Flauros.Style.Color (Colors, blue, defaultColors, red, yellow)
import Halogen.Store.Monad (class MonadStore, updateStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.setting colorRed

colorRed :: {| Identified + LangInfo' + Setting' + () }
colorRed = withDefSetting
  { id:       sid "color-red"
  , category: "color"
  , behavior: mkBehavior
      { default:   red defaultColors
      , onChange: \prev new -> do
          DS.log Info $ "red was " <> show prev <> ", now is " <> show new
          updateStore $ DS.action \s -> s { colors = s.colors { red = new } }
      , values:    ColorPicker identity identity
      , storage:   BrowserStorage
      , isEnabled: \_ -> true
      }
  , lines:     asLines
      [ english /\
          [ "name" /\ line (color red & roundedIcon "gradient" & "red")
          , "desc" /\ line ("The color displayed as " & color red & "`red`")
          ]
      , esperanto /\
          [ "name" /\ line (color red & "ruĝo")
          , "desc" /\ line ("La koloro vidigota pro " & color red & "`ruĝo`")
          ]
      ]
  }
