module Flauros.Prefab.Core.Country where

import Prelude

import Data.Tuple.Nested ((/\))
import Effect.Class (class MonadEffect)
import Flauros.Data.Country (Country, withDefCountry)
import Flauros.Data.Store (LangInfo')
import Flauros.Data.Store as DS
import Flauros.Lang (asLines, english, esperanto, line)
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry (register)
import Flauros.Prelude (Identified, asHashMap, sid)
import Halogen.Store.Monad (class MonadStore)
import Type.Row (type (+))

init :: forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit
init = do
  register Regs.country althesia
  register Regs.country celaddor
  register Regs.country gehenna
  register Regs.country kh'nazhga'
  register Regs.country naswatBiti

althesia :: {| Identified + LangInfo' + Country + () }
althesia = withDefCountry
  { id:         sid "althesia"
  , population: 289_000_000
  , relations:  asHashMap
      [ sid "celaddor"   /\  5.0
      , sid "kh'nazhga'" /\ -4.0
      ]
  , stats:
      { crime:        5.0
      , disparity:   10.0
      , instability:  4.0
      , wealth:       9.0
      }
  , lines:     asLines
      [ english /\
          [ "name"    /\ line "Althesia"
          , "full"    /\ line "Unified Kingdoms of Althesia"
          , "adj"     /\ line "Althesian"
          , "demonym" /\ line "Althesian"
          ]
      , esperanto /\
          [ "name"    /\ line "Altezio"
          , "full"    /\ line "Unuecaj Reĝlandoj de Altezio"
          , "adj"     /\ line "Altezia"
          , "demonym" /\ line "alteziano"
          ]
      ]
  }

celaddor :: {| Identified + LangInfo' + Country + () }
celaddor = withDefCountry
  { id:         sid "celaddor"
  , population: 57_000_000
  , relations:  asHashMap
      [ sid "althesia"   /\  3.0
      , sid "kh'nazhga'" /\ -3.0
      ]
  , stats:
      { crime:        3.0
      , disparity:    8.0
      , instability:  2.0
      , wealth:       8.0
      }
  , lines:     asLines
      [ english /\
          [ "name"    /\ line "Celaddor"
          , "full"    /\ line "Republic of Celaddor"
          , "adj"     /\ line "Celaddorian"
          , "demonym" /\ line "Celaddoran"
          ]
      , esperanto /\
          [ "name"    /\ line "Seladoro"
          , "full"    /\ line "Respubliko Seladora"
          , "adj"     /\ line "Seladora"
          , "demonym" /\ line "seladorano"
          ]
      ]
  }

gehenna :: {| Identified + LangInfo' + Country + () }
gehenna = withDefCountry
  { id:         sid "gehenna"
  , population: 117_000_000
  , relations:  asHashMap
      [ sid "althesia"   /\ -4.0
      , sid "celaddor"   /\ -5.0
      , sid "kh'nazhga'" /\  2.0
      ]
  , stats:
      { crime:        2.0
      , disparity:    3.0
      , instability:  3.0
      , wealth:       2.0
      }
  , lines:     asLines
      [ english /\
          [ "name"    /\ line "Gehenna"
          , "full"    /\ line "Gehenna Free People's Communes"
          , "adj"     /\ line "Gehennan"
          , "demonym" /\ line "Gehennan"
          ]
      , esperanto /\
          [ "name"    /\ line "Geheno"
          , "full"    /\ line "Gehena Libera Popola Komunumo"
          , "adj"     /\ line "Gehena"
          , "demonym" /\ line "Gehenano"
          ]
      ]
  }

kh'nazhga' :: {| Identified + LangInfo' + Country + () }
kh'nazhga' = withDefCountry
  { id:         sid "kh'nazhga'"
  , population: 632_000_000
  , relations:  asHashMap
      [ sid "althesia"   /\ -5.0
      , sid "celaddor"   /\ -2.0
      ]
  , stats:
      { crime:        4.0
      , disparity:    4.0
      , instability:  4.0
      , wealth:       4.0
      }
  , lines:     asLines
      [ english /\
          [ "name"    /\ line "Kh'nazhga'" -- more apostrophes means more fantasy
          , "full"    /\ line "Common Republic of Kh'nazhga'"
          , "adj"     /\ line "Kh'nazhga'n"
          , "demonym" /\ line "Kh'nazhga'n"
          ]
      , esperanto /\
          [ "name"    /\ line "Ĥnaĵgaŭo" -- overuse diacritics instead of apostrophes?
          , "full"    /\ line "Komuna Ĥnaĵgaŭa Respubliko"
          , "adj"     /\ line "Ĥnaĵgaŭa"
          , "demonym" /\ line "ĥnaĵgaŭano"
          ]
      ]
  }

naswatBiti :: {| Identified + LangInfo' + Country + () }
naswatBiti = withDefCountry
  { id:         sid "naswat-biti"
  , population: 164_000_000
  , relations:  asHashMap
      [ sid "althesia"   /\ -1.0
      , sid "kh'nazhga'" /\  1.0
      ]
  , stats:
      { crime:        5.0
      , disparity:    5.0
      , instability:  4.0
      , wealth:       3.0
      }
  , lines:     asLines
      [ english /\
          [ "name"    /\ line "Naswat Biti"
          , "full"    /\ line "Republic of Naswat Biti"
          , "adj"     /\ line "Naswat Bitian"
          , "demonym" /\ line "Naswat Bitian"
          ]
      , esperanto /\
          [ "name"    /\ line "Nasvato-Bitio"
          , "full"    /\ line "Respubliko de Nasvato-Bitio"
          , "adj"     /\ line "Nasvata-Bitia"
          , "demonym" /\ line "nasvato-bitiano"
          ]
      ]
  }