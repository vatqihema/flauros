const registries = new Map();
const params = new Map(); // idea: map of arrays, idx are for each param

export const mkEmptyKey = () => [];
export const pushKeyNode = keys => node => {
  keys.push(node);
};

export const compileRegistryKey = rkdata => {
  // console.log("js compileRegistryKey input is " + JSON.stringify(rkdata));
  const add = (arr, node) => {
    const v0 = node.value0;
    const v1 = node.value1;

    if (v0 !== undefined) {
      // console.log("js v0 = " + JSON.stringify(v0));
      // console.log("js v0 type is " + JSON.stringify(typeof v0));
      if (typeof v0 == "string") {
        // console.log("js v0 instanceof String");
        arr.push(v0);
      }
      else if (arr.length === 0) {
        add(arr, v0);
      } else {
        let newArr = [];
        arr.push(newArr);
        add(newArr, v0);
      }
    }
    if (v1 !== undefined) {
      add(arr, v1);
    }
  };
  let rArr = [];
  add(rArr, rkdata);
  // console.log("js compileRegistryKey key is now " + JSON.stringify(rArr));
  return rArr;
};

export const addToRegistry = regName => elemName => elem => () => {
    let reg = registries.get(regName);
    if (reg === undefined) {
        reg = new Map();
        registries.set(regName, reg);
    }
    if (reg.get(elemName) !== undefined) {
      console.log("js addToRegistry: overwriting value in registry " + regName + " for " + elemName);
    }
    reg.set(elemName, elem);
    // console.log("js addToRegistry: registered `" + regName + "` -> `" + elemName + "`");
};
export const lookupInRegistries = regNames => elemName => () => {
    let lastElem = undefined;
    for (const regName of regNames) {
        let reg = registries.get(regName);
        if (reg === undefined) {
            // console.log("js lookupInRegistries: registry `" + regName + "` was undefined");
            return undefined;
        } else {
            lastElem = reg.get(elemName);
            if (lastElem === undefined) {
                // console.log("js lookupInRegistries: element `" + elemName + "` was undefined for registries " + regNames);
                return undefined;
            }
        }
    }
    return lastElem;
}
export const lookupAllInRegistries = regNames => () => {
    if (regNames.length == 0)
        return []
    let regs = [];
    for (const regName of regNames) {
        let reg = registries.get(regName);
        if (reg === undefined) {
            console.log("js lookupAllInRegistries: registry `" + regName + "` was undefined");
            return [];
        } else {
            // console.log("js lookupAllInRegistries: adding `" + regName + "` with " + reg.size + " items");
            regs.push(reg);
        }
    }
    let elems = [];
    for (const key of regs[0].keys()) {
        // console.log("js lookupAllInRegistries: checking " + JSON.stringify(key));
        let inAll = true;
        for (let i = 1; i < regs.length; ++i) {
            if (!(key in regs[i])) {
                // console.log("js lookupAllInRegistries: " + JSON.stringify(elems) + " not in reg " + i + " ignoring");
                inAll = false;
                break;
            }
        }
        if (inAll) {
            elems.push(regs[0].get(key));
        }
    }
    // console.log("js lookupAllInRegistries: returning " + elems.length + " items");
    return elems;
}
export const lookupInWhichRegistries = elemName => () => {
  let regs = [];
  for (const regName of registries.keys()) {
    let reg = registries.get(regName);
    if (reg !== undefined && reg.has(elemName)) {
      regs.push(regName);
    }
  }
  return regs;
}