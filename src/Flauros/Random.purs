module Flauros.Random where

import Prelude

import Data.Array as A
import Data.Enum (class BoundedEnum, fromEnum, toEnum)
import Data.Foldable (foldl, minimumBy)
import Data.FoldableWithIndex (class FoldableWithIndex, foldWithIndexM, foldlWithIndex)
import Data.Int (floor, toNumber)
import Data.List (List)
import Data.Map (Map)
import Data.Map as Map
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Ord (abs)
import Data.Unfoldable (replicateA)
import Effect (Effect)
import Effect.Class (class MonadEffect, liftEffect)
import Random.LCG (lcgM, lcgNext, mkSeed, unSeed)

foreign import getCurrentSeed :: Effect Number
foreign import jsRandom :: Effect Number
foreign import setCurrentSeed :: Number -> Effect Unit

class Random :: forall k. (k -> Type) -> k -> Constraint
class Random m a where
  random :: m a

instance randomBool :: MonadEffect m => Random m Boolean where
  random = (\i -> i `mod` 2 == 0) <$> random
instance randomInt :: MonadEffect m => Random m Int where
  random = liftEffect do
    seed <- getCurrentSeed
    let next = unSeed $ lcgNext (mkSeed $ floor seed)
    setCurrentSeed $ toNumber next
    pure next
instance randomNumber :: MonadEffect m => Random m Number where
  random = random <#> toNumber >>> (_ / (toNumber lcgM)) 

class RandomRange m a where
  -- | Returns a value between a minimum and maximum (inclusive)
  randomRange :: a -> a -> m a

instance randomRangeInt :: MonadEffect m => RandomRange m Int where
  randomRange mn mx = random <#> (_ * (mxNum - mnNum)) >>> (_ + mnNum) >>> floor
    where mnNum = toNumber mn
          mxNum = toNumber mx
instance randomRangeNumber :: MonadEffect m => RandomRange m Number where
  randomRange mn mx = (_ * (mn + (mx - mn))) <$> random

randomNth :: forall a m. MonadEffect m => Array a -> m (Maybe a)
randomNth arr = A.index arr <<< (_ - 1) <$> randomRange 1 (A.length arr)

randomNthElse :: forall a m. MonadEffect m => a -> Array a -> m a
randomNthElse defVal arr =  fromMaybe defVal <$> randomNth arr

randomEnum :: forall m e. MonadEffect m => Bounded e => BoundedEnum e => m e
randomEnum = map (fromMaybe bottom <<< toEnum) $ randomRange (fromEnum (bottom :: e)) (fromEnum (top :: e))

rollEach :: forall k m. MonadEffect m => Map k Number -> m (Maybe k)
rollEach = foldWithIndexM f Nothing
  where f k Nothing chance = random <#> (_ <= chance) <#> case _ of
          true  -> Just k
          false -> Nothing
        f _ e _ = pure e

randomWeightedBy :: forall k m f a. FoldableWithIndex k f => MonadEffect m => (a -> Number) -> f a -> m (Maybe k)
randomWeightedBy f weights =
  let total = foldl (\acc w -> f w + acc) 0.0 weights
      spans = _.m $ foldlWithIndex (\k { m, c } w -> { m: Map.insert (c + f w) k m, c: c + f w }) { m: Map.empty, c: 0.0 } weights
   in randomRange 0.0 total <#> \roll -> map _.value (Map.lookupGE roll spans)

randomWeighted :: forall k m f. FoldableWithIndex k f => MonadEffect m => f Number -> m (Maybe k)
randomWeighted weights =
  let total = foldl (+) 0.0 weights
      spans = _.m $ foldlWithIndex (\k { m, c } w -> { m: Map.insert (c + w) k m, c: c + w }) { m: Map.empty, c: 0.0 } weights
   in randomRange 0.0 total <#> \roll -> map _.value (Map.lookupGE roll spans)

curveRandom :: forall m a. Ord a => Semiring a => EuclideanRing a => RandomRange m a => MonadEffect m => { goal :: a, rolls :: Int, min :: a, max :: a } -> m a
curveRandom { goal, rolls, min, max } = do
  let roll   = randomRange min max
  allRolls <- replicateA rolls roll :: m (List a)
  pure $ fromMaybe min $ minimumBy (comparing (\r -> abs (goal - r))) $ allRolls