module Flauros.Data.Actor where

import Prelude

import Data.Array as Array
import Data.Date (Date)
import Data.HashMap (HashMap)
import Data.HashMap as HM
import Data.Maybe (Maybe(..), fromMaybe)
import Flauros.Data.FormList (FormList(..), IDFormList)
import Flauros.Data.Item (Item)
import Flauros.Data.Knowledge (Fact)
import Flauros.Data.Stat (StatState)
import Flauros.Lang (Name)
import Flauros.Prelude (IDMap, Tagged, Identifier, sid, withDefTags)
import Prim.Row (class Nub, class Union)
import Record as Record

playerID :: Identifier
playerID = sid "player"

type Actor r =
  ( name     :: Name
  , ancestry :: Identifier
  , birthday :: Date
  , knows    :: HashMap String Fact
  , stats    :: IDMap StatState
  , bag      :: IDMap Int
  | r
  )

withDefActor :: forall r1 r2 r3
  .  Union r1 ( knows :: HashMap String Fact
              , stats :: IDMap StatState
              , bag   :: IDMap Int
              | Tagged ()
              ) r2
  => Nub r2 r3
  => {| r1 } 
  -> {| r3 }
withDefActor r = Record.merge r $ withDefTags
  { knows: HM.empty :: HashMap String Fact
  , stats: HM.empty :: IDMap StatState
  , bag:   HM.empty :: IDMap Int
  }

mkFirstLast :: String -> String -> Name
mkFirstLast first last = { format: "first-last", names: [ first, last ] }

mkFamilyGiven :: String -> String -> Name
mkFamilyGiven first last = { format: "family-given", names: [ first, last ] }

mkOneName :: String -> Name
mkOneName name = { format: "one-name", names: [ name ] }

mkTitle :: String -- ^ The string key for the title at `translate title key`
        -> Name -- ^ The untitled name
        -> Name -- ^ The name with the title
mkTitle titleKey name = { format: "title", names: [ titleKey, name.format ] <> name.names }

type Trait r =
  ( onTraitAdd    :: forall ra. {| Actor ra } -> {| Actor ra }
  , onTraitRemove :: forall ra. {| Actor ra } -> {| Actor ra }
  | r
  )

---------
-- Bag --
---------

itemCount :: forall r. Identifier -> {| Actor r } -> Int
itemCount itemID { bag } = fromMaybe 0 $ HM.lookup itemID bag

modItemCount :: forall r. Identifier -> Int -> {| Actor r } -> {| Actor r }
modItemCount itemID m a = a { bag = HM.alter f itemID a.bag }
  where f (Just n) = if n + m <= 0 then Nothing else Just (n + m)
        f Nothing  = if m <= 0 then Nothing else Just m

giveList :: forall mr ir ar. IDFormList ( count :: Int | mr ) (item :: Item ir) -> {| Actor ar } -> {| Actor ar }
giveList { list: FormList arr } actor = Array.foldl (\a row -> modItemCount row.id row.meta.count a) actor arr