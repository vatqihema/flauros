module Flauros.Data.TagInfo where

import Prelude

import Flauros.Prelude (Tagged, withDefTags)
import Prim.Row (class Nub, class Union)
import Record as Record

-- | This record describes a tag that another record might have
-- | Most of its behavior is language dependent and so defined by the lines, which unfortunately makes it difficult to verify
-- | The first character in a key for a line determines what grouping it falls under
-- |   ? matches during searches
-- |   ! matches when determining random adjectives
-- |   ‽ matches both searches, and when searching for adjectives
type TagInfo :: forall k. k -> k
type TagInfo r = r

withDefTagInfo :: forall r1 r2 r3. Union r1 (Tagged ()) r2 => Nub r2 r3 => {| r1 } -> {| r3 }
withDefTagInfo r = Record.merge r $ withDefTags {}