module Flauros.Data.Knowledge where

import Prelude

import Control.Alt ((<|>))
import Data.DateTime (DateTime)
import Data.Generic.Rep (class Generic)
import Data.HashMap (HashMap)
import Data.Hashable (class Hashable, hash)
import Data.Int (floor, toNumber)
import Data.Int as Int
import Data.Maybe (fromMaybe)
import Data.Number as Number
import Data.Show.Generic (genericShow)
import Data.String as String
import Flauros.Data.System.JSON (class FromJSON, class ToJSON, fromJSON, toJSON)
import Flauros.Lang (LineParam(..), TranslateWith)
import Flauros.Lang.Text (Text)
import Flauros.Prelude (class Transmute, IDMap, Identifier, isBase, sid, transmute)

type KnowTranslatable = { id :: Identifier, key :: String, params :: Array KnowVal }

defTranslatable :: Identifier -> KnowTranslatable
defTranslatable id = { id, key: "default", params: [] }

eval :: TranslateWith -> KnowTranslatable -> Text
eval translateWith { id, key, params } = translateWith id key (map transmute params)

data KnowVal
  = KnowBool   Boolean
  | KnowID     Identifier
  | KnowInt    Int
  | KnowLine KnowTranslatable
  | KnowNull
  | KnowNum    Number
  | KnowStr String
derive instance eqKnowVal      :: Eq KnowVal
derive instance genericKnowVal :: Generic KnowVal _
instance fromJSONKnowVal   :: FromJSON   KnowVal where
  fromJSON j = (fromJSON j <#> KnowBool)
           <|> (fromJSON j <#> KnowID)
           <|> (fromJSON j <#> (_.i :: { i :: Int } -> Int) >>> KnowInt )
           <|> (fromJSON j <#> \(_ :: Unit) -> KnowNull)
           <|> (fromJSON j <#> KnowNum)
           <|> (fromJSON j <#> KnowStr)
           <|> (fromJSON j <#> KnowLine)
instance toJSONKnowVal     :: ToJSON     KnowVal where
  toJSON = case _ of
    KnowBool b   -> toJSON b
    KnowID id    -> toJSON id
    KnowInt i    -> toJSON { i }
    KnowLine l   -> toJSON l
    KnowNull     -> toJSON unit
    KnowNum n    -> toJSON n
    KnowStr s -> toJSON s
instance showKnowVal :: Show KnowVal where show v = genericShow v
instance hashableKnowVal :: Hashable KnowVal where
  hash = case _ of
    KnowBool b   -> hash b
    KnowID id    -> hash id
    KnowInt i    -> i
    KnowLine l   -> ((hash l.id * 17 + hash l.key) * 17 + hash l.params) * 17
    KnowNull     -> 514229
    KnowNum n    -> hash n
    KnowStr s -> hash s

instance Transmute Boolean KnowVal where transmute = KnowBool
instance Transmute Int     KnowVal where transmute = KnowInt
instance Transmute Number  KnowVal where transmute = KnowNum
instance Transmute String  KnowVal where transmute = KnowStr
instance Transmute Unit    KnowVal where transmute _ = KnowNull
instance Transmute KnowVal LineParam where
  transmute = case _ of
    KnowBool b   -> PBool b
    KnowID id    -> PID id
    KnowInt i    -> PInt i
    KnowLine l   -> PID l.id -- todo this isn't great, do something else?
    KnowNull     -> PNull
    KnowNum n    -> PNum n
    KnowStr s    -> PStr s

worldID :: Identifier
worldID = sid "world"

type Fact =
  { who       :: Identifier
  , whenFirst :: DateTime
  , whenLast  :: DateTime
  , what      :: KnowVal
  }

data Scope = Global | Local Identifier
derive instance eqScope      :: Eq Scope
derive instance genericScope :: Generic Scope _
instance showScope :: Show Scope where show = genericShow
instance fromJSONScope :: FromJSON Scope where
  fromJSON j = (fromJSON j <#> (Local <<< sid))
           <|> (fromJSON j <#> Local)
           <|> (fromJSON j <#> \(_ :: Unit) -> Global)
instance toJSONScope :: ToJSON Scope where
  toJSON = case _ of
    Global    -> toJSON unit
    Local sid -> if isBase sid then toJSON sid.base else toJSON sid
instance hashableScope   :: Hashable Scope where
  hash = case _ of
    Global  -> 6661
    Local s -> hash s

type FactMap = IDMap (IDMap Fact)

type VariableStore = HashMap Scope KnowVal

------------------------
-- KnowVal conversion --
------------------------

asBool :: KnowVal -> Boolean
asBool =  case _ of
  KnowBool b   -> b
  KnowID _     -> true
  KnowInt i    -> i /= 0
  KnowLine _   -> true
  KnowNull     -> false
  KnowNum n    -> n /= 0.0
  KnowStr s    -> String.null s

asInt :: KnowVal -> Int
asInt =  case _ of
  KnowBool b   -> if b then 1 else 0
  KnowID _     -> 0
  KnowInt i    -> i
  KnowLine _   -> 0
  KnowNull     -> 0
  KnowNum n    -> floor n
  KnowStr s    -> fromMaybe 0 $ Int.fromString s

asNumber :: KnowVal -> Number
asNumber =  case _ of
  KnowBool b   -> if b then 1.0 else 0.0
  KnowID _     -> 0.0
  KnowInt i    -> toNumber i
  KnowLine _   -> 0.0
  KnowNull     -> 0.0
  KnowNum n    -> n
  KnowStr s    -> fromMaybe 0.0 $ Number.fromString s