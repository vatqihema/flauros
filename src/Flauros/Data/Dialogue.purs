module Flauros.Data.Dialogue where

import Prelude

import Data.HashMap (HashMap)
import Data.HashMap as HM
import Data.Maybe (Maybe(..))
import Effect.Class (class MonadEffect)
import Flauros.Data.Actor (Actor, playerID)
import Flauros.Data.Knowledge (KnowVal)
import Flauros.Data.Store as DS
import Flauros.Lang (class Lineable, Line, line)
import Flauros.Prelude (class Transmute, Identifier, Tagged, Identified, sid, transmute, withDefTags)
import Halogen.Store.Monad (class MonadStore)
import Prim.Row (class Nub, class Union)
import Record as Record
import Type.Row (type (+))

newtype DialogueM a = DialogueM (forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m a)

noAction :: DialogueM Unit
noAction = DialogueM (pure unit)

onAction :: forall a. (forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m a) -> DialogueM a
onAction = DialogueM

type Dialogue r =
  ( onDialogueInit :: DialogueM Unit
  , nodes          :: HashMap String (Array DialogueStep)
  | r
  )


withDefDialogue :: forall r1 r2 r3
  .  Union r1 ( onDialogueInit :: DialogueM Unit
              | Tagged ()
              ) r2
  => Nub r2 r3
  => {| r1 } 
  -> {| r3 }
withDefDialogue r = Record.merge r $ withDefTags
  { onDialogueInit: noAction
  }

type Statement r = ( who :: Identifier, what :: Line DS.Store | r )

data DialogueStep
  = DSAction DS.Action
  | DSBr
  | DSLoop (Array DialogueStep)
  | DSLookBreak
  | DSRemember { name :: Identifier, who :: Identifier, what :: KnowVal }
  | DSRun (DialogueM Unit)
  | DSSay   {| Statement () }
  | DSSayLn {| Statement () }
  | DSTopic { steps :: Array DialogueStep | Statement () }
  | DSTopicPresent
  | DSWhen (DialogueM Boolean) (Array DialogueStep)

action :: DS.Action -> DialogueStep
action = DSAction

-- | Waits for the player to choose one of the decisions or topics mentioned above
-- When topics exist in the queue and the end of a node is reached, this is automatically called
ask :: DialogueStep
ask = DSTopicPresent 

run :: (forall m. MonadEffect m => MonadStore DS.Action DS.Store m => m Unit) -> DialogueStep
run a = DSRun $ onAction a

break :: DialogueStep
break = DSBr

decision :: forall a. Lineable DS.Store a => a -> Array DialogueStep -> DialogueStep
decision what steps = DSTopic { who: conscious, what: line what, steps }

-- | A replacement for `say narrator`
narrate :: forall a. Lineable DS.Store a => a -> DialogueStep
narrate = say narrator

-- | A replacement for `sayLn narrator`
narrateLn :: forall a. Lineable DS.Store a => a -> DialogueStep
narrateLn = sayLn narrator

onActor :: Identifier -> ({| Identified + Actor + () } -> {| Identified + Actor + () }) -> DialogueStep
onActor id f = DSAction \(DS.Store store) -> DS.Store $ store { actors = HM.update (Just <<< f) id store.actors }

onPlayer :: ({| Identified + Actor + () } -> {| Identified + Actor + () }) -> DialogueStep
onPlayer = onActor playerID

remember :: forall t. Transmute t KnowVal => { name :: Identifier, who :: Identifier, what :: t } -> DialogueStep
remember {  name, who, what } = DSRemember { name, who, what: transmute what }

say :: forall a. Lineable DS.Store a => Identifier -> a -> DialogueStep
say who what = DSSay { who, what: line what }

sayLn :: forall a. Lineable DS.Store a => Identifier -> a -> DialogueStep
sayLn who what = DSSayLn { who, what: line what }

topic :: forall a. Lineable DS.Store a => a -> Array DialogueStep -> DialogueStep
topic what steps = DSTopic { who: playerID, what: line what, steps }

topic_ :: forall a. Lineable DS.Store a => a -> DialogueStep -> DialogueStep
topic_ what step = DSTopic { who: playerID, what: line what, steps: [ step ] }

when :: DialogueM Boolean -> Array DialogueStep -> DialogueStep
when = DSWhen

when_ :: DialogueM Boolean -> DialogueStep -> DialogueStep
when_ b s = DSWhen b [ s ]

--------------------
-- Default people --
--------------------

-- | The player's internal monologue!
conscious :: Identifier
conscious = sid "conscious"

-- | I narrate things that aren't dialogue!
narrator :: Identifier
narrator = sid "narrator"