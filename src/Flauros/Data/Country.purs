module Flauros.Data.Country where

import Prelude

import Flauros.Prelude (Tagged, IDMap, withDefTags)
import Prim.Row (class Nub, class Union)
import Record as Record

type Country r =
  ( population :: Int
  , relations  :: IDMap Number -- ^ Country ID -> Number modifier (usually -10 to 10)
  , stats      :: { crime       :: Number
                  , disparity   :: Number
                  , instability :: Number
                  , wealth      :: Number
                  }
  | r
  )

withDefCountry :: forall r1 r2 r3. Union r1 (Tagged ()) r2 => Nub r2 r3 => {| r1 } -> {| r3 }
withDefCountry r = Record.merge r $ withDefTags {}