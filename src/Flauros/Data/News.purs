module Flauros.Data.News where

import Prelude

import Flauros.Lang (LineParam)
import Flauros.Prelude (Tagged, withDefTags)
import Prim.Row (class Nub, class Union)
import Record as Record

data Priority = Never | Low | Normal | High | Important

type NewsStory r =
  ( headline :: Array LineParam
  | r
  )

type NewsGen s r =
  ( priority :: s -> Priority
  | r
  )

withDefNews :: forall r1 r2 r3. Union r1 (Tagged ()) r2 => Nub r2 r3 => {| r1 } -> {| r3 }
withDefNews r = Record.merge r $ withDefTags {}

withDefNewsGen :: forall s r1 r2 r3. Union r1 (priority :: s -> Priority | Tagged ()) r2 => Nub r2 r3 => {| r1 } -> {| r3 }
withDefNewsGen r = Record.merge r $ withDefTags
  { priority: \(_ :: s) -> Normal
  }