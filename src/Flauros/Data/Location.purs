module Flauros.Data.Location where

import Prelude

import Data.Argonaut.Decode.Generic (genericDecodeJson)
import Data.Argonaut.Encode.Generic (genericEncodeJson)
import Data.Bounded.Generic (genericBottom, genericTop)
import Data.Date (Date)
import Data.Enum (class BoundedEnum, class Enum)
import Data.Enum.Generic (genericCardinality, genericFromEnum, genericPred, genericSucc, genericToEnum)
import Data.Generic.Rep (class Generic)
import Data.Show.Generic (genericShow)
import Flauros.Data.Knowledge (KnowTranslatable)
import Flauros.Data.System.JSON (class FromJSON, class ToJSON)
import Flauros.Prelude (Identifier, Identified)
import Type.Row (type (+))

type FullHousing s r = Identified + Location + Building + Housing + Availability s + r

--------------
-- Location --
--------------

-- | What is the area surrounding the location like?
data Surroundings = Gentrified | Suburb | Downtown | Slum | Rural | University
derive instance eqSurroundings      :: Eq Surroundings
derive instance genericSurroundings :: Generic Surroundings _
derive instance ordSurroundings     :: Ord Surroundings
instance showSurroundings :: Show Surroundings where show = genericShow
instance toJSONSurroundings   :: ToJSON   Surroundings where toJSON   = genericEncodeJson
instance fromJSONSurroundings :: FromJSON Surroundings where fromJSON = genericDecodeJson
instance boundedSurroundings :: Bounded Surroundings where
  top    = genericTop
  bottom = genericBottom
instance enumSurroundings :: Enum Surroundings where
  succ = genericSucc
  pred = genericPred
instance boundedEnumSurroundings :: BoundedEnum Surroundings where
  cardinality = genericCardinality
  fromEnum    = genericFromEnum
  toEnum      = genericToEnum

type Location r =
  ( address      :: KnowTranslatable
  , surroundings :: Surroundings
  | r
  )

type Building r =
  ( buildingName    :: KnowTranslatable
  , built           :: Date
  , floors          :: { basement :: Int, aboveground :: Int }
  , lastRefurbished :: Date
  , owner           :: Identifier
  , quality         :: Number
  | r
  )

-------------
-- Housing --
-------------

-- | What purpose does this room serve?
data RoomType = Bath | Closet | Bedroom | Kitchen | Studio | LivingRoom | Staircase | Balcony | Basement
derive instance eqRoomType      :: Eq RoomType
derive instance genericRoomType :: Generic RoomType _
derive instance ordRoomType     :: Ord RoomType
instance showRoomType :: Show RoomType where show = genericShow
instance toJSONRoomType   :: ToJSON   RoomType where toJSON   = genericEncodeJson
instance fromJSONRoomType :: FromJSON RoomType where fromJSON = genericDecodeJson
instance boundedRoomType :: Bounded RoomType where
  top    = genericTop
  bottom = genericBottom
instance enumRoomType :: Enum RoomType where
  succ = genericSucc
  pred = genericPred
instance boundedEnumRoomType :: BoundedEnum RoomType where
  cardinality = genericCardinality
  fromEnum    = genericFromEnum
  toEnum      = genericToEnum

roomTypeBaseRent :: RoomType -> Number
roomTypeBaseRent = case _ of
  Bath       -> 60.0
  Closet     -> 10.0
  Bedroom    -> 80.0
  Kitchen    -> 110.0       
  Studio     -> 230.0
  LivingRoom -> 70.0
  Staircase  -> 20.0
  Balcony    -> 20.0
  Basement   -> 40.0

-- | How big is it, relative to an alright apartment one (very subjective)
data RoomSize = Small | Medium | Large
derive instance eqRoomSize      :: Eq RoomSize
derive instance genericRoomSize :: Generic RoomSize _
derive instance ordRoomSize     :: Ord RoomSize
instance showRoomSize :: Show RoomSize where show = genericShow
instance toJSONRoomSize   :: ToJSON   RoomSize where toJSON   = genericEncodeJson
instance fromJSONRoomSize :: FromJSON RoomSize where fromJSON = genericDecodeJson
instance boundedRoomSize :: Bounded RoomSize where
  top    = genericTop
  bottom = genericBottom
instance enumRoomSize :: Enum RoomSize where
  succ = genericSucc
  pred = genericPred
instance boundedEnumRoomSize :: BoundedEnum RoomSize where
  cardinality = genericCardinality
  fromEnum    = genericFromEnum
  toEnum      = genericToEnum

roomSizeRentMult :: RoomSize -> Number
roomSizeRentMult = case _ of
  Small  -> 0.65
  Medium -> 1.0
  Large  -> 1.5

-- | How well kept up has this place been?
data Condition = Pristine | Maintained | Dingy | Derelict
derive instance eqCondition      :: Eq Condition
derive instance genericCondition :: Generic Condition _
derive instance ordCondition     :: Ord Condition
instance showCondition :: Show Condition where show = genericShow
instance toJSONCondition   :: ToJSON   Condition where toJSON   = genericEncodeJson
instance fromJSONCondition :: FromJSON Condition where fromJSON = genericDecodeJson
instance boundedCondition :: Bounded Condition where
  top    = genericTop
  bottom = genericBottom
instance enumCondition :: Enum Condition where
  succ = genericSucc
  pred = genericPred
instance boundedEnumCondition :: BoundedEnum Condition where
  cardinality = genericCardinality
  fromEnum    = genericFromEnum
  toEnum      = genericToEnum

conditionRentMult :: Condition -> Number
conditionRentMult = case _ of
  Pristine   -> 1.5
  Maintained -> 1.0
  Dingy      -> 0.5
  Derelict   -> 0.3

type Housing r =
  ( floor       :: Int
  , rooms       :: Array { type :: RoomType, size :: RoomSize, condition :: Condition }
  , unitAddress :: KnowTranslatable
  | r
  )

------------------
-- Availability --
------------------

type Availability s r =
  ( availableUnits :: Array { unit :: String, floor :: Int }
  , describeUnits  :: KnowTranslatable
  , monthlyRent    :: s -> { unit :: String, floor :: Int } -> Number
  | r
  )