module Flauros.Data.Weather where

import Prelude

import CSS.Color (Color)
import Data.Date as Date
import Data.Enum (fromEnum)
import Data.Generic.Rep (class Generic)
import Data.HashMap (HashMap)
import Data.HashMap as HM
import Data.Map (Map)
import Data.Map as Map
import Data.Maybe (Maybe(..))
import Data.Show.Generic (genericShow)
import Flauros.Data.Time (class HasDate, Reoccurance, getDate, getYear, reoccuranceIn)
import Flauros.Prelude (Tag, Tagged, Identifier, withDefTags)
import Flauros.Style.Color (Colors)
import Prim.Row (class Nub, class Union)
import Record as Record

type Weather s r =
  ( eventMods  :: HashMap Tag Number
  , colors     :: { text :: Colors -> Color }
  , contagion  :: Number -- ^ `1.0 * (this / days since r) ^ occurances = % to force repeat weather`
  , delay      :: Maybe Int
  , followedBy :: Array Identifier
  , tempMod    :: s -> Date.Date -> Int -> Int
  | r
  )

withDefWeather :: forall s r1 r2 r3. Union r1 ( eventMods  :: HashMap Tag Number
                                              , contagion  :: Number
                                              , delay      :: Maybe Int
                                              , followedBy :: Array Identifier
                                              , tempMod    :: s -> Date.Date -> Int -> Int
                                              | Tagged ()
                                              ) r2 => Nub r2 r3 => {| r1 } -> {| r3 }
withDefWeather r = Record.merge r $ withDefTags
  { eventMods: HM.empty :: HashMap Tag Number
  , contagion:  0.0
  , delay:      Nothing :: Maybe Int
  , followedBy: [] :: Array Identifier
  , tempMod:    \(_ :: s) (_ :: Date.Date) (_ :: Int) -> 0
  }

type Temperature = Int

type WeatherHistoryRow = { weather :: Identifier, temperature :: Int, chance :: Int }

type WeatherHistory = Map Date.Date WeatherHistoryRow

weatherOn :: forall d. HasDate d => d -> WeatherHistory -> Maybe WeatherHistoryRow
weatherOn date history = Map.lookup (getDate date) history

data Season = Spring | Summer | Fall | Winter
derive instance eqSeason :: Eq Season
derive instance genericSeason :: Generic Season _
derive instance ordSeason :: Ord Season
instance showSeason :: Show Season where
  show = genericShow

startOfSpring :: Reoccurance
startOfSpring = { month: Date.March,     day: 3 } -- 96 days
startOfSummer :: Reoccurance
startOfSummer = { month: Date.June,      day: 8 } -- 89 days
startOfFall   :: Reoccurance
startOfFall   = { month: Date.September, day: 5 } -- 86 days
startOfWinter :: Reoccurance
startOfWinter = { month: Date.December,  day: 1 } -- 91 days

season :: forall d. HasDate d => d -> Season
season dt =
  let date = getDate dt
      year = getYear date
      mk e = reoccuranceIn e $ fromEnum year
   in case date of
        d | d <= mk startOfSpring -> Winter
        d | d <= mk startOfSummer -> Spring
        d | d <= mk startOfFall   -> Summer
        d | d <= mk startOfWinter -> Fall
        _                         -> Winter