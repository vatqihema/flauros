module Flauros.Data.Month where

import Prelude

import CSS.Color (Color)
import Data.Date as Date
import Data.HashMap (HashMap)
import Data.HashMap as HM
import Data.Maybe (fromJust)
import Data.Time (Time)
import Flauros.Data.Time (class HasDate, TimeOfDay(..), getMonth)
import Flauros.Prelude (Identifier, Tag, Tagged, WeightedMap, IDMap, sid, withDefTags)
import Flauros.Style.Color (Colors, blue, orange, purple, yellow)
import Partial.Unsafe (unsafePartial)
import Prim.Row (class Nub, class Union)
import Record as Record

type Theme =
  { text      :: Colors -> Color
  , highlight :: Colors -> Color
  , dawn      :: Colors -> Color
  , day       :: Colors -> Color
  , dusk      :: Colors -> Color
  , night     :: Colors -> Color
  }

type Month r =
  ( enum      :: Date.Month
  , flower    :: Identifier
  , temp      :: { min :: Int, max :: Int }
  , timeOfDay :: { dawn :: Time, day :: Time, dusk :: Time, night :: Time }
  , theme     :: Theme
  , eventMods :: HashMap Tag Number
  , tempChangeRate :: Number
  , tempCurve      :: Number -> Number
  , weather   :: WeightedMap Identifier
  | r
  )

withDefMonth :: forall r1 r2 r3. Union r1 (eventMods :: HashMap Tag Number, tempChangeRate :: Number, tempCurve :: Number -> Number | Tagged ()) r2 => Nub r2 r3 => {| r1 } -> {| r3 }
withDefMonth r = Record.merge r $ withDefTags
  { eventMods: HM.empty :: HashMap Tag Number
  , tempChangeRate: 0.33
  , tempCurve: \(_ :: Number) -> 0.5
  }

withDefTheme :: forall r1 r2 r3. Union r1 (dawn :: Colors -> Color, day :: Colors -> Color, dusk :: Colors -> Color, night :: Colors -> Color) r2 => Nub r2 r3 => {| r1 } -> {| r3}
withDefTheme r = Record.merge r
  { dawn:  blue
  , day:   yellow
  , dusk:  orange
  , night: purple
  }

monthID :: Date.Month -> Identifier
monthID = sid <<< case _ of
  Date.January   -> "january"
  Date.February  -> "february"
  Date.March     -> "march"
  Date.April     -> "april"
  Date.May       -> "may"
  Date.June      -> "june"
  Date.July      -> "july"
  Date.August    -> "august"
  Date.September -> "september"
  Date.October   -> "october"
  Date.November  -> "november"
  Date.December  -> "december"

timeOfDay :: forall r. {| Month r } -> Time -> TimeOfDay
timeOfDay { timeOfDay: tod } t
  | t <= tod.dawn  = Night
  | t <= tod.day   = Dawn
  | t <= tod.dusk  = Day
  | t <= tod.night = Dusk
  | otherwise      = Night

timeOfDayColor :: forall r. {| Month r } -> Time -> Colors -> Color
timeOfDayColor { theme, timeOfDay: tod } t colors
  | t <= tod.dawn  = theme.night colors
  | t <= tod.day   = theme.dawn  colors
  | t <= tod.dusk  = theme.day   colors
  | t <= tod.night = theme.dusk  colors
  | otherwise      = theme.night colors

monthOf :: forall r d. HasDate d => IDMap {| Month r } -> d -> {| Month r }
monthOf m d = unsafePartial $ fromJust $ HM.lookup (monthID $ getMonth d) m