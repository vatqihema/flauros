module Flauros.Data.Store where

import Prelude

import Data.Argonaut (Json)
import Data.Date (Month(..))
import Data.Either (Either(..))
import Data.HashMap (HashMap)
import Data.HashMap as HM
import Data.Hashable (class Hashable)
import Data.Map as Map
import Data.Maybe (Maybe(..), fromJust)
import Data.Newtype (class Newtype)
import Data.Time.Duration (class Duration)
import Data.Tuple (Tuple(..))
import Effect.Class (class MonadEffect, liftEffect)
import Effect.Class.Console as Console
import Flauros.Data.Actor (Actor, mkFirstLast, playerID, withDefActor)
import Flauros.Data.Knowledge (VariableStore, FactMap)
import Flauros.Data.System.JSON (class ToJSON, toJSON)
import Flauros.Data.Time (CurrentTime, TimeOfDay(..), adjustDateTime, getDate, mkDate)
import Flauros.Data.Weather (Temperature, WeatherHistory)
import Flauros.Lang (class LangState, AllLines, Lang, LangInfo, Translate, TranslateAll, TranslateAllWith, TranslateOr, TranslateWith, TranslateWithOr, countLines, emptyLines, english, translate, translateAll, translateAllWith, translateContext, translateOr, translateWith, translateWithOr)
import Flauros.Log (LogLevel(..), LogType(..), formatJSONStringify)
import Flauros.Prelude (IDMap, Identified, Identifier, default, sid)
import Flauros.Style.Color (Colors, defaultColors)
import Foreign (Foreign)
import Halogen.Store.Monad (class MonadStore, getStore)
import Halogen.Store.Select (Selector(..), selectEq)
import Partial.Unsafe (unsafePartial)
import Prim.Row (class Nub, class Union)
import Record as Record
import Type.Row (type (+))

type StoreRec =
  { actors         :: IDMap {| Identified + Actor + () }
  , allLines       :: AllLines Store
  , colors         :: Colors
  , currentTime    :: CurrentTime
  , facts          :: FactMap
  , isDebug        :: Boolean
  , lang           :: Lang
  , logLevel       :: LogLevel
  , settingsBrowser :: IDMap Foreign
  , settingsSave    :: IDMap Foreign
  , temperature    :: Temperature
  , timeOfDay      :: TimeOfDay
  , uiState        :: HashMap String Json
  , variables      :: VariableStore
  , weather        :: Identifier
  , weatherHistory :: WeatherHistory
  }

newtype Store = Store StoreRec

type SerializableStore =
  { global ::
      { colors        :: Colors
      , isDebug       :: Boolean
      , lang          :: Lang
      , logLevel      :: LogLevel
      , settings      :: IDMap Json
      }
  , unique ::
      { actors         :: IDMap {| Identified + Actor + () }
      , currentTime    :: CurrentTime
      , facts          :: FactMap
      , seed           :: Number
      , settings       :: IDMap Json
      , temperature    :: Temperature
      , timeOfDay      :: TimeOfDay
      , uiState        :: HashMap String Json
      , variables      :: VariableStore
      , weather        :: Identifier
      , weatherHistory :: WeatherHistory
      }
  }

derive instance storeNewtype :: Newtype Store _

instance LangState Store where
  allLines (Store s) = s.allLines
  mapAllLines f (Store s) = Store s { allLines = f s.allLines }

type Action = Store -> Store

reduce :: Store -> Action -> Store
reduce store a = a store

initialStore :: Store
initialStore = Store
  { actors:         HM.singleton playerID $ withDefActor
      { id:         playerID
      , name:       mkFirstLast "Farris" "c'Amri"
      , birthday:   mkDate 970 April 24
      , ancestry:   sid "chimera"
      , bag:        HM.empty :: IDMap Int
      }
  , allLines:       emptyLines
  , colors:         defaultColors
  , currentTime:    default
  , facts:          HM.empty
  , isDebug:        true
  , lang:           english
  , logLevel:       Dev
  , settingsBrowser: HM.empty
  , settingsSave:    HM.empty
  , timeOfDay:      Day
  , temperature:    2
  , uiState:        HM.empty
  , variables:      HM.empty
  , weather:        sid "clear"
  , weatherHistory: Map.singleton (getDate (default :: CurrentTime)) { weather: sid "clear", temperature: 2, chance: 100 }
  }

type AllLines' = AllLines Store

type LangInfo' r = LangInfo Store r

withDefLangInfo :: forall rin rout. Union rin (LangInfo' ()) rout => Nub rout rout
                => {| rin }
                -> {| rout }
withDefLangInfo r = Record.merge r ({ lines: HM.empty } :: {| LangInfo' ()})

mkTranslate :: Store -> Translate
mkTranslate store@(Store s) = translate (translateContext store s.lang)

mkTranslateOr :: Store -> TranslateOr
mkTranslateOr store@(Store s) = translateOr (translateContext store s.lang)

mkTranslateWith :: Store -> TranslateWith
mkTranslateWith store@(Store s) = translateWith (translateContext store s.lang)

mkTranslateWithOr :: Store -> TranslateWithOr
mkTranslateWithOr store@(Store s) = translateWithOr (translateContext store s.lang)

mkTranslateAll :: Store -> TranslateAll
mkTranslateAll store@(Store s) = translateAll (translateContext store s.lang)

mkTranslateAllWith :: Store -> TranslateAllWith
mkTranslateAllWith store@(Store s) = translateAllWith (translateContext store s.lang)

mkCountLines :: Store -> (String -> Boolean) -> Identifier -> Int
mkCountLines store@(Store s) = countLines (translateContext store s.lang)

log :: forall m. MonadEffect m => MonadStore Action Store m => LogType -> String -> m Unit
log logType msg = do
  Store store <- getStore
  when (store.logLevel == Dev || logType == Error) do
    liftEffect (Console.log (show logType <> ": " <> msg))

logStringify :: forall m a. MonadEffect m => MonadStore Action Store m => LogType -> a -> m Unit
logStringify logType obj = do
  Store store <- getStore
  when (store.logLevel == Dev || logType == Error) do
    liftEffect (Console.log (show logType <> ": " <> formatJSONStringify obj))

elseLogLeft :: forall m l r a. MonadEffect m => MonadStore Action Store m => Show l => Either l r -> (r -> m a) -> m Unit
elseLogLeft = case _ of
  Left l  -> \_ -> log Error $ "elseLogLeft: " <> show l
  Right r -> \f -> void $ f r

---------------
-- Selectors --
---------------

-- | A selector that never updates; used when store determines initial values only
selectNone :: forall store. Selector store store
selectNone = Selector { eq: \_ _ -> false, select: identity }

selectAllEqOn :: forall a store. Eq a => (store -> a) -> Selector store store
selectAllEqOn f = Selector { eq: \a b -> (f a) == (f b), select: identity }

selectAllWhen :: forall store. (store -> store -> Boolean) -> Selector store store
selectAllWhen pred = Selector { eq: pred, select: identity }

selectKey :: forall k a store. Eq a => Hashable k => (store -> HashMap k a) -> k -> Selector store (Maybe a)
selectKey f k = Selector { eq, select: HM.lookup k <<< f }

selectWeather :: Selector Store Identifier
selectWeather = selectEq \(Store s) -> s.weather

selectUnion :: forall a b store. Eq a => Eq b => Selector store a -> Selector store b -> Selector store (Tuple a b)
selectUnion (Selector { eq: eqa, select: selecta }) (Selector { eq: eqb, select: selectb }) = Selector
  { eq: \(Tuple a1 a2) (Tuple b1 b2) -> eqa a1 b1 && eqb a2 b2
  , select: \store -> Tuple (selecta store) (selectb store)
  }

selectStyle :: Selector Store Store
selectStyle = Selector { eq: \(Store a) (Store b) -> a.colors == b.colors, select: identity }

------------
-- Modify --
------------

action :: (StoreRec -> StoreRec) -> Store -> Store
action f (Store s) = Store $ f s

setLogLevel :: LogLevel -> Action
setLogLevel ll (Store store) = Store $ store {logLevel = ll}

shiftTime :: forall d. Duration d => d -> Action
shiftTime dur (Store store) = Store $ store { currentTime = unsafePartial $ fromJust $ adjustDateTime dur store.currentTime }

setUIState :: forall j. ToJSON j => String -> j -> Store -> Store
setUIState key json = action \st@{ uiState } -> st { uiState = HM.insert key (toJSON json) uiState }

onActor :: Identifier -> ({| Identified + Actor + () } -> {| Identified + Actor + () }) -> Action
onActor id f (Store store) = Store $ store { actors = HM.update (Just <<< f) id store.actors }

onPlayer :: ({| Identified + Actor + () } -> {| Identified + Actor + () }) -> Action
onPlayer f (Store store) = Store $ store { actors = HM.update (Just <<< f) playerID store.actors }

-------------
-- Getters --
-------------

lookupUI :: String -> Store -> Maybe Json
lookupUI k (Store { uiState }) = HM.lookup k uiState

withActor :: forall a. Identifier -> ({| Identified + Actor + () } -> a) -> Store -> Maybe a
withActor id getter (Store { actors }) = HM.lookup id actors <#> getter