module Flauros.Data.Gen where

import Prelude

import Data.Argonaut (Json, JsonDecodeError)
import Data.Either (Either)
import Effect.Class (class MonadEffect)
import Flauros.Data.Store as DS
import Flauros.Data.System.JSON (class FromJSON, class ToJSON, fromJSON, toJSON)
import Flauros.Prelude (Identifier, Tagged, Identified, withDefTags)
import Foreign (Foreign, unsafeFromForeign, unsafeToForeign)
import Prim.Row (class Nub, class Union)
import Record as Record
import Type.Row (type (+))

type GenConfig cfg i o =
  { setup  :: forall m. MonadEffect m => DS.Store -> i -> m cfg
  , create :: Identifier -> cfg -> o
  }

newtype GenFns cfg i o = GenFns
  { setup    :: forall m. MonadEffect m => DS.Store -> i -> m cfg
  , create   :: Identifier -> cfg -> o
  , toJson   :: cfg -> Json
  , fromJson :: Json -> Either JsonDecodeError cfg
  }

type Gen i o r =
  ( setup :: GenFns Foreign i o
  | r
  )

type Gen_ o r = Gen Unit o r

withDefGen :: forall r1 r2 r3. Union r1 (| Tagged ()) r2 => Nub r2 r3 => {| r1 } -> {| r3 }
withDefGen r = Record.merge r $ withDefTags {}

mkGenFns :: forall cfg i o. ToJSON cfg => FromJSON cfg => GenConfig cfg i o -> GenFns Foreign i o
mkGenFns { setup, create } = GenFns
  { setup:    \store input -> setup store input <#> unsafeToForeign
  , create:   \id          -> unsafeFromForeign >>> create id
  , fromJson: \json        -> fromJSON json <#> \(v :: cfg) -> unsafeToForeign v
  , toJson:   \obj         -> toJSON ((unsafeFromForeign obj) :: cfg) 
  }

-- setup :: forall a r m. MonadEffect m => DS.Store -> {| Gen a r } -> m Setup
-- setup store { setup: GenFns fns } = fns.setup store

quick :: forall i o r m. MonadEffect m => DS.Store -> i -> {| Identified + Gen i o r } -> m o
quick store input { id, setup: GenFns fns } = fns.setup store input <#> fns.create id