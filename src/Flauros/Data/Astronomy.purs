module Flauros.Data.Astronomy where

import Prelude

import Data.Date (Date, Month(..), diff)
import Data.Generic.Rep (class Generic)
import Data.HashMap (HashMap)
import Data.HashMap as HM
import Data.Number (cos, log, pi, sin, sqrt, (%))
import Data.Show.Generic (genericShow)
import Data.Time.Duration (Days(..))
import Flauros.Data.Time (class HasDate, getDate, mkDate)
import Flauros.Prelude (Identifier, Tag, Tagged, distance, sid, withDefTags)
import Prim.Row (class Nub, class Union)
import Record as Record

type Planet r =
  ( albedo        :: Number
  , radius        :: Number
  , aphelion      :: Number
  , perihelion    :: Number
  , semiMajorAxis :: Number
  | r
  )

-- | Values taken from Wikipedia as of revision https://en.wikipedia.org/w/index.php?title=Earth&oldid=1122429476
earth :: {| Planet () }
earth =
  { albedo:        0.367
  , radius:        6371.0--km
  , aphelion:      152100000.0--km
  , perihelion:    147095000.0--km
  , semiMajorAxis: 149598023.0--km
  }

withDefPlanet :: forall r1 r2 r3. Union r1 (| Tagged ()) r2 => Nub r2 r3 => {| r1 } -> {| r3 }
withDefPlanet r = Record.merge r $ withDefTags {}

distanceFromSun :: forall r. {| Planet r } -> Number -> Number
distanceFromSun { aphelion, perihelion, semiMajorAxis } angle =
  let x = if angle <= 0.5 * pi || angle >= 1.5 * pi then aphelion else perihelion
      y = semiMajorAxis
      c = cos angle
      s = sin angle
   in (x*y) / sqrt (x*x*s*s + y*y*c*c)

apparentMagnitude :: forall r. {| Planet r } -> Number -> {| Planet r } -> Number -> Number
apparentMagnitude pSrc aSrc pDest aDest =
  let luminosity = reflectiveLuminosity pSrc aSrc
      dist = distance { mag: distanceFromSun pSrc aSrc, angle: aSrc } { mag: distanceFromSun pDest aDest, angle: aDest }
   in -2.5 * log dist

-- apparentMagnitude :: forall r. {| Planet r } -> Number -> {| Planet r } -> Number -> Number
-- apparentMagnitude pSrc aSrc pDest aDest =
--   let mag  = absoluteMagnitude pDest aDest
--       dist = distance { mag: distanceFromSun pSrc aSrc, angle: aSrc } { mag: distanceFromSun pDest aDest, angle: aDest }
--    in mag + 5.0 * log dist - 5.0

absoluteMagnitude :: forall r. {| Planet r } -> Number -> Number
absoluteMagnitude planet angle =
  let luminosity = reflectiveLuminosity planet angle
   in -2.5 * log (luminosity / nominalTotalSolarIrradiance)

reflectiveLuminosity :: forall r. {| Planet r } -> Number -> Number
reflectiveLuminosity planet angle =
  let radius   = distanceFromSun planet angle
      flux     = planetFlux radius
      crossSec = pi * planet.radius * planet.radius  
   in planet.albedo * crossSec * flux

solarLuminosity :: Number
solarLuminosity = 3.916e26

nominalTotalSolarIrradiance :: Number
nominalTotalSolarIrradiance = 1361.0

solarAbsMagnitude :: Number
solarAbsMagnitude = -26.74

planetFlux :: Number -> Number
planetFlux radius = nominalTotalSolarIrradiance / (4.0 * pi * radius * radius)

kmToAU :: Number -> Number
kmToAU = (_ / 6.68459e-9)

data MoonPhase
  = NewMoon
  | WaxingCrescent
  | FirstQuarter
  | WaxingGibbous
  | FullMoon
  | WaningGibbous
  | LastQuarter
  | WaningCrescent
derive instance eqMoonPhase :: Eq MoonPhase
derive instance genericMoonPhase :: Generic MoonPhase _
derive instance ordMoonPhase :: Ord MoonPhase
instance showMoonPhase :: Show MoonPhase where show = genericShow

type Moon r =
  ( phase     :: MoonPhase
  , eventMods :: HashMap Tag Number
  | r
  )

withDefMoon :: forall r1 r2 r3. Union r1 (eventMods :: HashMap Tag Number | Tagged ()) r2 => Nub r2 r3 => {| r1 } -> {| r3 }
withDefMoon r = Record.merge r $ withDefTags
  { eventMods: HM.empty :: HashMap Tag Number
  }

moonPeriod :: Number
moonPeriod = 29.5306

firstNewMoon :: Date
firstNewMoon = mkDate 1222 October (31 - 16)

moonPhase :: forall d. HasDate d => d -> MoonPhase
moonPhase d =
  let Days d  = diff (getDate d) firstNewMoon
      inCycle = d % moonPeriod
      pos     = inCycle + if inCycle < 0.0 then moonPeriod else 0.0
   in case pos of
        p | p <   1.0 -> NewMoon
        p | p <=  7.0 -> WaxingCrescent
        p | p <=  9.0 -> FirstQuarter
        p | p <= 15.0 -> WaxingGibbous
        p | p <= 16.0 -> FullMoon
        p | p <= 21.0 -> WaningGibbous
        p | p <= 23.5 -> LastQuarter -- should be 3 days of LastQuarter if due to the remainder this calendar has an extra day
        _             -> WaningCrescent

-- IDs --

newMoonID        :: Identifier
newMoonID        = sid "new-moon"
waxingCrescentID :: Identifier
waxingCrescentID = sid "waxing-crescent"
firstQuarterID   :: Identifier
firstQuarterID   = sid "first-quarter"
waxingGibbousID  :: Identifier
waxingGibbousID  = sid "waxing-gibbous"
fullMoonID       :: Identifier
fullMoonID       = sid "full-moon"
waningGibbousID  :: Identifier
waningGibbousID  = sid "waning-gibbous"
lastQuarterID    :: Identifier
lastQuarterID    = sid "last-quarter"
waningCrescentID :: Identifier
waningCrescentID = sid "waning-crescent"

phaseID :: MoonPhase -> Identifier
phaseID = case _ of
  NewMoon        -> newMoonID
  WaxingCrescent -> waxingCrescentID
  FirstQuarter   -> firstQuarterID
  WaxingGibbous  -> waxingGibbousID
  FullMoon       -> fullMoonID
  WaningGibbous  -> waningGibbousID
  LastQuarter    -> lastQuarterID
  WaningCrescent -> waningCrescentID
