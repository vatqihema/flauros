module Flauros.Data.Holiday where

import Prelude

import Data.Array as Array
import Data.Enum (fromEnum)
import Data.Foldable (class Foldable, foldl, foldr)
import Data.HashMap (HashMap)
import Data.HashMap as HM
import Data.Map (Map)
import Data.Map as Map
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Time (Time)
import Flauros.Data.Time (class HasDate, class HasDateTime, Reoccurance, getDay, getMonth, getTime)
import Flauros.Prelude (Identified, Tagged, Tag, withDefTags)
import Prim.Row (class Nub, class Union)
import Record as Record
import Type.Row (type (+))

data Priority = Hidden | SummoningDay | Minor | Major

type Holiday r =
  ( eventMods :: HashMap Tag Number
  , priority  :: Priority
  , span      :: Map Reoccurance { begin :: Time, end :: Time }
  | r
  )

type HolidayRec = {| Identified + Holiday + () }

withDefHoliday :: forall r1 r2 r3. Union r1 (eventMods :: HashMap Tag Number | Tagged ()) r2 => Nub r2 r3 => {| r1 } -> {| r3 }
withDefHoliday r = Record.merge r $ withDefTags
  { eventMods: HM.empty :: HashMap Tag Number
  }

type HolidaySchedule = Map Reoccurance (Array HolidayRec)

mkSchedule :: forall f. Foldable f => f HolidayRec -> HolidaySchedule
mkSchedule = foldl f (Map.empty :: HolidaySchedule)
  where f sched holiday  = foldr (Map.alter (add holiday)) sched $ Map.keys holiday.span
        add h (Just arr) = Just $ Array.snoc arr h
        add h Nothing    = Just $ [ h ]

-- | Returns all holidays occuring on a given date for the schedule
holidaysOn :: forall d. HasDate d => d -> HolidaySchedule -> Array HolidayRec
holidaysOn d = fromMaybe [] <<< Map.lookup { month: getMonth d, day: fromEnum $ getDay d }

-- | Returns all holidays occuring on a given date and time for the schedule
-- Since not all holidays occur all-day-long, the following will hold true
-- @
-- length (holidaysAt datetime schedule) <= length (holidaysOn (getDate datetime) schedule)
-- @
holidaysAt:: forall d. HasDateTime d => d -> HolidaySchedule -> Array HolidayRec
holidaysAt dt sch = fromMaybe [] (Array.filter inSpan <$> Map.lookup key sch)
  where key        = { month: getMonth dt, day: fromEnum $ getDay dt }
        inSpan hol = fromMaybe false (Map.lookup key hol.span <#> \{ begin, end } -> between begin end $ getTime dt)

allDay :: { begin :: Time, end :: Time }
allDay = { begin: bottom, end: top }

isVisible :: forall r. {| Holiday + r } -> Boolean
isVisible h = case h.priority of
  Hidden -> false
  _      -> true