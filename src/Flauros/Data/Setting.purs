module Flauros.Data.Setting where

import Prelude

import CSS (Color)
import Data.Argonaut (class DecodeJson, class EncodeJson, Json, JsonDecodeError, jsonZero)
import Data.Argonaut.Decode.Generic (genericDecodeJson)
import Data.Argonaut.Encode.Generic (genericEncodeJson)
import Data.Either (Either)
import Data.FunctorWithIndex (mapWithIndex)
import Data.Generic.Rep (class Generic)
import Data.HashMap (HashMap)
import Data.HashMap as HM
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Newtype (class Newtype)
import Flauros.Data.System.JSON (class FromJSON, class ToJSON, fromJSON, toJSON)
import Flauros.Prelude (IDMap, Tagged, Identified, withDefTags)
import Foreign (Foreign, unsafeFromForeign, unsafeToForeign)
import Halogen.Store.Monad (class MonadStore)
import Prim.Row (class Nub, class Union)
import Record as Record
import Type.Row (type (+))

data ValueRange a
  = Choice (Array a)
  | ColorPicker (Color -> a) (a -> Color)

valueRangeToForeign :: forall a. ValueRange a -> ValueRange Foreign
valueRangeToForeign = case _ of
  Choice a -> Choice $ map unsafeToForeign a
  ColorPicker i o -> ColorPicker (i >>> unsafeToForeign) (unsafeFromForeign >>> o)

data Storage = BrowserStorage | SaveStorage
derive instance eqStorage :: Eq Storage
derive instance ordStorage :: Ord Storage
derive instance genericStorage :: Generic Storage _
instance decodeJsonStorage :: DecodeJson Storage where decodeJson = genericDecodeJson
instance encodeJsonStorage :: EncodeJson Storage where encodeJson = genericEncodeJson
instance fromJSONStorage :: FromJSON Storage where fromJSON = genericDecodeJson
instance toJSONStorage :: ToJSON Storage where toJSON = genericEncodeJson

type SettingConfig a s v =
  { default  :: v
  , onChange :: forall m. MonadStore a s m => v -> v -> m Unit
  , values   :: ValueRange v
  , storage  :: Storage
  , isEnabled :: s -> Boolean
  }
  
withDefConfig :: forall r1 r2 r3 s. Union r1 ( storage :: Storage, isEnabled :: s -> Boolean ) r2 => Nub r2 r3 => {| r1 } -> {| r3 }
withDefConfig r = Record.merge r
  { storage: BrowserStorage
  , isEnabled: \(_ :: s) -> true
  }

newtype SettingBehavior a s v = SettingBehavior
  { encode   :: v -> Json
  , decode   :: Json -> Either JsonDecodeError v
  , default  :: v
  , onChange :: forall m. MonadStore a s m => v -> v -> m Unit
  , values   :: ValueRange v
  , isEnabled :: s -> Boolean
  , storage  :: Storage
  }

mkBehavior :: forall a s v. FromJSON v => ToJSON v => Show v => SettingConfig a s v -> SettingBehavior a s Foreign
mkBehavior { default, onChange, values, isEnabled, storage } = SettingBehavior
  { encode:   \obj -> toJSON (unsafeFromForeign obj :: v)
  , decode:   \j -> fromJSON j <#> (unsafeToForeign :: v -> Foreign)
  , default:  unsafeToForeign default
  , onChange: \old new -> onChange (unsafeFromForeign old :: v) (unsafeFromForeign new :: v)
  , values:   valueRangeToForeign values
  , isEnabled
  , storage
  }

type Setting a s r =
  ( category :: String
  , behavior :: SettingBehavior a s Foreign
  | r
  )

withDefSetting :: forall r1 r2 r3. Union r1 (| Tagged () ) r2 => Nub r2 r3 => {| r1 } -> {| r3 }
withDefSetting r = Record.merge r $ withDefTags {}

currentValue :: forall a s r1 r2. {| Identified + Setting a s + r1 } -> { settingsBrowser :: IDMap Foreign, settingsSave :: IDMap Foreign | r2 } -> Foreign
currentValue setting st = fromMaybe b.default $ HM.lookup setting.id $ hm
  where SettingBehavior b = setting.behavior
        hm = case b.storage of
          BrowserStorage -> st.settingsBrowser
          SaveStorage    -> st.settingsSave