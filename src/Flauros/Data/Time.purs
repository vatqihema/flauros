module Flauros.Data.Time where

import Prelude

import Data.Argonaut (class DecodeJson, class EncodeJson)
import Data.Argonaut.Decode.Generic (genericDecodeJson)
import Data.Argonaut.Encode.Generic (genericEncodeJson)
import Data.Bounded.Generic (genericBottom, genericTop)
import Data.Date (Date, Month(..), Weekday(..), canonicalDate)
import Data.Date as Date
import Data.DateTime (DateTime(..), Hour, Millisecond, Minute, Second, Time(..), adjust, date, time)
import Data.DateTime as DateTime
import Data.Enum (class BoundedEnum, class Enum, fromEnum, toEnum)
import Data.Enum.Generic (genericCardinality, genericFromEnum, genericPred, genericSucc, genericToEnum)
import Data.Generic.Rep (class Generic)
import Data.Hashable (class Hashable)
import Data.Maybe (Maybe(..), fromJust)
import Data.Newtype (class Newtype, unwrap)
import Data.Number (abs)
import Data.Time as Time
import Data.Time.Duration (class Duration, Days)
import Flauros.Data.System.JSON (class FromJSON, class ToJSON)
import Flauros.Prelude (class Default)
import Partial.Unsafe (unsafePartial)
  
data TimeOfDay = Dawn | Day | Dusk | Night
derive instance eqTimeOfDay :: Eq TimeOfDay
derive instance ordTimeOfDay :: Ord TimeOfDay
derive instance genericTimeOfDay :: Generic TimeOfDay _
instance Bounded TimeOfDay where
  top    = genericTop
  bottom = genericBottom
instance Enum TimeOfDay where
  succ = genericSucc
  pred = genericPred
instance BoundedEnum TimeOfDay where
  cardinality = genericCardinality
  fromEnum    = genericFromEnum
  toEnum      = genericToEnum
instance decodeJsonTimeOfDay :: DecodeJson TimeOfDay where decodeJson = genericDecodeJson
instance encodeJsonTimeOfDay :: EncodeJson TimeOfDay where encodeJson = genericEncodeJson
instance fromJSONTimeOfDay :: FromJSON TimeOfDay where fromJSON = genericDecodeJson
instance toJSONTimeOfDay :: ToJSON TimeOfDay where toJSON = genericEncodeJson
instance hashableTimeOfDay :: Hashable TimeOfDay where
  hash = fromEnum

-- | The current in-game time
newtype CurrentTime = CurrentTime DateTime

type Reoccurance = { month :: Month, day :: Int }

derive instance eqCurrentTime :: Eq CurrentTime
derive instance newtypeCurrentTime :: Newtype CurrentTime _
derive newtype instance ordCurrentTime :: Ord CurrentTime
derive newtype instance fromJSONCurrentTime :: FromJSON CurrentTime
derive newtype instance toJSONCurrentTime :: ToJSON CurrentTime
derive newtype instance hasDateCurrentTime :: HasDate CurrentTime
derive newtype instance hasDateTimeCurrentTime :: HasDateTime CurrentTime
derive newtype instance hasTimeCurrentTime :: HasTime CurrentTime

-- | Defaults to 1222-09-03 8:00am
instance defaultCurrentTime :: Default CurrentTime where
  default = CurrentTime $ DateTime date time
    where date = mkDate 1222 September 3
          time = unsafePartial $ fromJust (Time <$> (toEnum 8) <*> (toEnum 0) <*> (toEnum 0) <*> (toEnum 0))

class HasDate a where 
  getDay       :: a -> Date.Day 
  getMonth     :: a -> Date.Month 
  getYear      :: a -> Date.Year
  getDate      :: a -> Date.Date
  getDayOfWeek :: a -> Weekday
  adjustDate   :: Days -> a -> a

instance hasDateDate :: HasDate Date.Date where
  getDay       = Date.day
  getMonth     = Date.month
  getYear      = Date.year
  getDate      = identity
  getDayOfWeek = Date.weekday
  adjustDate days dt = unsafePartial $ fromJust $ Date.adjust days dt

instance hasDateDateTime :: HasDate DateTime where
  getDay   = Date.day <<< date
  getMonth = Date.month <<< date
  getYear  = Date.year <<< date
  getDate  = date
  getDayOfWeek = Date.weekday <<< date
  adjustDate days dt = unsafePartial $ fromJust $ adjust days dt

class HasTime a where
  getTime        :: a -> Time
  getHour        :: a -> Hour
  getMinute      :: a -> Minute
  getSecond      :: a -> Second
  getMillisecond :: a -> Millisecond

instance hasTimeTime :: HasTime Time where
  getTime        = identity
  getHour        = Time.hour
  getMinute      = Time.minute
  getSecond      = Time.second
  getMillisecond = Time.millisecond

instance hasTimeDateTime :: HasTime DateTime where
  getTime        = time
  getHour        = Time.hour <<< time
  getMinute      = Time.minute <<< time
  getSecond      = Time.second <<< time
  getMillisecond = Time.millisecond <<< time

class (HasDate a, HasTime a) <= HasDateTime a where
  getDateTime :: a -> DateTime
  adjustDateTime :: forall d. Duration d => d -> a -> Maybe a

instance hasDateTimeDateTime :: HasDateTime DateTime where
  getDateTime = identity
  adjustDateTime = DateTime.adjust

----------------
-- Date Utils --
----------------

getAgeInYears :: forall d1 d2. HasDate d1 => HasDate d2 => d1 -> d2 -> Int
getAgeInYears currentTime dateTime =
  let dt       = getDate dateTime
      ct       = getDate currentTime
      dtYear   = fromEnum $ getYear dt
      ctYear   = fromEnum $ getYear currentTime
      pastBD   = getMonth dt < getMonth ct || (getMonth dt == getMonth ct && getDay dt <= getDay ct)
   in ctYear - dtYear + (if pastBD then 0 else -1)

mkTimeHM :: Int -> Int -> Time
mkTimeHM h m = unsafePartial $ fromJust (Time <$> toEnum h <*> toEnum m <*> toEnum 0 <*> toEnum 0)

mkDate :: Int -> Month -> Int -> Date
mkDate year month day = unsafePartial $ fromJust (canonicalDate <$> toEnum year <*> Just month <*> toEnum day)

atMidnight :: forall d. HasDate d => d -> DateTime
atMidnight d = DateTime (getDate d) bottom

firstDayOfMonth :: forall d. HasDate d => d -> Date
firstDayOfMonth d = canonicalDate (getYear d) (getMonth d) bottom

weekdayStr :: Weekday -> String
weekdayStr = case _ of
  Monday    -> "monday"
  Tuesday   -> "tuesday"
  Wednesday -> "wednesday"
  Thursday  -> "thursday"
  Friday    -> "friday"
  Saturday  -> "saturday"
  Sunday    -> "sunday"

reoccuranceIn :: Reoccurance -> Int -> Date
reoccuranceIn { month, day } year = mkDate year month day

isReoccurance :: forall d. HasDate d => Reoccurance -> d -> Boolean
isReoccurance { month, day } d = month == getMonth d && day == fromEnum (getDay d)

toReoccurance :: forall d. HasDate d => d -> Reoccurance
toReoccurance d = { month: getMonth d, day: fromEnum (getDay d) }

dateHash :: forall d. HasDate d => d -> Int
dateHash d = (fromEnum (getYear d)) * 409 + (fromEnum (getMonth d)) * 37 + (fromEnum (getDay d))

daysBetween :: forall d1 d2. HasDate d1 => HasDate d2 => d1 -> d2 -> Number
daysBetween a b = unwrap $ (Date.diff (getDate a) (getDate b) :: Days)