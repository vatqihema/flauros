module Flauros.Data.Body where

import Data.HashSet (HashSet)
import Flauros.Prelude (Identifier)

type BodyPart r =
  ( isEssential :: Boolean        -- ^ Will the creature die if this is destroyed?
  , isOrganic   :: Boolean        -- ^ Is this made of organic *living* material?
  , equipSlots  :: HashSet String -- ^ What equipment slots does possessing this give?
  | r
  )

data BodyNode = BodyNode
  { partID   :: Identifier
  , children :: Array BodyNode
  }

