module Flauros.Data.Event where

import Prelude

import Data.Generic.Rep (class Generic)
import Data.HashMap (HashMap)
import Data.HashMap as HM
import Data.HashSet (HashSet)
import Data.Hashable (class Hashable, hash)
import Data.Map (Map)
import Data.Maybe (fromMaybe)
import Flauros.Data.Actor (Actor)
import Flauros.Data.Store as DS
import Flauros.Data.Time (TimeOfDay)
import Flauros.Prelude (Tag)
import Halogen.Store.Monad (class MonadStore)

modOf :: forall r. { eventMods :: HashMap Tag Number | r } -> Tag -> Number
modOf rec id = fromMaybe 0.0 $ HM.lookup id rec.eventMods

type EventMod =
  { today :: Number
  , total :: Number
  }

type EventMods = Map Tag EventMod

data EventTrigger
  = Manual
  | AtTime TimeOfDay
  | OnEventMod String

derive instance eqEventTrigger :: Eq EventTrigger
derive instance generictypeEventTrigger :: Generic EventTrigger _
instance hashableEventTrigger :: Hashable EventTrigger where
  hash = case _ of
    Manual       -> 1867
    AtTime t     -> 1997 * hash t
    OnEventMod s -> 1999 * hash s

type Event :: forall k. k -> Row Type -> Row Type
type Event s r =
  ( trigger :: HashSet EventTrigger
  | r
  )

newtype EventAction r = EventAction (forall m. MonadStore DS.Action DS.Store m => m r)

onEvent :: forall r. (forall m. MonadStore DS.Action DS.Store m => m r) -> EventAction r
onEvent = EventAction

data ActionResult r = Yield r | Cancel

-------------------
-- Common Events --
-------------------

type Consumable r =
  ( onConsume :: {| Actor () } -> EventAction (ActionResult {| Actor () })
  | r
  )