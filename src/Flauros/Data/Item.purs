module Flauros.Data.Item where

import Prelude

import CSS (Color)
import Data.Enum (class Enum, succ)
import Data.Enum.Generic (genericPred, genericSucc)
import Data.Generic.Rep (class Generic)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Number (log)
import Flauros.Data.Time (class HasDate, CurrentTime, daysBetween)
import Flauros.Prelude (class Transmute, Tagged, default, transmute, withDefTags)
import Flauros.Style.Color (Colors, sand)
import Prim.Row (class Nub, class Union)
import Record as Record

type Price = Number

inflationMult :: forall d. HasDate d => d -> Number -> Number
inflationMult hd magnitude =
  let diff = daysBetween (default :: CurrentTime) hd
   in if diff <= 60.0 then 1.0
                      else 1.0 + 0.1 * log ((diff - 60.0) * 0.5) * magnitude

type Item r =
  ( price     :: Price
  , size      :: Size
  , textColor :: Colors -> Color
  , units     :: Maybe String
  | r
  )

withDefItem :: forall r1 r2 r3. Union r1 (textColor :: Colors -> Color, units :: Maybe String | Tagged ()) r2 => Nub r2 r3 => {| r1 } -> {| r3 }
withDefItem r = Record.merge r $ withDefTags
  { textColor: sand
  , units: Nothing :: Maybe String
  }

------------
--- Size ---
------------

type Size = Number

data SizeCategory = Microscopic |  Miniscule | Tiny | Little | Medium | Big | Huge | Massive | Giant | Enormous | Colossal
derive instance eqSizeCategory :: Eq SizeCategory
derive instance genericSizeCategory :: Generic SizeCategory _
derive instance ordSizeCategory :: Ord SizeCategory
instance enumSizeCategory :: Enum SizeCategory where
  pred = genericPred
  succ = genericSucc

instance Transmute SizeCategory SizeCategory where
  transmute s = s
instance Transmute Number SizeCategory where
  transmute w
    | w >= colossal  = Colossal
    | w >= enormous  = Enormous
    | w >= giant     = Giant
    | w >= massive   = Massive
    | w >= huge      = Huge
    | w >= big       = Big
    | w >= medium    = Medium
    | w >= little    = Little
    | w >= tiny      = Tiny
    | w >= miniscule = Miniscule
    | otherwise      = Microscopic

microscopic :: Size
microscopic = 0.01
miniscule :: Size
miniscule = 0.05
tiny      :: Size
tiny      = 0.25
little    :: Size
little    = 0.5
medium    :: Size
medium    = 1.0
big       :: Size
big       = 2.0
huge      :: Size
huge      = 3.0
massive   :: Size
massive   = 5.0
giant     :: Size
giant     = 7.0
enormous  :: Size
enormous  = 10.0
colossal  :: Size
colossal  = 20.0

sizeCatOf :: Size -> SizeCategory
sizeCatOf = transmute

sizeOf :: SizeCategory -> Size
sizeOf = case _ of
  Microscopic -> microscopic
  Miniscule   -> miniscule
  Tiny        -> tiny
  Little      -> little
  Medium      -> medium
  Big         -> big
  Huge        -> huge
  Massive     -> massive
  Giant       -> giant
  Enormous    -> enormous
  Colossal    -> colossal
  
nextSizeUp :: Size -> Size
nextSizeUp s = sizeOf $ fromMaybe (max cat Colossal) $ succ cat
  where cat = sizeCatOf s

-- | Almost entirely arbitrary expected average prices for a given size
avgPrice :: forall s. Transmute s SizeCategory => s -> Number
avgPrice s = case transmute s of
  Microscopic -> 100.0
  Miniscule   -> 20.0
  Tiny        -> 5.0
  Little      -> 20.0
  Medium      -> 50.0
  Big         -> 100.0
  Huge        -> 400.0
  Massive     -> 1000.0
  Giant       -> 10000.0
  Enormous    -> 50000.0
  Colossal    -> 500000.0

-- | For a given size and price, returns a number representing how many multiples of the avg price for size it is
multOfAvgPrice :: forall s. Transmute s SizeCategory => s -> Price -> Number
multOfAvgPrice s p = p / avgPrice s

isNMults :: forall s. Transmute s SizeCategory => Number -> Price -> s -> Boolean
isNMults n p s = multOfAvgPrice s p >= n