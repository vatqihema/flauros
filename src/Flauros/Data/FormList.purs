module Flauros.Data.FormList where

import Prelude

import Data.Array as Array
import Data.HashSet (HashSet)
import Data.HashSet as HS
import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype)
import Effect.Class (class MonadEffect)
import Flauros.Prelude (Identifier)
import Flauros.Random (randomNth, randomWeightedBy)

newtype FormList :: forall k. Row Type -> k -> Type
newtype FormList meta registry = FormList (Array (FormRow meta))
derive instance newtypeFormList :: Newtype (FormList meta registry) _

type FormRow meta = { id :: Identifier, meta :: {| meta } }

noMeta :: Identifier -> FormRow ()
noMeta id = { id, meta: {} }

type IDFormList :: forall k. Row Type -> k -> Type
type IDFormList meta registry =
  { id :: Identifier
  , random :: forall m. MonadEffect m => Array (FormRow meta) -> m (Maybe (FormRow meta))
  , tags :: HashSet String
  , list :: FormList meta registry
  }

-- validate :: FormList m -> m Boolean

-- instance FormMetaName s 

formlist_ :: forall r. Identifier -> Array Identifier -> IDFormList () r
formlist_ id ids = { id, random: randomNth, tags: HS.empty, list: FormList $ map (\id -> { id, meta: {} }) $ ids }

formlist :: forall m r. Identifier -> Array (FormRow m) -> IDFormList m r
formlist id ids = { id, random: randomNth, tags: HS.empty, list: FormList ids }

---------------------
-- Common Metadata --
---------------------

type Countable r = ( count :: Int | r )

type Damagable r = ( damage :: Number | r )

randomByWeight :: forall m meta. MonadEffect m => Array (FormRow ( weight :: Number | meta )) -> m (Maybe (FormRow ( weight :: Number | meta )))
randomByWeight arr = randomWeightedBy _.meta.weight arr <#> case _ of
  Just idx -> Array.index arr idx 
  Nothing  -> Nothing