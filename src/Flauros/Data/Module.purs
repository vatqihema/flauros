module Flauros.Data.Module where

import Prelude

import Data.Argonaut (class DecodeJson, class EncodeJson)
import Data.Argonaut.Decode.Generic (genericDecodeJson)
import Data.Argonaut.Encode.Generic (genericEncodeJson)
import Data.Generic.Rep (class Generic)
import Data.HashSet (HashSet)
import Data.Show.Generic (genericShow)
import Flauros.Data.System.JSON (class FromJSON, class ToJSON)
import Flauros.Prelude (Tagged, withDefTags)
import Prim.Row (class Nub, class Union)
import Record as Record

data Version = Version Int Int Int Int
derive instance eqVersion      :: Eq Version
derive instance genericVersion :: Generic Version _
derive instance ordVersion     :: Ord Version
instance showVersion :: Show Version where show = genericShow
instance decodeJsonVersion :: DecodeJson Version where decodeJson = genericDecodeJson
instance encodeJsonVersion :: EncodeJson Version where encodeJson = genericEncodeJson
instance fromJSONVersion   :: FromJSON   Version where fromJSON = genericDecodeJson
instance toJSONVersion     :: ToJSON     Version where toJSON = genericEncodeJson

type Module r =
  ( authors      :: HashSet String
  , version      :: Version
  , dependencies :: HashSet String
  , priority     :: Number
  | r
  )

withDefModule :: forall r1 r2 r3
               . Union r1 (priority :: Number | Tagged ()) r2
              => Nub r2 r3
              => {| r1 }
              -> {| r3 }
withDefModule r = Record.merge r $ withDefTags
  { priority: 0.0
  }