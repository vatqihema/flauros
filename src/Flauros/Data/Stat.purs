module Flauros.Data.Stat where

import Prelude

import Data.Maybe (Maybe(..))
import Flauros.Prelude (Tagged, withDefTags)
import Prim.Row (class Nub, class Union)
import Record as Record

type StatDef r =
  ( min     :: Number
  , max     :: Number
  , default :: Number
  | r
  )

withDefStatDef :: forall r1 r2 r3. Union r1 (default :: Number, min :: Number, max :: Number | Tagged ()) r2 => Nub r2 r3 => {| r1 } -> {| r3 }
withDefStatDef r = Record.merge r $ withDefTags defStatDef

defStatDef :: {| StatDef () }
defStatDef =
  { default: 0.0
  , min:     -9999999.0 -- bottom -- (-infinity) -- these have deserialization issues with json if I use infinities :(
  , max:     9999999.0  -- top -- infinity
  }

orDefStat :: forall r. Maybe {| StatDef r } -> {| StatDef () }
orDefStat Nothing  = defStatDef
orDefStat (Just s) = { default: s.default, min: s.min, max: s.max }

type StatState =
  { base :: Number
  , mod  :: Number
  , limits :: { min :: Number, max :: Number }
  }

defStatState :: forall r. {| StatDef r } -> StatState
defStatState def = { base: def.default, mod: 0.0, limits: { min: def.min, max: def.max } }

baseState :: Number -> StatState
baseState base = { base, mod: 0.0, limits: { min: defStatDef.min, max: defStatDef.max } }

modifyBase :: (Number -> Number) -> StatState -> StatState
modifyBase f s = s { base = f s.base }

modifyMod :: (Number -> Number) -> StatState -> StatState
modifyMod f s = s { mod = f s.base }

totalStat :: StatState -> Number
totalStat { base, mod, limits: lim } = min lim.max $ max lim.min $ base + mod