module Flauros.Data.System.Browser
  ( localStorageSize
  , readLocalStorage
  , writeLocalStorage
  ) where

import Prelude

import Data.Argonaut (JsonDecodeError(..), parseJson, stringify)
import Data.Either (Either(..))
import Data.Int (floor)
import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Class (class MonadEffect, liftEffect)
import Flauros.Data.System.JSON (class FromJSON, class ToJSON, fromJSON, toJSON)

foreign import getLocalStorage    :: forall a. (a -> Maybe a) -> (Maybe a) -> String -> Effect (Maybe String)
foreign import setLocalStorage    :: String -> String -> Effect Unit
foreign import localStorageLength :: Effect Number

localStorageSize :: forall m. MonadEffect m => m Int
localStorageSize = floor <$> liftEffect localStorageLength

readLocalStorage :: forall a m. FromJSON a => MonadEffect m => String -> m (Either JsonDecodeError a)
readLocalStorage key = liftEffect do
  getLocalStorage Just Nothing key <#> case _ of
    Nothing  -> Left $ Named ("localStorage was undefined for key `" <> key <> "`") MissingValue
    Just str -> parseJson str >>= fromJSON

writeLocalStorage :: forall a m. ToJSON a => MonadEffect m => String -> a -> m Unit
writeLocalStorage key = liftEffect <<< setLocalStorage key <<< stringify <<< toJSON