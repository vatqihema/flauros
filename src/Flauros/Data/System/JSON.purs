module Flauros.Data.System.JSON where

import Prelude

import CSS (Color, rgba, toRGBA)
import Data.Argonaut (Json, JsonDecodeError(..), decodeJson, encodeJson, fromObject, fromString, jsonEmptyObject, toObject, (.:), (~>))
import Data.Argonaut.Decode.Class (gDecodeJson)
import Data.Argonaut.Decode.Decoders (decodeArray, decodeEither, decodeForeignObject, decodeIdentity, decodeList, decodeMap, decodeMaybe, decodeNonEmptyArray, decodeNonEmpty_Array, decodeNonEmpty_List, decodeSet, decodeTuple)
import Data.Argonaut.Decode.Decoders as Decoders
import Data.Argonaut.Encode.Class (gEncodeJson)
import Data.Argonaut.Encode.Encoders (encodeArray, encodeEither, encodeForeignObject, encodeIdentity, encodeList, encodeMap, encodeMaybe, encodeNonEmptyArray, encodeNonEmpty_Array, encodeNonEmpty_List, encodeSet, encodeTuple)
import Data.Argonaut.Encode.Encoders as Encoders
import Data.Array.NonEmpty (NonEmptyArray)
import Data.Bifunctor (lmap)
import Data.Date (Date, Month)
import Data.Date as Date
import Data.DateTime (DateTime(..), Time(..), hour, millisecond, minute, second)
import Data.Either (Either(..))
import Data.Enum (class BoundedEnum, fromEnum, toEnum)
import Data.HashMap (HashMap)
import Data.HashMap as HM
import Data.HashSet (HashSet)
import Data.HashSet as HS
import Data.Hashable (class Hashable)
import Data.Identity (Identity)
import Data.JSDate (JSDate)
import Data.JSDate as JSDate
import Data.List (List)
import Data.Map (Map)
import Data.Maybe (Maybe(..))
import Data.NonEmpty (NonEmpty)
import Data.Set (Set)
import Data.String (CodePoint)
import Data.String as String
import Data.String.CodeUnits (charAt)
import Data.String.NonEmpty (NonEmptyString)
import Data.Symbol (class IsSymbol, reflectSymbol)
import Data.Tuple (Tuple)
import Data.Tuple.Nested ((/\))
import Flauros.Prelude (MyID(..), padBefore)
import Foreign.Object (Object)
import Foreign.Object as Object
import Prim.Row (class Cons)
import Prim.Row as Row
import Prim.RowList (class RowToList, Nil, RowList)
import Prim.RowList as RowList
import Record as Record
import Type.Prelude (Proxy(..))

-- These technically break purity, but we'll try to be careful and not pass in anything that triggers that
foreign import dateToISOString :: JSDate -> String
foreign import parseJSDate     :: String -> JSDate


class GFromJSON (r :: Row Type) (l :: RowList Type) | l -> r where
  gFromJSON :: forall proxy. Object Json -> proxy l -> Either JsonDecodeError (Record r)

class GToJSON (r :: Row Type) (l :: RowList Type) where
  gToJSON :: forall proxy. Record r -> proxy l -> Object Json

instance GFromJSON () Nil where gFromJSON = gDecodeJson
instance gToJSONNil :: GToJSON r Nil where gToJSON = gEncodeJson

instance gToJSONCons ::
  ( ToJSON value
  , GToJSON row tail
  , IsSymbol field
  , Cons field value tail' row
  ) =>
  GToJSON row (RowList.Cons field value tail) where
  gToJSON row _ = do
    let _field = Proxy :: Proxy field
    Object.insert
      (reflectSymbol _field)
      (toJSON $ Record.get _field row)
      (gToJSON row (Proxy :: Proxy tail))

instance gFromJSONCons ::
  ( FromJSONField val
  , GFromJSON rs ts
  , IsSymbol field
  , Row.Cons field val rs row
  , Row.Lacks field rs
  ) => GFromJSON row (RowList.Cons field val ts) where
  gFromJSON obj _ =
    let _field = Proxy :: Proxy field
        fieldName = reflectSymbol _field
        fieldValue = Object.lookup fieldName obj
     in case fromJSONField fieldValue of
          Just fieldVal -> do
            val <- lmap (AtKey fieldName) fieldVal
            rest <- gFromJSON obj (Proxy :: Proxy ts)
            Right $ Record.insert _field val rest
          Nothing ->
            Left $ AtKey fieldName MissingValue

-- Orphan instances aren't allowed (which makes me sad even if they're usually bad)
-- Having to wrap everything bothers me so much I'm basically going to re-export
-- Data.Argonaut.Decode.Class and Data.Argonaut.Encode.Class

class FromJSON a where
  fromJSON :: Json -> Either JsonDecodeError a

class ToJSON a where
  toJSON :: a -> Json

class FromJSONField a where
  fromJSONField :: Maybe Json -> Maybe (Either JsonDecodeError a)

instance fromJSONFieldMaybe :: FromJSON (Maybe a) => FromJSONField (Maybe a) where
  fromJSONField = case _ of
    Just j  -> Just $ fromJSON j
    Nothing -> Just $ Right Nothing
else instance fromJSONFieldElse :: FromJSON a => FromJSONField a where
  fromJSONField = (fromJSON <$> _)

-------------
-- Generic --
-------------

-------------
-- Default --
-------------

instance ToJSON   Boolean where toJSON = encodeJson
instance FromJSON Boolean where fromJSON = decodeJson
instance ToJSON   Number where toJSON = encodeJson
instance FromJSON Number where fromJSON = decodeJson
instance ToJSON   Int where toJSON = encodeJson
instance FromJSON Int where fromJSON = decodeJson
instance ToJSON   String where toJSON = encodeJson
instance FromJSON String where fromJSON = decodeJson
instance ToJSON   Json where toJSON = encodeJson
instance FromJSON Json where fromJSON = decodeJson
instance ToJSON   CodePoint where toJSON = encodeJson
instance FromJSON CodePoint where fromJSON = decodeJson
instance ToJSON   NonEmptyString where toJSON = encodeJson
instance FromJSON NonEmptyString where fromJSON = decodeJson
instance ToJSON   Unit where toJSON = encodeJson
instance FromJSON Unit where fromJSON = decodeJson
instance ToJSON   Void where toJSON = encodeJson
instance FromJSON Void where fromJSON = decodeJson
instance ToJSON a   => ToJSON   (Identity a) where toJSON = encodeIdentity toJSON
instance FromJSON a => FromJSON (Identity a) where fromJSON = decodeIdentity fromJSON
instance ToJSON a   => ToJSON   (Maybe a) where toJSON = encodeMaybe toJSON
instance FromJSON a => FromJSON (Maybe a) where fromJSON = decodeMaybe fromJSON
instance (ToJSON a, ToJSON b) => ToJSON (Tuple a b) where toJSON = encodeTuple toJSON toJSON
instance (FromJSON a, FromJSON b) => FromJSON (Tuple a b) where fromJSON = decodeTuple fromJSON fromJSON
instance (ToJSON a, ToJSON b) => ToJSON (Either a b) where toJSON = encodeEither toJSON toJSON
instance (FromJSON a, FromJSON b) => FromJSON (Either a b) where fromJSON = decodeEither fromJSON fromJSON
instance ToJSON a => ToJSON (NonEmpty Array a) where toJSON = encodeNonEmpty_Array toJSON
instance FromJSON a => FromJSON (NonEmpty Array a) where fromJSON = decodeNonEmpty_Array fromJSON
instance ToJSON a => ToJSON (NonEmptyArray a) where toJSON = encodeNonEmptyArray toJSON
instance FromJSON a => FromJSON (NonEmptyArray a) where fromJSON = decodeNonEmptyArray fromJSON
instance ToJSON a => ToJSON (NonEmpty List a) where toJSON = encodeNonEmpty_List toJSON
instance FromJSON a => FromJSON (NonEmpty List a) where fromJSON = decodeNonEmpty_List fromJSON
-- instance ToJSON a => ToJSON (NonEmptyList a) where toJSON = encodeNonEmptyList toJSON
instance ToJSON a => ToJSON (Array a) where toJSON = encodeArray toJSON
instance FromJSON a => FromJSON (Array a) where fromJSON = decodeArray fromJSON
instance ToJSON a => ToJSON (List a) where toJSON = encodeList toJSON
instance FromJSON a => FromJSON (List a) where fromJSON = decodeList fromJSON
instance ToJSON a => ToJSON (Object a) where toJSON = encodeForeignObject toJSON
instance FromJSON a => FromJSON (Object a) where fromJSON = decodeForeignObject fromJSON
instance (Ord a, ToJSON a) => ToJSON (Set a) where toJSON = encodeSet toJSON
instance (Ord a, FromJSON a) => FromJSON (Set a) where fromJSON = decodeSet fromJSON
instance (Ord a, ToJSON a, ToJSON b) => ToJSON (Map a b) where toJSON = encodeMap toJSON toJSON
instance (Ord a, FromJSON a, FromJSON b) => FromJSON (Map a b) where fromJSON = decodeMap fromJSON fromJSON

instance ToJSON Char where toJSON = encodeJson
instance FromJSON Char where
  fromJSON j = decodeJson j >>= \s -> case charAt 0 s of
    Just c | String.length s == 1 -> Right c
    _                             -> Left $ TypeMismatch $ "Expected Char, found String " <> show s

instance (GToJSON r l, RowToList r l) => ToJSON (Record r) where
  toJSON r = fromObject $ gToJSON r (Proxy :: Proxy l)

instance (GFromJSON r l, RowToList r l) => FromJSON (Record r) where
  fromJSON j = case toObject j of
    Just obj -> gFromJSON obj (Proxy :: Proxy l)
    Nothing  -> Left $ TypeMismatch "Record must be Object" 

instance fromJSONDate :: FromJSON Date where
  fromJSON json = decodeJson json <#> parseJSDate <#> JSDate.toDateTime >>= case _ of
    Nothing             -> pure bottom
    Just (DateTime d _) -> pure d

instance toJSONDate :: ToJSON Date where
  toJSON d = fromString $ pad 4 (Date.year d) <> "-" <> pad 2 (Date.month d) <> "-" <> pad 2 (Date.day d)
    where pad :: forall e. BoundedEnum e => Int -> e -> String
          pad n = padBefore n '0' <<< show <<< fromEnum

instance fromJSONTime :: FromJSON Time where
  fromJSON json = decodeJson json
              >>= \obj -> Time
              <$> (enumFromJSON =<< obj .: "hour")
              <*> (enumFromJSON =<< obj .: "minute")
              <*> (enumFromJSON =<< obj .: "second")
              <*> (enumFromJSON =<< obj .: "milli")

instance toJSONTime :: ToJSON Time where
  toJSON d = "hour"   ::= fromEnum (hour d)
          ~> "minute" ::= fromEnum (minute d)
          ~> "second" ::= fromEnum (second d)
          ~> "milli"  ::= fromEnum (millisecond d)
          ~> jsonEmptyObject

instance fromJSONDateTime :: FromJSON DateTime where
  fromJSON json = decodeJson json <#> parseJSDate <#> JSDate.toDateTime >>= case _ of
    Nothing -> pure bottom
    Just dt -> pure dt

instance toJSONDateTime :: ToJSON DateTime where
  toJSON = fromString <<< dateToISOString <<< JSDate.fromDateTime

instance fromJSONMonth :: FromJSON Month where
  fromJSON = decodeJson >=> enumFromJSON

instance toJSONMonth :: ToJSON Month where
  toJSON = fromEnum >>> encodeJson

instance FromJSON Color where
  fromJSON json = fromJSON json >>= \v ->
    rgba <$> v .: "r" <*> v .: "g" <*> v .: "b" <*> v .: "a"

instance ToJSON Color where
  toJSON = toJSON <<< toRGBA

instance (FromJSON k, FromJSON v, Hashable k) => FromJSON (HashMap k v) where
  fromJSON j = decodeArray fromJSON j <#> HM.fromArray

instance (ToJSON k, ToJSON v) => ToJSON (HashMap k v) where
  toJSON = encodeArray toJSON <<< HM.toArrayBy (\k v -> encodeTuple toJSON toJSON ( k /\ v))

instance (FromJSON a, Hashable a) => FromJSON (HashSet a) where
  fromJSON j = decodeArray fromJSON j <#> HS.fromArray

instance (ToJSON a) => ToJSON (HashSet a) where
  toJSON = encodeArray toJSON <<< HS.toArray

-------------
-- Prelude --
-------------

instance fromJSONMyID   :: FromJSON MyID   where fromJSON = map MyID <$> fromJSON
instance toJSONMyID     :: ToJSON MyID     where toJSON (MyID id) = toJSON id

-----------
-- Utils --
-----------

into :: forall a. ToJSON a => String -> a -> Tuple String Json
into = Encoders.assoc toJSON
infix 7 into as ::=

has :: forall a. FromJSON a => Object Json -> String -> Either JsonDecodeError a
has = Decoders.getField fromJSON
infix 7 has as ..:

enumFromJSON :: forall e. BoundedEnum e => Int -> Either JsonDecodeError e
enumFromJSON i = case toEnum i of
  Just e  -> Right e
  Nothing -> Left $ TypeMismatch $ "Number " <> show i <> " could not be converted to enum"