module Flauros.Data.System.Save where

import Prelude

import Data.Argonaut (Json, stringify)
import Data.Date (Month(..))
import Data.Either (Either(..))
import Data.FunctorWithIndex (mapWithIndex)
import Data.HashMap (HashMap)
import Data.HashMap as HM
import Data.Hashable (class Hashable)
import Data.Map as Map
import Data.Maybe (Maybe(..), fromJust)
import Data.Newtype (class Newtype)
import Data.Time.Duration (class Duration)
import Data.TraversableWithIndex (forWithIndex)
import Data.Tuple (Tuple(..))
import Effect.Class (class MonadEffect, liftEffect)
import Effect.Class.Console as Console
import Flauros.Data.Actor (Actor, mkFirstLast, playerID, withDefActor)
import Flauros.Data.Knowledge (Fact, Scope(..), FactMap)
import Flauros.Data.Setting (Setting, SettingBehavior(..))
import Flauros.Data.Store as DS
import Flauros.Data.System.JSON (class ToJSON, toJSON)
import Flauros.Data.Time (CurrentTime, TimeOfDay(..), adjustDateTime, getDate, mkDate)
import Flauros.Data.Weather (Temperature, WeatherHistory)
import Flauros.Lang (class LangState, AllLines, Lang, LangInfo, Translate, TranslateAll, TranslateOr, TranslateWith, TranslateWithOr, TranslateAllWith, emptyLines, english, translate, translateAll, translateAllWith, translateContext, translateOr, translateWith, translateWithOr)
import Flauros.Log (LogLevel(..), LogType(..), formatJSONStringify)
import Flauros.Prefab.Registries (Setting')
import Flauros.Prefab.Registries as Regs
import Flauros.Prefab.Registry as Registry
import Flauros.Prelude (Identified, Identifier, IDMap, default, prettyShowID, sid, (|>))
import Flauros.Random (getCurrentSeed, setCurrentSeed)
import Flauros.Style.Color (Colors, defaultColors)
import Foreign (Foreign, unsafeFromForeign, unsafeToForeign)
import Halogen.Store.Monad (class MonadStore, getStore, updateStore)
import Halogen.Store.Select (Selector(..), selectEq)
import Partial.Unsafe (unsafePartial)
import Prim.Row (class Nub, class Union)
import Random.LCG (unSeed)
import Record as Record
import Record.Builder as Builder
import Type.Prelude (Proxy(..))
import Type.Row (type (+))

-------------------
-- Serialization --
-------------------

deserializeStates :: forall r m. MonadStore DS.Action DS.Store m => IDMap {| Setting' + r } -> IDMap Json -> m (IDMap Foreign)
deserializeStates settings jsons = forWithIndex jsons \id json ->
  case HM.lookup id settings of
    Just setting -> let SettingBehavior b = setting.behavior in case b.decode json of
      Right val -> pure $ unsafeToForeign val
      Left _ -> do
        DS.log Error $ "Failed to decode setting `" <> prettyShowID id <> "` (" <> stringify json <> "), using default"
        pure b.default
    Nothing      -> do
      DS.log Warning $ "No known setting `" <> prettyShowID id <> "`, deserializing setting as Json"
      pure $ unsafeToForeign json -- assume Json already since missing setting

serializeStates :: forall r m. MonadStore DS.Action DS.Store m => IDMap {| Setting' + r } -> IDMap Foreign -> m (IDMap Json)
serializeStates settings foreigns = forWithIndex foreigns \id obj ->
  case HM.lookup id settings of
    Just setting -> let SettingBehavior b = setting.behavior in pure $ b.encode obj
    Nothing      -> do
      DS.log Warning $ "No known setting `" <> prettyShowID id <> "`, serializing setting Foreign as is"
      pure $ unsafeFromForeign obj -- assume Json already since missing setting

serializeStore :: forall m. MonadStore DS.Action DS.Store m => DS.Store -> m DS.SerializableStore
serializeStore (DS.Store { actors, colors, currentTime, facts, isDebug, lang, logLevel, settingsBrowser, settingsSave, temperature, timeOfDay, uiState, variables, weather, weatherHistory }) = do
  settings <- Registry.lookupAll Regs.setting
  stBr <- serializeStates settings settingsBrowser
  stSv <- serializeStates settings settingsSave
  seed <- liftEffect $ getCurrentSeed
  pure { global: { colors, isDebug, lang, logLevel, settings: stBr }
       , unique: { actors, currentTime, facts, seed, temperature, timeOfDay, uiState, variables, weather, weatherHistory, settings: stSv }
       }

deserializeAndApply :: forall m. MonadStore DS.Action DS.Store m => DS.SerializableStore -> m Unit
deserializeAndApply ser = do
  settings <- Registry.lookupAll Regs.setting
  stBr <- deserializeStates settings ser.global.settings
  stSv <- deserializeStates settings ser.unique.settings
  let uprec s = flip Builder.build s
              $ Builder.merge (Record.delete (Proxy :: Proxy "settings") ser.global)
            >>> Builder.merge (Builder.build (Builder.delete (Proxy :: Proxy "settings") >>> Builder.delete (Proxy :: Proxy "seed")) ser.unique)
  updateStore $ DS.action (uprec >>> _ { settingsBrowser = stBr, settingsSave = stSv})
            >>> DS.action (uprec >>> _ { settingsBrowser = stBr, settingsSave = stSv})
            >>> DS.action (uprec >>> _ { settingsBrowser = stBr, settingsSave = stSv})
  liftEffect $ setCurrentSeed ser.unique.seed