export const getLocalStorage = just => nothing => key => () => {
    const str = window.localStorage[key];
    return str === undefined
        ? nothing
        : just(str);
}
export const setLocalStorage = key => value => () => {
    window.localStorage[key] = value;
}
export const localStorageLength = () => {
  return window.localStorage.length;
}