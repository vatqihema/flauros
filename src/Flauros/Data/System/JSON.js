export const dateToISOString = date => {
  return date.toISOString();
}
export const parseJSDate = iso => {
  return new Date(iso);
}
