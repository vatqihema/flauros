module Flauros.Log where

import Prelude

import Data.Argonaut (class DecodeJson, class EncodeJson)
import Data.Argonaut.Decode.Generic (genericDecodeJson)
import Data.Argonaut.Encode.Generic (genericEncodeJson)
import Data.Generic.Rep (class Generic)
import Data.Show.Generic (genericShow)
import Flauros.Data.System.JSON (class FromJSON, class ToJSON)

foreign import formatJSONStringify :: forall a. a -> String

data LogType = Error | Warning | Info
derive instance eqLogType      :: Eq LogType
derive instance genericLogtype :: Generic LogType _
derive instance ordLogType     :: Ord LogType
instance showLogType :: Show LogType where
  show = genericShow

data LogLevel = Prod | Dev
derive instance eqLogLevel      :: Eq LogLevel
derive instance genericLogLevel :: Generic LogLevel _
derive instance ordLogLevel     :: Ord LogLevel
instance decodeJsonLogLevel :: DecodeJson LogLevel where decodeJson = genericDecodeJson
instance encodeJsonLogLevel :: EncodeJson LogLevel where encodeJson = genericEncodeJson
instance fromJSONLogLevel   :: FromJSON LogLevel   where fromJSON = genericDecodeJson
instance toJSONLogLevel     :: ToJSON LogLevel     where toJSON = genericEncodeJson
instance showLogLevel :: Show LogLevel where
  show = genericShow