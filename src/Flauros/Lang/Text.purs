module Flauros.Lang.Text where

import Prelude

import CSS.Color (Color)
import CSS.FontStyle (FontStyle(..))
import Data.Foldable (foldl)
import Data.Generic.Rep (class Generic)
import Data.HashSet (HashSet)
import Data.HashSet as HS
import Data.List (List(..), (:))
import Data.List as List
import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype, unwrap)
import Data.Show.Generic (genericShow)
import Flauros.Prelude (class Default, class Transmute, transmute)
import Flauros.Style.Color (Colors)

data IconStyle = Outlined | Rounded
derive instance eqIconStyle      :: Eq IconStyle
derive instance genericIconStyle :: Generic IconStyle _
derive instance ordIconStyle     :: Ord IconStyle
instance showIconStyle :: Show IconStyle where show = genericShow

data Mirror = MirrorH | MirrorV

data TextPart
  = TStr        String
  | TBr
  | TBackground (Colors -> Color)
  | TClasses    (Array String)
  | TClear
  | TColor      (Colors -> Color)
  | TIcon       IconStyle String
  | TFont       String
  | TFontStyle  FontStyle
  | TFontWeight Number
  | TMeta       String
  | TMirror     Mirror
  | TSortPriority Int
  | TSVG String Number
  | TSVGPath String Number Number
  | TTooltip    Text String

newtype Text = Text (List TextPart)
instance defaultTextPart :: Default TextPart where
  default = TStr ""

derive instance newtypeText :: Newtype Text _
instance defaultText :: Default Text where
  default = mempty
instance monoidText :: Monoid Text where
  mempty = Text Nil
instance semigroupText :: Semigroup Text where
  append (Text a) (Text b) = Text $ a <> b

instance transmuteTextText :: Transmute Text Text where
  transmute = identity
instance transmuteStringText :: Transmute String Text where
  transmute "" = Text mempty
  transmute s  = Text $ pure $ TStr s
instance transmuteTextPartText :: Transmute TextPart Text where
  transmute = Text <<< pure

appendAsText :: forall t1 t2. Transmute t1 Text => Transmute t2 Text => t1 -> t2 -> Text
appendAsText a b = transmute a <> transmute b
infixr 5 appendAsText as &

textPriority :: Text -> Int
textPriority (Text t) = case List.head t of
  Just (TSortPriority i) -> i
  _                      -> 0

compareText :: Text -> Text -> Ordering
compareText a b =
  let priorA = textPriority a
      priorB = textPriority b
   in if priorA == priorB
      then plainify a `compare` plainify b
      else compare priorA priorB

isEmptyText :: Text -> Boolean
isEmptyText (Text Nil) = true
isEmptyText _          = false

---------------
-- Accessors --
---------------

getMeta :: Text -> HashSet String
getMeta = HS.fromFoldable <<< List.catMaybes <<< map m <<< unwrap
  where m (TMeta s) = Just s
        m _         = Nothing

--------------
-- Builders --
--------------

plain :: String -> Text
plain "" = Text Nil
plain s  = Text $ TStr s : Nil

background :: (Colors -> Color) -> TextPart
background = TBackground

clear :: TextPart
clear = TClear

color :: (Colors -> Color) -> TextPart
color = TColor

fontWeight :: Number -> TextPart
fontWeight = TFontWeight

italics :: TextPart
italics = TFontStyle Italic

hMirror :: TextPart
hMirror = TMirror MirrorH

vMirror :: TextPart
vMirror = TMirror MirrorV

whenText :: Boolean -> (Unit -> Text) -> Text
whenText pred f = if pred then f unit else Text Nil

mapText :: (String -> String) -> Text -> Text
mapText f = Text <<< map mElem <<< unwrap
  where mElem = case _ of
          TStr s       -> TStr $ f s
          t            -> t

plainify :: Text -> String
plainify = foldl (\a t -> a <> mkPlain t) "" <<< unwrap
  where mkPlain = case _ of
          TBr          -> "\n"
          TStr s       -> s
          TTooltip t _ -> plainify t
          _            -> ""

outlinedIcon :: String -> TextPart
outlinedIcon = TIcon Outlined

roundedIcon :: String -> TextPart
roundedIcon = TIcon Rounded

tclasses :: Array String -> TextPart
tclasses = TClasses

tfont :: String -> TextPart
tfont fontName = TFont fontName

tmeta :: String -> TextPart
tmeta = TMeta

tsvg :: String -> Number -> TextPart
tsvg = TSVG

tsvgPath :: String -> Number -> Number -> TextPart
tsvgPath = TSVGPath

tooltip :: Text -> String -> TextPart
tooltip = TTooltip