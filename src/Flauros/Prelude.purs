module Flauros.Prelude where

import Prelude

import CSS.Color as CSS
import Data.Array ((!!), (..))
import Data.Array as Array
import Data.Either (Either(..))
import Data.Foldable (class Foldable, all, foldl, foldr)
import Data.FoldableWithIndex (class FoldableWithIndex, foldlWithIndex)
import Data.FunctorWithIndex (class FunctorWithIndex, mapWithIndex)
import Data.Generic.Rep (class Generic)
import Data.HashMap (HashMap)
import Data.HashMap as HM
import Data.HashSet (HashSet)
import Data.HashSet as HS
import Data.Hashable (class Hashable, hash)
import Data.Int (toNumber)
import Data.List (List(..))
import Data.List as List
import Data.Map (Map)
import Data.Map as Map
import Data.Maybe (Maybe(..), maybe)
import Data.Newtype (class Newtype)
import Data.Number (abs, cos, sin, sqrt)
import Data.Set (Set)
import Data.Set as Set
import Data.Show.Generic (genericShow)
import Data.String as String
import Data.String.CodeUnits as CodeUnits
import Data.Symbol (class IsSymbol)
import Data.Tuple (Tuple)
import Halogen.Svg.Attributes as SVG
import Prim.Row (class Nub, class Union)
import Prim.Row as Row
import Prim.RowList (class RowToList, RowList)
import Prim.RowList as RL
import Record as Record
import Record.Builder (Builder)
import Record.Builder as Builder
import Type.Prelude (class ListToRow, Proxy(..))

foreign import isPrefixOf :: String -> String -> Boolean

-- | An object identifier defining the base record, plus all modifiers it can have
type Identifier =
  { base :: String
  , mods :: HashSet String
  , meta :: HashMap String String -- maybe include { "unique": "nth" } to tell duplicates apart if need be?
  }

defaultID :: String -> Identifier
defaultID cat = sid (cat <> "/~🌺~d̤̮ĕ̈f̤̮ă̈ṳ̮l̆̈t̤̮~🌻~") -- will probably regret -- want to keep Identifier a row type, but don't want to conflict with anything

-- | Creates a basic ID from the given string, with no mods or meta
sid :: String -> Identifier
sid s = { base: s, meta: HM.empty, mods: HS.empty }

-- | Creates an Identifier of the form `scope/elemID` with no mods or meta
scoped :: Identifier -> String -> Identifier
scoped scope name = scope { base = scope.base <> "/" <> name }

modID :: String -> Identifier
modID = scoped $ sid "module"

withMod :: Identifier -> String -> Identifier
withMod id k = id { mods = HS.insert k id.mods }

isBase :: Identifier -> Boolean
isBase { mods, meta } = HS.isEmpty mods && HM.isEmpty meta

prettyShowID :: Identifier -> String
prettyShowID id = show id.base
               <> (if HS.isEmpty id.mods then "" else " [" <> String.joinWith ", " (Array.fromFoldable id.mods) <> "]")
               <> (if HM.isEmpty id.meta then "" else " {" <> String.joinWith ", " (HM.toArrayBy (\k v -> show k <> ": " <> show v) id.meta) <> "}")

data Handle :: forall k. k -> Type
data Handle a = Handle Int Identifier
derive instance eqHandle :: Eq (Handle a)
derive instance generictypeHandle :: Generic (Handle a) _
instance showHandle :: Show (Handle a) where show = genericShow

instance hashableHandle :: Hashable (Handle a) where
  hash (Handle n id) = n + hash id

-- | A newtype wrapper around Identifiers for typeclass, params, etc
newtype MyID = MyID Identifier
derive instance eqMyID :: Eq MyID
derive instance generictypeMyID :: Generic MyID _
derive instance newtypeMyID :: Newtype MyID _
derive newtype instance hashableMyID :: Hashable MyID
instance showMyID :: Show MyID where show = genericShow

type Tag = String

type Tagged r =
  ( tags :: HashSet Tag
  | r
  )

type Identified r =
  ( id :: Identifier
  | Tagged r
  )

withDefTags :: forall rin rout1 rout2. Union rin (Tagged ()) rout1 => Nub rout1 rout2
                => {| rin }
                -> {| rout2 }
withDefTags r = Record.merge r { tags: HS.empty :: HashSet Tag }

-- | Type alias for Maps with Identifiers as keys
type IDMap = HashMap Identifier

-- | Type alias for Maps with a Number weight as the value, for use when choosing weighted values
type WeightedMap k = HashMap k Number

-- | Synonym for Map.fromFoldable that is shorter and easier to read
asMap :: forall f k v. Foldable f => Ord k => f (Tuple k v) -> Map k v
asMap = Map.fromFoldable

asHashMap :: forall f k v. Foldable f => Hashable k => f (Tuple k v) -> HashMap k v
asHashMap = HM.fromFoldable

asHashSet :: forall f a. Foldable f => Hashable a => f a -> HashSet a
asHashSet = HS.fromFoldable

asList :: forall f a. Foldable f => f a -> List a
asList = List.fromFoldable

asSet :: forall f a. Foldable f => Ord a => f a -> Set a
asSet = Set.fromFoldable

mapLeft :: forall a b r. (a -> b) -> Either a r -> Either b r
mapLeft f = case _ of
  Left l  -> Left $ f l
  Right r -> Right r

frequenciesOf :: forall f k a. Foldable f => Ord k => (a -> k) -> f a -> Map k Int
frequenciesOf keyOf = foldr alter Map.empty
  where alter row = Map.alter (Just <<< maybe 1 (_ + 1)) (keyOf row)

updateWithDef :: forall k v. Ord k => (v -> v) -> k -> v -> Map k v -> Map k v
updateWithDef f k def = flip Map.alter k case _ of
  Just v  -> Just $ f v
  Nothing -> Just $ f def

hmUpdateWithDef :: forall k v. Hashable k => (v -> v) -> k -> v -> HashMap k v -> HashMap k v
hmUpdateWithDef f k def = flip HM.alter k case _ of
  Just v  -> Just $ f v
  Nothing -> Just $ f def

insertIn :: forall k1 k2 v. Hashable k1 => Hashable k2 => k1 -> k2 -> v -> HashMap k1 (HashMap k2 v) -> HashMap k1 (HashMap k2 v)
insertIn k1 k2 v hm = flip2 HM.alter k1 hm case _ of
  Just im -> Just $ HM.insert k2 v im
  Nothing -> Just $ HM.singleton k2 v
 
updateIn :: forall k1 k2 v. Hashable k1 => Hashable k2 => (v -> Maybe v) -> k1 -> k2 -> HashMap k1 (HashMap k2 v) -> HashMap k1 (HashMap k2 v)
updateIn f k1 k2 = HM.update (Just <<< HM.update f k2) k1 

padBefore :: Int -> Char -> String -> String
padBefore n c str
  | String.length str < n = Array.foldl (\acc _ -> CodeUnits.singleton c <> acc) str (0 .. (n - String.length str - 1))
  | otherwise             = str

isSubsetOf :: forall a. Hashable a => HashSet a -> HashSet a -> Boolean
isSubsetOf sub big = all (_ `HS.member` big) sub

flip2 :: forall a b c d. (a -> b -> c -> d) -> b -> c -> a -> d
flip2 f b c a = f a b c

nthByMod :: forall a. Int -> Array a -> Maybe a
nthByMod num array
  | Array.null array = Nothing
  | otherwise        = array !! (num `mod` Array.length array)

groupOn :: forall a f k. Foldable f => Hashable k => (a -> k) -> f a -> HashMap k (Array a)
groupOn f = foldl (\acc v -> HM.alter (alt v) (f v) acc) HM.empty
  where alt v = case _ of
          Just a  -> Just $ Array.snoc a v
          Nothing -> Just [ v ]

mapWithPrefixedIndex :: forall f a b i. Show i => FunctorWithIndex i f => String -> (String -> a -> b) -> f a -> f b
mapWithPrefixedIndex prefix f = mapWithIndex (\i -> f (prefix <> show i))

occurrences :: forall f a. Foldable f => (a -> Boolean) -> f a -> Int
occurrences cond = foldl (\acc v -> if cond v then acc + 1 else acc) 0

countKeys :: forall f k v. FoldableWithIndex k f => (k -> Boolean) -> f v -> Int
countKeys cond = foldlWithIndex (\k acc _ -> if cond k then acc + 1 else acc) 0

justWhen :: forall a. Boolean -> a -> Maybe a
justWhen true a = Just a
justWhen _    _ = Nothing

type Vec2D = { angle :: Number, mag :: Number }

distance :: Vec2D -> Vec2D -> Number
distance a b =
  let dx = abs $ cos a.angle * a.mag - cos b.angle * b.mag
      dy = abs $ sin a.angle * a.mag - sin b.angle * b.mag
   in sqrt (dx * dx + dy * dy)

-------------
-- Default --
-------------

class Default a where
  default :: a

instance defaultArray   :: Default (Array a)     where default = []
instance defaultHashMap :: Default (HashMap k v) where default = HM.empty
instance defaultHashSet :: Default (HashSet a)   where default = HS.empty
instance defaultInt     :: Default Int           where default = 0
instance defaultList    :: Default (List a)      where default = Nil
instance defaultMap     :: Default (Map k v)     where default = Map.empty
instance defaultMaybe   :: Default (Maybe a)     where default = Nothing
instance defaultNumber  :: Default Number        where default = 0.0
instance defaultSet     :: Default (Set a)       where default = Set.empty
instance defaultString  :: Default String        where default = ""

class OverrideDefault :: forall k. k -> Type -> Constraint
class OverrideDefault key a where
  overridden :: forall proxy. proxy key -> a

newtype Override :: forall k. k -> Type -> Type
newtype Override key a = Override a 
derive instance newtypeOverride :: Newtype (Override key a) _

insertDefault :: forall l a r1 r2. Default a => Row.Cons l a r1 r2 => Row.Lacks l r1 => IsSymbol l => Proxy l -> Builder (Record r1) (Record r2)
insertDefault pxy = Builder.insert pxy (default :: a)

class GDefault (r :: Row Type) (l :: RowList Type) | l -> r where
  gDefault :: forall proxy. proxy l -> {| r }

instance gDefaultNil :: GDefault () RL.Nil where
  gDefault _ = {}

instance gDefaultConsOverride ::
  ( OverrideDefault key a
  , GDefault rowTail listTail
  , IsSymbol s
  , Row.Cons s (Override key a) rowTail row
  , Row.Lacks s rowTail
  ) => GDefault row (RL.Cons s (Override key a) listTail) where
  -- TODO new implementation that uses Record.Builder for better performance
  gDefault _ = Record.insert (Proxy :: Proxy s) (Override (overridden (Proxy :: Proxy key) :: a)) ((gDefault (Proxy :: Proxy listTail)))

else instance gDefaultCons ::
  ( Default a
  , GDefault rowTail listTail
  , IsSymbol s
  , Row.Cons s a rowTail row
  , Row.Lacks s rowTail
  ) => GDefault row (RL.Cons s a listTail) where
  -- TODO new implementation that uses Record.Builder for better performance
  gDefault _ = Record.insert (Proxy :: Proxy s) (default :: a) ((gDefault (Proxy :: Proxy listTail)))

instance (GDefault r l, RowToList r l) => Default {| r } where
  default = gDefault (Proxy :: Proxy l)

class Monad m <= DefaultM a m where
  defaultM :: m a

---------------
-- Transmute --
---------------

-- | Transmute provides a lossless way to convert from one type to another
class Transmute a b where
  transmute :: a -> b

-- instance transmuteIdentity :: Transmute a a where
--   transmute = identity

instance transmuteMaybe :: Transmute a (Maybe a) where
  transmute = Just
  
instance transmuteArrayList :: Transmute (Array a) (List a) where
  transmute = List.fromFoldable
instance transmuteIntNumber :: Transmute Int Number where
  transmute = toNumber
instance transmuteListArray :: Transmute (List a) (Array a) where
  transmute = Array.fromFoldable

instance Transmute (CSS.Color) (SVG.Color) where
  transmute c =
    let { r, g, b, a} = CSS.toRGBA c
     in SVG.RGBA r g b a

---------------
-- Operators --
---------------

pipeLeft :: forall a b. (a -> b) -> a -> b
pipeLeft f a = f a

infixr 0 pipeLeft as <|

pipeRight :: forall a b. a -> (a -> b) -> b
pipeRight a f = f a

infixl 0 pipeRight as |>

class MapElem m k where
  mapElem      :: forall v.              (v -> v) -> k -> m k v -> m k v
  mapElemOrDef :: forall v. Default v => (v -> v) -> k -> m k v -> m k v

instance mapElemMap :: Ord k => MapElem Map k where
  mapElem f k = Map.alter (map f) k
  mapElemOrDef f k = Map.alter (case _ of
    Nothing -> Just $ f default
    Just v  -> Just $ f v) k

instance mapElemHashMap :: Hashable k => MapElem HashMap k where
  mapElem f k = HM.alter (map f) k
  mapElemOrDef f k = HM.alter (case _ of
    Nothing -> Just $ f default
    Just v  -> Just $ f v) k

------------
-- Record --
------------

class Maybeify (i :: Row Type) (o :: Row Type) | i -> o

class MaybeifyList :: forall i o. i -> o -> Constraint
class MaybeifyList i o | i -> o

instance nilMaybeifyList :: MaybeifyList RL.Nil RL.Nil
instance consMaybeifyList :: (IsSymbol name, MaybeifyList tail tailOut) => MaybeifyList (RL.Cons name a tail) (RL.Cons name (Maybe a) tailOut)

instance maybeifyImpl ::
  ( RowToList i li
  , ListToRow lo o
  , MaybeifyList li lo
  ) => Maybeify i o
