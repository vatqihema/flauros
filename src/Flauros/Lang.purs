module Flauros.Lang where

import Prelude

import CSS (Color)
import Data.Argonaut (class DecodeJson, class EncodeJson)
import Data.Array as Array
import Data.Date (Date)
import Data.DateTime (DateTime)
import Data.Foldable (class Foldable)
import Data.Generic.Rep (class Generic)
import Data.HashMap (HashMap)
import Data.HashMap as HM
import Data.HashSet as HS
import Data.Hashable (class Hashable)
import Data.Map (Map)
import Data.Map as Map
import Data.Maybe (Maybe(..), fromMaybe, maybe)
import Data.Newtype (class Newtype)
import Data.Show.Generic (genericShow)
import Data.Tuple (Tuple)
import Effect.Class (class MonadEffect)
import Flauros.Data.Stat (StatState)
import Flauros.Data.System.JSON (class FromJSON, class ToJSON)
import Flauros.Lang.Text (Text, TextPart, (&))
import Flauros.Lang.Text as Text
import Flauros.Prelude (Identifier, MyID(..), countKeys, hmUpdateWithDef, transmute)
import Flauros.Style.Color (Colors, cyan, red)
import Halogen.Store.Monad (class MonadStore, getStore, updateStore)
import Prim.Row (class Nub, class Union)
import Record as Record

type Name =
  { format :: String
  , names  :: Array String
  }

data LineParam
  = PBool Boolean
  | PDate Date
  | PDateTime DateTime
  | PInt  Int
  | PName Name
  | PNull
  | PNum  Number
  | PStat StatState
  | PStr  String
  | PID   Identifier
  | PMap  (HashMap String LineParam)
  
derive instance eqLineParam :: Eq LineParam
derive instance genericLineParam :: Generic LineParam _
instance showLineParam :: Show LineParam where show a = genericShow a

newtype Lang = Lang String
derive instance eqLang :: Eq Lang
derive instance newtypeLang :: Newtype Lang _
derive instance ordLang :: Ord Lang
derive newtype instance decodeJsonLang :: DecodeJson Lang
derive newtype instance encodeJsonLang :: EncodeJson Lang
derive newtype instance fromJSONLang   :: FromJSON Lang
derive newtype instance toJSONLang     :: ToJSON Lang
derive newtype instance hashableLang   :: Hashable Lang

newtype Line store = Line (store -> PreviousResult -> Identifier -> Array LineParam -> Text)

type LangInfo store r =
  ( lines :: HashMap Lang (HashMap String (Line store))
  | r
  )

withDefLangInfo :: forall s rin rout. Union rin (LangInfo s ()) rout => Nub rout rout
                => {| rin }
                -> {| rout }
withDefLangInfo r = Record.merge r { lines: HM.empty :: HashMap Lang (HashMap String (Line s))}

newtype AllLines s = AllLines (HashMap Lang (HashMap String (HashMap String (Line s))))

emptyLines :: forall s. AllLines s
emptyLines = AllLines HM.empty

-----------
-- Lines --
-----------

class LangState s where
  allLines    :: s -> AllLines s
  mapAllLines :: (AllLines s -> AllLines s) -> s -> s

class Lineable s a where
  intoText  :: a -> s -> PreviousResult -> Identifier -> Array LineParam -> Text

instance lineableString :: Lineable s String where
  intoText s = let t = transmute s in \_ _ _ _ -> t

instance lineableText :: Lineable s Text where
  intoText t _ _ _ _ = t

instance lineableTextPart :: Lineable s TextPart where
  intoText t _ _ _ _ = transmute t

instance lineableMyID :: Lineable s a => Lineable s (MyID -> a) where
  intoText f store prev id mp = intoText (f $ MyID id) store prev id mp
else
instance lineableArrayLineParams :: Lineable s a => Lineable s (Array LineParam -> a) where
  intoText f store prev id mp = intoText (f mp) store prev id mp
else
instance lineablePreviousResult :: Lineable s a => Lineable s (PreviousResult -> a) where
  intoText f store prev id mp = intoText (f prev) store prev id mp
else
instance lineableTranslator :: (LangState s, Lineable s a) => Lineable s (Translator -> a) where
  intoText f store prev id mp = intoText (f $ Translator (translate (translateContext store english))) store prev id mp
else
instance lineableTranslatorWith :: (LangState s, Lineable s a) => Lineable s (TranslatorWith -> a) where
  intoText f store prev id mp = intoText (f $ TranslatorWith (translateWith (translateContext store english))) store prev id mp
else
instance lineableStore :: Lineable s a => Lineable s (s -> a) where
  intoText f store prev id mp = intoText (f store) store prev id mp

line :: forall a s. Lineable s a => a -> Line s
line = Line <<< intoText

type Translate = Identifier -> String -> Text
type TranslateOr = Identifier -> String -> Text -> Text
type TranslateWith = Identifier -> String -> Array LineParam -> Text
type TranslateWithOr = Identifier -> String -> Text -> Array LineParam -> Text
type TranslateAll = Identifier -> Map String Text
type TranslateAllWith = Identifier -> Array LineParam -> Map String Text

newtype Translator = Translator Translate
newtype TranslatorWith = TranslatorWith TranslateWith

data TranslateContext s = TranslateContext
  { store :: s
  , lang  :: Lang
  }

translateContext :: forall s. s -> Lang -> TranslateContext s
translateContext store lang = TranslateContext { store, lang }

translateContextM :: forall a m s. MonadStore a s m => Lang -> m (TranslateContext s)
translateContextM lang = getStore <#> \store -> TranslateContext { store, lang }

elemPStr :: String -> Array LineParam -> Boolean
elemPStr s = Array.elem (PStr s)

paramFail :: String -> Array LineParam -> Text
paramFail name params = Text.color red & name & ": fail match " & Text.color cyan & "`" <> show params <> "`"

newtype PreviousResult = PreviousResult Text

--------------
-- Stateful --
--------------

setInAllLines :: forall s. Lang -> Identifier -> String -> Line s -> AllLines s -> AllLines s
setInAllLines lang elemID key l (AllLines lines) =
  let ulines = hmUpdateWithDef ulang lang        HM.empty
      ulang  = hmUpdateWithDef uelem elemID.base HM.empty
      uelem  = HM.insert key l
   in AllLines $ ulines lines

setTranslation :: forall m s. LangState s => MonadEffect m => MonadStore (s -> s) s m => Lang -> Identifier -> String -> Line s -> m Unit
setTranslation lang elemID key l = updateStore $ mapAllLines (setInAllLines lang elemID key l)

-- Key

translateOr :: forall s. LangState s => TranslateContext s -> Identifier -> String -> Text -> Text
translateOr t elemID key orElse = translateWithOr t elemID key orElse []

translate :: forall s. LangState s => TranslateContext s -> Identifier -> String -> Text
translate t elemID key = translateWithOr t elemID key mempty []

translateWithOr :: forall s. LangState s => TranslateContext s -> Identifier -> String -> Text -> Array LineParam -> Text
translateWithOr (TranslateContext { store, lang }) elemID key orElse params =
  let AllLines lines = allLines store
      mLang   = HM.lookup lang lines
      myLine  = mLang >>= HM.lookup elemID.base >>= HM.lookup key
      baseStr = myLine <#> \(Line f) -> f store (PreviousResult mempty) elemID params 
      applyMods base =
        let app s modID = case mLang >>= HM.lookup modID >>= HM.lookup key of
              Nothing       -> s
              Just (Line f) -> f store (PreviousResult s) elemID params 
         in Array.foldl app base $ Array.sort $ HS.toArray elemID.mods
   in (maybe orElse applyMods baseStr)

translateWith :: forall s. LangState s => TranslateContext s -> Identifier -> String -> Array LineParam -> Text
translateWith t elemID key = translateWithOr t elemID key mempty

-- All

translateAll :: forall s. LangState s => TranslateContext s -> Identifier -> Map String Text
translateAll ctx elemID = translateAllWith ctx elemID mempty

translateAllWith :: forall s. LangState s => TranslateContext s -> Identifier -> Array LineParam -> Map String Text
translateAllWith (TranslateContext { store, lang }) elemID params =
  let AllLines lines = allLines store
      lineByKs = fromMaybe HM.empty (HM.lookup lang lines >>= HM.lookup elemID.base)
   in Map.fromFoldableWithIndex $ lineByKs <#> \(Line f) -> f store (PreviousResult mempty) elemID params

countLines :: forall s. LangState s => TranslateContext s -> (String -> Boolean) -> Identifier -> Int
countLines (TranslateContext { store, lang }) f elemID =
  let AllLines lines = allLines store
      lineByKs = fromMaybe HM.empty (HM.lookup lang lines >>= HM.lookup elemID.base)
   in countKeys f lineByKs

-----------
-- Utils --
-----------

asLines :: forall f s. Foldable f => Functor f => f (Tuple Lang (f (Tuple String (Line s)))) -> HashMap Lang (HashMap String (Line s))
asLines = map (map HM.fromFoldable) >>> HM.fromFoldable


-----------
-- Langs --
-----------
-- Only core langs, these are just QoL as Langs are easily creatable

english :: Lang
english = Lang "english"

esperanto :: Lang
esperanto = Lang "esperanto"

-- Utils

-- | Se akuzativa kaj/aŭ plurala, ĝi aldonos "n", "j", aŭ "jn"
sufikso :: Array LineParam -> String
sufikso ps = (if elemPStr "plu" ps then "j" else "")
          <> (if elemPStr "aku" ps then "n" else "")

colorPrevious :: forall s. (Colors -> Color) -> Line s
colorPrevious col = line \(PreviousResult r) -> Text.color col & r