module Flauros.Style.Typography where

import Prelude

import CSS (CSS, fontSize, fontWeight, inlineBlock, lineHeight, rem, weight)
import CSS as CSS
import CSS.Size (unitless)
import CSS.Transform (scale)

-----------
-- Fonts --
-----------

abrilFatface :: CSS
abrilFatface = CSS.fontFaceFamily "Abril Fatface"

caveat :: CSS
caveat = CSS.fontFaceFamily "Caveat"

comicNeue :: CSS
comicNeue = CSS.fontFaceFamily "Comic Neue"

lori :: CSS
lori = CSS.fontFaceFamily "Lori"

merriWeather :: CSS
merriWeather = CSS.fontFaceFamily "Merriweather"

openDyslexic :: CSS
openDyslexic = CSS.fontFaceFamily "OpenDyslexic"

robotoFlex :: CSS
robotoFlex = CSS.fontFaceFamily "Roboto Flex"

syneTactile :: CSS
syneTactile = CSS.fontFaceFamily "Syne Tactile"

ubuntuMono :: CSS
ubuntuMono = CSS.fontFaceFamily "Ubuntu Mono"

vt323 :: CSS
vt323 = CSS.fontFaceFamily "VT323"

---------------------
-- Specific Styles --
---------------------

apartmentBody :: CSS
apartmentBody = do
  lori
  fontSize $ rem 1.0

apartmentHeader :: CSS
apartmentHeader = do
  merriWeather
  fontSize $ rem 1.1

calendarBody :: CSS
calendarBody = do
  lori
  fontSize $ rem 1.2
  fontWeight $ weight 400.0
  lineHeight $ unitless 1.43

calendarBadge :: CSS
calendarBadge = do
  lori
  fontSize $ rem 0.75
  lineHeight $ unitless 0.75

calendarHeader :: CSS
calendarHeader = do
  abrilFatface
  fontSize $ rem 1.2
  fontWeight $ weight 400.0
  lineHeight $ unitless 1.43

cityName :: CSS
cityName = do
  lori
  fontSize $ rem 3.0
  fontWeight $ weight 400.0
  lineHeight $ unitless 1.16

handwriting :: CSS
handwriting = do
  caveat
  fontSize $ rem 1.2
  fontWeight $ weight 400.0
  lineHeight $ unitless 1.2

monoHeader :: CSS
monoHeader = do
  ubuntuMono
  fontSize $ rem 2.0
  lineHeight $ unitless 1.1

monoBody :: CSS
monoBody = do
  ubuntuMono
  fontSize $ rem 1.0

retro :: CSS
retro = do
  vt323
  fontSize $ rem 1.0
  lineHeight $ unitless 0.8

seerbookBody :: CSS
seerbookBody = do
  robotoFlex
  fontSize $ rem 1.0

seerbookHeader :: CSS
seerbookHeader = do
  robotoFlex
  fontSize $ rem 1.5
  lineHeight $ unitless 1.0

seerbookStatus :: CSS
seerbookStatus = do
  robotoFlex
  fontSize $ rem 1.0
  lineHeight $ unitless 1.1

theNews :: CSS
theNews = do
  abrilFatface
  fontSize $ rem 1.7
  lineHeight $ unitless 1.75

weatherReport :: CSS
weatherReport = do
  lori
  fontSize $ rem 1.5
  lineHeight $ unitless 1.1334

worldPanelHeader :: CSS
worldPanelHeader = do
  caveat
  fontSize $ rem 1.2
  fontWeight $ weight 400.0
  lineHeight $ unitless 1.43

------------------
-- Misc Effects --
------------------

mirrorH :: CSS
mirrorH = do
  CSS.display inlineBlock
  CSS.transform $ scale (-1.0) 1.0

mirrorV :: CSS
mirrorV = do
  CSS.display inlineBlock
  CSS.transform $ scale 1.0 (-1.0)