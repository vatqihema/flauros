module Flauros.Style.Color where

import Prelude

import CSS (rgba, toRGBA)
import CSS.Color (Color, rgb)
import Data.Maybe (fromMaybe)
import Data.Newtype (class Newtype)
import Effect.Class (class MonadEffect, liftEffect)
import Flauros.Random (class Random, random, randomNth)


-- I like these names for these colors and if you tell me they're wrong I'll fight you

black      :: Colors -> Color
black      = _.black
blue       :: Colors -> Color
blue       = _.blue
clay       :: Colors -> Color
clay       = _.clay
cornflower :: Colors -> Color
cornflower = _.cornflower
cyan       :: Colors -> Color
cyan cols  = cols.cyan
gold       :: Colors -> Color
gold       = _.gold
grass      :: Colors -> Color
grass      = _.grass
green      :: Colors -> Color
green      = _.green
lavender   :: Colors -> Color
lavender   = _.lavender
lilac      :: Colors -> Color
lilac      = _.lilac
lime       :: Colors -> Color
lime       = _.lime
orange     :: Colors -> Color
orange     = _.orange
navy       :: Colors -> Color
navy       = _.navy
pink       :: Colors -> Color
pink       = _.pink
purple     :: Colors -> Color
purple     = _.purple
red        :: Colors -> Color
red        = _.red
redder     :: Colors -> Color
redder     = _.redder
sand       :: Colors -> Color
sand       = _.sand
voidish    :: Colors -> Color
voidish    = _.voidish
voider     :: Colors -> Color
voider     = _.voider
voidest    :: Colors -> Color
voidest    = _.voidest
white      :: Colors -> Color
white      = _.white
yellow     :: Colors -> Color
yellow     = _.yellow
transparent :: Colors -> Color
transparent _ = rgba 0 0 0 0.0

type Colors =
  { black      :: Color
  , blue       :: Color
  , clay       :: Color
  , cornflower :: Color
  , cyan       :: Color
  , gold       :: Color
  , grass      :: Color
  , green      :: Color
  , lavender   :: Color
  , lilac      :: Color
  , lime       :: Color
  , navy       :: Color
  , orange     :: Color
  , pink       :: Color
  , purple     :: Color
  , red        :: Color
  , redder     :: Color
  , sand       :: Color
  , voidish    :: Color
  , voider     :: Color
  , voidest    :: Color
  , white      :: Color
  , yellow     :: Color
  }

defaultColors :: Colors
defaultColors =
  { black:      rgb 0x28 0x28 0x2e -- #28282e
  , blue:       rgb 0xb0 0xa9 0xe4 -- #b0a9e4
  , clay:       rgb 0xde 0xa3 0x8b -- #dea38b
  , cornflower: rgb 0xac 0xcc 0xe4 -- #accce4
  , cyan:       rgb 0xb3 0xe3 0xe2 -- #b3e3e2
  , gold:       rgb 0xe3 0xe1 0x61 -- #e3e161
  , grass:      rgb 0x87 0xa8 0x89 -- #87a889
  , green:      rgb 0xb0 0xeb 0x93 -- #b0eb93
  , lavender:   rgb 0x6c 0x56 0x71 -- #6c5671
  , lilac:      rgb 0xf0 0xab 0xf0 -- #f0abf0
  , lime:       rgb 0xe9 0xf5 0x9d -- #e9f59d
  , navy:       rgb 0x42 0x39 0x6f -- #42396f
  , orange:     rgb 0xfb 0xbb 0x78 -- #fbbb78
  , pink:       rgb 0xff 0xb3 0xce -- #ffb2ce
  , purple:     rgb 0x87 0x55 0xa3 -- #8755a3
  , red:        rgb 0xff 0x82 0x84 -- #ff8284
  , redder:     rgb 0xf6 0x45 0x60 -- #f64560
  , sand:       rgb 0xff 0xe6 0xc6 -- #ffe6c6
  , voidish:    rgb 0x1d 0x1d 0x22 -- #1d1d22
  , voider:     rgb 0x15 0x15 0x1a -- #15151a
  , voidest:    rgb 0x10 0x0f 0x14 -- #100f14
  , white:      rgb 0xff 0xf7 0xe4 -- #fff7e4
  , yellow:     rgb 0xff 0xf7 0xa0 -- #fff7a0
  }

withAlpha :: Number -> Color -> Color
withAlpha a c = let { r, g, b } = toRGBA c in rgba r g b a

newtype RandomColor = RandomColor (Colors -> Color)

derive instance newtypeRandomColor :: Newtype RandomColor _

randomColor :: forall m. MonadEffect m => m (Colors -> Color)
randomColor = random <#> \(RandomColor c) -> c

instance randomRandomColor :: MonadEffect m => Random m RandomColor where
  random = liftEffect $ map (RandomColor <<< fromMaybe black) $ randomNth allBaseColors

newtype Highlight = Highlight (Colors -> Color)

instance randomRandomHighlight :: MonadEffect m => Random m Highlight where
  random = liftEffect $ map (Highlight <<< fromMaybe yellow) $ randomNth allHighlightColors

randomHighlight :: forall m. MonadEffect m => m (Colors -> Color)
randomHighlight = random <#> \(Highlight c) -> c

-- Collections

allBaseColors :: Array (Colors -> Color)
allBaseColors =
  [ black
  , blue
  , clay
  , cornflower
  , cyan
  , grass
  , green
  , lavender
  , lilac
  , lime
  , navy
  , orange
  , pink
  , purple
  , red
  , redder
  , sand
  , voidish
  , voider
  , voidest
  , white
  , yellow
  ]

allHighlightColors :: Array (Colors -> Color)
allHighlightColors =
  [ blue
  , clay
  , cornflower
  , cyan
  , grass
  , green
  , lilac
  , lime
  , orange
  , pink
  , purple
  , red
  , redder
  , sand
  , white
  , yellow
  ]