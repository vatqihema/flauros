module Main where

import Prelude

import Effect (Effect)
import Flauros.Component.Page as Page
import Flauros.Data.Store as DS
import Halogen.Aff as HA
import Halogen.Store.Monad (runStoreT)
import Halogen.VDom.Driver (runUI)

main :: Effect Unit
main = do
  HA.runHalogenAff do
    body <- HA.awaitBody
    page <- runStoreT DS.initialStore DS.reduce Page.page
    runUI page unit body
