# 🔮 Flauros ✨

A web browser fantasy lifesim rpg written in Purescript

## 🗺️ Summary

Magic brought the industrial revolution to generic fantasy land, and now its a modern capitalist hell. Adventuring is no longer a viable job, although you were unlikely to be successful at it anyway.

It's September, and you are moving into a new apartment with a new job.

## ⚗️ State

It's not at all playable. Sorry.

## 📜 Credits

#### Caro Asercion ([CC BY 3.0](https://creativecommons.org/licenses/by/3.0/))
*Artichoke icon* https://game-icons.net/1x1/caro-asercion/artichoke.html
as Flauros.Component.Util.Icon.artichokeIcon

#### Delapouite: ([CC BY 3.0](https://creativecommons.org/licenses/by/3.0/))
*Perspective dice 6 faces random icon* https://game-icons.net/1x1/delapouite/perspective-dice-six-faces-random.html
as Flauros.Component.Util.Icon.perspectiveDice6FacesRandomIcon

*Rule book icon* https://game-icons.net/1x1/delapouite/rule-book.html
as Flauros.Component.Util.Icon.ruleBookIcon

#### Lorc: ([CC BY 3.0](https://creativecommons.org/licenses/by/3.0/))
*Laurel crown icon* https://game-icons.net/1x1/lorc/laurel-crown.html
as Flauros.Component.Util.Icon.laurelCrownIcon

*Pentagram rose icon* https://game-icons.net/1x1/lorc/pentagram-rose.html
as Flauros.Component.Util.Icon.pentagramRoseIcon